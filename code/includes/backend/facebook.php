<?php

require_once('Facebook/HttpClients/FacebookHttpable.php');
require_once('Facebook/HttpClients/FacebookCurl.php');
require_once('Facebook/HttpClients/FacebookCurlHttpClient.php');
require_once('Facebook/GraphObject.php');
require_once('Facebook/GraphSessionInfo.php');
require_once('Facebook/Entities/AccessToken.php');
require_once('Facebook/Entities/SignedRequest.php');
require_once('Facebook/FacebookSignedRequestFromInputHelper.php');
require_once('Facebook/FacebookJavaScriptLoginHelper.php');
require_once('Facebook/FacebookSession.php');
require_once('Facebook/FacebookRequest.php');
require_once('Facebook/FacebookResponse.php');
require_once('Facebook/FacebookSDKException.php');
require_once('Facebook/FacebookRequestException.php');
require_once('Facebook/FacebookAuthorizationException.php');
require_once('Facebook/GraphUser.php');

use Facebook\FacebookSession;
use Facebook\FacebookJavaScriptLoginHelper;
use Facebook\FacebookRequest;
use Facebook\FacebookRequestException;
use Facebook\GraphUser;

function fb_get_valid_session($appid, $secret)
{
    FacebookSession::setDefaultApplication($appid, $secret);

    $session = NULL;
    
    // First try from stored token in session
    if (isset($_SESSION['token']))
    {
        $session = new FacebookSession($_SESSION['token']);
        try
        {
            $session->Validate($appid, $secret);
        }
        catch(\Exception $ex)
        {
            // Session is not valid any more, get a new one.
            $session = NULL;
            unset($_SESSION['token']);
        }
    }

    // No stored token in session, or token is invalid,
    // so try getting the token from JavaScript.
    if (!isset($session))
    {
        $helper = new FacebookJavaScriptLoginHelper();

        try 
        {
            $session = $helper->getSession();
            
            if ($session)
                $session->Validate($appid, $secret);
        } catch(\Exception $ex) 
        {
            $session = NULL;
        }

        if ($session)
        {
            // Success! Store in token.
            $_SESSION['token'] = $session->getToken();
        }
    }

    return $session;
}

function fb_get_user_id_from_session($session)
{
    $userId = NULL;
    
    try
    {
        $user_profile = (new FacebookRequest(
            $session, 'GET', '/me', array('fields' => 'id')
        ))->execute()->getGraphObject(GraphUser::className());

        $userId = $user_profile->getId();
    }
    catch(Exception $e)
    {
        $userId = NULL;
    }
    
    return $userId;
}
?>