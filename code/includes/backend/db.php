<?php

function db_connect($host, $db_name, $user, $pass)
{
    try
    {
        // Connect to DB
        $conn = new PDO('mysql:host=' . $host . ';dbname=' . $db_name, $user, $pass);
        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    }
    catch (PDOException $e)
    {
        $conn = NULL;
    }
    
    return $conn;
}

function db_get($conn, $id, $lock_token)
{
    $data = '';
    
    try
    {
        // Create a new lock
        $stmt = $conn->prepare('INSERT INTO locks (facebook_id, lock_token, version) VALUES(:id, :lock_token, -1) ON DUPLICATE KEY UPDATE lock_token=:lock_token, version=-1');
        $stmt->execute(array('id' => $id, 'lock_token' => $lock_token));
        
        // Load
        $stmt = $conn->prepare('SELECT data FROM states WHERE facebook_id = :id LIMIT 1');
        $stmt->execute(array('id' => $id));
        
        $result = $stmt->fetchAll();
        
        if (count($result) == 1)
        {
            // Store the data
            $data = $result[0]['data'];
        }
    }
    catch (PDOException $e)
    {
        $data = NULL;
    }
    
    return $data;
}

function db_post($conn, $id, $data, $lock_token, $version)
{
    $ret = array(
        'success' => false
    );
    
    try
    {
        // Check lock
        $stmt = $conn->prepare('SELECT lock_token, version FROM locks WHERE facebook_id = :id');
        $stmt->execute(array('id' => $id));
        
        $result = $stmt->fetchAll();
        
        $passed_lock = false;
        if (count($result) === 1)
        {
            // Verify lock
            $ret['actual_lock'] = $result[0]['lock_token'];
            $passed_lock = $ret['actual_lock'] === $lock_token;
            
            // Verify version
            $ret['actual_version'] = $result[0]['version'];
            $newer_version = $ret['actual_version'] < $version;
            
            // Insert data
            if ($passed_lock && $newer_version)
            {
                $conn->beginTransaction();
                
                // Insert data
                $stmt = $conn->prepare('INSERT INTO states (facebook_id, data) VALUES(:id, :data) ON DUPLICATE KEY UPDATE data=:data');
                $stmt->execute(array('id' => $id, 'data' => $data));

                // Update version
                $stmt = $conn->prepare('UPDATE locks SET version = :version WHERE facebook_id = :id');
                $stmt->execute(array('id' => $id, 'version' => $version));
                
                $conn->commit();
                
                $ret['success'] = true;
            }
        }
        else
        {
            $ret['missing_lock'] = true;
        }
    }
    catch (PDOException $e)
    {
        $ret['success'] = false;
    }
    
    return $ret;
}

?>