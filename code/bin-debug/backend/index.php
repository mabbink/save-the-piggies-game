<?php

session_start();

header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");

require_once('facebook.php');
require_once('db.php');
require_once('config.php');

$response = array(
    'code' => 500,
    'message' => 'Unknown server error'
);

// Check for a valid facebook session
$fb_session = fb_get_valid_session(FB_APP_ID, FB_APP_SECRET);
if ($fb_session)
{
    // Check the user ID is set in the session
    $userId = fb_get_user_id_from_session($fb_session);
    
    if ($userId)
    {
        // Send or retrieve?
        $posting = false;
        switch ($_SERVER['REQUEST_METHOD'])
        {
            case 'POST':
                $posting = true;
            case 'GET':
                if ($conn = db_connect(DB_HOST, DB_NAME, DB_USER, DB_PSWD))
                {
                    if ($posting)
                    {
                        // Storing data
                        if (isset($_POST['data']) && NULL != $_POST['data'])
                        {
                            $data = $_POST['data'];
                            
                            if (isset($_POST['lock']))
                                $lock = $_POST['lock'];
                            else 
                                $lock = NULL;
                            if (isset($_POST['version']))
                                $version = $_POST['version'];
                            else 
                                $version = NULL;
                                
                            $res = db_post($conn, $userId, $data, $lock, $version);
                            if ($res['success'])
                                set_response(200, "OK");
                            else
                            {
                                if (isset($res['actual_lock']) && $res['actual_lock'] != $lock)
                                    set_response(423, "Lock not valid");
                                else
                                if (isset($res['actual_version']) && $res['actual_version'] >= $version)
                                    set_response(409, "Version not newer than existing data");
                                else
                                if (isset($res['missing_lock']))
                                    set_response(500, "No lock in database; get data first");
                                else
                                    set_response(500, "Error storing to database");
                            }
                        }
                        else
                        {
                            set_response(400, "Necessary parameters not supplied");
                        }
                    }
                    else
                    {
                        // Getting data
                        $lock = uniqid();
                        $storedData = db_get($conn, $userId, $lock);
                        if (is_string($storedData))
                        {
                            if (!empty($storedData))
                                set_response(200, $storedData, $lock);
                            else
                                set_response(404, "No stored data", $lock);
                        }
                        else
                            set_response(500, "Error retrieving from database");
                    }
                }
                else
                {
                    set_response(500, "Couldn't connect to database");
                }
                break;
            default:
                set_response(400, 'Unsupported method');
                break;
        }
    }
    else
    {
        set_response(500, "Couldn't get user ID");
    }
}
else
{
    set_response(401, 'Not authorized');
}

exit(json_encode($response));

function set_response($code, $message, $lock = NULL)
{
    global $response;
    $response['code'] = $code;
    $response['message'] = $message;
    if (NULL != $lock)
        $response['lock'] = $lock;
}

?>