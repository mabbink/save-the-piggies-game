package
{
	import flash.display.DisplayObject;

	public final class DisplayUtils
	{
		public static function scaleToSize(
			item:DisplayObject,
			width:Number, height:Number,
			keepAspect:Boolean
		):void
		{
			if (keepAspect)
			{
				var factor:Number = Math.min(
					width / item.width,
					height / item.height
				);
				
				item.scaleX = item.scaleY = factor;
			}
			else
			{
				item.scaleX = width / item.width;
				item.scaleY = height / item.height;
			}
		}
	}
}