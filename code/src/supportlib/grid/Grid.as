package supportlib.grid {
	public interface Grid 
	{
		function getWidth():int;
		function getHeight():int;
		function readNeighbourIndices(index:int, result:Vector.<int>):int;
	}
}