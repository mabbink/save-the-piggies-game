package supportlib.grid {
	import de.polygonal.ds.Collection;
	import de.polygonal.ds.DLL;
	import de.polygonal.ds.IntHashSet;
	import de.polygonal.ds.Itr;
	import de.polygonal.ds.ListSet;
	import de.polygonal.ds.SLL;
	import de.polygonal.ds.SLLNode;
	import de.polygonal.ds.Set;

	public class GridPathfinder
	{
		private var gridWidth:int;
		private var gridHeight:int;
		
		private var closedSet:Set;
		private var openSet:Set;
		private var openSetSorted:SLL;
		
		private var g_score:Vector.<int>;
		private var f_score:Vector.<int>;
		
		private var came_from:Vector.<int>;
		
		private var neighbours:Vector.<int>;
		
		private var grid:Grid;
		
		public function GridPathfinder(
			grid:Grid
		)
		{
			this.grid = grid;
			this.gridWidth = grid.getWidth();
			this.gridHeight = grid.getHeight();
			
			neighbours = new Vector.<int>(8, true);
			
			came_from = new Vector.<int>(gridWidth * gridHeight, true);
			
			closedSet = new IntHashSet(gridWidth * gridHeight, -1, true);
			openSet = new IntHashSet(gridWidth * gridHeight, -1, true);
			openSetSorted = new SLL(0);
			
			g_score = new Vector.<int>(gridWidth * gridHeight, true);
			f_score = new Vector.<int>(gridWidth * gridHeight, true);
		}
		
		public function find(
			start:int,
			goal:int
		):Boolean
		{
			if (start < 0 || goal < 0) return false;
			if (start >= gridWidth * gridHeight || goal >= gridWidth * gridHeight) return false;
			
			// Init data
			var i:int = 0;
			
			closedSet.clear();
			openSet.clear();
			openSetSorted.clear();
			
			// Start algorithm
			g_score[start] = 0;
			f_score[start] = heuristic_cost_estimate(start, goal);

			addToOpenSet(start);
			
			while (!openSet.isEmpty())
			{
				var current:int = openSetSorted.removeHead() as int;
				if (current === goal)
				{
					return true;
				}
				
				openSet.remove(current);
				closedSet.set(current);
				
				var numNeighbours:int = getNeighbourNodes(current);
				for (i = 0; i < numNeighbours; ++i)
				{
					var neighbour:int = neighbours[i];
					var tentative_g_score:int = g_score[current] + dist_between(current, neighbour);
					
					if (closedSet.contains(neighbour))
					{
						if (tentative_g_score >= g_score[neighbour])
						{
							continue;
						}
					}
					
					if (!openSet.contains(neighbour) || tentative_g_score < g_score[neighbour])
					{
						came_from[neighbour] = current;
						g_score[neighbour] = tentative_g_score;
						f_score[neighbour] = g_score[neighbour] + heuristic_cost_estimate(neighbour, goal);
						addToOpenSet(neighbour);
					}
				}
			}
			
			return false;
		}
		
		private function addToOpenSet(
			node:int
		):void
		{
			if (openSet.set(node))
			{
				var added:Boolean = false;

				if (openSetSorted.size() > 0)
				{
					var fscoreNode:int = f_score[node];
					var listNode:SLLNode = openSetSorted.getNodeAt(0);

					while (listNode && !added)
					{
						var other:int = listNode.val as int;
						var fscoreOther:int = f_score[other];
						if (fscoreNode < fscoreOther || (fscoreOther === fscoreNode && node < other))
						{
							openSetSorted.insertBefore(listNode, node);
							added = true;
						}
						
						listNode = listNode.next;
					};
				}
				
				if (!added)
				{
					openSetSorted.append(node);
				}
			}
		}
		
		public function buildPath(
			start:int,
			end:int
		):Vector.<int>
		{
			var path:Vector.<int> = new Vector.<int>;
			
			var current:int = end;
			while (current !== start)
			{
				var next:int = came_from[current];
				path.push(current);
				current = next;
			}
			
			return path;
		}
		
		private function dist_between(
			a:int,
			b:int
		):int
		{
			if (a - b == -1 || a - b == 1 || (a % gridWidth) === (b % gridWidth))
				return 0x00010000;
			else
				return 0x00016A09;	// sqrt 2
		}
		
		private var neighbourResult:Vector.<int> = new Vector.<int>(4);
		private function getNeighbourNodes(
			node:int
		):int
		{
			var numIndices:int = grid.readNeighbourIndices(node, neighbourResult);
			
			for (var i:int = 0; i < numIndices; ++i)
				neighbours[i] = neighbourResult[i];
				
			return numIndices;
		}
		
		private function heuristic_cost_estimate(
			a:int,
			b:int
		):int
		{
			var ax:int = getX(a);
			var ay:int = getY(a);
			var bx:int = getX(b);
			var by:int = getY(b);
			
			var dx:int = bx - ax;
			var dy:int = by - ay;
			
			if (dx < 0)
				dx = -dx;
			if (dy < 0)
				dy = -dy;
			
			var cost:int = dx + dy;
			return cost << 16;
		}
		
		private function getX(
			node:int
		):int
		{
			return node % gridWidth;
		}
		
		private function getY(
			node:int
		):int
		{
			return (node - (node % gridWidth)) / gridWidth;
		}
	}
}