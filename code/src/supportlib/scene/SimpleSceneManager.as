package supportlib.scene {
	import flash.display.DisplayObjectContainer;
	import flash.events.Event;
	import flash.utils.getTimer;
	import org.as3commons.logging.api.getLogger;
	import org.as3commons.logging.api.ILogger;
	
	
	/**
	 * A SceneManager that updates on EnterFrame events.
	 *  
	 * @author wmarsh
	 * 
	 */
	public class SimpleSceneManager implements SceneManager
	{
		private static const logger:ILogger = getLogger(SimpleSceneManager);

		private var sceneStack:Array;
		
		private var holderGraphic:DisplayObjectContainer;
		
		private var currentScene:Scene;
		
		private var lastTime:int;
		
		/**
		 * 
		 * @param holderGraphic Graphic to use as the scene container.
		 * 
		 */
		public function SimpleSceneManager(holderGraphic:DisplayObjectContainer)
		{
			this.holderGraphic = holderGraphic;
			
			holderGraphic.addEventListener(Event.ENTER_FRAME, onEnterFrame);
			
			sceneStack = [];
		}
		
		private function switchToScene(scene:Scene, init:Boolean, cleanup:Boolean):void
		{
			logger.info("Switching to new scene: {0}", [scene]);
			
			if (null != this.currentScene && cleanup)
			{
				logger.info("Cleaning up scene {0}", [currentScene]);
				this.currentScene.cleanup(this);
			}
			
			if (init)
				scene.init(this);
			
			this.currentScene = scene;
			
			this.currentScene.draw();
			
			this.lastTime = getTimer();
		}
		
		public function setActiveScene(scene:Scene):void
		{
			clearStack(true);
			switchToScene(scene, true, true);
		}
		
		public function pushScene(scene:Scene):void
		{
			logger.info("pushing scene {0}", [scene]);
			
			if (currentScene)
				sceneStack.push(currentScene);
			
			switchToScene(scene, true, false);
		}
		
		public function popScene(cleanup:Boolean):void
		{
			logger.info("pop");
			
			var oldScene:Scene = null;
			if (sceneStack.length > 0)
			{
				oldScene = sceneStack.pop();
			}
			
			switchToScene(oldScene, false, cleanup);
			oldScene.resume(this);
		}
		
		public function getActiveScene():Scene
		{
			return this.currentScene;
		}
		
		public function getDisplayContainer():*
		{
			return this.holderGraphic;
		}
		
		public function clearStack(cleanup:Boolean):void
		{
			while (sceneStack.length > 0)
			{
				if (cleanup)
				{
					logger.info("Cleaning up scene {0}", [sceneStack[0]]);
					sceneStack[0].cleanup(this);
				}
				sceneStack.shift();
			}
		}
		
		/**
		 * Launch scene updates and drawing.
		 * 
		 * @param e EnterFrame event.
		 * 
		 */
		private function onEnterFrame(e:Event):void
		{
			var time:int = getTimer();
			
			if (-1 != lastTime)
			{
				if (time != lastTime)
				{
					if (null != this.currentScene)
						this.currentScene.update(time - lastTime);
				}
			}
			
			if (null != this.currentScene)
				this.currentScene.draw();
			
			lastTime = time;
		}
	}
}