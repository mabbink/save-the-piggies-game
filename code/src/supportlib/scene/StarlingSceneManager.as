package supportlib.scene {
	import flash.events.*;
	import org.as3commons.logging.api.*;
	import starling.core.*;
	import starling.events.EnterFrameEvent;
	
	/**
	 * A SceneManager that uses the Starling framework.
	 *  
	 * @author wmarsh
	 * 
	 */
	public class StarlingSceneManager implements SceneManager
	{
		private static const logger:ILogger = getLogger(StarlingSceneManager);

		private var sceneStack:Array;
		private var currentScene:Scene;
		
		private var starling:Starling;
		
		public function StarlingSceneManager(starling:Starling)
		{
			this.starling = starling;
			sceneStack = [];
			
			starling.root.addEventListener(EnterFrameEvent.ENTER_FRAME, onEnterFrame);
		}
		
		private function switchToScene(scene:Scene, init:Boolean, cleanup:Boolean):void
		{
			logger.info("Switching to new scene: {0}", [scene]);
			
			if (null != this.currentScene && cleanup)
			{
				logger.info("Cleaning up scene {0}", [currentScene]);
				this.currentScene.cleanup(this);
			}
			
			if (init)
				scene.init(this);
			
			this.currentScene = scene;
			
			this.currentScene.draw();
		}
		
		public function setActiveScene(scene:Scene):void
		{
			clearStack(true);
			switchToScene(scene, true, true);
		}
		
		public function pushScene(scene:Scene):void
		{
			logger.info("pushing scene {0}", [scene]);
			
			if (currentScene)
				sceneStack.push(currentScene);
			
			switchToScene(scene, true, false);
		}
		
		public function popScene(cleanup:Boolean):void
		{
			logger.info("pop");
			
			var oldScene:Scene = null;
			if (sceneStack.length > 0)
			{
				oldScene = sceneStack.pop();
			}
			
			switchToScene(oldScene, false, cleanup);
			oldScene.resume(this);
		}
		
		public function getActiveScene():Scene
		{
			return this.currentScene;
		}
		
		public function getDisplayContainer():*
		{
			return starling.root;
		}
		
		public function clearStack(cleanup:Boolean):void
		{
			while (sceneStack.length > 0)
			{
				if (cleanup)
				{
					logger.info("Cleaning up scene {0}", [sceneStack[0]]);
					sceneStack[0].cleanup(this);
				}
				sceneStack.shift();
			}
		}
		
		/**
		 * Launch scene updates
		 * 
		 * @param e EnterFrame event.
		 * 
		 */
		private function onEnterFrame(e:EnterFrameEvent):void
		{
			var msElapsed:int = Math.round(e.passedTime * 1000);
			if (null != this.currentScene)
				this.currentScene.update(msElapsed);
			
			if (null != this.currentScene)
				this.currentScene.draw();
		}
	}
}