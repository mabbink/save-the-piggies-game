package supportlib.scene {
	import flash.display.DisplayObjectContainer;

	/**
	 * Manages scenes and a graphical container to hold them.
	 *  
	 * @author wmarsh
	 * 
	 */
	public interface SceneManager
	{
		/**
		 * 
		 * @param scene The new scene to manage.
		 * 
		 */
		function setActiveScene(scene:Scene):void;
		
		/**
		 * 
		 * @return The currently-managed scene. 
		 * 
		 */
		function getActiveScene():Scene;
		
		/**
		 * Push a scene to the stack and activate it.
		 *  
		 * @param scene Scene to push to the stack.
		 * 
		 */
		function pushScene(scene:Scene):void;
		
		/**
		 * Pop a scene from the stack.
		 *  
		 * @param cleanup true if the cleanup function should run on the scene.
		 * 
		 */
		function popScene(cleanup:Boolean):void;
		
		/**
		 * Clear the scene stack.
		 * 
		 * @param cleanup true is scenes should be cleaned up.
		 * 
		 */
		function clearStack(cleanup:Boolean):void;
		
		/**
		 * 
		 * @return The DisplayObjectContainer that the scenes should put their graphics into.
		 * 
		 */
		function getDisplayContainer():*;
	}
}