package supportlib.scene {
	/**
	 * A managed scene.
	 *  
	 * @author wmarsh
	 * 
	 */
	public interface Scene
	{
		/**
		 * Initialise the scene (add graphics to layer, etc.).
		 * 
		 * @param manager The SceneManager managing this scene.
		 * 
		 */
		function init(manager:SceneManager):void;
		
		/**
		 * Cleanup the scene (remove graphics, clean up event listeners, etc.)
		 * 
		 * @param manager
		 * 
		 */
		function cleanup(manager:SceneManager):void;
		
		/**
		 * Update the scene. 
		 *  
		 * @param msElapsed Seconds elapsed since last update.
		 * 
		 */
		function update(msElapsed:int):void;
		
		
		/**
		 * Tells the scene that it's the best time to finalise drawing.
		 * 
		 */
		function draw():void;
		
		/**
		 * Tells the scene that it's being resumed.
		 *  
		 * @param manager The scene manager.
		 * 
		 */
		function resume(manager:SceneManager):void;
	}
}