// Copyright (c) 2013 Wayne Marsh

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

package supportlib {
	import flash.display.Stage;
	import flash.events.KeyboardEvent;

	public class Keyboard
	{
		private var stage:Stage;
		
		private var time:int = 1;
		private var timestamps:Vector.<int>;
		
		private var timeStampAnyKey:int = -1;
		
		public function Keyboard(
			stage:Stage
		)
		{
			this.stage = stage;
			
			timestamps = new Vector.<int>(256, true);
			
			var i:uint;
			for (i = 0; i < timestamps.length; ++i)
				timestamps[i] = -1;
			
			stage.addEventListener(
				KeyboardEvent.KEY_DOWN,
				keyDown,
				false
			);
			stage.addEventListener(
				KeyboardEvent.KEY_UP, 
				keyUp,
				false
			);
		}
		
		public function cleanup():void
		{
			stage.removeEventListener(
				KeyboardEvent.KEY_DOWN,
				keyDown,
				false
			);
			stage.removeEventListener(
				KeyboardEvent.KEY_UP, 
				keyUp,
				false
			);
		}
		
		private function advanceTime():void
		{
			if (time === int.MAX_VALUE)
				time = 1;
			else
				++time;
		}
		
		private function keyDown(
			e:KeyboardEvent
		):void
		{
			++timeStampAnyKey;			
			
			if (e.keyCode > 255)
				return;

			if (-1 === timestamps[e.keyCode])
			{
				advanceTime();
				timestamps[e.keyCode] = time;
			}
		}
		
		private function keyUp(
			e:KeyboardEvent
		):void
		{
			if (e.keyCode > 255)
				return;
			
			timestamps[e.keyCode] = -1;
		}
		
		public function isPressed(
			code:uint
		):int
		{
			if (code > 255)
				return -1;
			
			return timestamps[code];
		}
		
		public function isAnyPressed(
			codes:Array
		):int
		{
			var highest:int = -1;
			
			for each (var code:uint in codes)
			{
				highest = Math.max(highest, isPressed(code));
			}
			
			return highest;
		}
		
		public function allKeys():int {return timeStampAnyKey;}
	}
}