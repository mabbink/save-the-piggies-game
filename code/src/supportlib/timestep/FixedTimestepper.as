package supportlib.timestep {
	/**
	 * Interface to a timing mechanism that provides fixed timestep updates.
	 *  
	 * @author wmarsh
	 * 
	 */
	public interface FixedTimestepper
	{
		/**
		 * Elapse time by specified number of MS, updating the state of the counters.
		 *  
		 * @param timeElapsedMs Time in MS to advance time state by.
		 * 
		 */
		function advanceTime(timeElapsedMs:int):void;
		
		/**
		 * Clear the counters, indicating that we've consumed/processed the frames.
		 * 
		 * Does not clear the blend factor; it doesn't make sense to consume this,
		 * as it would push out the regualarity of the frames.
		 * 
		 */
		function consumeFrames():void;
		
		/**
		 * Read how many frames have been pumped out by the time stepper.
		 * 
		 * @return The number of whole frames to process.
		 * 
		 */
		function readFrameCounter():uint;
		
		/**
		 * Read how far we are between the current frame and the next frame.
		 * 
		 * @return A number between [0-1) telling us how far between the current frame and the next frame the updates are.
		 * 
		 */
		function readBlendFactor():Number;
		
		/**
		 * Reads the length of the fixed step.
		 *  
		 * @return The length of the fixed step in milliseconds.
		 * 
		 */
		function getStepLength():int;
	}
}