package supportlib.timestep {
	import org.as3commons.logging.api.ILogger;
	import org.as3commons.logging.api.getLogger;

	/**
	 * Standard implementation of the fixed time stepper.
	 *  
	 * @author wmarsh
	 * 
	 */
	public class FixedTimestepperStandard implements FixedTimestepper
	{
		private static const logger:ILogger = getLogger(FixedTimestepperStandard);
		
		private var updateLength:int;
		private var maxFrameCount:int;
		
		private var timeBank:int;
		private var frameCount:uint;
		
		/**
		 * @param updateLength Length of fixed update in milliseconds.
		 * @param maxFrameCount Maximum number of frames in bank
		 * 
		 */
		public function FixedTimestepperStandard(updateLength:int, maxFrameCount:int)
		{
			if (maxFrameCount <= 0 && maxFrameCount != -1)
				throw new ArgumentError("Invalid value " + maxFrameCount + " for maxFrameCount." +
					" Should be -1 or positive.");
			
			this.updateLength = updateLength;
			this.maxFrameCount = maxFrameCount;
			
			this.timeBank = 0;
			this.frameCount = 0;
		}
		
		public function advanceTime(timeElapsedMs:int):void
		{
			this.timeBank += timeElapsedMs;
			
			var remainder:int = this.timeBank % this.updateLength;
			var framesElapsed:int = (this.timeBank - remainder) / this.updateLength;
			
			this.timeBank -= framesElapsed * this.updateLength;
			
			this.frameCount += framesElapsed;
			
			if (-1 != maxFrameCount)
			{
				if (this.frameCount > maxFrameCount)
				{
					if (logger.warnEnabled)
						logger.warn(
							"Frame count is {0}, max is {1}. Limiting to {1}",
							[frameCount, maxFrameCount]
						);
					
					frameCount = maxFrameCount;
				}
			}
		}
		
		public function consumeFrames():void
		{
			this.frameCount = 0;
		}
		
		public function readFrameCounter():uint
		{
			return this.frameCount;
		}
		
		public function readBlendFactor():Number
		{
			return Number(this.timeBank) / this.updateLength;
		}
		
		public function getStepLength():int
		{
			return updateLength;
		}
	}
}