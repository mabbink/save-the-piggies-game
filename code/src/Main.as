package
{
	import flash.display.Sprite;
	import flash.display.StageAlign;
	import flash.display.StageScaleMode;
	import flash.events.Event;
	
	import org.as3commons.logging.api.ILogger;
	import org.as3commons.logging.api.getLogger;
	import org.as3commons.logging.util.captureUncaughtErrors;
	
	import savethepiggies.Director;
	
	[SWF(backgroundColor="0x6b9b38")]
	public class Main extends Sprite
	{
		private static const logger:ILogger = getLogger(Main);
		
		private var gameContainer:Sprite;
		
		public function Main()
		{
			gameContainer = new Sprite;
			addChild(gameContainer);
			
			if (IsDebugEnvironment())
			{
				initLogging(stage, false);
				logger.info("Initialized logging");
			}
			
			addEventListener(Event.ADDED_TO_STAGE, addedToStage);
		}
		
		private function addedToStage(e:Event):void
		{
			captureUncaughtErrors(loaderInfo);
			
			removeEventListener(Event.ADDED_TO_STAGE, addedToStage);
			
			stage.scaleMode = StageScaleMode.NO_SCALE;
			stage.align = StageAlign.TOP_LEFT;
			
			initGame();
		}
		
		private function initGame():void
		{
			var director:Director = new Director(gameContainer);
		}
		
		public static function IsDebugEnvironment():Boolean
		{
			CONFIG::debug
			{
				return true;
			}
			
			return false;
		}
	}
}