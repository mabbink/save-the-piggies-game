package
{
	

	public class ApplicationVars
	{
		public static const START_MONEY:int 				= 100;
		public static const PIGGY_ENERGY_DRAIN:Number 		= 0.0006;
		public static const PIGGY_FULLNESS_DRAIN:Number 	= 0.001;
		
		
		public static const LUPIN:String 			= "Lupin";
		public static const PREPPED_LUPIN:String 	= "Prepared Lupin";
		public static const COOKED_LUPIN:String 	= "Cooked Lupin";
		public static const TOMATO:String 			= "Tomato";
		public static const CUCUMBER:String 		= "Cucumber";
		public static const PEPPER:String 			= "Pepper";
		public static const APPLE:String 			= "Apple";
		public static const BANANA:String 			= "Banana";
		public static const GRAPE:String 			= "Grapes";
		public static const FLOWER:String 			= "Flowers";
		public static const FLOUR:String 			= "Flour";
		
		public static const PICMAC:String 			= "Picmac";
		public static const BURRITO:String 			= "Burrito";
		public static const PIZZA:String 			= "Pizza";
		public static const SMOOTHY:String 			= "Smoothy";
		public static const VIVERA_BURGER:String 	= "Vivera burger";
		public static const SHASHLIK:String 		= "Shashlik";
		public static const FRUIT_SHASHLIK:String 	= "Fruit Shashlik";
		
		
		public static const SELLABLES:Array 		= [
			ApplicationVars.PICMAC, 
			ApplicationVars.BURRITO, 
			ApplicationVars.PIZZA, 
			ApplicationVars.SMOOTHY, 
			ApplicationVars.VIVERA_BURGER, 
			ApplicationVars.SHASHLIK, 
			ApplicationVars.FRUIT_SHASHLIK
		];

	}
}