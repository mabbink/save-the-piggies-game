package savethepiggies.persistence
{
	

	/**
	 * Interface for saving/retrieving data from a data store.
	 * Uses callbacks to support asynchronous data stores.
	 * 
	 */
	public interface Persistence
	{
		/**
		 * 
		 * @param data Data to save
		 * @param callback Callback function that will receive the following object:
		 * On success:
		 * {
		 * 	success:true
		 * }
		 * 
		 * On error:
		 * {
		 * 	success:false,
		 * 	errorCode:<int>,
		 * 	errorMessage:<String>
		 * }
		 * 
		 */
		function save(data:Array = null, callback:Function = null):void;
		
		/**
		 * 
		 * @param callback Callback function that will receive the following object:
		 * On success:
		 * {
		 * 	success:true,
		 * 	data:<ByteArray> (null if nothing saved)
		 * }
		 * 
		 * On error:
		 * {
		 * 	success:false,
		 * 	errorCode:<int>,
		 * 	errorMessage:<String>
		 * }
		 * 
		 */
		function load(callback:Function):void;
	}
}