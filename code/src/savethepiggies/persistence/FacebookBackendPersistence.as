package savethepiggies.persistence
{
	import com.hurlant.util.Base64;
	
	import flash.utils.ByteArray;
	
	import facebook.GraphAPI;
	
	import org.as3commons.logging.api.ILogger;
	import org.as3commons.logging.api.getLogger;
	
	/**
	 * Persistence implementiton using a web server backend
	 * and authorization with the Facebook API.
	 * 
	 */
	public final class FacebookBackendPersistence implements Persistence
	{
		private static const DEFAULT_URL:String = "backend/";
		
		public static const ERROR_CODE_BAD_AUTH:int = 401;
		public static const ERROR_CODE_NOT_FOUND:int = 404;
		
		private static const logger:ILogger = getLogger(FacebookBackendPersistence);
		
		private var fb:GraphAPI;
		
		private var lock_token:String;
		private var version:int;
		
		public function FacebookBackendPersistence(fb:GraphAPI){
			
			this.fb = fb; if (!fb) throw new ArgumentError("GraphAPI reference is null!");
			
			lock_token = "";
			version = 0;
		}
		
		public function save(data:Array = null, callback:Function = null):void
		{
			if(data){
				var detailsCopy:ByteArray = new ByteArray;
				detailsCopy.writeBytes(data[0], 0, data[0].length);
				detailsCopy.compress();
				detailsCopy.position = 0;
				
				var dataCopy:ByteArray = new ByteArray;
				dataCopy.writeBytes(data[1], 0, data[1].length);
				dataCopy.compress();
				dataCopy.position = 0;
				
				var result:String = Base64.encodeByteArray(detailsCopy)+':'+Base64.encodeByteArray(dataCopy)
			
			}else{
				var result:String = ':';
			}
			logger.info('Saving compressed encoded:'+result.length);
			
			++version;
			
			retryWrap(callback, saveData, result, lock_token, version);
		}
		
		public function load(callback:Function):void
		{
			retryWrap(callback, loadData);
		}
		
		private function saveData(callback:Function, args:Array):void
		{
			if (args.length != 3)
				throw new ArgumentError("Should be 3 arguments");
			
			var data:String = String(args[0]);
			var lock:String = String(args[1]);
			var version:int = int(args[2]);
			
			// Make request to server
			new FacebookBackendHttp(DEFAULT_URL, function(serverResponse:Object):void {
				logger.info("Service responded with return code: {0}, message: {1}", [serverResponse.code, serverResponse.message]);
				
				if (serverResponse.code === 200)
				{
					callback({
						success:true
					});
				}
				else
				{
					callback({
						success:false,
						errorCode:serverResponse.code,
						errorMessage:serverResponse.message
					});
				}
			}, data, lock, version);
		}
		
		private function loadData(callback:Function, args:Array):void
		{
			// Make request to server
			new FacebookBackendHttp(DEFAULT_URL, function(serverResponse:Object):void {
				logger.info("Service responded with return code: {0}, message: {1}, lock_token: {2}", [serverResponse.code, serverResponse.message, serverResponse.lock]);
				
				if (serverResponse.hasOwnProperty("lock"))
					lock_token = serverResponse.lock;
				
				if (serverResponse.code === 200)
				{
					try
					{
						logger.info('Loaded compressed encoded:'+serverResponse.message.length);
						
						var dataBlocks:Array = serverResponse.message.split(':');
						var details:ByteArray = Base64.decodeToByteArray(dataBlocks[0]);
						var data:ByteArray = Base64.decodeToByteArray(dataBlocks[1]);
						if (details)
						{
							if (details.length == 0)
								throw new Error("Invalid data");
							details.uncompress();
							data.uncompress();
						}
					}
					catch (e:Error)
					{
						logger.error("Decoding server response error: {0}", [e]);
						details = null;
						data = null;
					}
					
					if (details && !CONFIG::constantDBRefresh)
					{
						logger.info('Loaded uncompressed unencoded: details:'+details.length);
						logger.info('Loaded uncompressed unencoded: data:'+data.length);
						
						details.position = 0;
						data.position = 0;
						callback({
							success:true,
							data:[details, data]
						});
					}
					else
					{
						callback({
							success:false,
							errorCode:500,
							errorMessage:"Invalid data from server"
						});
					}
				}
				else
				{
					callback({
						success:false,
						errorCode:serverResponse.code,
						errorMessage:serverResponse.message
					});
				}
			});
		}
		
		private function retryWrap(callback:Function, operationFunction:Function, ... args):void
		{
			// Try initially with cached Facebook token 
			operationFunction(retryCallback, args);
			
			function retryCallback(r:Object):void
			{
				if (r.success === true || (r.success === false && r.code === ERROR_CODE_NOT_FOUND))
				{
					// Great, let's forward that to the user-supplied callback
					callback(r);
				}
				else
				{
					// If it's just a bad access token, let's try with
					// one updated from Facebook's servers
					if (r.errorCode === ERROR_CODE_BAD_AUTH)
					{
						logger.info("Trying again, forcing user access token retrieval from Facebook servers");
						fb.getLoginStatus(function(r:Object):void{
							operationFunction(callback, args);
						}, true);
					}
					else
					{
						logger.info("Non-authorization error.")
						// Otherwise I guess a bad thing has happened. Fall through.
						callback(r);
					}
				}
			}
		}
	}
}