package savethepiggies.persistence
{
	import flash.net.SharedObject;
	import flash.utils.setTimeout;
	
	import org.as3commons.logging.api.ILogger;
	import org.as3commons.logging.api.getLogger;
	
	public final class SharedObjectPersistence implements Persistence
	{
		private static const logger:ILogger = getLogger(SharedObjectPersistence);
		
		private var name:String;
		
		public function SharedObjectPersistence(
			name:String
		)
		{
			this.name = name;
		}
		
		public function save(data:Array = null, callback:Function = null):void
		{
			var res:Object = {};
			
			try
			{
				var so:SharedObject = SharedObject.getLocal(name);
				if(data){
					so.data.save = data;
				}else{
					delete so.data.save;
				}
				so.flush();
				
				res.success = true;
			}
			catch (e:Error)
			{
				logger.warn('Couldn\'t write to shared object "{0}": {1}', [name, e]);
				
				res.success = false;
				res.errorCode = 500;
				res.errorMessage = "Couldn't write to shared object";
			}
			
			// Use a timeout to replicate asynchronous nature and not
			// run the callback before this method returns!
			setTimeout(function():void{callback(res);}, 10);
		}
		
		public function load(callback:Function):void
		{
			var res:Object = {};
			
			try
			{
				var so:SharedObject = SharedObject.getLocal(name);
				var data:Array;
				//so.clear();
				 
				if (so.data.hasOwnProperty("save") && !CONFIG::constantDBRefresh)
				{
					trace('Loading from:'+name+' length:'+so.data.save.length);
					
					res.success = true;
					res.data = so.data.save;
				}
				else
				{
					res.success = false;
					res.errorCode = 401;
					res.errorCode = "No data saved";
				}
			}
			catch (e:Error)
			{
				logger.warn('Couldn\'t load from shared object "{0}": {1}', [name, e]);
				
				res.success = false;
				res.errorCode = 500;
				res.errorMessage = "Couldn't read from shared object";
			} 
			   
			// Use a timeout to replicate asynchronous nature and not
			// run the callback before this method returns!
			setTimeout(function():void{callback(res);}, 10);
		}
	}
}