package savethepiggies.persistence
{
	import flash.events.Event;
	import flash.events.IOErrorEvent;
	import flash.events.SecurityErrorEvent;
	import flash.net.URLLoader;
	import flash.net.URLLoaderDataFormat;
	import flash.net.URLRequest;
	import flash.net.URLRequestMethod;
	import flash.net.URLVariables;
	
	import org.as3commons.logging.api.ILogger;
	import org.as3commons.logging.api.getLogger;

	/**
	 * Encapsulates HTTP fetch/saves with Facebook backend. 
	 * 
	 */
	internal final class FacebookBackendHttp
	{
		private static const logger:ILogger = getLogger(FacebookBackendHttp);
		
		private var processed:Boolean = false;
		
		private var callback:Function;
		
		private var loader:URLLoader;
		
		public function FacebookBackendHttp(
			url:String,
			callback:Function,
			data:String = null,
			lock:String = null,
			version:int = int.MIN_VALUE
		)
		{
			this.callback = callback;
			
			var req:URLRequest = new URLRequest(url);
			req.method = data ? URLRequestMethod.POST : URLRequestMethod.GET;
			
			if (data)
			{
				var requestVars:URLVariables = new URLVariables();
				requestVars.data = data;
				requestVars.lock = lock;
				requestVars.version = version;
				req.data = requestVars;
			}
			
			loader = new URLLoader();
			toggleEvents(true);
			
			try
			{
				loader.dataFormat = URLLoaderDataFormat.TEXT;
				loader.load(req);
			}
			catch (e:Error)
			{
				logger.error("Exception on URLLoader creation: {0}", [e]);
				process(null);
			}
		}
		
		private function toggleEvents(eventsOn:Boolean):void
		{
			logger.debug("Toggled events: {0}", [eventsOn]);
			var func:Function = eventsOn ? loader.addEventListener : loader.removeEventListener;
			
			func(Event.COMPLETE, onLoadComplete);
			func(SecurityErrorEvent.SECURITY_ERROR, onSecurityError);
			func(IOErrorEvent.IO_ERROR, onIoError);
		}
		
		private function onLoadComplete(e:*):void
		{
			logger.debug("onLoadComplete");
			if (logger.debugEnabled)
				logger.debug("response: {0}", [loader.data]);
			process(loader.data);
		}
		
		private function onSecurityError(e:*):void
		{
			logger.debug("onSecurityError");
			process(null);
		}
		
		private function onIoError(e:*):void
		{
			logger.debug("onIoError: {0}", [e]);
			process(null);
		}
		
		private function process(
			response:String
		):void
		{
			if (processed)
				logger.warn("Already processed!");
			else
			{
				toggleEvents(false);
				
				// Try to process response as correctly-formed data
				try
				{
					var obj:Object = JSON.parse(response);
					if (!(obj.code is int) || !(obj.message is String))
						throw new Error("Bad response");
				}
				catch (e:Error)
				{
					logger.warn("Bad response from server: {0}", [response]);

					// Use a default error instead
					obj = {
						code:500,
						message:"Malformed response"
					};
				}
				
				callback(obj);
			}
		}
	}
}