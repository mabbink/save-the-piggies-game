package savethepiggies 
{
	public interface IdGenerator 
	{
		function generate():uint;
	}
}