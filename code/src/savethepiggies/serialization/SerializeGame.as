package savethepiggies.serialization 
{
	import flash.geom.Point;
	import flash.utils.ByteArray;
	
	import org.as3commons.logging.api.ILogger;
	import org.as3commons.logging.api.getLogger;
	
	import savethepiggies.game.GameContext;
	import savethepiggies.game.GameObject;
	import savethepiggies.game.behaviours.PersistBehaviour;
	
	public final class SerializeGame 
	{
		public static const VERSION:uint = 0xDEAD17;
		private static const logger:ILogger = getLogger(SerializeGame);
		
		public function SerializeGame() 
		{
		}
		
		public static function serialize(
			gameContext:GameContext,
			mapCenter:Point,
			mapZoom:Number
		):Array
		{
			var details:ByteArray = new ByteArray;
			var data:ByteArray = new ByteArray;
			
			// Write version
			details.writeUnsignedInt(VERSION);
			
			// Write date stamp
			var d:Date = new Date;
			details.writeUTF("" + d.getTime());
			
			// Write misc game state
			details.writeUnsignedInt(gameContext.cash);
			if(gameContext.todaysTimestamp){
				details.writeFloat(gameContext.todaysTimestamp);
			}else{
				details.writeFloat(0);
			}
			if(gameContext.todaysPiggy1){
				details.writeUTF(gameContext.todaysPiggy1);
			}else{
				details.writeUTF('');
			}
			if(gameContext.todaysPiggy2){
				details.writeUTF(gameContext.todaysPiggy2);
			}else{
				details.writeUTF('');
			}
			if(gameContext.todaysPiggy3){
				details.writeUTF(gameContext.todaysPiggy3);
			}else{
				details.writeUTF('');
			}
			if(gameContext.invitedPeople){
				details.writeUTF(gameContext.invitedPeople);
			}else{
				details.writeUTF('');
			}
			
			details.writeFloat(mapCenter.x);
			details.writeFloat(mapCenter.y);
			details.writeFloat(mapZoom);
			
			details.writeUnsignedInt(gameContext.tutorialStep);
			
			details.writeBoolean(gameContext.shownEvilGuyTip);
			
			//settings
			details.writeBoolean(gameContext.enableFacebookPosting);
			details.writeBoolean(gameContext.enableBackgroundMusic);
			details.writeBoolean(gameContext.enableSoundEffects);
			
			// Write objects
			gameContext.gameObjects.visit(function(g:GameObject):void
			{
				if(g.isPart)
					return;
				if(g.recipeName.toLocaleLowerCase().indexOf("tutorial-recipe") != -1)
					return;
				
				data.writeUnsignedInt(g.id);
				data.writeUTF(g.recipeName);
				data.writeInt(g.createdAtI);
				data.writeInt(g.createdAtJ);
				
				var persist:PersistBehaviour = g.getBehaviourByType(PersistBehaviour);
				if (persist)
					data.writeObject(persist.save());
				else
					data.writeObject({});
			});
			logger.debug('Serializing, length:'+details.length+':'+data.length);
			
			return [details, data];
		}
	}

}