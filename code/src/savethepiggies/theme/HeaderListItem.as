package savethepiggies.theme
{
	import feathers.controls.Label;
	
	import starling.display.Image;
	import starling.display.Quad;
	import starling.display.Sprite;
	import starling.textures.Texture;

	public final class HeaderListItem extends Sprite{
		
		private var isShop:Boolean;
		private var itemName:String;
		private var amount:int;
		private var profit:int;
		
		private var bg:Quad;
		private var nameLabel:Label;
		private var counterLabel:Label;
		
		public function HeaderListItem(pName:String, pAmount:int, pProfit:int, pOdd:Boolean, pIcon:Texture, pIsShop:Boolean){
			this.itemName = pName;
			this.isShop = pIsShop;
			this.amount = pAmount;
			this.profit = pProfit;
			
			bg = new Quad(60, 9, 0xffffff);
			addChild(bg);
			
			var line:Quad = new Quad(60, 0.2, 0xe3e3e3);
			addChild(line);
			
			nameLabel = new Label();
			nameLabel.x = 10;
			nameLabel.y = 2;
			nameLabel.width = 40;
			if(pIsShop){
				nameLabel.nameList.add("headerListItemRed");
			}else{
				nameLabel.nameList.add("headerListItemGreen");
			}
			addChild(nameLabel);
			
			if(pIcon){
				var icon:Image = new Image(pIcon);
				icon.x = icon.y = 1;
				addChild(icon);
			}else{
				nameLabel.x = 2;
				nameLabel.width = 48;
			}
			
			counterLabel = new Label();
			counterLabel.x = 48;
			counterLabel.width = 10;
			counterLabel.y = 2;
			if(pIsShop){
				counterLabel.nameList.add("headerListItemCounterGold");
			}else{
				counterLabel.nameList.add("headerListItemCounter");
			}
			addChild(counterLabel);
			
			updateText();
			reColor(pOdd);
		}
		private function updateText():void{
			if(isShop){
				nameLabel.text = amount.toString()+' x '+itemName;
				if(amount == -1){
					counterLabel.text = '';
				}else{
					counterLabel.text = '$'+profit;
				}
			}else{
				nameLabel.text = itemName;
				if(amount == -1){
					counterLabel.text = '';
				}else{
					counterLabel.text = amount+'x';
				}
			}
		}
		
		
		public function updateAmount(pAmount:int):void{
			amount = pAmount;
			updateText();
		}
		public function reColor(pOdd:Boolean):void{
			if(pOdd || isShop){
				bg.color = 0xffffff;
			}else{
				bg.color = 0xf5f5f5;
			}
		}
	}
}