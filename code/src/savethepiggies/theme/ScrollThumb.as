package savethepiggies.theme 
{
	import feathers.core.IValidating;
	import feathers.display.Scale9Image;
	import feathers.textures.Scale9Textures;
	import feathers.utils.display.getDisplayObjectDepthFromStage;
	import flash.geom.Rectangle;
	import starling.display.Image;
	import starling.display.Sprite;
	import starling.textures.Texture;
	
	public class ScrollThumb extends Sprite implements IValidating
	{
		private var scale9:Scale9Image;
		private var markings:Image;
		
		public function ScrollThumb(
			thumbTexture:Texture,
			markingsTexture:Texture
		) 
		{
			scale9 = new Scale9Image(new Scale9Textures(
				thumbTexture,
				new Rectangle(
				.8 * 31/8, .8 * 22/8,
				.8 * 27/8, .8 * 146/8
				)
			));
			addChild(scale9);
			
			markings = new Image(markingsTexture);
			addChild(markings);
		}
		
		public override function set width(val:Number):void
		{
			scale9.width = val;
		}
		
		public override function set height(val:Number):void
		{
			scale9.height = val;
		}
		
		public function get depth():int
		{
			return getDisplayObjectDepthFromStage(this);
		}
		
		public function validate():void
		{
			markings.x = (scale9.width - markings.width) / 2;
			markings.y = (scale9.height - markings.height) / 2;
		}
	}

}