package savethepiggies.theme
{
	
	import starling.display.Image;
	import starling.display.Sprite;
	import starling.events.Touch;
	import starling.events.TouchEvent;
	import starling.events.TouchPhase;
	import starling.utils.AssetManager;

	public final class Checkbox extends Sprite {
		
		private var _changeCallback:Function;
		private var _bg:Image;
		private var _textures:Array;
		private var _checked:Boolean;
		
		public function Checkbox(assetManager:AssetManager, pChangeCallback:Function) {
			_changeCallback = pChangeCallback;
			
			_textures = [assetManager.getTexture('checkbox-open'), assetManager.getTexture('checkbox-checked')]
			_bg = new Image(_textures[0]);
			addChild(_bg);
			_checked = false;
			
			this.useHandCursor = true;
			this.addEventListener(TouchEvent.TOUCH, onTouch);
		}
		private function onTouch(e:TouchEvent):void{
			var touches:Vector.<Touch> = e.getTouches(this, TouchPhase.BEGAN);
			if(touches.length == 0) return;
			
			setEnabled(!_checked);
			_changeCallback(_checked);
		}
		
		public function setEnabled(pTrue:Boolean):void{
			_checked = pTrue;
			_bg.texture = _textures[_checked ? 1 : 0];
		}
	}
}