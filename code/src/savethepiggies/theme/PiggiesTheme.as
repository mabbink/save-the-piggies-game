package savethepiggies.theme 
{
	import flash.geom.Rectangle;
	import flash.text.TextFormatAlign;
	
	import feathers.controls.Alert;
	import feathers.controls.Button;
	import feathers.controls.ButtonGroup;
	import feathers.controls.Header;
	import feathers.controls.IScrollBar;
	import feathers.controls.Label;
	import feathers.controls.List;
	import feathers.controls.Panel;
	import feathers.controls.ScrollBar;
	import feathers.controls.Scroller;
	import feathers.controls.renderers.DefaultListItemRenderer;
	import feathers.controls.text.BitmapFontTextRenderer;
	import feathers.core.DisplayListWatcher;
	import feathers.display.Scale9Image;
	import feathers.text.BitmapFontTextFormat;
	import feathers.textures.Scale9Textures;
	
	import starling.display.DisplayObjectContainer;
	import starling.display.Image;
	import starling.textures.TextureAtlas;
	
	public final class PiggiesTheme extends DisplayListWatcher 
	{
		private var atlas:TextureAtlas;
		
		public function PiggiesTheme(
			topLevelContainer:DisplayObjectContainer,
			atlas:TextureAtlas
		)
		{
			super(topLevelContainer);
			
			this.atlas = atlas;
			
			setInitializerForClass(Label, function(label:Label):void{
				label.textRendererProperties.textFormat = new BitmapFontTextFormat("ui-font")
			});
			
			setInitializerForClass(Label, function(label:Label):void{
				label.textRendererProperties.textFormat = new BitmapFontTextFormat("ui-font", 5, 0x34b7d7);
			}, "moneyLabel");
			
			setInitializerForClass(Label, function(label:Label):void{
				label.textRendererProperties.textFormat = new BitmapFontTextFormat("ui-font", 5, 0xffff00, 'right');
			}, "shopTotalGold");
			
			
			
			setInitializerForClass(Label, function(label:Label):void{
				label.textRendererProperties.textFormat = new BitmapFontTextFormat("ui-font", 7, 0x1591cb, 'center');
			}, "settingsTitle");
			
			setInitializerForClass(Label, function(label:Label):void{
				label.textRendererProperties.textFormat = new BitmapFontTextFormat("ui-font", 5, 0x444444, 'left');
			}, "settingsLabel");
			
			setInitializerForClass(Label, function(label:Label):void{
				label.textRendererProperties.textFormat = new BitmapFontTextFormat("ui-font", 5, 0x444444, 'center');
				label.textRendererProperties.wordWrap = true;
			}, "settingsLabelCenter");
			
			setInitializerForClass(Label, function(label:Label):void{
				label.textRendererProperties.textFormat = new BitmapFontTextFormat("ui-font", 5, 0xff0000, 'left');
			}, "settingsWarnLabel");
			
			
			
			
			
			
			setInitializerForClass(Label, function(label:Label):void{
				label.textRendererProperties.textFormat = new BitmapFontTextFormat("ui-font", 4, 0xa7a7a7);
			}, "headerButton");
			
			setInitializerForClass(Label, function(label:Label):void{
				label.textRendererProperties.textFormat = new BitmapFontTextFormat("ui-font", 4, 0xabd225);
			}, "headerListItemGreen");
			
			setInitializerForClass(Label, function(label:Label):void{
				label.textRendererProperties.textFormat = new BitmapFontTextFormat("ui-font", 4, 0xeb10bf);
			}, "headerListItemPurple");
			
			setInitializerForClass(Label, function(label:Label):void{
				label.textRendererProperties.textFormat = new BitmapFontTextFormat("ui-font", 4, 0xc34b00);
			}, "headerListItemRed");
			
			setInitializerForClass(Label, function(label:Label):void{
				label.textRendererProperties.textFormat = new BitmapFontTextFormat("ui-font", 4, 0xffffff, 'center');
			}, "headerButtonCounter");
			
			setInitializerForClass(Label, function(label:Label):void{
				label.textRendererProperties.textFormat = new BitmapFontTextFormat("ui-font", 4, 0x09c3f9, 'right');
			}, "headerListItemCounter");
			
			setInitializerForClass(Label, function(label:Label):void{
				label.textRendererProperties.textFormat = new BitmapFontTextFormat("ui-font", 4, 0xe4e708, 'right');
			}, "headerListItemCounterGold");
			
			setInitializerForClass(Label, function(label:Label):void{
				label.textRendererProperties.textFormat = new BitmapFontTextFormat("ui-font", 4, 0xffff00, 'center');
			}, "balanceLabel");
			
			setInitializerForClass(Label, function(label:Label):void{
				label.textRendererProperties.textFormat = new BitmapFontTextFormat("ui-font", 8, 0xe4e708, 'center');
			}, "shopGold");
			
			setInitializerForClass(Button, buttonInitializer);
			setInitializerForClass(Panel, panelInitializer);
			setInitializerForClass(List, listInitializer);
			setInitializerForClass(Header, panelHeaderInitializer, Panel.DEFAULT_CHILD_NAME_HEADER);
			setInitializerForClass(DefaultListItemRenderer, itemRendererInitializer);
			setInitializerForClass(ScrollBar, verticalScrollBarInitializer, Scroller.DEFAULT_CHILD_NAME_VERTICAL_SCROLL_BAR);
			
			setInitializerForClass(Button, scrollBarMinimumTrackInitializer, ScrollBar.DEFAULT_CHILD_NAME_MINIMUM_TRACK);
			setInitializerForClass(Button, scrollBarThumbInitializer, ScrollBar.DEFAULT_CHILD_NAME_THUMB);
			setInitializerForClass(Button, scrollBarDecrementButtonInitializer, ScrollBar.DEFAULT_CHILD_NAME_DECREMENT_BUTTON);
			setInitializerForClass(Button, scrollBarIncrementButtonInitializer, ScrollBar.DEFAULT_CHILD_NAME_INCREMENT_BUTTON);
			
			setInitializerForClass(Alert, alertInitializer);
			setInitializerForClass(BitmapFontTextRenderer, alertMessageInitializer, Alert.DEFAULT_CHILD_NAME_MESSAGE);
			setInitializerForClass(Header, alertHeaderInitializer, Alert.DEFAULT_CHILD_NAME_HEADER);
			setInitializerForClass(ButtonGroup, alertButtonGroupInitializer, Alert.DEFAULT_CHILD_NAME_BUTTON_GROUP);
		}
		
		
		private function buttonInitializer(button:Button):void
		{
			button.defaultSkin = new Scale9Image(
				new Scale9Textures(
					atlas.getTexture("button_bg"),
					new Rectangle(
						30/8, 16/8,
						317/8, 72/8
					)
				)
			);
			button.disabledSkin = new Scale9Image(
				new Scale9Textures(
					atlas.getTexture("button_bg_disabled"),
					new Rectangle(
						30/8, 16/8,
						317/8, 72/8
					)
				)
			);
			
			button.useHandCursor = true;
			
			button.defaultLabelProperties.textFormat = new BitmapFontTextFormat("ui-button-font", 6);
		}
		
		private function panelInitializer(panel:Panel):void
		{
			panel.backgroundSkin = new Image(atlas.getTexture("panel1"));
			
			panel.padding = 2;
			panel.paddingBottom = 4;
		}
		
		private function panelHeaderInitializer(header:Header):void
		{
			header.backgroundSkin = new Image(atlas.getTexture("panelHeaderBg"));
			
			header.titleProperties.textFormat = new BitmapFontTextFormat("ui-font", 5);
		}
		
		private function listInitializer(list:List):void
		{
			list.interactionMode = Scroller.INTERACTION_MODE_MOUSE;
			list.scrollBarDisplayMode = Scroller.SCROLL_BAR_DISPLAY_MODE_FIXED;
			list.verticalScrollPolicy = Scroller.SCROLL_POLICY_ON;
			
			list.verticalScrollBarFactory = function():IScrollBar
			{
				var scrollBar:ScrollBar = new ScrollBar();
				scrollBar.direction = ScrollBar.DIRECTION_VERTICAL;
				return scrollBar;
			};
		}
		
		private function itemRendererInitializer(itemRenderer:DefaultListItemRenderer):void
		{
			itemRenderer.defaultSkin = new Image(atlas.getTexture("listItemBg"));
			itemRenderer.defaultLabelProperties.textFormat = new BitmapFontTextFormat("ui-font", 3);
			itemRenderer.minHeight = itemRenderer.maxHeight = 134.84 / 8.0;
			itemRenderer.gap = 2;
			itemRenderer.horizontalAlign = DefaultListItemRenderer.HORIZONTAL_ALIGN_LEFT;
			itemRenderer.paddingLeft = 2;
		}
		
		private function verticalScrollBarInitializer(scrollBar:ScrollBar):void
		{
		}
		
		private function scrollBarMinimumTrackInitializer(thumb:Button):void
		{
			thumb.defaultSkin = new Scale9Image(
				new Scale9Textures(
					atlas.getTexture("scroll-track"),
					new Rectangle(
						.8 * 23 / 8.0,
						.8 * 74 / 8.0,
						.8 * 39 / 8.0,
						.8 * 625 / 8.0
					)
				)
			);
		}
		
		private function scrollBarThumbInitializer(thumb:Button):void
		{
			thumb.defaultSkin = new ScrollThumb(
				atlas.getTexture("scroll-thumb"),
				atlas.getTexture("scroll-thumb-markings")
			);
			
			thumb.useHandCursor = true;
		}
		
		private function scrollBarDecrementButtonInitializer(decrement:Button):void
		{
			decrement.defaultSkin = new Image(atlas.getTexture("scroll-down"));
			decrement.useHandCursor = true;
		}
		
		private function scrollBarIncrementButtonInitializer(increment:Button):void
		{
			increment.defaultSkin = new Image(atlas.getTexture("scroll-up"));
			increment.useHandCursor = true;
		}
		
		private function alertInitializer(alert:Alert):void
		{
			alert.backgroundSkin = new Image(atlas.getTexture("AlertDialog"));
			
			if(alert.title.indexOf('Buy') > -1){
				alert.width = 160;
				alert.height = 100;
			}else if(alert.title.indexOf('Invite') > -1){
				alert.width = 160;
				alert.height = 100;
			}else if(alert.title.indexOf('Evil guy') > -1){
				alert.width = 170;
				alert.height = 100;
			}else{
				alert.width = atlas.getTexture("AlertDialog").width;
				alert.height = atlas.getTexture("AlertDialog").height;
			}
			alert.paddingLeft = 10;
			alert.paddingRight = 10;
			alert.paddingTop = 1;
			
		}
		
		private function alertMessageInitializer(renderer:BitmapFontTextRenderer):void
		{
			renderer.textFormat = new BitmapFontTextFormat("ui-button-font", 6, 16777215, TextFormatAlign.LEFT);
			renderer.wordWrap = true;
		}
		
		private function alertHeaderInitializer(header:Header):void
		{
			header.titleProperties.textFormat = new BitmapFontTextFormat("ui-button-font", 10);
			header.paddingTop = 5;
			header.paddingBottom = 10;
		}
		
		private function alertButtonGroupInitializer(group:ButtonGroup):void
		{
			group.paddingBottom = 5;
			group.paddingLeft = 5;
			group.paddingRight = 5;
			group.gap = 2;
			group.direction = ButtonGroup.DIRECTION_HORIZONTAL;
		}
	}

}