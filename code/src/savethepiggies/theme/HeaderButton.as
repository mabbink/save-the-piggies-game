package savethepiggies.theme
{
	import feathers.controls.Label;
	
	import savethepiggies.GlobalContext;
	
	import starling.display.Image;
	import starling.display.Sprite;

	public final class HeaderButton extends Sprite {
		
		private var bg:Image;
		private var label:Label;
		private var counter:Label;
		
		public function HeaderButton(globalContext:GlobalContext, text:String){
			bg = new Image(globalContext.assetManager.getTexture('header_button'));
			addChild(bg);
			
			label = new Label();
			label.touchable = false;
			label.text = text;
			label.x = 2;
			label.y = 3;
			label.width = 20;
			label.nameList.add("headerButton");
			addChild(label);
			
			counter = new Label();
			counter.touchable = false;
			counter.text = '0';
			counter.x = 22;
			counter.y = 3;
			counter.width = 8;
			counter.nameList.add("headerButtonCounter");
			addChild(counter);
		}
		
		public function updateCounter(pNewCount:int):void{
			counter.text = pNewCount.toString();
		}
	}
}