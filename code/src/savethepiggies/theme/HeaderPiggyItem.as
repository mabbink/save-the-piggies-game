package savethepiggies.theme
{
	import feathers.controls.Label;
	
	import savethepiggies.game.GameObject;
	import savethepiggies.game.behaviours.PiggyPropertiesBehaviour;
	
	import starling.display.Image;
	import starling.display.Quad;
	import starling.display.Sprite;
	import starling.textures.Texture;

	public final class HeaderPiggyItem extends Sprite {
		
		public var piggy:GameObject;
		
		private var bg:Quad;
		private var nameLabel:Label;
		private var bonusLabel:Label;
		private var task:int;
		private var group:int;
		
		public function HeaderPiggyItem(pPiggy:GameObject, pPiggyIcon:Texture){
			this.piggy = pPiggy;
			
			bg = new Quad(70, 9, 0xffffff);
			addChild(bg);
			
			var line:Quad = new Quad(70, 0.2, 0xe3e3e3);
			addChild(line);
			
			nameLabel = new Label();
			nameLabel.x = 14;
			nameLabel.y = 0.4;
			nameLabel.width = 50;
			nameLabel.nameList.add("headerListItemRed");
			nameLabel.text = pPiggy.getBehaviourByType(PiggyPropertiesBehaviour).name;
			addChild(nameLabel);
			
			bonusLabel = new Label();
			bonusLabel.x = 14;
			bonusLabel.y = 4;
			bonusLabel.width = 50;
			bonusLabel.nameList.add("headerListItemPurple");
			bonusLabel.text = '';
			addChild(bonusLabel);
			
			
			var piggyIcon:Image = new Image(pPiggyIcon);
			piggyIcon.x = 2;
			piggyIcon.y = 1;
			var scale:Number = 7 / piggyIcon.height;
			piggyIcon.height = 7;
			piggyIcon.width = piggyIcon.width * scale;
			addChild(piggyIcon);
		}
		
		public function onMouseOver():void{
			bg.color = 0xeeeeee;
		}
		public function onMouseOut():void{
			bg.color = 0xffffff;
		}
		
		public function updateTask(target:GameObject):void{
			
			var name = target.recipeName.toLowerCase();
			if(name.indexOf('lupin') > -1){
				bonusLabel.text = 'Harvesting lupine';
				task = 1;
				group = 1;
				
			}else if(name.indexOf('flower') > -1){
				bonusLabel.text = 'Harvesting flours';
				task = 2;
				group = 1;
				
			}else if(name.indexOf('vegetable') > -1){
				bonusLabel.text = 'Harvesting vegetables';
				task = 3;
				group = 1;
				
			}else if(name.indexOf('flour') > -1){
				bonusLabel.text = 'Harvesting flour';
				task = 4;
				group = 1;
				
			}else if(name.indexOf('fruit') > -1){
				bonusLabel.text = 'Harvesting fruit';
				task = 5;
				group = 1;
			}
				
			else if(name.indexOf('prepper') > -1){
				bonusLabel.text = 'Preparing';
				task = 6;
				group = 2;
				
			}else if(name.indexOf('cooker') > -1){
				bonusLabel.text = 'Cooking';
				task = 7;
				group = 2;
				
			}else if(name.indexOf('kitchen') > -1){
				bonusLabel.text = 'Baking';
				task = 8;
				group = 2;
				
			}else if(name.indexOf('shop') > -1){
				bonusLabel.text = 'Selling';
				task = 9;
				group = 2;
				
			}else if(name.indexOf('statue') > -1){
				bonusLabel.text = 'Guarding';
				task = 10;
				group = 2;
			}
			
			else if(name.indexOf('corn') > -1){
				bonusLabel.text = 'Eating';
				task = 11;
				group = 3;
				
			}else if(name.indexOf('house') > -1){
				bonusLabel.text = 'Sleeping';
				task = 12;
				group = 3;
				
			}else if(name.indexOf('mudpool') > -1){
				bonusLabel.text = 'Relaxing: Mudpool';
				task = 13;
				group = 3;
				
			}else if(name.indexOf('haystack') > -1){
				bonusLabel.text = 'Relaxing: Haystack';
				task = 14;
				group = 3;
			}
			
			var labels = ['headerListItemRed','headerListItemGreen','headerListItemPurple'];
			for(var i = 0; i < labels.length; i++){
				bonusLabel.nameList.remove(labels[i]);
			}
			bonusLabel.alpha = 0.4 + (group*.2);
		}
		public function getTask():int{
			return task;
		}
		public function getGroup():int{
			return group;
		}
	}
}