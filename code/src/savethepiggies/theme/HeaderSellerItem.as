package savethepiggies.theme
{
	import flash.desktop.Icon;
	
	import feathers.controls.Label;
	
	import savethepiggies.game.GameObject;
	import savethepiggies.game.behaviours.PiggyPropertiesBehaviour;
	
	import starling.display.Image;
	import starling.display.Quad;
	import starling.display.Sprite;
	import starling.textures.Texture;

	public final class HeaderSellerItem extends Sprite {
		
		public var piggy:GameObject;
		
		private var bg:Quad;
		private var nameLabel:Label;
		private var bonusLabel:Label;
		private var sellingIcon:Image;
		
		public function HeaderSellerItem(pPiggy:GameObject, pPiggyIcon:Texture, pSellingIcon:Texture = null){
			this.piggy = pPiggy;
			
			bg = new Quad(60, 14, 0xf4f3f3);
			addChild(bg);
			
			var line:Quad = new Quad(60, 0.2, 0xe3e3e3);
			addChild(line);
			
			var props:PiggyPropertiesBehaviour = pPiggy.getBehaviourByType(PiggyPropertiesBehaviour);
			
			nameLabel = new Label();
			nameLabel.x = 10;
			nameLabel.y = 2;
			nameLabel.width = 40;
			nameLabel.nameList.add("headerListItemRed");
			nameLabel.text = props.name;
			addChild(nameLabel);
			
			bonusLabel = new Label();
			bonusLabel.x = 10;
			bonusLabel.y = 8;
			bonusLabel.width = 40;
			bonusLabel.nameList.add("headerListItemPurple");
			bonusLabel.text = 'Sales bonus: '+((props.sales-5)*10)+'%';
			addChild(bonusLabel);
			
			
			if(pSellingIcon){
				sellingIcon = new Image(pSellingIcon);
				sellingIcon.x = 1;
				sellingIcon.y = 3;
				addChild(sellingIcon);
			}
			
			var piggyIcon:Image = new Image(pPiggyIcon);
			piggyIcon.x = 47;
			piggyIcon.y = 3;
			var scale:Number = 10 / piggyIcon.height;
			piggyIcon.height = 10;
			piggyIcon.width = piggyIcon.width * scale;
			addChild(piggyIcon);
		}
		
		public function updateSelling(pSellingIcon:Texture):void{
			sellingIcon.texture = pSellingIcon;
			sellingIcon.readjustSize();
		}
	}
}