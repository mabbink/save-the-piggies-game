package savethepiggies.map 
{
	import org.as3commons.logging.api.*;
	
	public final class MapLayer implements PropertyStore
	{
		private static const logger:ILogger = getLogger(MapLayer);
		
		private var _name:String;
		private var _data:Vector.<uint>;
		private var properties:SimplePropertyStore;
		
		public function MapLayer(
			name:String,
			data:Vector.<uint>
		) 
		{
			_name = name;
			_data = data;
			properties = new SimplePropertyStore;
			
			logger.info('Constructed map layer "{0}" with {1} tiles', [_name, _data.length]);
		}
		
		public function get name():String { return _name; }
		public function get data():Vector.<uint> { return _data; }
		
		public function getProperty(name:String):String { return properties.getProperty(name); }
		public function hasProperty(name:String):Boolean { return properties.hasProperty(name); }
		public function setProperty(name:String, value:String):void { properties.setProperty(name, value); }
	}

}