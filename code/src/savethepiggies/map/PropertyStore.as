package savethepiggies.map 
{
	public interface PropertyStore 
	{
		function getProperty(name:String):String;
		function setProperty(name:String, value:String):void;
		function hasProperty(name:String):Boolean;
	}
}