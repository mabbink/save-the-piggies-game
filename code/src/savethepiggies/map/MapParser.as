package savethepiggies.map
{
	import com.hurlant.util.*;
	import flash.utils.*;
	import org.as3commons.logging.api.*;
	
	public final class MapParser
	{
		private static const logger:ILogger = getLogger(MapParser);
		
		public static function ParseMap(mapData:XML):Map
		{
			// Output
			var width:int, height:int;
			var tileWidth:int, tileHeight:int;
			var layers:Vector.<MapLayer> = new <MapLayer>[];
			
			// Read
			var xml:XML = mapData;
			
			// Read width and height
			width = xml.@width;
			height = xml.@height;
			tileWidth = xml.@tilewidth;
			tileHeight = xml.@tileheight;
			logger.info("Map dimensions: {0}x{1}, {2}x{3} tiles", [width, height, tileWidth, tileHeight]);
			
			// Create map
			var map:Map = new Map(width, height, tileWidth, tileHeight);
			
			// Prepare a set of tile IDs
			var tiles:Array = [];
			
			// Read layers
			var layerNodes:* = xml.layer;
			for each (var layerNode:*in layerNodes)
			{
				var b:ByteArray = Base64.decodeToByteArray(layerNode.data);
				b.uncompress();
				b.endian = Endian.LITTLE_ENDIAN;
				b.position = 0;
				var layerData:Vector.<uint> = new Vector.<uint>(b.length / 4);
				var dataIndex:int = 0;
				while (b.position != b.length)
				{
					var gid:uint = b.readUnsignedInt();
					layerData[dataIndex++] = gid;
					
					// Store tileId in set
					if (tiles.indexOf(gid) == -1)
						tiles.push(gid);
				}
				
				var newLayer:MapLayer = new MapLayer(layerNode.@name, layerData);
				addProperties(newLayer, layerNode.properties[0]);
				layers.push(newLayer);
				map.addLayer(newLayer);
				
				// Register tiles
				for each (var tileId:uint in tiles)
				{
					if (tileId === 0) continue;
					
					var setName:String = getTilesetName(tileId);
					var tileset:* = getTilesetByName(setName);
					
					var setIndex:uint = tileId - tileset.@firstgid;
					
					logger.info("Tile {0} is in set {1} at index {2}", [tileId, setName, setIndex]);
					
					// Offsets
					var offsetX:int = 0;
					var offsetY:int = 0;
					
					if (tileset.tileoffset != undefined)
					{
						offsetX = tileset.tileoffset.@x;
						offsetY = tileset.tileoffset.@y;
					}
					
					// Properties
					var tileProperties:PropertyStore = new SimplePropertyStore;
					for each (var props:* in tileset.tile.(@id == setIndex).properties.property)
					{
						logger.info("Tile {0} has property {1}:{2}", [tileId, props.@name, props.@value]);
						tileProperties.setProperty(props.@name, props.@value);
						
					}
					
					map.registerTile(tileId, setName, setIndex, offsetX, offsetY, tileProperties);
				}
				
				function getTilesetName(gid:uint):String
				{
					var name:String = null;
					for (var i:int = 0; i < xml.tileset.length(); ++i)
					{
						if (uint(xml.tileset[i].@firstgid) <= gid)
							name = xml.tileset[i].@name;
					}
					
					if (name == null)
						throw new Error("Tile not found in tileset");
					
					return name;
				}
				
				function getTilesetByName(name:String):*
				{
					for each (var tileset:* in xml.tileset)
						if (tileset.@name == name) return tileset;
					return null;
				}
			}
			
			return map;
		}
		
		private static function addProperties(target:PropertyStore, propertyNode:*):void
		{
			if (propertyNode != undefined)
				for each (var property:* in propertyNode.property)
					target.setProperty(property.@name, property.@value);
		}
	}

}