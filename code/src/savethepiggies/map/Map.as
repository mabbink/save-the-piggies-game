package savethepiggies.map
{
	import flash.geom.Point;
	
	import org.as3commons.logging.api.ILogger;
	import org.as3commons.logging.api.getLogger;
	
	public final class Map
	{
		private static const logger:ILogger = getLogger(Map);
		
		private var layers:Array;
		private var tiles:Array;
		
		private var _width:int, _height:int;
		private var _tileWidth:int, _tileHeight:int;
		private var thw:Number, thh:Number;
		
		private var transform:Array;
		private var transformReverse:Array;
		
		public function Map(width:int, height:int, tileWidth:int, tileHeight:int)
		{
			_width = width;
			_height = height;
			_tileWidth = tileWidth;
			_tileHeight = tileHeight;
			
			thw = _tileWidth / 2.0;
			thh = _tileHeight / 2.0;
			
			layers = [];
			tiles = [];
			
			transform = [
				thw, -thw, 0,
				thh, thh, 0,
				0, 0, 1
			];
			transformReverse = [
				thh, thw, 0,
				-thh, thw, 0,
				0, 0, 2 * thw * thh
			];
			
			for (var i:int = 0; i < transformReverse.length; ++i)
				transformReverse[i] *= 1 / (2 * thw * thh);
		}
		
		public function registerTile(
			globalId:uint,
			tilesetName:String,
			tilesetIndex:uint,
			offsetX:int,
			offsetY:int,
			properties:PropertyStore
		):void
		{
			if (tiles[globalId]) logger.warn("Warning, tile with ID {0} already exists. Replacing.", [globalId]);
			
			tiles[globalId] = {
				tilesetName:tilesetName,
				tilesetIndex:tilesetIndex,
				offsetX:offsetX,
				offsetY:offsetY,
				properties:properties
			};
		}
		
		public function addLayer(layer:MapLayer):void
		{
			layers.push(layer);
		}
		
		public function getLayerByName(name:String):MapLayer
		{
			var layer:MapLayer = null;
			
			for each (var candidate:MapLayer in layers)
			{
				if (candidate.name == name)
				{
					layer = candidate;
					break;
				}
			}
			
			return layer;
		}
		
		public function getTileProperty(tileGlobalId:uint, propertyName:String):String
		{
			var propertyValue:String = null;
			if (!tiles[tileGlobalId]) logger.warn("getTileProperty: Tile with ID {0} doesn't exist!", [tileGlobalId]);
			else
				propertyValue = PropertyStore(tiles[tileGlobalId].properties).getProperty(propertyName);
			
			return propertyValue;
		}
		
		public function getTilesetName(tileGlobalId:uint):String
		{
			var tilesetName:String = null;
			
			if (!tiles[tileGlobalId]) logger.warn("getTilesetName: Tile with ID {0} doesn't exist!", [tileGlobalId]);
			else
				tilesetName = tiles[tileGlobalId].tilesetName;
			
			return tilesetName;
		}
		
		public function getTileOffsetX(tileGlobalId:uint):int
		{
			var offsetX:int = 0;
			
			if (!tiles[tileGlobalId]) logger.warn("getTileOffsetX: Tile with ID {0} doesn't exist!", [tileGlobalId]);
			else
				offsetX = tiles[tileGlobalId].offsetX;
			
			return offsetX;
		}
		
		public function getTileOffsetY(tileGlobalId:uint):int
		{
			var offsetY:int = 0;
			
			if (!tiles[tileGlobalId]) logger.warn("getTileOffsetY: Tile with ID {0} doesn't exist!", [tileGlobalId]);
			else
				offsetY = tiles[tileGlobalId].offsetY;
			
			return offsetY;
		}
		
		public function getIndexInTileset(tileGlobalId:uint):uint
		{
			var tilesetIndex:uint = uint.MAX_VALUE;
			
			if (!tiles[tileGlobalId]) logger.warn("getIndexInTileset: Tile with ID {0} doesn't exist!", [tileGlobalId]);
			else
				tilesetIndex = tiles[tileGlobalId].tilesetIndex;
			
			return tilesetIndex;
		}
		
		public function isometricToCartesian(i:Number, j:Number, resultPoint:Point = null):Point
		{
			if (!resultPoint) resultPoint = new Point;

			resultPoint.x = i * transform[0] + j * transform[1] + transform[2];
			resultPoint.y = i * transform[3] + j * transform[4] + transform[5];
			
			return resultPoint;
		}
		
		public function cartesianToIsometric(x:Number, y:Number, resultPoint:Point = null):Point
		{
			if (!resultPoint) resultPoint = new Point;
			
			resultPoint.x = x * transformReverse[0] + y * transformReverse[1] + transformReverse[2];
			resultPoint.y = x * transformReverse[3] + y * transformReverse[4] + transformReverse[5];
			
			return resultPoint;
		}

		public function get width():int { return _width; }
		public function get height():int { return _height; }
		public function get tileWidth():int { return _tileWidth; }
		public function get tileHeight():int { return _tileHeight; }
	}

}