package savethepiggies.map 
{
	/**
	 * ...
	 * @author 
	 */
	internal class SimplePropertyStore implements PropertyStore 
	{
		private var o:Object;
		
		public function SimplePropertyStore() 
		{
			o = { };
		}
		
		/* INTERFACE savethepiggies.map.PropertyStore */
		
		public function getProperty(name:String):String 
		{
			return o[name];
		}
		
		public function setProperty(name:String, value:String):void 
		{
			o[name] = value;
		}
		
		public function hasProperty(name:String):Boolean
		{
			return o.hasOwnProperty(name);
		}
	}

}