package savethepiggies.sound {
	
	import flash.events.Event;
	import flash.media.Sound;
	import flash.media.SoundChannel;
	import flash.media.SoundMixer;
	import flash.media.SoundTransform;
	import flash.utils.Dictionary;
	import flash.utils.getDefinitionByName;
	
	import starling.animation.Tween;
	import starling.core.Starling;
	
	public final class SoundManager {
		
		private static var _sounds:Dictionary = new Dictionary();
		
		private static var _soundOptions:Array = [
			SO_Background,
			SO_Birds,
			SO_Bought_piggy1,
			SO_Bought_piggy2,
			SO_Button_no,
			SO_Button_yes,
			SO_Buy,
			SO_Button,
			SO_Close_popup,
			SO_Collect_money,
			SO_Evil_background,
			SO_Evil_catch,
			SO_Evil_run,
			SO_Game_intro,
			SO_Haystack,
			SO_Market,
			SO_Mudpool,
			SO_Open_popup,
			SO_Piggy_assign_task1,
			SO_Piggy_assign_task2,
			SO_Piggy_guard,
			SO_Piggy_hungry,
			SO_Piggy_select,
			SO_Piggy_tired,
			SO_Sell,
			SO_Sleeping,
			SO_Upgrade,
			SO_Work_cooker,
			SO_Work_kitchen,
			SO_Work_storing
		];
		
		private static var _enableBgMusic:Boolean = true;
		private static var _enableEffects:Boolean = true;
		
		public static function enableMusic(pBackground:Boolean, pEffects:Boolean):void{
			_enableBgMusic = pBackground;
			_enableEffects = pEffects;
			
			if(_enableBgMusic){
				startBackgroundMusic();
			}else{
				stopBackgroundMusic();
			}
		}
		
		public static function playSound(pData:Object):void {
			if(pData.id == 'SO_Game_intro' || pData.id == 'SO_Background' || pData.id == 'SO_Birds' || pData.id == 'SO_Market'){
				if(!_enableBgMusic) return;
			}else{
				if(!_enableEffects) return;
			}
			
			var sound:Sound;
			var channel:SoundChannel;
			
			if(_sounds[pData.id]) {
				sound = _sounds[pData.id].sound;
				
				//if the sound is not yet done playing, we want to create a new instance
				if(_sounds[pData.id].playing) {
					if(pData.preventMultipleInstances) return;
					sound = null;
				}
			}
			
			if(!sound){
				var myClass:Class = getDefinitionByName(pData.id) as Class;
				sound =  new myClass();
				_sounds[pData.id] = {sound:sound};
			}
			
			channel = sound.play(0, 0, new SoundTransform(pData.volume ? pData.volume : 1, pData.position ? pData.position : 0));
			if(!channel) return;
			channel.addEventListener(Event.SOUND_COMPLETE, onSoundComplete);
			
			if(pData.loop) {
				
				var tween:Tween = new Tween(channel, getSoundLength(sound));
				tween.onComplete = playSound;
				tween.onCompleteArgs = [pData];
				Starling.juggler.add(tween);
				
				_sounds[pData.id].tween = tween;
			}else{
				_sounds[pData.id].tween = null;
			}
			
			
			_sounds[pData.id].channel = channel;
			_sounds[pData.id].playing = true;
		}
		private static function getSoundLength(music:Sound):int{
			var Milliseconds:Number = (music.bytesTotal / (music.bytesLoaded / music.length));
			
			var Minutes:uint = Math.floor(Milliseconds/60000);
			var Seconds:uint = (Milliseconds%60000);
			var SecondsTens:uint = Math.floor(Seconds/10000);
			
			Seconds = Math.ceil(Seconds%10000)
			Seconds /= 1000;
			
			return (Minutes*60)+Seconds+1;
		}
		private static function onSoundComplete(pEvent:Event):void{
			var channel:SoundChannel = pEvent.target as SoundChannel;
			for(var key:String in _sounds){
				if(_sounds[key].channel == channel) _sounds[key].playing = false;
			}
		}
		
		public static function stop(pID:String):void{
			if(_sounds[pID]) {
				_sounds[pID].channel.stop();
				_sounds[pID].playing = false;
				if(_sounds[pID].tween) Starling.juggler.remove(_sounds[pID].tween);
			}
		}
		
		
		
		public static function startBackgroundMusic():void{
			SoundManager.playSound({id:'SO_Birds', loop:true, volume:.5, preventMultipleInstances:true});
			SoundManager.playSound({id:'SO_Background', loop:true, volume:.5, preventMultipleInstances:true});
		}
		public static function stopBackgroundMusic():void{
			SoundManager.stop('SO_Birds');	
			SoundManager.stop('SO_Background');
		}
		
		
		
		public static function setSoundVolume(pVolume:Number):void{
			SoundMixer.soundTransform = new SoundTransform(pVolume);
		}
		
		
		
		public static function fadeInSound(pID:String, pStartVolume:Number, pEndVolume:Number = 1, pLoop:Boolean = false, pTime:uint = 600):void{
			/*
			TweenMax.killTweensOf(_channels[pID]);
				playSound(pID, pLoop, pStartVolume);
				TweenMax.to(_channels[pID], pTime, {volume:pEndVolume, ease:Strong.easeOut});
			}
			*/
		}
		public static function fadeOutSound(pID:String, pEndVolume:Number = 0, pTime:uint = 1000):void{
			/*
			if(!_channels[pID]) return;
			TweenMax.killTweensOf(_channels[pID]);
			TweenMax.to(_channels[pID], pTime, {volume:pEndVolume, ease:Strong.easeOut, onComplete:stop, onCompleteParams:[pID]});
			*/
		}
	}
}