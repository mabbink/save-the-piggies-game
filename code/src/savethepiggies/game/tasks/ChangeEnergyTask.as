package savethepiggies.game.tasks 
{
	import godmode.core.BehaviorTask;
	import godmode.core.StatefulBehaviorTask;
	import savethepiggies.game.behaviours.PiggyCounterBehaviour;
	import savethepiggies.game.GameObject;
	
	public final class ChangeEnergyTask extends StatefulBehaviorTask 
	{
		private var amount:Number;
		private var energy:PiggyCounterBehaviour;
		
		public function ChangeEnergyTask(piggy:GameObject, restAmount:Number) 
		{
			this.amount = restAmount;
			energy = piggy.getBehaviourByType(PiggyCounterBehaviour);
		}
		
		protected override function updateTask(dt:Number):int
		{
			energy.changeEnergy(dt * amount);
			
			return BehaviorTask.RUNNING;
		}
	}

}