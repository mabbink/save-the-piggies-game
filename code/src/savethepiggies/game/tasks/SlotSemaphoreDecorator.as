package savethepiggies.game.tasks 
{
import godmode.core.BehaviorTask;
import godmode.core.BehaviorTaskContainer;
import godmode.core.StatefulBehaviorTask;
import godmode.data.Entry;

/** Runs its task only when it has acquired the given semaphore */
public final class SlotSemaphoreDecorator extends StatefulBehaviorTask
    implements BehaviorTaskContainer
{
    public function SlotSemaphoreDecorator (semaphore :Entry, task :BehaviorTask) {
        _task = task;
        _semaphore = semaphore;
    }

    public function get children () :Vector.<BehaviorTask> {
        return new <BehaviorTask>[ _task ];
    }

    override public function get description () :String {
        return super.description + ":" + _semaphore.value.name;
    }

    override protected function reset () :void {
        if (_semaphoreAcquired) {
            _semaphore.value.release();
            _semaphoreAcquired = false;
        }
        _task.deactivate();
    }

    override protected function updateTask (dt :Number) :int {
        if (!_semaphoreAcquired && _semaphore.exists) {
            _semaphoreAcquired = _semaphore.value.acquire();
            if (!_semaphoreAcquired) {
                return FAIL;
            }
        }
        return _task.update(dt);
    }

    protected var _task :BehaviorTask;
    protected var _semaphore :Entry;

    protected var _semaphoreAcquired :Boolean;
}
}