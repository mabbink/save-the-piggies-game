package savethepiggies.game.tasks 
{
	import godmode.core.StatefulBehaviorTask;
	import godmode.core.BehaviorTask;
	
	public final class RunForTask extends StatefulBehaviorTask 
	{
		private var duration:Number;
		private var t:Number;
		private var task:BehaviorTask;
		
		public function RunForTask(
			duration:Number,
			task:BehaviorTask
		)
		{
			this.duration = duration;
			this.task = task;
			t = 0;
		}
		
		protected override function reset():void
		{
			t = 0;
			task.deactivate();
		}
		
		protected override function updateTask(dt:Number):int
		{
			t += dt;
			
			var taskRet:int = task.update(dt);
			if (BehaviorTask.RUNNING != taskRet)
				return taskRet;
			
			return t < duration ? BehaviorTask.RUNNING : BehaviorTask.SUCCESS;
		}
	}

}