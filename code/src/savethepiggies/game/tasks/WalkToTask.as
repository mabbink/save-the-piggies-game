package savethepiggies.game.tasks 
{
	import flash.geom.Point;
	
	import godmode.core.BehaviorTask;
	import godmode.core.StatefulBehaviorTask;
	import godmode.data.Entry;
	
	import org.as3commons.logging.api.ILogger;
	import org.as3commons.logging.api.getLogger;
	
	import savethepiggies.game.GameObject;
	import savethepiggies.game.IsoSprite;
	import savethepiggies.game.behaviours.AnimationSwitcherBehaviour;
	import savethepiggies.game.behaviours.PiggyInventoryBehaviour;
	import savethepiggies.game.behaviours.PiggyPropertiesBehaviour;
	
	import starling.display.MovieClip;
	
	public final class WalkToTask extends StatefulBehaviorTask 
	{
		private static const logger:ILogger = getLogger(WalkToTask);
		
		private var path:Entry;
		private var sprite:IsoSprite;
		private var animSwitcher:AnimationSwitcherBehaviour;
		private var inventory:PiggyInventoryBehaviour;
		private var speed:Number;
		
		private var anim:MovieClip;
		private var flattenedPath:Vector.<PathSegment>;
		
		
		
		
		public function WalkToTask(
			piggy:GameObject,
			path:Entry,
			forceThisInventory:* = -1,		//when the value is -1, this class will get the value from PiggyInventoryBehaviour
			forceThisSpeed:Number = -1.0	//when the value is -1, this class will get the value from PiggyPropertiesBehaviour
		)
		{
			this.path = path;
			this.sprite = piggy.getIsoSprite();
			this.animSwitcher = piggy.getBehaviourByType(AnimationSwitcherBehaviour);
			
			if(forceThisInventory == -1)
				this.inventory = piggy.getBehaviourByType(PiggyInventoryBehaviour);
			else this.inventory = forceThisInventory;
			
			if(forceThisSpeed < 0.0)
				this.speed = 0.2 + (piggy.getBehaviourByType(PiggyPropertiesBehaviour).speed*0.15);
			else this.speed = forceThisSpeed;
		}
		
		protected override function reset():void
		{
			flattenedPath = null;
			anim = null;
		}
		
		private static function flattenPath(path:Array, pSpeed:Number):Vector.<PathSegment>
		{
			var fp:Vector.<PathSegment> = new Vector.<PathSegment>;
			
			for (var i:int = 0; i < path.length - 1; ++i)
			{
				fp.push(new PathSegment(
					path[i],
					path[i + 1],
					pSpeed
				));
			}
			
			return fp;
		}
		
		private var out:Point = new Point;
		protected override function updateTask(dt:Number):int
		{
			if (null == flattenedPath) flattenedPath = flattenPath(path.value, speed);
			
			if (anim) anim.advanceTime(dt);
			
			while (dt > 0 && flattenedPath.length > 0)
			{
				dt = flattenedPath[0].update(dt);
				flattenedPath[0].pos(out);
				sprite.moveTo(
					out.x, out.y
				);
				
				if (!anim)
				{
					flattenedPath[0].dir(out);
					anim = animSwitcher.switchToAnim(
						animSwitcher.getDirectionalAnimName("normal", out, (inventory == null) ? false : inventory.holding != null)
					);
				}
				
				if (flattenedPath[0].isDone)
				{
					flattenedPath.shift();
					anim = null;
				}
			}
			
			return flattenedPath.length == 0 ? BehaviorTask.SUCCESS : BehaviorTask.RUNNING;
		}
	}

}
import flash.geom.Point;


class PathSegment
{
	private var from:Point, to:Point;
	private var totalTime:Number;
	private var t:Number;
	
	private var done:Boolean;
	
	public function PathSegment(
		from:Point,
		to:Point,
		speed:Number
	)
	{
		this.from = from;
		this.to = to;
		totalTime = from.subtract(to).length / speed;
		t = 0;
	}
	
	public function update(dt:Number):Number
	{
		t += dt;
		var remainder:Number = 0;
		if (t >= totalTime)
		{
			done = true;
			remainder = t - totalTime;
			t = totalTime;
		}
		
		return remainder;
	}
	
	public function get isDone():Boolean { return done; }
	public function pos(out:Point):Point 
	{
		if (!out) out = new Point;
		var a:Number = t / totalTime;
		var b:Number = 1 - a;
		out.x = from.x * b + to.x * a;
		out.y = from.y * b + to.y * a;
		return out;
	}
	
	public function dir(out:Point):Point 
	{
		if (!out) out = new Point;
		out.x = to.x - from.x;
		out.y = to.y - from.y;
		out.normalize(1);
		return out;
	}
}