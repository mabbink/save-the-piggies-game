package savethepiggies.game.tasks 
{
	import godmode.core.BehaviorTask;
	import godmode.core.StatefulBehaviorTask;
	import godmode.data.MutableEntry;
	import savethepiggies.game.behaviours.AnimationSwitcherBehaviour;
	import savethepiggies.game.GameObject;
	import savethepiggies.game.IsoSprite;
	import starling.display.MovieClip;
	
	public final class AnimationTask extends StatefulBehaviorTask 
	{
		private var sprite:IsoSprite;
		private var switcher:AnimationSwitcherBehaviour;
		private var anim:MovieClip;
		private var animName:MutableEntry;
		
		public function AnimationTask(
			gameObject:GameObject,
			animName:MutableEntry
		) 
		{
			this.sprite = gameObject.getIsoSprite();
			this.animName = animName;
			this.switcher = gameObject.getBehaviourByType(AnimationSwitcherBehaviour);
		}
		
		protected override function reset():void
		{
			anim = null;
		}
		
		override protected function updateTask(dt:Number):int 
		{
			if (!anim)
				anim = switcher.switchToAnim(animName.value);
			
			if (anim) anim.advanceTime(dt);
			
			return BehaviorTask.RUNNING;
		}
	}

}