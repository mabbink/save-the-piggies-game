package savethepiggies.game.tasks 
{
	import godmode.core.BehaviorTask;
	import godmode.core.StatefulBehaviorTask;
	import savethepiggies.game.behaviours.PiggyCounterBehaviour;
	import savethepiggies.game.GameObject;
	
	public final class ChangeFoodTask extends StatefulBehaviorTask 
	{
		private var amount:Number;
		private var food:PiggyCounterBehaviour;
		
		public function ChangeFoodTask(piggy:GameObject, foodAmount:Number) 
		{
			this.amount = foodAmount;
			food = piggy.getBehaviourByType(PiggyCounterBehaviour);
		}
		
		protected override function updateTask(dt:Number):int
		{
			food.changeFullness(dt * amount);
			
			return BehaviorTask.RUNNING;
		}
	}

}