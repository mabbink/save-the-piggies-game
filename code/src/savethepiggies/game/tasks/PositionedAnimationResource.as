package savethepiggies.game.tasks 
{
	import godmode.core.ScopedResource;
	import godmode.data.MutableEntry;
	import savethepiggies.game.GameObject;
	import savethepiggies.game.IsoSprite;
	
	public final class PositionedAnimationResource implements ScopedResource 
	{
		private var sprite:IsoSprite;
		private var posI:MutableEntry, posJ:MutableEntry;
		private var startPosI:Number, startPosJ:Number;
		
		public function PositionedAnimationResource(
			piggy:GameObject,
			positionI:MutableEntry,
			positionJ:MutableEntry
		) 
		{
			sprite = piggy.getIsoSprite();
			this.posI = positionI;
			this.posJ = positionJ;
		}
		
		public function acquire():void 
		{
			startPosI = sprite.offsetI;
			startPosJ = sprite.offsetJ;
			sprite.moveTo(posI.value, posJ.value);
		}
		
		public function release():void 
		{
			sprite.moveTo(startPosI, startPosJ);
		}
		
	}

}