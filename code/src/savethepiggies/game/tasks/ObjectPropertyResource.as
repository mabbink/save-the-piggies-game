package savethepiggies.game.tasks 
{
	import godmode.core.ScopedResource;
	import godmode.data.Blackboard;
	import godmode.data.Entry;
	
	public final class ObjectPropertyResource implements ScopedResource
	{
		private var object:Object;
		private var propertyName:String;
		private var value:*;
		
		private var oldValue:Entry;
		
		public function ObjectPropertyResource(
			object:Object,
			propertyName:String,
			value:Entry
		) 
		{
			this.object = object;
			this.propertyName = propertyName;
			this.value = value;
		}
		
		public function acquire():void 
		{
			if (object.hasOwnProperty(propertyName))
				oldValue = Blackboard.staticEntry(object[propertyName]);
			else
				oldValue = null;
				
			object[propertyName] = value.value;
		}
		
		public function release():void 
		{
			if (null == oldValue)
				delete object[propertyName];
			else
				object[propertyName] = oldValue.value;
		}
	}

}