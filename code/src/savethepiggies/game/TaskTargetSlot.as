package savethepiggies.game 
{
	
	public final class TaskTargetSlot 
	{
		private var _i:int;
		private var _j:int;
		private var _properties:Object;
		
		public function TaskTargetSlot(i:int, j:int) 
		{
			_i = i;
			_j = j;
			_properties = { };
		}
		
		public function get i():int { return _i; }
		public function get j():int { return _j; }
		public function get properties():Object { return _properties; }
	}

}