package savethepiggies.game 
{
	public interface SelectionListener 
	{
		function selected(id:uint):void;
	}
}