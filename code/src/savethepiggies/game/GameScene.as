package savethepiggies.game
{
	import com.greensock.TweenNano;
	
	import flash.display.StageDisplayState;
	import flash.events.KeyboardEvent;
	import flash.events.MouseEvent;
	import flash.events.TimerEvent;
	import flash.external.ExternalInterface;
	import flash.geom.Point;
	import flash.ui.Keyboard;
	import flash.utils.ByteArray;
	import flash.utils.Timer;
	import flash.utils.getTimer;
	
	import feathers.controls.Alert;
	import feathers.controls.Button;
	import feathers.controls.ToggleSwitch;
	import feathers.core.PopUpManager;
	import feathers.data.ListCollection;
	
	import org.as3commons.logging.api.ILogger;
	import org.as3commons.logging.api.getLogger;
	import org.gestouch.events.GestureEvent;
	import org.gestouch.gestures.Gesture;
	import org.gestouch.gestures.PanGesture;
	import org.gestouch.gestures.TapGesture;
	
	import savethepiggies.GlobalContext;
	import savethepiggies.ReservedIdGenerator;
	import savethepiggies.debug.DebugDraw;
	import savethepiggies.debug.DebugDrawActive;
	import savethepiggies.debug.DebugDrawNull;
	import savethepiggies.game.behaviours.DrawableBehaviour;
	import savethepiggies.game.behaviours.PersistBehaviour;
	import savethepiggies.game.behaviours.PiggyPropertiesBehaviour;
	import savethepiggies.game.behaviours.PiggyTasksBehaviour;
	import savethepiggies.game.behaviours.StorageBehaviour;
	import savethepiggies.game.behaviours.TappableBehaviour;
	import savethepiggies.game.behaviours.UpdatableBehaviour;
	import savethepiggies.game.events.MarketEvent;
	import savethepiggies.game.events.PiggiesEvent;
	import savethepiggies.game.events.PlacingEvent;
	import savethepiggies.game.events.TaskTargetSelectedEvent;
	import savethepiggies.map.MapLayer;
	import savethepiggies.map.MapParser;
	import savethepiggies.serialization.SerializeGame;
	import savethepiggies.sound.SoundManager;
	
	import starling.animation.Transitions;
	import starling.animation.Tween;
	import starling.core.Starling;
	import starling.display.DisplayObjectContainer;
	import starling.display.Image;
	import starling.display.Sprite;
	import starling.events.Event;
	
	import supportlib.scene.Scene;
	import supportlib.scene.SceneManager;
	import supportlib.timestep.FixedTimestepper;
	import supportlib.timestep.FixedTimestepperStandard;
	
	public final class GameScene implements Scene
	{
		private static const logger:ILogger = getLogger(GameScene);
		
		private static const LOGIC_STEP:int = 1000 / 30;
		private static const FRAME_SKIP:int = 10;
		
		private var globalContext:GlobalContext;
		private var gameContext:GameContext;
		
		private var graphicsRoot:DisplayObjectContainer;
		
		private var saveGameData:ByteArray;
		private var saveGameDetails:ByteArray;
		
		private var mapDisplay:MapDisplay;
		
		private var cooker:Cooker;
		
		private var uiLayer:Sprite;
		private var marketButton:Button;
		private var marketUi:MarketUi;
		private var placeUi:PlaceUi;
		private var hud:Hud;
		private var tutorialManager:TutorialManager;
		
		private var gestures:Array;
		
		private var assetScaleFactor:Number;
		
		private var stepper:FixedTimestepper;
		
		private var mapCenter:Point
		private var mapZoom:Number;
		private var keysDown:Array;
		
		private var debugDraw:DebugDraw;
		
		private var catchup:Number = -1;
		private var cachupPopup:Alert;
		
		
		public function GameScene(
			globalContext:GlobalContext,
			saveGame:Array = null
		)
		{
			this.globalContext = globalContext;
			gameContext = new GameContext;
			gameContext.save = save;
			
			if(saveGame){
				this.saveGameDetails = saveGame[0];
				this.saveGameData = saveGame[1];
			}
			
			logger.debug('Starting gamescene, with loadData:'+(saveGameData != null));
			
			if(CONFIG::debug){
				gameContext.cash = 100000;
			}else{
				gameContext.cash = ApplicationVars.START_MONEY;
			}
			gameContext.tutorialStep = 0;
			
			gameContext.map = MapParser.ParseMap(globalContext.assetManager.getXml("default"));
			gameContext.grid = new GameGrid(gameContext.map.width * 2, gameContext.map.height * 2);
			gameContext.taskTargetManager = new TaskTargetManager(gameContext.map, tapToCartesianTransform);
			gameContext.selectionGroupPiggies = new SelectionGroup;
			gameContext.selectionGroupTaskTargets = new SelectionGroup;
			gameContext.gameObjects = new ObjectRegistry();
			gameContext.updatableGameObjects = new ObjectRegistry();
			gameContext.drawableGameObjects = new ObjectRegistry();
			gameContext.piggies = new ObjectRegistry();
			gameContext.idGenerator = new ReservedIdGenerator();
			
			assetScaleFactor = globalContext.assetManager.scaleFactor;
			
			gestures = [];
			stepper = new FixedTimestepperStandard(LOGIC_STEP, FRAME_SKIP);
			
			mapZoom = 0.3;
			mapCenter = new Point(
				-gameContext.map.isometricToCartesian(28, 34).x, 
				-gameContext.map.isometricToCartesian(28, 34).y
			);
			logger.debug('Gamescene made');
		}
		
		public function cleanup(manager:SceneManager):void
		{
			globalContext.director.starling.nativeStage.removeEventListener(MouseEvent.MOUSE_WHEEL, onMouseWheel);
			
			gameContext.gameObjects.visit(function(go:GameObject):void {
				go.cleanup();
			});
			gameContext.taskTargetManager.cleanup();
			marketUi.cleanup();
			placeUi.cleanup();
			
			marketUi.removeEventListener(MarketEvent.CHECKOUT, onCheckout);
			marketButton.removeEventListener(Event.TRIGGERED, showMarket);
			gameContext.taskTargetManager.addEventListener(
				TaskTargetSelectedEvent.TASK_TARGET_SELECTED,
				onTaskTargetSelected
			);
			
			hud.cleanup();
			
			for each (var g:Gesture in gestures) g.dispose();
			
			DisplayObjectContainer(manager.getDisplayContainer()).removeChild(graphicsRoot);
		}
		
		public function init(manager:SceneManager):void
		{
			logger.debug('Initializing gamescene');
				
			graphicsRoot = new Sprite;
			DisplayObjectContainer(manager.getDisplayContainer()).addChild(graphicsRoot);
			
			if(ExternalInterface.available){
				ExternalInterface.addCallback("handleWheel", onMouseWheel);
				ExternalInterface.addCallback("handleKeyDown", onKeyDown);
				ExternalInterface.addCallback("handleKeyUp", onKeyUp);
			}
			globalContext.director.starling.nativeStage.addEventListener(MouseEvent.MOUSE_WHEEL, onMouseWheel);
			
			Starling.current.nativeStage.addEventListener(KeyboardEvent.KEY_DOWN, onKeyDown);
			Starling.current.nativeStage.addEventListener(KeyboardEvent.KEY_UP, onKeyUp);
			
			keysDown = [false, false, false, false];
			
			gameContext.mapLayer = new Sprite();
			graphicsRoot.addChild(gameContext.mapLayer);
			
			mapDisplay = new MapDisplay(globalContext.assetManager.getTextureAtlas("map-desktop")); 
			mapDisplay.drawMap(gameContext.map);
			gameContext.mapLayer.addChild(mapDisplay);
			gameContext.isoSpriteDisplay = new IsoSpriteDisplay();

			gameContext.isoSpriteDisplay.addLayer("below");
			gameContext.isoSpriteDisplay.addLayer("default");
			gameContext.isoSpriteDisplay.addLayer("cover");
			
			gameContext.mapLayer.addChild(gameContext.taskTargetManager);
			gameContext.mapLayer.addChild(gameContext.isoSpriteDisplay);

			gameContext.upgradeCallback = upgrade;
			gameContext.removeCallback = remove;
			gameContext.moveCallback = move;
			gameContext.deleteGameObject = deleteGameObject;
			gameContext.buyCallback = buy;
			gameContext.incommingEvilGuy = incommingEvilGuy;
			gameContext.piggyCaptured = piggyCaptured;
			gameContext.evilGuyScaredAway = evilGuyScaredAway;
			
			logger.debug('Initializing touch');
			initTouch(graphicsRoot);
			
			logger.debug('Initializing UI');
			initUi(graphicsRoot);
		
			logger.debug('Initializing Storage');
			gameContext.storage = new Storage(gameContext);
		
			logger.debug('Initializing Cooker');
			cooker = new Cooker(
				globalContext,
				gameContext
			);
			
			logger.debug('Initializing game objects. Save data:'+(saveGameData != null));
			
			if (null == saveGameData || !restoreGameStateFromSave())
			{
				addInitialObjects();
				
				cooker.cook("piggy-pretty-recipe", 29.25, 36.25);
				cooker.cook("piggy-smart-recipe", 29.75, 36.25);
				cooker.cook("piggy-ralf-recipe", 29.25, 36.75);
				cooker.cook("piggy-mohawk-recipe", 29.75, 36.75);
				
				gameContext.storage.giveStartingIngredients();
			}
			zoomMapIn({
					zoom: 0.3,
					x: -gameContext.map.isometricToCartesian(20, 0).x,
					y: -gameContext.map.isometricToCartesian(20, 0).y
				}
			);
			SoundManager.playSound({id:'SO_Game_intro'});
			TweenNano.delayedCall(3, SoundManager.startBackgroundMusic);
			
			
			gameContext.taskTargetManager.showAll();
			
			gameContext.taskTargetManager.addEventListener(
				TaskTargetSelectedEvent.TASK_TARGET_SELECTED,
				onTaskTargetSelected
			);
			
			tutorialManager = new TutorialManager(this,gameContext, mapDisplay, uiLayer, globalContext, cooker, save, destroyGameObject);
			if(gameContext.tutorialStep < tutorialManager.POPUPS.length){
				tutorialManager.showTutorialStep(gameContext.tutorialStep ? gameContext.tutorialStep : 1);
			}
			
			// Add the evil guy if we dont have one
			
			var totalBadGuys:int = gameContext.gameObjects.getByTag('evilGuy').length;
			var totalEvilFactories:int = gameContext.gameObjects.getByTag('factory').length;
			
			if(totalBadGuys == 0 && totalEvilFactories > 0)
				cooker.cook('evilGuy-recipe', 0.75, 36.75);
			
			
			var unlockedPiggies:Array = gameContext.piggies.getByTag('piggy');
			if(ExternalInterface.available) ExternalInterface.call('showUnlockedPiggies', unlockedPiggies);
			
			
			
			if (Main.IsDebugEnvironment())
				debugDraw = new DebugDrawActive(gameContext.mapLayer, globalContext.assetManager);
			else
				debugDraw = new DebugDrawNull();
		}
		private function zoomMapIn(pFrom:Object = null, pTo:Object = null, pDoneCallback:Function = null, pTime:Number = 5):void{
			if(!pFrom){
				pFrom = {
					zoom: mapZoom,
					x: mapCenter.x,
					y: mapCenter.y
				}
			};
			if(!pTo){
				pTo = {
					zoom: 0.74,
					x: -gameContext.map.isometricToCartesian(28, 34).x,
					y: -gameContext.map.isometricToCartesian(28, 34).y
				}
			}
			var t:Tween = new Tween(pFrom, pTime, Transitions.EASE_IN_OUT);
			t.animate('zoom', pTo.zoom);
			t.animate('x', pTo.x);
			t.animate('y', pTo.y);
			t.onUpdate = function():void{
				mapZoom = pFrom.zoom;
				mapCenter.x = pFrom.x;
				mapCenter.y = pFrom.y;
			};
			t.onComplete = pDoneCallback;
			Starling.juggler.add(t);
		}
		public function moveCameraTo(pObj:GameObject, pTime:Number):void{
			zoomMapIn(
				null,
				{x:-pObj.getIsoSprite().x, y:-pObj.getIsoSprite().y, zoom:mapZoom},
				null,
				pTime
			);
		}
		
		private function initTouch(starlingRoot:DisplayObjectContainer):void
		{
			var panGesture:PanGesture = new PanGesture(starlingRoot);
			panGesture.addEventListener(GestureEvent.GESTURE_CHANGED, onPanChange);
			gestures.push(panGesture);
			
			var tapGesture:TapGesture = new TapGesture(starlingRoot);
			tapGesture.addEventListener(GestureEvent.GESTURE_RECOGNIZED, onTap);
			gestures.push(tapGesture);
		}
		
		private function initUi(starlingRoot:DisplayObjectContainer):void
		{
			uiLayer = new Sprite;
			starlingRoot.addChild(uiLayer);
			
			logger.debug('Initializing placement UI');
			placeUi = new PlaceUi(
				gameContext,
				gameContext.map,
				gameContext.grid,
				globalContext.assetManager.getTexture("Placement"),
				globalContext.assetManager.getTexture("tick"),
				globalContext.assetManager.getTexture("cross")
			);
			placeUi.addEventListener(PlacingEvent.PLACED, onPlaced);
			
			gameContext.mapLayer.addChild(placeUi);
			
			logger.debug('Initializing market');
			marketButton = new Button;
			marketButton.label = "Market";
			uiLayer.addChild(marketButton);
			
			logger.debug('Initializing market 2');
			marketUi = new MarketUi(globalContext, gameContext, globalContext.assetManager, this);
			uiLayer.addChild(marketUi);
			marketUi.visible = false;
			
			marketButton.addEventListener(Event.TRIGGERED, showMarket);
			marketUi.addEventListener(MarketEvent.CHECKOUT, onCheckout);
			
			logger.debug('Initializing HUD');
			hud = new Hud(this, gameContext, globalContext);
			starlingRoot.addChild(hud);
			
			// Debug draw
			if (Main.IsDebugEnvironment())
			{
				logger.debug('Initializing debug mode');
				
				var toggle:ToggleSwitch = new ToggleSwitch();
				toggle.name = "debugDraw";
				toggle.x = 100;
				uiLayer.addChild(toggle);
			}
		}
		
		public function save(pInvalidateProgress:Boolean = false):void
		{
			if(pInvalidateProgress){
				globalContext.persistence.save(null, function(o:*):void { 
					if(ExternalInterface.available) ExternalInterface.call('rebootGame');
				} );
			}else{
				var ba:Array = SerializeGame.serialize(gameContext, mapCenter, mapZoom);
				globalContext.persistence.save(ba, function(o:*):void { } );
			}
			logger.info("Saved!");
		}
		
		private function restoreGameStateFromSave():Boolean
		{
			if(!saveGameData || !saveGameDetails) return false;
			
			gameContext.recoveringGameState = true;
			
			saveGameData.position = 0;
			saveGameDetails.position = 0;
			
			// Read version
			var version:uint = saveGameDetails.readUnsignedInt();
			if (SerializeGame.VERSION != version)
				return false;
				
			// Read timestamp
			var d:Date = new Date(Number(saveGameDetails.readUTF()));
			
			// Read misc. state
			gameContext.cash 				= saveGameDetails.readUnsignedInt();
			gameContext.todaysTimestamp 	= saveGameDetails.readFloat();
			gameContext.todaysPiggy1 		= saveGameDetails.readUTF();
			gameContext.todaysPiggy2 		= saveGameDetails.readUTF();
			gameContext.todaysPiggy3 		= saveGameDetails.readUTF();
			gameContext.invitedPeople		= saveGameDetails.readUTF();
			
			mapCenter.x 					= saveGameDetails.readFloat();
			mapCenter.y 					= saveGameDetails.readFloat();
			mapZoom 						= saveGameDetails.readFloat();
			
			try{ gameContext.tutorialStep = saveGameDetails.readUnsignedInt(); }catch(pError:Object){};
			try{ gameContext.shownEvilGuyTip = saveGameDetails.readBoolean(); }catch(pError:Object){};
			
			try{ gameContext.enableFacebookPosting = saveGameDetails.readBoolean(); }catch(pError:Object){};
			try{ gameContext.enableBackgroundMusic = saveGameDetails.readBoolean(); }catch(pError:Object){};
			try{ gameContext.enableSoundEffects = saveGameDetails.readBoolean(); }catch(pError:Object){};
			
			
			SoundManager.enableMusic(gameContext.enableBackgroundMusic, false);
			TweenNano.delayedCall(2, function(){
				SoundManager.enableMusic(gameContext.enableBackgroundMusic, gameContext.enableSoundEffects);
			});
			
			// Read game objects
			var objectList:Array = [];
			
			var TOTAL:int = 0;
			function readData():void
			{
				while (saveGameData.position != saveGameData.length)
				{
					var id:uint 			= saveGameData.readUnsignedInt();
					var recipeName:String 	= saveGameData.readUTF();
					var i:int 				= saveGameData.readInt();
					var j:int 				= saveGameData.readInt();
					var properties:Object 	= saveGameData.readObject();
					
					gameContext.idGenerator.reserve(id);
					
					objectList.push( {
						id:id, recipeName:recipeName, i:i, j:j, properties:properties
					});
					
					TOTAL++;
				}
				trace("### Read from Memory -> "+TOTAL);
			}
			
			
			readData();
			
			var persisters:Array = [];
			
			var numOfEvilGuys:int = 0;
			for each (var o:Object in objectList)
			{
				//
				switch(o.recipeName)
				{
					case "evilGuy-recipe":
						if(++numOfEvilGuys > 1)
						{
							logger.debug('Only one evil guy can be made!! Skip this one...');
							continue;
						}
						break;
				}
					
				var g:GameObject = cooker.cook(o.recipeName, o.i, o.j, o.id);
				
				var persist:PersistBehaviour = g.getBehaviourByType(PersistBehaviour);
				if (persist)
				{
					persisters.push(persist);
					persist.load(o.properties);
				}
			}
			
			for each (persist in persisters)
				persist.afterLoad();
			
			
			var now:Date = new Date();
			catchup = (now.getTime() - d.getTime()) / 1000;
			catchup = Math.min(60 * 60 * 24, catchup);
			
			if (catchup > 0){
				/*
				var filter:ColorMatrixFilter = new ColorMatrixFilter;
				filter.adjustSaturation( -1);
				graphicsRoot.filter = filter;
				*/
				
				SoundManager.playSound({id:'SO_Open_popup'});
				cachupPopup = Alert.show(
					"We are continuing the game. A lot has happened since you were gone.",
					"Calculating",
					new ListCollection([]),
					new Image(globalContext.assetManager.getTexture('carl'))
				);
			}
				
			return true;
		}
		
		
		public function reboot():void{
			save(true);
		}
		
		
		private function addInitialObjects():void
		{
			var layerIndex:int = 1;
			for (;;)
			{
				var layer:MapLayer = gameContext.map.getLayerByName("Objects" + layerIndex);
				if (!layer) break;
				for (var j:int = 0; j < gameContext.map.height; ++j)
				{
					for (var i:int = 0; i < gameContext.map.width; ++i)
					{
						var tileId:uint = layer.data[i + j * gameContext.map.width];
						if (0 != tileId)
						{
							var type:String = gameContext.map.getTileProperty(tileId, "type");
							logger.debug("Tile ID: {0}, Type: {1}, Pos: ({2}, {3})", [tileId, type, i, j]);
							cooker.cook(type + "-recipe", i, j);
						}
					}
				}
				
				++layerIndex;
			}
		}
		
		private function move(
			g:GameObject,
			offsetI:int,
			offsetJ:int,
			placeOnTopOf:Array,
			w:uint,
			h:uint
		):void
		{
			SoundManager.playSound({id:'SO_Button_yes'});
			
			var sg:IsoSprite = g.getIsoSprite();
			var recipeName:String = g.recipeName;
			var idx:int = recipeName.indexOf("-recipe");
			if(idx > -1)
				recipeName = recipeName.substr( 0, idx);
			
			
			var storageItems:Array = null;
			if(g.recipeName.toLocaleLowerCase().indexOf("storage") != -1)
			{
				var b:StorageBehaviour = g.getBehaviourByType(StorageBehaviour);
				storageItems = b.getAllItems();
			}
			destroyGameObject(g);
			
			function onPlacedNewObject(gameObject:GameObject):void
			{
				if(gameObject.recipeName.toLocaleLowerCase().indexOf("storage") != -1)
				{
					var b:StorageBehaviour = gameObject.getBehaviourByType(StorageBehaviour);
					b.storeItem(storageItems,false, false, false);
				}
			}
			
			placeUi.place(
				recipeName,
				placeOnTopOf,
				sg,
				w, h,
				0, 0,
				-mapCenter.x+(sg.x+mapCenter.x), -mapCenter.y+(sg.y+mapCenter.y),
				false,
				storageItems != null ? onPlacedNewObject : null
			);
			placeUi.addEventListener(PlacingEvent.PLACED, onPlaced);

			gameContext.selectionGroupTaskTargets.deselectAll();
		}
		
		private function upgrade(
			g:GameObject,
			offsetI:int,
			offsetJ:int,
			recipeName:String,
			cost:uint,
			description:String
		):void
		{
			function doUpgrade(e:Event):void {
				if (gameContext.cash >= cost)
				{
					SoundManager.playSound({id:'SO_Upgrade'});
					
					gameContext.cash -= cost;
					
					
					var storageItems:Array = null;
					if(g.recipeName.toLocaleLowerCase().indexOf("storage") != -1)
					{
						var b:StorageBehaviour = g.getBehaviourByType(StorageBehaviour);
						storageItems = b.getAllItems();
					}
					
					destroyGameObject(g);
					
					var object:GameObject = cooker.cook(recipeName + "-recipe", offsetI, offsetJ);
					if(object.recipeName.toLocaleLowerCase().indexOf("storage") != -1)
					{
						var b:StorageBehaviour = object.getBehaviourByType(StorageBehaviour);
						b.storeItem(storageItems,false, false, false);
					}
					save();
					
					
					var continueWork:Function = function(pType:String):void{
						gameContext.piggies.visit(function(piggy:GameObject){
							var behaviour:PiggyTasksBehaviour = piggy.getBehaviourByType(PiggyTasksBehaviour);
							var token:Object = behaviour.getTaskToken();
							if(token) if(token.targetId == g.id) behaviour.buildAndAssignTask(object.id, pType);
						});
					}
					
					if(g.hasTag('mudpool')){
						continueWork('mudRelax');
					
					}else if(g.hasTag('haystack')){
						continueWork('hayRelax');
					
					}else if(g.hasTag('field')){
						continueWork('useField');
						
					}else if(g.hasTag('prepper')){
						continueWork('prep');
						
					}else if(g.hasTag('cooker')){
						continueWork('cook');
						
					}else if(g.hasTag('cooker')){
						continueWork('cook');
						
					}else if(g.hasTag('statue')){
						continueWork('guard');
						
					}else if(g.hasTag('house')){
						gameContext.piggies.visit(function(piggy:GameObject){
							var behaviour:PiggyTasksBehaviour = piggy.getBehaviourByType(PiggyTasksBehaviour);
							if(behaviour.isResting()) behaviour.rest();
						});
					}
					
					var sprite:IsoSprite = object.getIsoSprite();
					sprite.alpha = 0;
					sprite.y += 5;
					TweenNano.to(sprite, 2, {alpha:1, y:'-5'});
				}
				else
				{
					SoundManager.playSound({id:'SO_Button_no'});
					
					e.stopImmediatePropagation();
					setButtons();
				}
			};
			
			function setButtons():void
			{
				alert.buttonsDataProvider = new ListCollection([
					{ label:"Upgrade (" + cost + ")", isEnabled:gameContext.cash >= cost, triggered:doUpgrade },
					{label:"Cancel"}
				]);
			}
			
			SoundManager.playSound({id:'SO_Open_popup'});
			var alert:Alert = Alert.show(
				description,
				"Upgrade",
				null,
				new Image(globalContext.assetManager.getTexture('ralf'))
			);
			
			alert.addEventListener(Event.CLOSE, close);
			
			function close():void
			{
				SoundManager.playSound({id:'SO_Close_popup'});
				
				t.removeEventListener(TimerEvent.TIMER, updateDataProvider);
				alert.removeEventListener(Event.CLOSE, close);
			}
			
			setButtons();
			
			var t:Timer = new Timer(100);
			t.addEventListener(TimerEvent.TIMER, updateDataProvider);
			t.start();
			
			function updateDataProvider(e:*):void
			{
				setButtons();
			}
		}
		
		
		private function remove(
			g:GameObject,
			offsetI:int,
			offsetJ:int,
			recipeName:String,
			cost:uint,
			description:String
		):void
		{
			
			function doRemove(event:Event):void{
				if (gameContext.cash >= cost)
				{
					SoundManager.playSound({id:'SO_Button_yes'});
					
					gameContext.cash -= cost;
					destroyGameObject(g);
				}else{
					SoundManager.playSound({id:'SO_Button_no'});
					
					event.stopImmediatePropagation();
				}
			}
			var alert:Alert = Alert.show(
				description,
				"Remove",
				new ListCollection([
					{ label:"Remove (" + cost + ")", isEnabled:gameContext.cash >= cost, triggered:doRemove },
					{label:"Cancel"}
				]),
				new Image(globalContext.assetManager.getTexture('ralf'))
			);
		}
		
		
		private function buy(
			g:GameObject,
			offsetI:int,
			offsetJ:int,
			recipe:Object
		):void
		{
			// It's not possible to buy something when the evil guy is out
			if(gameContext.isEvilGuyOut) return;
			
			function doRemove(event:Event):void
			{
				if (gameContext.cash >= recipe.cost)
				{
					SoundManager.playSound({id:'SO_Game_intro'});
					
					var zoomToCenter:Function = function():void{
						
						zoomMapIn(null, {
							zoom: mapZoom,
							x: -gameContext.map.isometricToCartesian(28, 34).x,
							y: -gameContext.map.isometricToCartesian(28, 34).y
						}, givePiggies);
					}
					
					var givePiggies:Function = function():void{
						destroyGameObject(g);
						
						var piggies:Array = [];
						var storeItems:Array = globalContext.assetManager.getObject("store-items").items as Array;
						for each (var item:Object in storeItems)
						{
							switch (item.category)
							{
								case "piggy":
									piggies.push({
										text:"",
										name: item.name,
										price:0,
										stats: "",
										isPiggy: true,
										thumbnail:globalContext.assetManager.getTexture(item.thumbnail),
										w:item.w,
										h:item.h,
										texture:item.placeTexture,
										x:item.placeTextureX,
										y:item.placeTextureY,
										recipe:item.recipe,
										placeOnTopOf:item.placeOnTopOf
									});
									break;
							}
						}
						
						// Multiply the vector 10 times (to randomize better the free piggies)
						var i:uint = 0;
						for( i = 0; i < 10; i++)
							piggies = piggies.concat(piggies);
						
						function randomize ( a : *, b : * ) : int
						{
							return ( Math.random() > .5 ) ? 1 : -1;
						}
						
						//The first 10 piggies will be given freely to the user
						piggies.sort( randomize );
						piggies = piggies.slice( 0, 10);
					
						
						// If this is the last evil factory
						var total:int = gameContext.gameObjects.getByTag('factory').length;
						if(total == 1)
						{
							piggies.pop();
							piggies.push({
								text:"",
								name: "Penny",
								price:0,
								stats: "",
								isPiggy: true,
								thumbnail:globalContext.assetManager.getTexture("shop-icon-pig-penny"),
								w:1,
								h:1,
								texture:"shop-icon-pig-penny",
								x:-6,
								y:9,
								recipe:"piggy-penny",
								placeOnTopOf:[]
							});	
						}
						
						//Simulate the purchase of the piggies
						var e:MarketEvent = new MarketEvent(MarketEvent.CHECKOUT);
						e.purchaseList = piggies;
						onCheckout(e);
					}
						
					
					gameContext.cash -= recipe.cost;
					TweenNano.to(g.getIsoSprite(), 3, {alpha:0, y:'+5', onComplete:zoomToCenter});
				}
				else
				{
					SoundManager.playSound({id:'SO_Button_no'});
					
					event.stopImmediatePropagation();
				}
			}
			var alert:Alert = Alert.show(
				recipe.description,
				"Buy",
				new ListCollection([
					{label:"Buy (" + recipe.cost + ")", isEnabled:gameContext.cash >= recipe.cost, triggered:doRemove },
					{label:"Cancel"}
				]),
				new Image(globalContext.assetManager.getTexture('ralf'))
			);
		}
		
		public function deleteGameObject(g:GameObject):void
		{
			destroyGameObject(g);
		}
		
		private function destroyGameObject(g:GameObject):void
		{
			if(g.hasTag('piggy')) Starling.current.stage.dispatchEvent(new PiggiesEvent(PiggiesEvent.PIGGY_REMOVED, g));
			
			gameContext.gameObjects.deregister(g);
			gameContext.updatableGameObjects.deregister(g);
			gameContext.drawableGameObjects.deregister(g);
			gameContext.piggies.deregister(g);
			
			g.visitIsoSprites(function(sprite:IsoSprite):void {
				gameContext.isoSpriteDisplay.removeIsoSprite(sprite);
			});
			
			g.visitTaskTargets(function(taskTarget:TaskTarget):void {
				gameContext.taskTargetManager.removeTarget(taskTarget);
			});
			
			g.visitSpecialNodes(function(specialNode:SpecialNode):void {
				gameContext.grid.removeSpecialNode(specialNode);
			});
			
		}
		
		private function tapToCartesianTransform(p:Point):void
		{
			p.x /= assetScaleFactor;
			p.y /= assetScaleFactor;
			p.x -= gameContext.mapLayer.x;
			p.y -= gameContext.mapLayer.y;
			p.x /= mapZoom;
			p.y /= mapZoom;
		};
		
		private function tapToIsoTransform(p:Point):void
		{
			p.x /= assetScaleFactor;
			p.y /= assetScaleFactor;
			p.x -= gameContext.mapLayer.x;
			p.y -= gameContext.mapLayer.y;
			p.x /= mapZoom;
			p.y /= mapZoom;
			gameContext.map.cartesianToIsometric(p.x, p.y, p);
		};
		
		private function onPlaced(e:PlacingEvent):void
		{
			var gameObject:GameObject = cooker.cook(e.recipeName + "-recipe", e.i, e.j);
			if(e.callOnFinallyPlaced != null)
				e.callOnFinallyPlaced(gameObject);
			save();
		}
		
		public function onCheckout(e:MarketEvent):void
		{
			var freedPiggies:Array = [];
			
			for each (var p:* in e.purchaseList){
				if(p.isPiggy) {
					freedPiggies.push(p);
				}else{
					placeUi.place(
						p.recipe,
						p.placeOnTopOf,
						new Image(globalContext.assetManager.getTexture(p.texture)),
						p.w, p.h,
						p.x, p.y,
						-mapCenter.x, -mapCenter.y
					);
				}
			}
			if(freedPiggies.length > 0) {
				placeUi.place(
					freedPiggies,
					p.placeOnTopOf,
					new Image(globalContext.assetManager.getTexture(p.texture)),
					p.w, p.h,
					p.x, p.y,
					-mapCenter.x, -mapCenter.y
				);
				congratulateFreeingPiggys(freedPiggies);
			}
		}
		private function congratulateFreeingPiggys(pFreedPiggies:Array):void{
			var index:int = 0;
			
			if(ExternalInterface.available && gameContext.enableFacebookPosting){
				for(var i = 0; i < pFreedPiggies.length; i++){
					var message:String = 'I just freed '+pFreedPiggies[i].name+' from the evil meat factories! Save piggies to in the Vivera piggies game on http://www.vivera.com/biggies';
					ExternalInterface.call("fbShareMessage", message, pFreedPiggies[i].name);
				}
			}
			
			function alertNextPiggy():void{
				var alert:Alert = Alert.show(
					"You just freed "+pFreedPiggies[index].name+"!",
					"Congratulations",
					new ListCollection([
						{label:"Close"}
					]), 
					new Image(globalContext.assetManager.getTexture('ralf'))
				);
				SoundManager.playSound({id:'SO_Bought_piggy'+(Math.random() > .5 ? 1 : 2)});
				alert.addEventListener(Event.CLOSE, showNextCongrats);
			};
			function showNextCongrats():void{
				index ++;
				if(index < pFreedPiggies.length) alertNextPiggy();
			}
			alertNextPiggy();
		}
		
		private function onPanChange(e:GestureEvent):void
		{
			var g:PanGesture = e.target as PanGesture;
			var offsetX:Number = g.offsetX / assetScaleFactor / mapZoom;
			var offsetY:Number = g.offsetY / assetScaleFactor / mapZoom;
			
			mapCenter.x += offsetX;
			mapCenter.y += offsetY;
			
		}
		
		private function onTap(e:GestureEvent):void
		{
			gameContext.selectionGroupPiggies.deselectAll();
			gameContext.selectionGroupTaskTargets.deselectAll();
			gameContext.taskTargetManager.showAll();
		}
		
		private function onMouseWheel(e:Object):void
		{
			//if we look in normal view online, we use the js scroll event. When we go fullscreen, this won't be catcht and we need to listen to the flash scroll event
			if(e is MouseEvent && ExternalInterface.available && Starling.current.nativeStage.displayState == StageDisplayState.NORMAL) return;
			
			mapZoom += e.delta * 0.01;
			
			if (mapZoom < 0.55) mapZoom = 0.55;
			if (mapZoom > 1) mapZoom = 1;
		}
		
		private function onKeyDown(e:Object):void{
			if(e.keyCode == Keyboard.LEFT){
				keysDown[0] = true;
			}else if(e.keyCode == Keyboard.RIGHT){
				keysDown[1] = true;
			}else if(e.keyCode == Keyboard.UP){
				keysDown[2] = true;
			}else if(e.keyCode == Keyboard.DOWN){
				keysDown[3] = true;
			}
			
			else if(e.keyCode == Keyboard.P){
				hud.showSettings();
			}
		}
		private function onKeyUp(e:Object):void{
			
			if(e.keyCode == Keyboard.LEFT){
				keysDown[0] = false;
			}else if(e.keyCode == Keyboard.RIGHT){
				keysDown[1] = false;
			}else if(e.keyCode == Keyboard.UP){
				keysDown[2] = false;
			}else if(e.keyCode == Keyboard.DOWN){
				keysDown[3] = false;
			}
			
		}
		
		
		
		private function onTaskTargetSelected(e:TaskTargetSelectedEvent):void
		{
			var piggySelected:Boolean = false;
			gameContext.piggies.visit(function(piggy:GameObject):void {
				if (piggy.getBehaviourByType(TappableBehaviour).isSelected){
					piggySelected = true;
				}
			});
			
			if (!piggySelected)
				SoundManager.playSound({id:'SO_Haystack', volume:.2});
				gameContext.selectionGroupTaskTargets.select(e.taskTarget.targetId);
		}
		
		private function showMarket():void
		{
			SoundManager.stopBackgroundMusic();
			
			SoundManager.playSound({id:'SO_Open_popup'});
			
			marketUi.show();
		}
		public function marketClosed():void{
			SoundManager.startBackgroundMusic();
		}
		
		
		private function incommingEvilGuy():void{
			if(!gameContext.shownEvilGuyTip){
				gameContext.shownEvilGuyTip = true;
				
				var alert:Alert = Alert.show(
					"Watch out! an evil guy from the factories is walking this way to steel a piggy.\n\nAssign a piggy to the guard statue. He might be able to warn us in time.",
					"Evil guy",
					new ListCollection([
						{label:"Close"}
					]), 
					new Image(globalContext.assetManager.getTexture('statueLVL3'))
				);
			}
			hud.incommingEvilGuy(true);
		}
		private function piggyCaptured(piggy:GameObject):void{
			var alert:Alert = Alert.show(
				"O no! the evil guy stole "+piggy.getBehaviourByType(PiggyPropertiesBehaviour).name+'\nTry to assign a piggy to the guard post the next time...',
				"Piggy stolen!",
				new ListCollection([
					{label:"Close"}
				]), 
				new Image(globalContext.assetManager.getTexture('ralf'))
			);
			hud.incommingEvilGuy(false);
		}
		private function evilGuyScaredAway(piggy:GameObject):void{
			var amount:int = 25 + int(Math.random()*25);
			gameContext.cash += amount;
			save();
			
			hud.incommingEvilGuy(false);
			
			var congrats:Function = function(){
				var alert:Alert = Alert.show(
					"Nice job! "+piggy.getBehaviourByType(PiggyPropertiesBehaviour).name+' scared the evil guy away!\n\nHe dropt '+amount+' coins, those are ours now!',
					"Evil guy scared away",
					new ListCollection([
						{label:"Close"}
					]), 
					new Image(globalContext.assetManager.getTexture('ralf'))
				);
			};
			
			TweenNano.delayedCall(2, congrats);
		}
		
		
		
		
		public function update(msElapsed:int):void
		{
			if(paused) return;
			
			stepper.advanceTime(msElapsed);
			
			var numFrames:uint = stepper.readFrameCounter();
			const secondsElapsed:Number = stepper.getStepLength() / 1000.0;
			stepper.consumeFrames();
			
			if (!marketUi.visible)
			{
				while (numFrames-- > 0)
				{
					discreteUpdate(secondsElapsed);
				}
			}
			
			var speed:Number = 15 * (1.5-mapZoom);
			if(keysDown[0]) mapCenter.x += speed;
			if(keysDown[1]) mapCenter.x -= speed;
			if(keysDown[2]) mapCenter.y += speed;
			if(keysDown[3]) mapCenter.y -= speed;
		}
		
		private function discreteUpdate(secondsElapsed:Number):void
		{
			if (catchup > 0)
			{
				var start:int = getTimer();
				
				while (catchup > 0 && getTimer() - start < 50)
				{
					secondsElapsed = Math.min(catchup, 10);
					catchup -= secondsElapsed;
					
					updateGameObjects(secondsElapsed);
				}
				
				var allRelaxing:Boolean = true;
				gameContext.piggies.visit(function(piggy:GameObject):void{
					var behaviour:PiggyTasksBehaviour = piggy.getBehaviourByType(PiggyTasksBehaviour);
					if(behaviour.getTaskToken()) if(!behaviour.getTaskToken().relaxing) allRelaxing = false;
				});
				if(allRelaxing) catchup = 0;
				
				
				if (catchup <= 0)
				{
					save();
					PopUpManager.removePopUp( cachupPopup, true );
					gameContext.recoveringGameState = false;
				}
			}
			else
			{
				updateGameObjects(secondsElapsed);
			}
			
			graphicsRoot.touchable = catchup <= 0;
		}
		
		private function updateGameObjects(secondsElapsed:Number):void
		{
			gameContext.updatableGameObjects.visit(function(updatable:GameObject):void {
				var u:Array = updatable.getBehavioursByType(UpdatableBehaviour);
				for each (var b:UpdatableBehaviour in u) b.update(secondsElapsed);
			});
		}
		
		public function draw():void
		{
			var gameHidden:Boolean = marketUi.visible;
			
			placeUi.panFactor = 1.0 / assetScaleFactor / mapZoom;
			
			placeUi.draw();
			
			var sw:Number = gameContext.mapLayer.stage.stageWidth;
			var sh:Number = gameContext.mapLayer.stage.stageHeight;
			
			gameContext.mapLayer.scaleX = gameContext.mapLayer.scaleY = mapZoom;
			gameContext.mapLayer.x = mapCenter.x * mapZoom + 0.5 * sw;
			gameContext.mapLayer.y = mapCenter.y * mapZoom + 0.5 * sh;
			
			if (!gameHidden)
			{
				gameContext.isoSpriteDisplay.orderSprites(false);
				
				gameContext.drawableGameObjects.visit(function(drawable:GameObject):void {
					var d:Array = drawable.getBehavioursByType(DrawableBehaviour);
					for each (var b:DrawableBehaviour in d) b.draw(mapZoom);
				});
			}
			
			if (Main.IsDebugEnvironment())
			{
				var hidden:Boolean = !uiLayer.getChildByName("debugDraw")["isSelected"];
				debugDraw.draw(gameContext.map, gameContext.grid, gameContext.isoSpriteDisplay.getIsoSprites(), hidden);
			}
			
			gameContext.mapLayer.visible = !gameHidden;
			drawUi();
		}
		
		private function drawUi():void
		{
			marketButton.visible = !marketUi.visible && gameContext.tutorialStep >= tutorialManager.SHOW_MARKET_BUTTON_FROM;
			hud.visible = !marketUi.visible;
			
			marketButton.x = 1;
			marketButton.y = Math.floor(uiLayer.stage.stageHeight - marketButton.height - 1);
			
			marketUi.draw();
			hud.draw();
		}
		
		
		private var paused:Boolean = false;
		public function pause(manager:SceneManager):void{
			paused = true;
		}
		public function resume(manager:SceneManager):void{
			paused = false;
		}
		
	}
}