package savethepiggies.game 
{
	import org.as3commons.logging.api.*;
	
	public final class SelectionGroup 
	{
		private static const logger:ILogger = getLogger(SelectionGroup)
		
		private var listeners:Array;
		
		public function SelectionGroup()
		{
			listeners = [];
		}
		
		public function deselectAll():void
		{
			for each (var listener:SelectionListener in listeners)
				listener.selected(0);
		}
		
		public function select(id:uint):void
		{
			for each (var listener:SelectionListener in listeners)
				listener.selected(id);
		}
		
		public function register(listener:SelectionListener):void
		{
			if (listeners.indexOf(listener) == -1)
				listeners.push(listener);
			else
				logger.warn("Listener already exists!");
		}
		
		public function deregister(listener:SelectionListener):void
		{
			var idx:int = listeners.indexOf(listener);
			if (idx == -1)
				logger.warn("Listener doesn't exist!");
			else
				listeners.splice(idx, 1);
		}
	}
}