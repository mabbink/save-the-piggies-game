package savethepiggies.game 
{
	import org.as3commons.logging.api.*;
	import starling.events.Event;
	import starling.events.EventDispatcher;
	import supportlib.grid.Grid;
	
	public final class GameGrid extends EventDispatcher implements Grid 
	{
		private static const logger:ILogger = getLogger(GameGrid);
		
		private var width:int, height:int;
		private var max:int;
		
		private var special:Vector.<Array>;
		
		public function GameGrid(width:int, height:int) 
		{
			this.width = width;
			this.height = height;
			this.max = width * height;
			
			special = new Vector.<Array>(width * height);
			for (var i:int = 0; i < special.length; ++i)
				special[i] = [];
		}
		
		public function addSpecialNode(
			id:uint,
			i:uint,
			j:uint,
			blockLeft:Boolean,
			blockUp:Boolean,
			blockRight:Boolean,
			blockDown:Boolean,
			occupiedType:String
		):SpecialNode
		{
			var node:SpecialNode = null;
			
			var inBounds:Boolean = i < width && j < height;
			if (!inBounds)
				logger.warn("Tried to add special node at {0}, {1} which is out of bounds {2}, {3}", [i, j, width, height]);
			else
			{
				node = new SpecialNode(id, blockLeft, blockUp, blockRight, blockDown, occupiedType);
				special[i + j * width].push(
					node
				);
				node.i = i;
				node.j = j;
				
				dispatchEvent(new Event(Event.CHANGE));
			}
			
			return node;
		}
		
		public function removeSpecialNode(
			node:SpecialNode
		):Boolean
		{
			var removed:Boolean = false;
			
			var i:int = node.i;
			var j:int = node.j;
			
			if (undefined != special[i + j * width])
			{
				var idx:int = special[i + j * width].indexOf(node);
				if ( -1 != idx)
				{
					special[i + j * width].splice(idx, 1);
					removed = true;
				}
			}
			
			if (!removed)
				logger.warn("Couldn't find special node with ID {0}", [node.id]);
			
			return removed;
		}
		
		public function getWidth():int { return width; }
		public function getHeight():int { return height; }
		
		public function countSpecialNodes(i:int, j:int):uint
		{
			var index:int = i + j * width;
			if (index < 0 || index >= special.length) return 0;
			
			return special[index].length;
		}
		
		public function getSpecialNodes(i:int, j:int):Array
		{
			var index:int = i + j * width;
			if (index < 0 || index >= special.length) return [];
			
			return special[index].concat();
		}
		
		public function readNeighbourIndices(node:int, result:Vector.<int>):int 
		{
			var neighbourCount:int = 0;
			var nodeX:int = node % width;
			
			// up
			if (node - width >= 0 && !blockedBetween(node, node - width)) 
				result[neighbourCount++] = node - width;
			// right
			if (nodeX + 1 < width && !blockedBetween(node, node + 1)) 
				result[neighbourCount++] = node + 1;
			// down
			if (node + width < max && !blockedBetween(node, node + width)) 
				result[neighbourCount++] = node + width;
			// left
			if (nodeX - 1 >= 0 && !blockedBetween(node, node - 1)) 
				result[neighbourCount++] = node - 1;
			
			return neighbourCount;
		}
		
		private function blockedBetween(a:int, b:int):Boolean
		{
			if (a == b) return false;
			
			// Swap
			if (a > b)
			{
				var tmp:int = a;
				a = b;
				b = tmp;
			}
			
			// Either up or right
			var checkA:int, checkB:int;
			if (b - a == 1)
			{
				checkA = 2;
				checkB = 0;
			}
			else
			{
				checkA = 3;
				checkB = 1;
			}
			
			var aSpecial:Array = null;
			var bSpecial:Array = null;
			
			aSpecial = special[a];
			bSpecial = special[b];
			
			for each (var s:SpecialNode in aSpecial)
				if (s.blocked[checkA]) return true;
				
			for each (s in bSpecial)
				if (s.blocked[checkB]) return true;
			
			return false;
		}
	}

}
