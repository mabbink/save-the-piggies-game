package savethepiggies.game 
{
	
	
	public class Behaviour implements Component
	{
		private var _id:uint;
		private var _gameObject:GameObject;
		
		public function Behaviour(
			id:uint
		) 
		{
			_id = id;
		}
		
		public function activate():void
		{
			
		}
		
		public function cleanup():void
		{
			
		}
		
		// "Events"
		public function onSelect():void
		{
			
		}
		
		public function onDeselect():void
		{
			
		}
		
		public function get id():uint { return _id; }
		public function get gameObject():GameObject { return _gameObject; }
		public function set gameObject(g:GameObject):void { _gameObject = g; }
	}
}