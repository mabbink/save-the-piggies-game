package savethepiggies.game.behaviours 
{
	import feathers.controls.Label;
	import feathers.text.BitmapFontTextFormat;
	
	import savethepiggies.game.SelectionGroup;
	import savethepiggies.game.SelectionListener;
	
	public final class StorageUiBehaviour extends DrawableBehaviour implements SelectionListener
	{
		private static const UI_X:Number = -5.5;
		private static const UI_Y:Number = -18;
		
		public var selectionGroup:SelectionGroup;
		
		private var storage:StorageBehaviour;
		
		private var labels:Vector.<Label>;
		private var isSelected:Boolean;
		
		private var capacityLabel:Label;
		
		/*
		private var lupin:Label;
		private var preppedLupin:Label;
		private var cookedLupin:Label;
		private var fruit:Label;
		private var vegetables:Label;
		private var flowers:Label;
		private var sausages:Label;
		private var burgers:Label;
		private var meatballs:Label;
		*/
		
		public function StorageUiBehaviour(id:uint) 
		{
			super(id);
		}
		
		public override function cleanup():void
		{
			selectionGroup.deregister(this);
		}
		
		public override function activate():void
		{
			super.activate();
			
			selectionGroup.register(this);
			
			storage = gameObject.getBehaviourByType(StorageBehaviour);
			
			labels = new Vector.<Label>();
			//capacityLabel = addLabel();
			
			/*
			lupin = addLabel();
			preppedLupin = addLabel();
			cookedLupin = addLabel();
			fruit = addLabel();
			vegetables = addLabel();
			flowers = addLabel();
			sausages = addLabel();
			burgers = addLabel();
			meatballs = addLabel();
			*/
		}
		
		private function addLabel():Label
		{
			var l:Label = new Label;
			
			l.text = "foo";
			l.touchable = false;
			
			labels.push(l);
			gameObject.getIsoSprite().addChild(l);
			l.textRendererProperties.snapToPixels = false;
			
			l.textRendererProperties.textFormat = new BitmapFontTextFormat("ui-font", 8);
			
			return l;
		}
		
		public override function draw(zoomLevel:Number):void
		{
			var shouldShow:Boolean = (zoomLevel > 0.4);
			
			showLabels(shouldShow && isSelected);
			
			if (isSelected && shouldShow)
			{
				fillLabels();
				positionLabels(zoomLevel);
			}
		}
		
		private function positionLabels(zoomLevel:Number):void
		{
			var top:int = UI_Y;
			for each (var l:Label in labels)
			{
				if (l.visible) top -= l.height;
			}

			var pos:Number = top;
				
			for (var i:int = 0; i < labels.length; ++i)
			{
				if (labels[i].visible)
				{
					labels[i].x = -0.5 * labels[i].width + UI_X;
					labels[i].y = pos;
					pos += labels[i].height;
				}
			}
		}
		
		private function fillLabels():void
		{
			
			//capacityLabel.text = "Filled: " + (storage.capacity - storage.freeSpace) + "/" + storage.capacity;
			
			function fillLabel(l:Label, type:String):void
			{
				var c:int = storage.countType(type);
				l.visible = c > 0;
				if (c > 0)
				{
					l.text = type + ": " + c;
				}
			}
			/*
			fillLabel(lupin, 			Storage.LUPIN);
			fillLabel(preppedLupin, 	Storage.PREPPED_LUPIN);
			fillLabel(cookedLupin, 		Storage.COOKED_LUPIN);
			fillLabel(fruit, 			Storage.FRUIT);
			fillLabel(vegetables, 		Storage.VEGETABLES);
			fillLabel(flowers, 			Storage.FLOWERS);
			fillLabel(sausages, 		Storage.SAUSAGES);
			fillLabel(burgers, 			Storage.BURGERS);
			fillLabel(meatballs, 		Storage.MEATBALLS);
			*/
			for (var i:int = 0; i < labels.length; ++i)
				if (labels[i].visible) labels[i].validate();
			
		}
		
		private function showLabels(show:Boolean):void
		{
			
			for each (var l:Label in labels)
				l.visible = show;
			
		}
		
		public function selected(id:uint):void 
		{
			isSelected = id == gameObject.id;
		}
	}

}