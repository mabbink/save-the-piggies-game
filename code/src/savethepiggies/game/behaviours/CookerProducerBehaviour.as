package savethepiggies.game.behaviours 
{
	import starling.display.DisplayObject;
	import starling.display.MovieClip;
	
	public final class CookerProducerBehaviour extends ProducerBehaviour 
	{
		private var smoke:MovieClip;
		private var flame:MovieClip;
		private var lid1:DisplayObject;
		private var lid2:DisplayObject;
		
		public function CookerProducerBehaviour(id:uint, inputSpace:int, produceTime:Number) 
		{
			super(id, inputSpace, produceTime);
		}
		
		public override function activate():void
		{
			super.activate();
			
			smoke = MovieClip(gameObject.getIsoSprite().getChildByName("smoke"));
			flame = MovieClip(gameObject.getIsoSprite().getChildByName("flame"));
			lid1 = DisplayObject(gameObject.getIsoSprite().getChildByName("lid1"));
			lid2 = DisplayObject(gameObject.getIsoSprite().getChildByName("lid2"));
			if(!lid2) lid2 = lid1;
		}
		
		protected override function get itemType():String { return ApplicationVars.COOKED_LUPIN; }
		
		public override function update(dt:Number):void
		{
			super.update(dt);
			
			lid1.visible = lid2.visible = smoke.visible = flame.visible = isWorking;
			
			if (isWorking)
			{
				smoke.advanceTime(dt);
				flame.advanceTime(dt);
				lid1.rotation = -.1 + (Math.random()*.1);
				lid2.rotation = -.1 + (Math.random()*.1);
			}
		}
		
	}

}