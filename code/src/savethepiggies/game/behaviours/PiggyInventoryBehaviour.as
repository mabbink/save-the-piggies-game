package savethepiggies.game.behaviours 
{
	import savethepiggies.game.Behaviour;
	
	public final class PiggyInventoryBehaviour extends Behaviour 
	{
		private var holding_:Array = null;
		//	[ { name:"foo", amount: 1 }, { name:"bar", amount: 3 } ]
		//
		//
		
		public function PiggyInventoryBehaviour(id:uint) 
		{
			super(id);
		}
		
		public function get holding():Array
		{
			return holding_;
		}
		
		public function set holding(values:Array):void
		{
			for each(var value:Object in values)
			{
				
				try
				{
					if((value.name as String).length <= 0 || value.amount <= 0)
					throw new Error("Problem with the values!");
				}
				catch (error:Error)
				{
					throw new Error("Problem with the values!");
				}
			}
			holding_ = values;
		}
	}
}