package savethepiggies.game.behaviours 
{
	import org.as3commons.logging.api.ILogger;
	import org.as3commons.logging.api.getLogger;
	import org.gestouch.events.GestureEvent;
	import org.gestouch.gestures.TapGesture;
	
	import savethepiggies.game.Behaviour;
	import savethepiggies.game.SelectionGroup;
	import savethepiggies.game.SelectionListener;
	
	public final class RemovableBehaviour extends Behaviour implements SelectionListener 
	{
		private static const logger:ILogger = getLogger(UpgradableBehaviour);
		
		public var offsetI:int, offsetJ:int;
		public var recipeName:String;
		public var selectionGroup:SelectionGroup;
		public var removeCallback:Function;
		public var cost:uint;
		public var description:String;
		
		private var gesture:TapGesture;
		
		public function RemovableBehaviour(id:uint) 
		{
			super(id);
		}
		
		public override function cleanup():void
		{
			selectionGroup.deregister(this);
			
			gesture.dispose();
		}
		
		public override function activate():void
		{
			selectionGroup.register(this);
			
			gesture = new TapGesture(gameObject.getIsoSprite().getChildByName("remove"));
			gesture.addEventListener(GestureEvent.GESTURE_RECOGNIZED, onRemoveClick);
		}
		
		private function onRemoveClick(e:*):void
		{
			removeCallback(gameObject, offsetI, offsetJ, recipeName, cost, description);
		}
		
		public function selected(id:uint):void 
		{
			if(id == gameObject.id){
				gameObject.getIsoSprite().getChildByName("removable").alpha = .7;
				gameObject.getIsoSprite().getChildByName("remove").visible = true;
			}else{
				gameObject.getIsoSprite().getChildByName("removable").alpha = 1;
				gameObject.getIsoSprite().getChildByName("remove").visible = false;
			}
		}
		
	}

}