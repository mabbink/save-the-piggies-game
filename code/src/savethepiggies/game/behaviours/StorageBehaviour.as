package savethepiggies.game.behaviours 
{
	import savethepiggies.game.Behaviour;
	import savethepiggies.game.events.StorageEvent;
	import savethepiggies.game.events.TutorialEvent;
	
	import starling.animation.Transitions;
	import starling.animation.Tween;
	import starling.core.Starling;
	import starling.display.Image;
	import starling.display.Sprite;
	import starling.utils.AssetManager;
	
	public final class StorageBehaviour extends Behaviour
	{
		private var _capacity:int;
		
		private var contents:Array;
		public var assetManager:AssetManager;
		
		
		public function StorageBehaviour(id:uint, capacity:int) 
		{
			super(id);
			
			contents = [];
			_capacity = capacity;
			
		}
		
		public override function activate():void
		{
			super.activate();
		}
		
		public function get hasSpace():Boolean { 
			return contents.length < _capacity;
		}
		public function hasItem(type:String):Boolean{
			if (null === type){
				return hasSpace;
			}else{
				for each (var c:String in contents)
					if (c === type) return true;
			}
			return false;
		}
		
		public function storeItem(items:Array, pShow:Boolean = true, dispatchEvent:Boolean = true, dispatchTutorialEvent:Boolean = true):Boolean
		{
			if (null === items) return false;
			if (!hasSpace) return false;
			
			var total:int = 0;
			for each(var item:Object in items)
				total += item.amount;
			
			if( total > freeSpace) return false;
			
			for each(item in items){
				for(var i:int = 0; i < item.amount; i++){
					contents.push(item.name);
				}
				if(pShow) showIcon(item.name);
				if(dispatchTutorialEvent)
					Starling.current.stage.dispatchEvent(new TutorialEvent({action: 'storingItem', type: item.name}, TutorialEvent.TUTORIAL_EVENT));
			}
			if(dispatchEvent)
				Starling.current.stage.dispatchEvent(new StorageEvent(StorageEvent.STORAGE_ADD, items));
			
			ProgressBehaviour(gameObject.getBehaviourByType(ProgressBehaviour)).update( 1 - (freeSpace / capacity) );
			
			return true;
		}
		
		
		
		private var containers:Vector.<Sprite> = new Vector.<Sprite>();
		
		public function showIcon(pIngredient:String):void{
			var container:Sprite;
			var icon:Image;
			
			for each(var storedContainer:Sprite in containers){
				if(storedContainer.alpha == 0) container = storedContainer;
			}
			if(container){
				icon = container.getChildByName('icon') as Image;
				icon.texture = assetManager.getTexture(pIngredient);
				icon.readjustSize();
			}else{ 
				container = new Sprite();
				container.x = -6;
				
				var bg:Image = new Image(assetManager.getTexture('icon-bg'));
				container.addChild(bg);
				
				icon = new Image(assetManager.getTexture(pIngredient));
				icon.name = 'icon';
				icon.x = 1;
				icon.y = 1;
				container.addChild(icon);
				containers.push(container);
				gameObject.getIsoSprite().addChild(container);
			}
			container.visible = true;
			container.y = -15;
			container.alpha = 1;
			
			var tween:Tween = new Tween(container, 5, Transitions.EASE_OUT);
			tween.animate('y', -23);
			tween.animate('alpha', 0);
			Starling.juggler.add(tween);
			
		}
		
		public function getAllItems(pDispatch:Boolean = false, pIngredients:Boolean = true, pProducts:Boolean = true):Array
		{
			var allItems:Array = [];
			var onArray:Object = {};
			for each(var name:String in contents)
				if(onArray[name] == null)
				{
					if(!pIngredients && ApplicationVars.SELLABLES.indexOf(name) == -1) continue;
					if(!pProducts && ApplicationVars.SELLABLES.indexOf(name) > -1) continue;
					
					allItems.push( {name:name, amount:countType(name)} );
					var idx:int = -1;
					while((idx = contents.indexOf(name)) != -1)
						contents.splice(idx,1)
					onArray[name] = true;
				}
			
			if(pDispatch) Starling.current.stage.dispatchEvent(new StorageEvent(StorageEvent.STORAGE_REMOVE, allItems));
			return allItems;
		}
		
		public function retrieveItems(items:Array):Object
		{
			//check
			var result = {success:true, missing:[]}
				
			var ok:int = 0;
			for each(var item:Object in items)
			{
				if(countType(item.name) >= item.amount){
					ok++;
				}else{
					result.missing.push(item.name);
				}
			}
			
			if( ok == items.length )
			{
				for each(var item:Object in items)
				{
					var amount:int = item.amount;
					while(amount-- > 0)
					{
						var i:int = contents.indexOf(item.name);
						Starling.current.stage.dispatchEvent(new StorageEvent(StorageEvent.STORAGE_REMOVE, [{name:item.name, amount:1}]));
						contents.splice(i, 1);
					}
					
				}
			}else{
				result.success = false;
			}
			
			ProgressBehaviour(gameObject.getBehaviourByType(ProgressBehaviour)).update( 1 - (freeSpace / capacity) );
			
			return result;
		}
		
		
		public function countType(type:String):int
		{
			var c:int = 0;
			
			for each (var e:String in contents)
				if (e === type) ++c;
			
			return c;
		}
		
		public function getState():Object { return contents.concat(); }
		public function setState(state:Object):void { 
			contents = state as Array; 
			
			var list:Array = [];
			for(var j:int = 0; j < contents.length; j++){
				var ingredient:String = contents[j];
				
				var exists:Boolean = false;
				for(var i:int = list.length; i--;){
					if(list[i].name == ingredient){
						list[i].amount ++;
						exists = true;
						break;
					}
				}
				if(!exists) list.push({name:ingredient, amount:1});
			}
			Starling.current.stage.dispatchEvent(new StorageEvent(StorageEvent.STORAGE_ADD, list));
		}
		
		public function get capacity():int { return _capacity; }
		public function get freeSpace():int { return _capacity - contents.length; }
	}

}