package savethepiggies.game.behaviours 
{
	public final class FieldPersistBehaviour extends PersistBehaviour 
	{
		public function FieldPersistBehaviour(id:uint) 
		{
			super(id);
		}
		
		public override function save():Object
		{
			var field:Object = gameObject.getBehaviourByType(FieldBehaviour);
			return field.getState();
		}
		
		public override function load(properties:Object):void
		{
			var field:FieldBehaviour = gameObject.getBehaviourByType(FieldBehaviour);
			field.setState(properties);
		}
	}

}