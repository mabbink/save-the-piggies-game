package savethepiggies.game.behaviours {
	import savethepiggies.game.Behaviour;
	
	public class PersistBehaviour extends Behaviour 
	{
		
		public function PersistBehaviour(id:uint) 
		{
			super(id);
		}
		
		public function save():Object
		{
			return { };
		}
		
		public function load(properties:Object):void
		{
			
		}
		
		public function afterLoad():void
		{
			
		}
		
	}

}