package savethepiggies.game.behaviours 
{
	import com.greensock.TweenNano;
	
	import flash.geom.Point;
	import flash.geom.Rectangle;
	
	import org.as3commons.logging.api.ILogger;
	import org.as3commons.logging.api.getLogger;
	
	import savethepiggies.game.Behaviour;
	import savethepiggies.game.IsoSprite;
	import savethepiggies.map.Map;
	
	import starling.display.DisplayObject;
	import starling.events.Touch;
	import starling.events.TouchEvent;
	import starling.events.TouchPhase;
	
	public final class HoverHideBehaviour extends Behaviour 
	{
		private static const logger:ILogger = getLogger(HoverHideBehaviour);
		
		public var mapLayer:DisplayObject;
		public var map:Map;
		public var speed:Number = 1;
		public var bounds:Rectangle;
		
		public function HoverHideBehaviour(id:uint) 
		{
			super(id);
		}
		
		public override function cleanup():void
		{
			super.cleanup();
			
			mapLayer.removeEventListener(TouchEvent.TOUCH, onTouch);
		}
		
		public override function activate():void
		{
			super.activate();
			
			mapLayer.addEventListener(TouchEvent.TOUCH, onTouch);
		}
		
		private var touchPos:Point = new Point;
		private var isoPos:Point = new Point;
		private var show:Boolean = false;
		private function onTouch(e:TouchEvent):void
		{
			var touch:Touch = e.getTouch(mapLayer, TouchPhase.HOVER);
			if (touch)
			{
				touch.getLocation(mapLayer, touchPos);
				map.cartesianToIsometric(touchPos.x, touchPos.y, isoPos);
				
				var sprite:IsoSprite = gameObject.getIsoSprite();
				show = !bounds.containsPoint(isoPos);
				if(!sprite.visible && show){
					TweenNano.delayedCall(speed, function(){
						sprite.alpha = show ? 1 : 0.1;
					});
				}else{
					sprite.alpha = show ? 1 : 0.1;
				}
			}
		}
	}

}