package savethepiggies.game.behaviours 
{
	import savethepiggies.game.Behaviour;
	
	public final class PiggyPropertiesBehaviour extends Behaviour 
	{
		public var name:String 			= '';
		public var speed:Number 		= 1;
		public var sales:Number 		= 1;
		public var guarding:Number 		= 1;
		public var prepping:Number 		= 1;
		public var relaxation:Number 	= 1;
		public var gathering:Number 	= 1;
		public var icon:String			= '';
		
		public function PiggyPropertiesBehaviour(id:uint) 
		{
			super(id);
		}
		
		public function save():Object
		{
			return {
				name:name,
				speed:speed,
				sales:sales,
				guarding:guarding,
				prepping:prepping,
				relaxation:relaxation,
				gathering:gathering,
				icon:icon
			};
		}
		
		public function load(p:Object):void
		{
			name = p.name;
			speed = p.speed;
			sales = p.sales;
			guarding = p.guarding;
			prepping = p.prepping;
			relaxation = p.relaxation;
			gathering = p.gathering;
			icon = p.icon;
		}
	}

}