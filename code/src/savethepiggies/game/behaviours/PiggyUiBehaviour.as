package savethepiggies.game.behaviours 
{
	import flash.geom.Rectangle;
	
	import feathers.controls.Label;
	import feathers.text.BitmapFontTextFormat;
	
	import savethepiggies.game.IsoSprite;
	
	import starling.display.DisplayObject;
	import starling.display.Sprite;
	
	public final class PiggyUiBehaviour extends DrawableBehaviour 
	{
		private static const UI_Y:Number = -17;
		
		private var counters:PiggyCounterBehaviour;
		private var properties:PiggyPropertiesBehaviour;
		private var inventory:PiggyInventoryBehaviour;
		
		
		private var bar:Sprite;
		private var barBg:DisplayObject;
		private var barWidth:Number;
		
		private var barHungry:Sprite;
		private var barSleep:Sprite;
		private var barHungryWidth:Number;
		
		private var labels:Vector.<Label>;
		
		private var bubble:DisplayObject;
		private var hungerIcon:DisplayObject;
		private var sleepIcon:DisplayObject;
		
		private var selected:Boolean;
		
		public function PiggyUiBehaviour(id:uint) 
		{
			super(id);
		}
		
		public override function activate():void
		{
			super.activate();
			
			counters = gameObject.getBehaviourByType(PiggyCounterBehaviour);
			properties = gameObject.getBehaviourByType(PiggyPropertiesBehaviour);
			inventory = gameObject.getBehaviourByType(PiggyInventoryBehaviour);
			
			createBar();
			
			bubble = gameObject.getIsoSprite().getChildByName("bubble");
			hungerIcon = gameObject.getIsoSprite().getChildByName("hungericon");
			sleepIcon = gameObject.getIsoSprite().getChildByName("sleepicon");
			
			labels = new Vector.<Label>();
			
			addLabel(-30, -56, 6, 0xc34c00);
			
			addLabel(-30, -49, 4, 0x959595);
			addLabel(-30, -43, 4, 0x959595);
			addLabel(-30, -37, 4, 0x959595);
			
			addLabel(0, -49, 4, 0x959595);
			addLabel(0, -43, 4, 0x959595);
			addLabel(0, -37, 4, 0x959595);
			
			addLabel(-6, -49, 4, 0x21a8da);
			addLabel(-6, -43, 4, 0x21a8da);
			addLabel(-6, -37, 4, 0x21a8da);
			
			addLabel(24, -49, 4, 0x21a8da);
			addLabel(24, -43, 4, 0x21a8da);
			addLabel(24, -37, 4, 0x21a8da);
		}
		
		private function addLabel(pX:int, pY:int, pSize:int, pColor:uint):void
		{
			var l:Label = new Label;
			l.x = pX;
			l.y = pY;
			l.text = "foo";
			l.touchable = false;
			
			gameObject.getIsoSprite().addChild(l);
			//l.textRendererProperties.snapToPixels = false;
			
			l.textRendererProperties.textFormat = new BitmapFontTextFormat("ui-font", pSize, pColor);
			
			labels.push(l);
		}
		
		private function createBar():void
		{
			//create the focus out bar
			var piggy:IsoSprite = gameObject.getIsoSprite();
			
			bar = new Sprite;
			piggy.addChild(bar);
			
			bar.x = piggy.getChildByName("bar").x;
			bar.y = piggy.getChildByName("bar").y;
			piggy.getChildByName("bar").x = 0;
			piggy.getChildByName("bar").y = 0;
			bar.addChild(piggy.getChildByName("bar"));
			barWidth = bar.width;
			
			barBg = piggy.getChildByName("bar_bg");
			
			
			//create the focus in bars
			barHungry = new Sprite();
			piggy.addChild(barHungry);
			
			barHungry.x = piggy.getChildByName("bar_hungry").x;
			barHungry.y = piggy.getChildByName("bar_hungry").y;
			piggy.getChildByName("bar_hungry").x = 0;
			piggy.getChildByName("bar_hungry").y = 0;
			barHungry.addChild(piggy.getChildByName("bar_hungry"));
			
			
			
			barSleep = new Sprite();
			piggy.addChild(barSleep);
			
			barSleep.x = piggy.getChildByName("bar_sleep").x;
			barSleep.y = piggy.getChildByName("bar_sleep").y;
			piggy.getChildByName("bar_sleep").x = 0;
			piggy.getChildByName("bar_sleep").y = 0;
			barSleep.addChild(piggy.getChildByName("bar_sleep"));
			
			
			
			barHungryWidth = barHungry.width;
		}
		
		public override function draw(zoomLevel:Number):void
		{
			var shouldShow:Boolean = (zoomLevel > 0);
			
			showElements(shouldShow, selected);
			
			
			function updateSelectedUI():void{
				var values:Array = [
					properties.name,
					'Speed:',
					'Sales:',
					'Guarding:',
					'Prepping:',
					'Relaxation:',
					'Gathering:',
					properties.speed,
					properties.sales,
					properties.guarding,
					properties.prepping,
					properties.relaxation,
					properties.gathering
				];
				for(var i:int = 0; i < values.length; i++){
					labels[i].text = values[i];
				}
				
				barHungry.clipRect = new Rectangle(0, 0, counters.fullness*barHungryWidth, 4);
				barSleep.clipRect = new Rectangle(0, 0, counters.energy*barHungryWidth, 4);
				
				hungerIcon.visible = sleepIcon.visible = bubble.visible = false;
			}
			function updateUnselectedUI():void{
				var lowestCounter:Number = Math.min(counters.energy, counters.fullness);
				bar.clipRect = new Rectangle(0, 0, barWidth * lowestCounter, bar.height);
				
				hungerIcon.visible = counters.mustEat;
				sleepIcon.visible = !hungerIcon.visible && counters.mustRest;
				
				bubble.y = bar.y - bubble.height - 2;
				bubble.visible = shouldShow && (hungerIcon.visible || sleepIcon.visible);
			}
			
			
			
			bar.y = UI_Y - 3;
			barBg.y = bar.y;
			
			if(shouldShow){
				if (selected){
					updateSelectedUI();
				}else{
					updateUnselectedUI();
				}
			}
			
			hungerIcon.x = bubble.x + 3.2;
			hungerIcon.y = bubble.y + 2;
			sleepIcon.x = bubble.x + 3.4;
			sleepIcon.y = bubble.y + 3;
		}
		
		
		
		private function showElements(shouldShow:Boolean, selected:Boolean):void{
			var piggy:IsoSprite = gameObject.getIsoSprite();
			piggy.getChildByName("piggy_balloon").visible = shouldShow && selected;
			barHungry.visible = barSleep.visible = shouldShow && selected;
			
			for each (var l:Label in labels){
				l.visible = shouldShow && selected;
			}
			bar.visible = barBg.visible = shouldShow && !selected;
		}
		
		
		private function fillLabels():void{
			/*
			carryingLabel.text = "Carrying: " + (inventory.holding == null ? "Nothing" : inventory.holding)
			sleepLabel.text = formatTime(counters.energy / counters.energyDrain) + " until sleep";
			eatLabel.text = formatTime(counters.fullness / counters.fullnessDrain) + " until hungry";
			speedLabel.text = "Speed bonus: " + Math.round(properties.speed * 100 - 100) + "%";
			strengthLabel.text = "Strength bonus: " + Math.round(properties.strength * 100 - 100) + "%";
			cookingLabel.text = "Cooking bonus: " + Math.round(properties.cooking * 100 - 100) + "%";
			relaxationLabel.text = "Relaxation bonus: " + Math.round(properties.relaxing * 100 - 100) + "%";
			
			speedLabel.visible = speedLabel.visible && properties.speed != 1;
			strengthLabel.visible = strengthLabel.visible && properties.strength != 1;
			cookingLabel.visible = cookingLabel.visible && properties.cooking!= 1;
			relaxationLabel.visible = relaxationLabel.visible && properties.relaxing != 1;
			
			
			for (var i:int = 0; i < labels.length; ++i){
				if (labels[i].visible) labels[i].validate();
			}
			*/
		}
		
		private static function formatTime(s:Number):String
		{
			if (s < 60)
				return Math.floor(s) + "s";
			if (s < 60 * 60)
				return Math.floor(s / 60) + "m";
			return Math.floor(s / 60 / 60) + "h";
		}
		
		public override function onSelect():void
		{
			selected = true;
		}
		
		public override function onDeselect():void
		{
			selected = false;
		}
	}

}