package savethepiggies.game.behaviours
{
	
	
	public final class PiggyCounterBehaviour extends UpdatableBehaviour
	{
		private var _energy:Number = 1;
		private var _energyDrain:Number;
		
		private var _fullness:Number = 1;
		private var _fullnessDrain:Number;
		
		public function PiggyCounterBehaviour(id:uint)
		{
			super(id);
		}
		
		public override function activate():void
		{
			
		}
		
		public function setEnergyDrain(amount:Number):void{
			_energyDrain = amount;
		}
		public function setFullnessDrain(amount:Number):void{
			_fullnessDrain = amount;
		}
		
		public function changeEnergy(amount:Number):Boolean
		{
			_energy += amount;
			if (_energy < 0)
				_energy = 0;
			if (_energy > 1)
				_energy = 1;
			
			return mustRest;
		}
		
		public function changeFullness(amount:Number):Boolean
		{
			_fullness += amount;
			if (_fullness < 0)
				_fullness = 0;
			if (_fullness > 1)
				_fullness = 1;
			
			return mustEat;
		}
		
		public override function update(dt:Number):void
		{
			changeEnergy(dt * -_energyDrain);
			changeFullness(dt * -_fullnessDrain);
		}
		
		public function get energyDrain():Number { return _energyDrain; }
		public function get energy():Number { return _energy; }
		public function set energy(val:Number):void { _energy = val; }
		
		public function get fullnessDrain():Number { return _fullnessDrain; }
		public function get fullness():Number { return _fullness; }
		public function set fullness(val:Number):void { _fullness = val; }
		
		public function get mustRest():Boolean
		{
			return energy == 0;
		}
		
		public function get mustEat():Boolean
		{
			return fullness == 0;
		}
		
	}

}