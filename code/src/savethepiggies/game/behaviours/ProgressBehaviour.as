package savethepiggies.game.behaviours 
{
	import savethepiggies.game.Behaviour;
	
	import starling.display.Image;
	import starling.utils.AssetManager;
	
	public final class ProgressBehaviour extends Behaviour 
	{
		public var assetManager:AssetManager;
		public var useColors:Boolean;
		
		private var bg:Image;
		private var green:Image;
		private var yellow:Image;
		private var red:Image;
		
		public function ProgressBehaviour(id:uint) 
		{
			super(id);
		}
		
		public override function activate():void
		{
			bg = new Image(assetManager.getTexture('progress-back'));
			bg.x = -20;
			bg.y = -6;
			gameObject.getIsoSprite().addChild(bg);
			
			green = new Image(assetManager.getTexture('progress-green'));
			green.x = -20;
			green.y = -6;
			green.alpha = 0;
			gameObject.getIsoSprite().addChild(green);
			
			yellow = new Image(assetManager.getTexture('progress-yellow'));
			yellow.x = -20;
			yellow.y = -6;
			green.alpha = 0;
			gameObject.getIsoSprite().addChild(yellow);
			
			red = new Image(assetManager.getTexture('progress-red'));
			red.x = -20;
			red.y = -6;
			red.alpha = 0;
			gameObject.getIsoSprite().addChild(red);
		}
		public override function cleanup():void
		{
			
		}
		
		public function show():void{
			if(bg) bg.visible = green.visible = yellow.visible = red.visible = true;
		}
		public function hide():void{
			if(bg) bg.visible = green.visible = yellow.visible = red.visible = false;
		}
		
		public function update(value:Number):void{
			green.scaleX = yellow.scaleX = red.scaleX = value;
			if(useColors){
				red.alpha = value == 1 ? 1 : 0;
				yellow.alpha = value >= .8 && value < 1 ? 1 : 0;
				green.alpha = value < .8 ? 1 : 0;
			}else{
				green.alpha = 1;
				yellow.alpha = red.alpha = 0;
			}
		}
		
	}
	
}

