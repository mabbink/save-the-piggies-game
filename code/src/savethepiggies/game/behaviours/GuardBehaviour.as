package savethepiggies.game.behaviours 
{
	import org.gestouch.events.GestureEvent;
	import org.gestouch.gestures.TapGesture;
	
	import savethepiggies.game.Behaviour;
	import savethepiggies.game.SelectionGroup;
	import savethepiggies.game.SelectionListener;
	
	import starling.display.DisplayObject;
	
	public final class GuardBehaviour extends Behaviour implements SelectionListener 
	{
		private var range_:int;
		private var piggyRange_:int;
		private var gesture_:TapGesture;
		private var selectionGroup_:SelectionGroup;
		private var x_:int;
		private var y_:int;
		
		public function GuardBehaviour(id:uint, range:uint, selectionGroup:SelectionGroup) 
		{
			super(id);
			
			this.range_ = range;
			this.piggyRange_ = 5;
			this.selectionGroup_ = selectionGroup;
		}
		
		public function setPiggyRange(piggyRange:int):void
		{
			this.piggyRange_ = piggyRange;
			updateRange();
		}
		
		private function updateRange():void
		{
			var obj:DisplayObject = gameObject.getIsoSprite().getChildByName("range");
			var r:Number = range
			var x:int = 0;
			var y:int = 5;
			obj.scaleX = obj.scaleY = r;
			x = Math.round(-obj.width*0.5);
			y = Math.round(-obj.height*0.5) + 15;
			obj.x = x;
			obj.y = y;
		}
		
		public function get range():Number
		{
			var factor:Number = (this.piggyRange_-5)*0.1;
			var total:Number = this.range_ + factor * this.range_;
			return total;
		}
		
		public override function cleanup():void
		{
			selectionGroup_.deregister(this);
			gesture_.dispose();
		}
		
		private function onTap(e:*):void
		{
			var obj:DisplayObject = gameObject.getIsoSprite().getChildByName("range");
			if(!obj.visible)
			{
				updateRange();
			}
			obj.visible = !obj.visible;
		}
		
		public override function activate():void
		{
			selectionGroup_.register(this);
			gesture_ = new TapGesture(gameObject.getIsoSprite().getChildByName("showRange"));
			gesture_.addEventListener(GestureEvent.GESTURE_RECOGNIZED, onTap);
			
			var obj:DisplayObject = gameObject.getIsoSprite().getChildByName("range");
			this.x_ = obj.x;
			this.y_ = obj.y;
		}
		
		public function selected(id:uint):void 
		{
			var obj:DisplayObject = gameObject.getIsoSprite().getChildByName("showRange");
			if(obj)
			{
				obj.visible = (id == gameObject.id);
			}
			
			obj = gameObject.getIsoSprite().getChildByName("range");
			if(obj)
			{
				obj.visible = false;
			}
		}
	}

}