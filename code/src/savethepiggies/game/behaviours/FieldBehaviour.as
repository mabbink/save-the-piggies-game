package savethepiggies.game.behaviours 
{
	import savethepiggies.game.GameObject;
	import savethepiggies.game.IsoSprite;
	
	import starling.utils.AssetManager;
	
	public final class FieldBehaviour extends UpdatableBehaviour 
	{
		public var assetManager:AssetManager;
		
		public var levels:int;
		public var mustPlant:Boolean;
		public var produceCount:int;
		public var growInterval:Number;
		public var harvestLength:Number;
		public var givesEnergy:Number;
		
		private var produceType:Object;
		private var level:int;
		private var produceAvailable:int;
		private var planted:Boolean;
		private var t:Number;
		
		public function FieldBehaviour(id:uint, produceType:Object, harvestLength:Number) 
		{
			super(id);
			
			this.produceType = produceType;
			this.harvestLength = harvestLength;
		}
		
		public override function activate():void
		{
			reset();
			level = levels - 1;
			planted = true;
			setGraphics();
			
			var b:ProgressBehaviour = gameObject.getBehaviourByType(ProgressBehaviour);
			if(b) b.hide();
		}
		
		public function getState():Object
		{
			return {
				level:				level,
				produceAvailable:	produceAvailable,
				planted:			planted,
				t:					t,
				produceType:		produceType,
				givesEnergy:		givesEnergy
			};
		}
		
		public function setState(state:Object):void
		{
			level 				= state.level;
			produceAvailable 	= state.produceAvailable;
			planted 			= state.planted;
			produceType 		= produceType;
			t 					= state.t;
			givesEnergy 		= state.givesEnergy;
			
			setGraphics();
		}
		
		private function reset():void
		{
			level = 0;
			t = 0;
			planted = false;
			produceAvailable = produceCount;
		}
		
		public function get needsPlanting():Boolean
		{
			return mustPlant && !planted;
		}
		
		public function plant():Boolean
		{
			if (!needsPlanting) return false;
			planted = true;
			level = 0;
			setGraphics();
			return true;
		}
		
		public override function update(dt:Number):void
		{
			t += dt;
			if (t >= growInterval && (!mustPlant || (mustPlant && planted)))
			{
				t = 0;
				level = Math.min(level + 1, levels - 1);
				setGraphics();
			}
		}
		
		public function get isProduceAvailable():Boolean
		{
			return level == levels - 1;
		}
		
		public function takeProduce(pPiggy:GameObject):String
		{
			if (!isProduceAvailable) return null;
			
			--produceAvailable;
			
			if (produceAvailable <= 0)
				reset();
				
			setGraphics();
			
			if(produceType is String){
				var ingredient:String = produceType as String;
			}else{
				var ingredient:String = produceType[Math.floor(Math.random() * produceType.length)];
			}
			
			PiggyTasksBehaviour(pPiggy.getBehaviourByType(PiggyTasksBehaviour)).showIcon(ingredient);
			
			return ingredient;
		}
		
		
		private function setGraphics():void
		{
			var sprite:IsoSprite = gameObject.getIsoSprite();
			
			// Hide all
			for (var i:int = 0; i < levels; ++i)
				sprite.getChildByName("level" + i).visible = false;
			
			// Show current level
			if ((!mustPlant || (mustPlant && planted)) && level != -1)
			{
				sprite.getChildByName("level" + level).visible = true;
			}
			
			var b:ProgressBehaviour = gameObject.getBehaviourByType(ProgressBehaviour);
			if(b){
				if(level == levels-1){
					b.hide();
				}else{
					b.show();
					b.update(level/(levels-1));
				}
			}
		}
	}

}