package savethepiggies.game.behaviours 
{
	public final class ProducerPersistBehaviour extends PersistBehaviour 
	{
		
		public function ProducerPersistBehaviour(id:uint) 
		{
			super(id);
			
		}
		
		public override function save():Object
		{
			var producer:ProducerBehaviour = gameObject.getBehaviourByType(ProducerBehaviour);
			
			return producer.getState();
		}
		
		public override function load(properties:Object):void
		{
			var producer:ProducerBehaviour = gameObject.getBehaviourByType(ProducerBehaviour);
			
			return producer.setState(properties);
		}
		
	}

}