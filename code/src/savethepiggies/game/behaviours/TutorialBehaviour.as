package savethepiggies.game.behaviours 
{
	import org.as3commons.logging.api.ILogger;
	import org.as3commons.logging.api.getLogger;
	import org.gestouch.events.GestureEvent;
	import org.gestouch.gestures.TapGesture;
	
	import savethepiggies.game.Behaviour;
	import savethepiggies.game.events.TutorialEvent;
	
	import starling.core.Starling;
	
	public final class TutorialBehaviour extends Behaviour 
	{
		private static const logger:ILogger = getLogger(UpgradableBehaviour);
		
		private var gesture:TapGesture;
		
		public function TutorialBehaviour(id:uint) 
		{
			super(id);
		}
		
		public override function cleanup():void
		{
			gesture.dispose();
		}
		
		public override function activate():void
		{
			gesture = new TapGesture(gameObject.getIsoSprite());
			gesture.addEventListener(GestureEvent.GESTURE_RECOGNIZED, onclick);
		}
		
		private function onclick(e:*):void
		{
			Starling.current.stage.dispatchEvent(new TutorialEvent({action: 'goNextStep'}, TutorialEvent.TUTORIAL_EVENT));
		}
		
	}

}