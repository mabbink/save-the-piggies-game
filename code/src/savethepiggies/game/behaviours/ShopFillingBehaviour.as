package savethepiggies.game.behaviours 
{
	import flash.events.TimerEvent;
	import flash.utils.Timer;
	
	import savethepiggies.game.Behaviour;
	import savethepiggies.game.Storage;
	
	public final class ShopFillingBehaviour extends Behaviour
	{
		public var storage:Storage;
		private var timer:Timer;
		
		public function ShopFillingBehaviour(id:uint) 
		{
			super(id);
		}
		
		public override function cleanup():void
		{
			timer.removeEventListener(TimerEvent.TIMER, check);
			timer.stop();
		}
		
		public override function activate():void
		{
			timer = new Timer(1000);
			timer.addEventListener(TimerEvent.TIMER, check);
			timer.start();
		}
		
		private function check(pEvent:TimerEvent):void
		{
			var sellableItems:Array = storage.getSellableOptions();
			var amount = 0;
			for each(var item:Object in sellableItems){
				amount += item.amount;
			}
			
			var show:int = 0;
			if(amount > 0) show = 1;
			if(amount > 20) show = 2;
			if(amount > 40) show = 3;
			if(amount > 60) show = 4;
			
			for(var i:int = 0; i < 5; i++){
				if(i == show){
					gameObject.getIsoSpriteWithLayerName('below').getChildByName("shopOpenStorage"+i).visible = true;
				}else{
					gameObject.getIsoSpriteWithLayerName('below').getChildByName("shopOpenStorage"+i).visible = false;
				}
			}
		}
		
	}
	
}

