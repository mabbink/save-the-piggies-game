package savethepiggies.game.behaviours 
{
	import org.as3commons.logging.api.ILogger;
	import org.as3commons.logging.api.getLogger;
	import org.gestouch.events.GestureEvent;
	import org.gestouch.gestures.TapGesture;
	
	import savethepiggies.game.Behaviour;
	import savethepiggies.game.SelectionGroup;
	import savethepiggies.game.SelectionListener;
	
	import starling.display.DisplayObject;
	
	public final class MovableBehaviour extends Behaviour implements SelectionListener 
	{
		private static const logger:ILogger = getLogger(MovableBehaviour);
		
		public var offsetI:int, offsetJ:int;
		public var placeOnTopOf:Array;
		public var selectionGroup:SelectionGroup;
		public var moveCallback:Function;
		public var description:String;
		public var w:uint;
		public var h:uint;
		
		private var gesture:TapGesture;
		
		public function MovableBehaviour(id:uint) 
		{
			super(id);
		}
		
		public override function cleanup():void
		{
			selectionGroup.deregister(this);
			
			gesture.dispose();
		}
		
		public override function activate():void
		{
			selectionGroup.register(this);
			
			gesture = new TapGesture(gameObject.getIsoSprite().getChildByName("move"));
			gesture.addEventListener(GestureEvent.GESTURE_RECOGNIZED, onMoveClick);
		}
		
		private function onMoveClick(e:*):void
		{
			moveCallback(gameObject, offsetI, offsetJ, placeOnTopOf, w, h);
		}
		
		public function selected(id:uint):void 
		{
			var obj:DisplayObject = gameObject.getIsoSprite().getChildByName("move");
			if(obj)
				obj.visible = id == gameObject.id;
		}
		
	}

}