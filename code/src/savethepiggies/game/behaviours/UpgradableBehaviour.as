package savethepiggies.game.behaviours 
{
	import org.as3commons.logging.api.ILogger;
	import org.as3commons.logging.api.getLogger;
	import org.gestouch.events.GestureEvent;
	import org.gestouch.gestures.TapGesture;
	
	import savethepiggies.game.Behaviour;
	import savethepiggies.game.SelectionGroup;
	import savethepiggies.game.SelectionListener;
	
	public final class UpgradableBehaviour extends Behaviour implements SelectionListener 
	{
		private static const logger:ILogger = getLogger(UpgradableBehaviour);
		
		public var offsetI:int, offsetJ:int;
		public var recipeName:String;
		public var selectionGroup:SelectionGroup;
		public var upgradeCallback:Function;
		public var cost:uint;
		public var description:String;
		
		private var gesture:TapGesture;
		
		public function UpgradableBehaviour(id:uint) 
		{
			super(id);
		}
		
		public override function cleanup():void
		{
			selectionGroup.deregister(this);
			
			gesture.dispose();
		}
		
		public override function activate():void
		{
			selectionGroup.register(this);
			
			gesture = new TapGesture(gameObject.getIsoSprite().getChildByName("upgrade"));
			gesture.addEventListener(GestureEvent.GESTURE_RECOGNIZED, onUpgradeClick);
		}
		
		private function onUpgradeClick(e:*):void
		{
			upgradeCallback(gameObject, offsetI, offsetJ, recipeName, cost, description);
		}
		
		public function selected(id:uint):void 
		{
			gameObject.getIsoSprite().getChildByName("upgrade").visible = id == gameObject.id;
		}
		
	}

}