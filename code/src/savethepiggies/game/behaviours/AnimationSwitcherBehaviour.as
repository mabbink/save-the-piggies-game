package savethepiggies.game.behaviours 
{
	import flash.geom.Point;
	
	import savethepiggies.game.Behaviour;
	import savethepiggies.game.IsoSprite;
	
	import starling.display.MovieClip;
	
	public final class AnimationSwitcherBehaviour extends Behaviour 
	{
		private var sprite:IsoSprite;
		
		public function AnimationSwitcherBehaviour(id:uint) 
		{
			super(id);
		}
		
		public override function activate():void
		{
			sprite = gameObject.getIsoSprite();
			if(gameObject.getTags().piggy) switchToAnim("questioning_ul");
		}
		
		public function switchToAnim(name:String):MovieClip
		{
			hideAllAnims();
			var choice:MovieClip = MovieClip(sprite.getChildByName(name));
			choice.visible = true;
			return choice;
		}
		
		private static const DIR_MAP:Array = [
			"ur",
			"dr",
			"dl",
			"ul"
		];
		public function getPiggyAnimName(prefix:String, direction:String):String
		{
			var hasInventory:Boolean = gameObject.getBehaviourByType(PiggyInventoryBehaviour).holding != null;
			
			var dir:String = "_" + direction;
			if ( -1 == DIR_MAP.indexOf(direction)) dir = "_ur";
			var inventory:String = hasInventory ? "_loaded" : "";
			
			return prefix + dir + inventory;
		}
		
		public function getDirectionalAnimName(prefix:String, dir:Point, loaded:Boolean = false):String
		{
			var inventory:String = loaded ? "_loaded" : "";
			
			return prefix + chooseDir(dir) + inventory;
		}
		
		private function chooseDir(dir:Point):String
		{
			var dx:Number = dir.x;
			var dy:Number = dir.y;
			
			if (Math.abs(dx) > Math.abs(dy))
				return dx < 0 ? "_ul" : "_dr";
			else
				return dy < 0 ? "_ur" : "_dl";
		}
		
		private function hideAllAnims():void
		{
			for (var i:int = 0; i < sprite.numChildren; ++i)
			{
				var child:* = sprite.getChildAt(i);
				if (child is MovieClip) child.visible = false;
			}
		}
	}

}