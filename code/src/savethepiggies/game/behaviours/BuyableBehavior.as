package savethepiggies.game.behaviours 
{
	import org.as3commons.logging.api.ILogger;
	import org.as3commons.logging.api.getLogger;
	
	import savethepiggies.game.Behaviour;
	import savethepiggies.game.SelectionGroup;
	import savethepiggies.game.SelectionListener;
	
	public final class BuyableBehavior extends Behaviour implements SelectionListener 
	{
		private static const logger:ILogger = getLogger(BuyableBehavior);
		
		public var recipe:Object;
		public var offsetI:int, offsetJ:int;
		public var selectionGroup:SelectionGroup;
		public var buyCallback:Function;
		
		public function BuyableBehavior(id:uint) 
		{
			super(id);
		}
		
		public override function cleanup():void
		{
			selectionGroup.deregister(this);
		}
		
		public override function activate():void
		{
			selectionGroup.register(this);
		}
		
		public function selected(id:uint):void
		{
			if(id == gameObject.id)
			{
				buyCallback( gameObject, offsetI, offsetJ, recipe);
			}
		}
		
	}
}