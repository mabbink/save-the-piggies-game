package savethepiggies.game.behaviours 
{
	import savethepiggies.game.IsoSprite;
	import savethepiggies.game.Storage;
	
	public class ProducerBehaviour extends UpdatableBehaviour 
	{
		private var workTimeLeft:Number;
		private var inputSpace:int;
		private var inputQueue:int;
		public var produceTime:Number;
		public var storage:Storage;
		
		
		public function ProducerBehaviour(
			id:uint,
			inputSpace:int,
			produceTime:Number
		)
		{
			super(id);
			
			workTimeLeft = 0;
			this.inputSpace = inputSpace;
			inputQueue = 0;
			this.produceTime = produceTime;
		}
		
		public override function update(dt:Number):void
		{
			if (isWorking)
			{
				workTimeLeft -= dt;
				
				// Finished work?
				if (!isWorking)
				{
					while (inputQueue > 0)
					{
						storeItem();
						--inputQueue;
					}
				}
			}else{
				if(!canBeFilled){
					while (inputQueue > 0)
					{
						storeItem();
						--inputQueue;
					}
				}
			}
		}
		
		protected function get itemType():String { return null; }
		
		protected function storeItem():Boolean
		{
			var s:IsoSprite = gameObject.getIsoSprite();
			return storage.store(itemType, s.offsetI, s.offsetJ);
		}
		
		private function startWorking():void
		{
			workTimeLeft = produceTime;
		}
		
		public function get canBeFilled():Boolean
		{
			return inputQueue < inputSpace;
		}
		
		public function fill(produceTimeBonus:Number):Boolean
		{
			if (!canBeFilled) return false;
			
			++inputQueue;
			if (inputQueue == inputSpace){
				startWorking();
				workTimeLeft -= produceTimeBonus;
			}
			
			return true;
		}
		
		public function getState():Object
		{
			return {
				workTimeLeft:workTimeLeft,
				inputSpace:inputSpace,
				inputQueue:inputQueue
			};
		}
		
		public function setState(state:Object):void
		{
			workTimeLeft = state.workTimeLeft;
			inputSpace = state.inputSpace;
			inputQueue = state.inputQueue;
		}
		
		protected function get isWorking():Boolean { 
			return workTimeLeft > 0;
		}
	}

}