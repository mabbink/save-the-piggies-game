package savethepiggies.game.behaviours 
{
	public final class StoragePersistBehaviour extends PersistBehaviour 
	{
		
		public function StoragePersistBehaviour(id:uint) 
		{
			super(id);
		}
		
		public override function save():Object
		{
			var storage:StorageBehaviour = gameObject.getBehaviourByType(StorageBehaviour);
			
			return storage.getState();
		}
		
		public override function load(properties:Object):void
		{
			var storage:StorageBehaviour = gameObject.getBehaviourByType(StorageBehaviour);
			
			storage.setState(properties);
		}
		
	}

}