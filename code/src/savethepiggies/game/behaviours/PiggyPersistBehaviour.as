package savethepiggies.game.behaviours {
	import savethepiggies.game.IsoSprite;
	public final class PiggyPersistBehaviour extends PersistBehaviour 
	{
		private var taskToken:Object;
		private var holding:Array;
		
		public function PiggyPersistBehaviour(id:uint) 
		{
			super(id);
		}
		
		public override function save():Object
		{
			var s:IsoSprite = gameObject.getIsoSprite();
			var tasks:PiggyTasksBehaviour = gameObject.getBehaviourByType(PiggyTasksBehaviour);
			var inventory:PiggyInventoryBehaviour = gameObject.getBehaviourByType(PiggyInventoryBehaviour);
			var counters:PiggyCounterBehaviour = gameObject.getBehaviourByType(PiggyCounterBehaviour);
			var properties:PiggyPropertiesBehaviour = gameObject.getBehaviourByType(PiggyPropertiesBehaviour);
			
			return {
				i:s.offsetI,
				j:s.offsetJ,
				taskToken:tasks.getTaskToken(),
				holding:inventory.holding,
				energy:counters.energy,
				fullness:counters.fullness,
				properties:properties.save()
			};
		}
		
		public override function load(properties:Object):void
		{
			var s:IsoSprite = gameObject.getIsoSprite();
			var counters:PiggyCounterBehaviour = gameObject.getBehaviourByType(PiggyCounterBehaviour);
			var propertiesBehaviour:PiggyPropertiesBehaviour = gameObject.getBehaviourByType(PiggyPropertiesBehaviour);
			
			s.moveTo(properties.i, properties.j);
			counters.energy = properties.energy;
			counters.fullness = properties.fullness;
			
			taskToken = properties.taskToken;
			holding = properties.holding;
			propertiesBehaviour.load(properties.properties);
		}
		
		public override function afterLoad():void
		{
			var tasks:PiggyTasksBehaviour = gameObject.getBehaviourByType(PiggyTasksBehaviour);
			var inventory:PiggyInventoryBehaviour = gameObject.getBehaviourByType(PiggyInventoryBehaviour);
			var sprite:IsoSprite = gameObject.getIsoSprite();
			
			if (taskToken)
			{
				
				if (taskToken.hasOwnProperty("forceI") && taskToken.hasOwnProperty("forceJ"))
					sprite.moveTo(taskToken.forceI, taskToken.forceJ);
				
				if (taskToken.hasOwnProperty("targetId") && taskToken.hasOwnProperty("type"))
					tasks.buildAndAssignTask(
						taskToken.targetId,
						taskToken.type,
						taskToken.inputItems,
						taskToken.outputItems
					)
			}
			
			inventory.holding = holding;
		}
	}

}