package savethepiggies.game.behaviours {
	import org.gestouch.events.GestureEvent;
	import org.gestouch.gestures.Gesture;
	import org.gestouch.gestures.TapGesture;
	
	import savethepiggies.game.Behaviour;
	import savethepiggies.game.SelectionGroup;
	import savethepiggies.game.SelectionListener;
	import savethepiggies.game.events.TutorialEvent;
	import savethepiggies.sound.SoundManager;
	
	import starling.core.Starling;
	
	public final class TappableBehaviour extends Behaviour implements SelectionListener
	{
		public var selectionGroup:SelectionGroup;
		
		private var _isSelected:Boolean;
		private var gesture:Gesture;
		private var _sound:String;
		
		public function TappableBehaviour(
			id:uint,
			sound:String
		) 
		{
			_sound = sound;
			super(id);
		}
		
		public override function cleanup():void
		{
			selectionGroup.deregister(this);
			gesture.dispose();
			
			super.cleanup();
		}
		
		public override function activate():void
		{
			super.activate();
			
			gesture = new TapGesture(gameObject.getIsoSprite());
			gesture.addEventListener(GestureEvent.GESTURE_RECOGNIZED, onTapped);
			selectionGroup.register(this);
		}
		
		public function onTapped(e:* = null):void
		{
			selectionGroup.select(id);
		}
		
		public function selected(id:uint):void
		{
			if (this.id === id && !_isSelected){
				gameObject.onSelect();
				if(_sound) SoundManager.playSound({id:_sound});
			}else{
			if (this.id !== id && _isSelected)
				gameObject.onDeselect();
			}
		}
		
		public override function onSelect():void
		{
			_isSelected = true;
			
			var data:Object = {
				action: 'select',
				tags: this.gameObject.getTags()
			}
			Starling.current.stage.dispatchEvent(new TutorialEvent(data, TutorialEvent.TUTORIAL_EVENT));
		}
		
		public override function onDeselect():void
		{
			_isSelected = false;
			
			var data:Object = {
				action: 'deselect',
				tags: this.gameObject.getTags()
			}
			Starling.current.stage.dispatchEvent(new TutorialEvent(data, TutorialEvent.TUTORIAL_EVENT));
		}
		
		public function get isSelected():Boolean { return _isSelected; }
	}
}