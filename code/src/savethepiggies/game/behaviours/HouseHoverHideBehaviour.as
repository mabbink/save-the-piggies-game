package savethepiggies.game.behaviours 
{
	import flash.geom.Point;
	import flash.geom.Rectangle;
	
	import org.as3commons.logging.api.ILogger;
	import org.as3commons.logging.api.getLogger;
	
	import savethepiggies.game.Behaviour;
	import savethepiggies.map.Map;
	
	import starling.display.DisplayObject;
	import starling.events.Touch;
	import starling.events.TouchEvent;
	import starling.events.TouchPhase;
	
	public final class HouseHoverHideBehaviour extends Behaviour 
	{
		private static const logger:ILogger = getLogger(HoverHideBehaviour);
		
		public var mapLayer:DisplayObject;
		public var map:Map;
		public var speed:Number;
		public var bounds:Rectangle;
		
		public function HouseHoverHideBehaviour(id:uint) 
		{
			super(id);
		}
		
		public override function cleanup():void
		{
			super.cleanup();
			
			mapLayer.removeEventListener(TouchEvent.TOUCH, onTouch);
		}
		
		public override function activate():void
		{
			super.activate();
			
			mapLayer.addEventListener(TouchEvent.TOUCH, onTouch);
		}
		
		private var touchPos:Point = new Point;
		private var isoPos:Point = new Point;
		private function onTouch(e:TouchEvent):void
		{
			var touch:Touch = e.getTouch(mapLayer, TouchPhase.HOVER);
			if (touch)
			{
				touch.getLocation(mapLayer, touchPos);
				map.cartesianToIsometric(touchPos.x, touchPos.y, isoPos);
				
				var sprite:DisplayObject = gameObject.getIsoSprite().getChildByName("overlay");
				if(sprite) sprite.visible = !bounds.containsPoint(isoPos);
				
				sprite = gameObject.getIsoSprite().getChildByName("overlayBottom");
				if(sprite) sprite.visible = bounds.containsPoint(isoPos);
				
				
			}
		}
	}
	
}


