package savethepiggies.game.behaviours 
{
	import starling.display.MovieClip;
	
	public final class PrepperProducerBehaviour extends ProducerBehaviour 
	{
		private var stone0:MovieClip;
		private var stone1:MovieClip;
		
		public function PrepperProducerBehaviour(id:uint, inputSpace:int, produceTime:Number) 
		{
			super(id, inputSpace, produceTime);
		}
		
		public override function activate():void
		{
			super.activate();
			
			stone0 = MovieClip(gameObject.getIsoSprite().getChildByName("stone0"));
			stone1 = MovieClip(gameObject.getIsoSprite().getChildByName("stone1"));
			stone1.currentFrame = stone1.numFrames / 2;
		}
		
		protected override function get itemType():String { return ApplicationVars.PREPPED_LUPIN; }
		
		public override function update(dt:Number):void
		{
			super.update(dt);
			
			if (isWorking)
			{
				stone0.advanceTime(dt);
				stone1.advanceTime(dt);
			}
		}
	}

}