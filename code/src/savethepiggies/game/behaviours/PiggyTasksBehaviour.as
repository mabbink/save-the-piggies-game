package savethepiggies.game.behaviours 
{
	import feathers.controls.Label;
	import feathers.text.BitmapFontTextFormat;
	
	import godmode.core.BehaviorTask;
	import godmode.core.BehaviorTree;
	import godmode.task.NoOpTask;
	
	import org.as3commons.logging.api.ILogger;
	import org.as3commons.logging.api.getLogger;
	import org.gestouch.events.GestureEvent;
	import org.gestouch.gestures.TapGesture;
	
	import savethepiggies.GlobalContext;
	import savethepiggies.game.GameContext;
	import savethepiggies.game.GameObject;
	import savethepiggies.game.TaskBuilder;
	import savethepiggies.game.TaskTarget;
	import savethepiggies.game.events.PiggiesEvent;
	import savethepiggies.game.events.ShopEvent;
	import savethepiggies.game.events.TaskTargetSelectedEvent;
	import savethepiggies.game.events.TutorialEvent;
	import savethepiggies.sound.SoundManager;
	
	import starling.animation.Transitions;
	import starling.animation.Tween;
	import starling.core.Starling;
	import starling.display.DisplayObject;
	import starling.display.Image;
	import starling.events.Event;
	import starling.textures.Texture;
	
	public final class PiggyTasksBehaviour extends UpdatableBehaviour 
	{
		public static const CONFUSED_MISSING_ITEM:String 		= 'missing item';
		public static const CONFUSED_NO_STORAGE_FOUND:String 	= 'no storage found';
		public static const CONFUSED_STORAGE_IS_FULL:String 	= 'storage is full';
		public static const CONFUSED_CANT_FIND_FIELD:String		= 'cant find field';
		
		private static const logger:ILogger = getLogger(PiggyTasksBehaviour);
		
		public var gameContext:GameContext;
		public var globalContext:GlobalContext;
		
		private var selected:Boolean;
		private var tree:BehaviorTree;
		private var taskBuilder:TaskBuilder;
		
		private var resting:Boolean;
		private var eating:Boolean;
		private var relaxing:Boolean;
		private var confused:Boolean;
		private var waiting:Boolean;
		
		private var taskToken:Object = null;
		
		private var taskSelectMenu:Object;
		private var lastTarget_:GameObject;
		
		public function PiggyTasksBehaviour(id:uint) 
		{
			super(id);
			taskSelectMenu =
			{
				displayObjects:new Array,
				labels:new Array,
				isoSprite:null
			};
		}
		
		public override function cleanup():void
		{
			super.cleanup();
			
			tree.dispose();
			
			gameContext.taskTargetManager.removeEventListener(TaskTargetSelectedEvent.TASK_TARGET_SELECTED, onTaskSelected);
			gameContext.gameObjects.removeEventListener(Event.REMOVED, onGameObjectRemoved);
		}
		
		public override function activate():void
		{
			super.activate();
			
			gameContext.taskTargetManager.addEventListener(TaskTargetSelectedEvent.TASK_TARGET_SELECTED, onTaskSelected);
			gameContext.gameObjects.addEventListener(Event.REMOVED, onGameObjectRemoved);
			
			tree = new BehaviorTree(
				NoOpTask.SUCCESS
			);
			
			var counters:PiggyCounterBehaviour = gameObject.getBehaviourByType(PiggyCounterBehaviour);
			var properties:PiggyPropertiesBehaviour = gameObject.getBehaviourByType(PiggyPropertiesBehaviour);
			
			counters.setEnergyDrain(ApplicationVars.PIGGY_ENERGY_DRAIN * (1.5-(properties.relaxation/8)));
			counters.setFullnessDrain(ApplicationVars.PIGGY_FULLNESS_DRAIN * (1.5-(properties.relaxation/8)));
			
			taskBuilder = new TaskBuilder(gameContext, gameObject, 
				counters.energyDrain,
				counters.fullnessDrain
			);
		}
		
		private function setTaskTree(root:BehaviorTask):void
		{
			tree.dispose();
			tree = new BehaviorTree(root);
		}
		
		
		
		private var evilGuyStruct:Object = 
		{
			doing:0,
			lastI:-1000.0,
			lastJ:-1000.0,
			lastCount:0,
			factory:null
		}
		
		
		public override function update(dt:Number):void
		{
			// Deplete energy and interrupt current task if we have to rest
			
			if(!relaxing){
				var counters:PiggyCounterBehaviour = gameObject.getBehaviourByType(PiggyCounterBehaviour);
				
				if (counters.mustRest && !resting){
					rest();
				}else if(counters.energy > 0.99 && resting){
					if(counters.mustEat){
						eat();
					}else if(!relaxing){
						relax();
					}
				}
				
				if(counters.mustEat && !eating && !resting){
					eat();
				}else if(counters.fullness > 0.99 && eating){
					if(counters.energy < .4){
						rest();
					}else if(!relaxing){
						relax();
					}
				}
			}
			
			if(lastTimeConfused) if(lastTimeConfused < new Date().time - 1000) isNolongerConfused();
			
			tree.update(dt);
		}
		
		
		private var lastTimeConfused:Number;
		public function isConfused(pReason:Object):void{
			if(pReason && !confused){
				
				confused = true;
				lastTimeConfused = new Date().time;
				
				if(!selected){
					gameObject.getIsoSprite().getChildByName("questionBalloon").visible = true;
					gameObject.getIsoSprite().getChildByName("questionIcon").visible = true;
				}
				
				var img:Image = Image(gameObject.getIsoSprite().getChildByName("questionIcon"));
				switch(pReason.reason){
					case PiggyTasksBehaviour.CONFUSED_MISSING_ITEM:
						img.texture = globalContext.assetManager.getTexture(pReason.item);
						break;
					
					case PiggyTasksBehaviour.CONFUSED_STORAGE_IS_FULL:
						img.texture = globalContext.assetManager.getTexture('noSpace');
						break;
					
					case PiggyTasksBehaviour.CONFUSED_NO_STORAGE_FOUND:
						img.texture = globalContext.assetManager.getTexture('noStorage');
						break;
					
					case PiggyTasksBehaviour.CONFUSED_CANT_FIND_FIELD:
						img.texture = globalContext.assetManager.getTexture('noField');
						break;
				}
				img.readjustSize();
			}
		}
		private function isNolongerConfused():void{
			confused = false;
			lastTimeConfused = null;
			
			gameObject.getIsoSprite().getChildByName("questionBalloon").visible = false;
			gameObject.getIsoSprite().getChildByName("questionIcon").visible = false;
		}
		
		public function rest():void{
			resting = true;
			taskToken = {resting:true};
			setTaskTree(
				taskBuilder.buildRestTask(taskToken)
			);
			
			SoundManager.playSound({id:'SO_Piggy_tired', overlap:false});
		}
		public function isResting():Boolean{
			return resting;
		}
		public function eat():void{
			
			taskToken = {eating:true};
			setTaskTree(
				taskBuilder.buildEatTask(taskToken)
			);
			eating = true;
			
			//SoundManager.playSound({id:'SO_Piggy_tired', overlap:false});
		}
		public function relax():void{
			
			trace('Relax:'+gameObject.recipeName);
			relaxing = true;
			taskToken = {relaxing:true};
			setTaskTree(
				taskBuilder.buildRelaxTask(taskToken)
			);
		}
		public function isWaiting():void{
			if(!waiting){
				waiting = true;
				var switcher:AnimationSwitcherBehaviour = gameObject.getBehaviourByType(AnimationSwitcherBehaviour);
				switcher.switchToAnim(switcher.getPiggyAnimName("questioning", 'ul')) 
			}
		}
		
		
		
		private var iconTween:Tween;
		private var iconBgTween:Tween;
		public function showIcon(pIngredient:String):void{
			var icon:Image = gameObject.getIsoSprite().getChildByName('takeIcon') as Image;
			icon.texture = globalContext.assetManager.getTexture(pIngredient);
			icon.readjustSize();
			icon.visible = true;
			icon.y = -15;
			icon.alpha = 1;
			
			if(!iconTween) {
				iconTween = new Tween(icon, 5, Transitions.EASE_OUT);
			}else{
				iconTween.reset(icon, 5, Transitions.EASE_OUT);
			}
			iconTween.animate('y', -23);
			iconTween.animate('alpha', 0);
			Starling.juggler.add(iconTween);
			
			
			var bg:Image = gameObject.getIsoSprite().getChildByName('takeIconBg') as Image;
			bg.visible = true;
			bg.x = -5;
			bg.y = -17;
			bg.alpha = 1;
			
			if(!iconBgTween) {
				iconBgTween = new Tween(bg, 5, Transitions.EASE_OUT);
			}else{
				iconBgTween.reset(bg, 5, Transitions.EASE_OUT);
			}
			iconBgTween.animate('y', -25);
			iconBgTween.animate('alpha', 0);
			Starling.juggler.add(iconBgTween);
		}
		
		
		
		public override function onSelect():void
		{
			selected = true;
			
			gameContext.taskTargetManager.showByFilter(function(t:TaskTarget):Boolean {
				switch (t.type)
				{
					case "cook":
					case "sell":
					case "useField":
					case "bake":
					case "prep":
					case "mudRelax":
					case "hayRelax":
					case "guard":
						return true;
					
					default:
						return false;
				}
			});
			
			if(confused){
				gameObject.getIsoSprite().getChildByName("questionBalloon").visible = false;
				gameObject.getIsoSprite().getChildByName("questionIcon").visible = false;
			}
		}
		
		private function cleanTaskSelect():void
		{
			if(taskSelectMenu.isoSprite != null)
			{
				var a:Array;
				a = taskSelectMenu.displayObjects as Array;
				while(a.length)
				{
					a[0].visible = false;
					a.shift();
				}
				a = taskSelectMenu.labels as Array;
				while(a.length)
				{
					a[0].visible = false;
					a.shift();
				}
				taskSelectMenu.isoSprite = null;
			}
		}
		
		public override function onDeselect():void
		{
			selected = false;
			
			if(confused){
				gameObject.getIsoSprite().getChildByName("questionBalloon").visible = true;
				gameObject.getIsoSprite().getChildByName("questionIcon").visible = true;
			}
			cleanTaskSelect();
		}
		
		private function onTaskSelected(e:TaskTargetSelectedEvent):void
		{
			if (selected)
			{
				e.stopImmediatePropagation();
				var taskTarget:TaskTarget = e.taskTarget
				
				var target:GameObject = gameContext.gameObjects.getById(taskTarget.targetId) as GameObject;
				
				if (!target)
					logger.error("Task target game object with ID {0} can't be found...", [taskTarget.targetId]);
				else
				{
					var haveMenu:Boolean = false;
					
					var targetName:String = target.recipeName;
					targetName = targetName.toLowerCase();
					var items:Array = globalContext.assetManager.getObject("shop-items").items as Array;
					for each (var item:Object in items)
					{
						
						var name:String = item.target;
						name = name.toLowerCase();
						if(targetName.indexOf(name) != -1)
						{
							var recipes:Array = item.recipes as Array;
							haveMenu = ( recipes.length ? true : false);
						}
					}
					
					cleanTaskSelect();
					
					if(haveMenu)
					{
						
						
						var map:Object = new Object();
						var behaviour:StorageBehaviour = gameContext.gameObjects.getSingleByTag("storage").getBehaviourByType(StorageBehaviour);
						
						var baseX:int = -39;
						var baseY:int = -26;
						var yOffset:int = 12;
						var width:int = 80;
						
						//show the backgrounds for the items
						taskSelectMenu.isoSprite = target.getIsoSprite();
						
						for(var i:int = 0; i < item.show.length; i++)
						{
							var imageName:String = item.show[i];
							var dO:starling.display.DisplayObject = taskSelectMenu.isoSprite.getChildByName(imageName);
							
							if(dO){
								taskSelectMenu.displayObjects.push(dO);
								dO.visible = true;
								
								if(dO.name == 'kitchen_balloon'){
									dO.x = baseX;
									dO.y = baseY + yOffset;
									dO.width = width;
								}else{
									dO.x = baseX;
									dO.y = baseY - (yOffset * (i-1));
									dO.height = yOffset;
									dO.width = width;
								}
							}
						}
						
						for(var i = 0; i < item.recipes.length; i++)
						{
							var itemRecipe = item.recipes[i];
							
							if(itemRecipe.labels)
							{
								//show or hide the right stuff if you can make
								var alpha:Number = 1;
								for each(var inputItem:Object in itemRecipe.inputItems)
								{
									if(behaviour.countType(inputItem.name) < inputItem.amount)
									{
										alpha = 0.7;
										break;
									}
								}
								taskSelectMenu.isoSprite.getChildByName(itemRecipe.spacer).alpha = alpha;
								
								//add the labels and icons
								for(var j = 0; j < itemRecipe.labels.length; j++)
								{
									var label:Object = itemRecipe.labels[j];
									
									if(label.asIcon)
									{
										var dO:starling.display.DisplayObject = taskSelectMenu.isoSprite.getChildByName(label.name);
										if(dO)
										{
											dO.visible = true;
											dO.alpha = alpha;
											taskSelectMenu.displayObjects.push(dO);
										}
										else
										{
											var texture:starling.textures.Texture = globalContext.assetManager.getTexture(label.image);
											if(texture)
											{
												var image:Image = new Image(texture);
												if (label.hasOwnProperty("properties"))
													for (var n:String in label.properties)
														image[n] = label.properties[n];
												
												if(label.isMainIcon){
													image.x = baseX + 1;
													image.y = baseY - (i * yOffset) + 1;
												}else{
													image.x = baseX + 78 - ((j-1) * 7);
													image.y = baseY - (i * yOffset) + .5;
													if(j > 6) {
														image.x += 4*7;
														image.y += 5;
													}
												}
												taskSelectMenu.isoSprite.addChild(image);
												taskSelectMenu.displayObjects.push(image);
											}
											else trace("Warning:: Some texture is missing! Can't find texture:"+label.image);
										}	
									}
									else
									{
										var l:Label;
										l = taskSelectMenu.isoSprite.getChildByName(label.name);
										if(l == null)
										{
											l = new Label;
											if(label.isTitle){
												l.x = -28.5;
												l.y = baseY - (i * yOffset) + 2;
											}
											if(label.isPrice){
												l.x = baseX + width - 10;
												l.y = baseY - (i * yOffset) + 1.5;
											}
											l.text = label.text;
											l.touchable = false;
											l.name = label.name;
											taskSelectMenu.isoSprite.addChild(l);
											l.textRendererProperties.snapToPixels = false;
											l.textRendererProperties.textFormat = new BitmapFontTextFormat(label.font, label.size, label.color);
										}
										l.visible = true;
										taskSelectMenu.labels.push(l);
									}
								}
							}
							
							var gesture:TapGesture;
							var spacer:starling.display.DisplayObject = taskSelectMenu.isoSprite.getChildByName(itemRecipe.spacer);
							map[itemRecipe.spacer] = itemRecipe;
							gesture = new TapGesture(spacer);
							function onTaskChoose(e:GestureEvent):void
							{
								gesture.removeEventListener(GestureEvent.GESTURE_RECOGNIZED,onTaskChoose);
								
								var itemRecipe:Object = map[e.target.target.name];
								
								var inputItems:Array = new Array;
								var outputItems:Array = new Array;
								for each( var i:Object in (itemRecipe.inputItems as Array))
									inputItems.push(i);
								for each( var i:Object in (itemRecipe.outputItems as Array))
									outputItems.push(i);
								
								gameContext.taskTargetManager.hideAll();
								buildAndAssignTask(
									target.id,
									taskTarget.type,
									inputItems,
									outputItems,
									true
								);
								gameContext.save();
								
								
								var data:Object = {
									action: 'taskSelect',
									recipe: target.recipeName
								}
								Starling.current.stage.dispatchEvent(new TutorialEvent(data, TutorialEvent.TUTORIAL_EVENT));
							}
							gesture.addEventListener(GestureEvent.GESTURE_RECOGNIZED, onTaskChoose);
							
						}
					}
					else
					{
						gameContext.taskTargetManager.hideAll();
						buildAndAssignTask(
							target.id,
							taskTarget.type,
							[],
							null,
							true
						);
						gameContext.save();
						
						
					}
					Starling.current.stage.dispatchEvent(new TutorialEvent({action: 'taskSelect',recipe: target.recipeName}, TutorialEvent.TUTORIAL_EVENT));
				}
				
			}

		}
		
		private function onGameObjectRemoved(e:Event):void
		{
			//var id:int = e.data.id;
			//if (bb.getEntry("slotGameObjectId").value == id)
				//clearSlot();
		}
		
		public function buildAndAssignTask(
			targetId:uint,
			type:String,
			inputItems:Array = null,
			outputItems:Array = null,
			fromUser:Boolean = false
		):void
		{
			gameObject.onDeselect();
			gameContext.taskTargetManager.showAll();
			
			resting 	= false;
			eating 		= false;
			relaxing 	= false;
			waiting		= false;
			
			
			if(taskToken){
				if(type != 'sell' && taskToken.type == 'sell') {
					Starling.current.stage.dispatchEvent(new ShopEvent(ShopEvent.SELLER_STOPPED, gameObject, null));
				}
			}
			
			var target:GameObject = gameContext.gameObjects.getById(targetId) as GameObject;
			if (!target)
			{
				taskToken = { };
				return;
			}
			
			if(lastTarget_ && lastTarget_.recipeName.toLocaleLowerCase().indexOf("statue") == 0)
			{
				var gb:GuardBehaviour = lastTarget_.getBehaviourByType(GuardBehaviour);
				gb.setPiggyRange(5);
			}
			
			lastTarget_ = target;
			
			taskToken = {
				targetId:targetId,
				type:type,
				inputItems:inputItems,
				outputItems:outputItems,
				fromUser:fromUser
			};
			
			if(Math.random() >= .5){
				SoundManager.playSound({id:'SO_Piggy_assign_task1'});
			}else{
				SoundManager.playSound({id:'SO_Piggy_assign_task2'});
			}
			
			Starling.current.stage.dispatchEvent(new PiggiesEvent(PiggiesEvent.PIGGY_ASSIGNED_TASK, gameObject, target));
			
			switch (type)
			{
				case "sell":
					var shopItems:Array = globalContext.assetManager.getObject("shop-items").items[0].recipes;
					setTaskTree(
						taskBuilder.buildSellTask(target, taskToken, shopItems)
					);
					
					if(taskToken.selling){
						Starling.current.stage.dispatchEvent(new ShopEvent(ShopEvent.SELLER_DEFINED, gameObject, taskToken.selling));
					}
					break;
				
				case "useField":
					setTaskTree(
						taskBuilder.buildUseFieldTask(target, taskToken)
					);
					if(target.getBehaviourByType(FieldBehaviour).givesEnergy) eating = true;
					break;
					
				case "bake":
					SoundManager.playSound({id:'SO_Work_kitchen', volume:.3});
					setTaskTree(
						taskBuilder.buildProduceTask(target, taskToken, inputItems, outputItems, null, gameContext.storage)
					);
					break;
					
				case "cook":
					SoundManager.playSound({id:'SO_Work_cooker'});
					setTaskTree(
						taskBuilder.buildUseProducerTask(target, taskToken, [{name:ApplicationVars.PREPPED_LUPIN, amount:1}])
					);
					break;
					
				case "prep":
					setTaskTree(
						taskBuilder.buildUseProducerTask(target, taskToken, [{name:ApplicationVars.LUPIN, amount:1}])
					);
					break;
				
				case "mudRelax":
					SoundManager.playSound({id:'SO_Mudpool'});
					setTaskTree(
						taskBuilder.buildUseMudTask(target, taskToken)
					);
					relaxing = true;
					break;
				
				case "hayRelax":
					SoundManager.playSound({id:'SO_Haystack'});
					setTaskTree(
						taskBuilder.buildUseMudTask(target, taskToken)
					);
					relaxing = true;
					break;
				
				case "guard":
					setTaskTree(
						taskBuilder.buildGuardTask(target, taskToken)
					);
					break;
				
				case "scare":
					setTaskTree(
						taskBuilder.buildScareTask(target, taskToken)
					);
					break;
				
				default:
					logger.error("Unknown task type {0}", [type]);
					break;
			}
		}
		
		public function getTaskToken():Object { return taskToken; }
	}

}