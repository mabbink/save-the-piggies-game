package savethepiggies.game.behaviours {
	import savethepiggies.game.Behaviour;
	import savethepiggies.game.IsoSprite;
	
	public final class SelectionRingBehaviour extends Behaviour 
	{
		public function SelectionRingBehaviour(
			id:uint
		) 
		{
			super(id);
		}
		
		public override function onSelect():void
		{
			var sprite:IsoSprite = gameObject.getIsoSprite();
			sprite.getChildByName("ring").visible = true;
			sprite.touchable = false;
		}
		
		public override function onDeselect():void
		{
			var sprite:IsoSprite = gameObject.getIsoSprite();
			sprite.getChildByName("ring").visible = false;
			sprite.touchable = true;
		}
	}

}