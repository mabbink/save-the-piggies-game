package savethepiggies.game.behaviours 
{
	import flash.events.TimerEvent;
	import flash.utils.Timer;
	
	import godmode.core.BehaviorTask;
	import godmode.core.BehaviorTree;
	import godmode.task.NoOpTask;
	
	import org.as3commons.logging.api.ILogger;
	import org.as3commons.logging.api.getLogger;
	
	import savethepiggies.GlobalContext;
	import savethepiggies.game.GameContext;
	import savethepiggies.game.GameObject;
	import savethepiggies.game.IsoSprite;
	import savethepiggies.game.TaskBuilder;
	import savethepiggies.sound.SoundManager;
	
	import starling.events.Event;
	
	public final class EvilGuyTasksBehaviour extends UpdatableBehaviour 
	{
		
		private static const logger:ILogger = getLogger(EvilGuyTasksBehaviour);
		
		public var gameContext:GameContext;
		public var globalContext:GlobalContext;
		
		public var minTimeToShow:int;
		public var maxTimeToShow:int;
		public var speedRun:Number;
		public var speedWalk:Number;
		public var minPiggiesToShowUp:int;
		
		private var selected:Boolean;
		private var tree:BehaviorTree;
		private var taskBuilder:TaskBuilder;
		
		private var taskToken:Object = null;
		
		private var taskSelectMenu:Object;
		
		
		private static var instance_:EvilGuyTasksBehaviour;
		
		public function EvilGuyTasksBehaviour(id:uint) 
		{
			super(id);
			
			if(instance_)
			{
				throw new Error("Only one evil guy is possible!!");
			}
			instance_ = this;
			
			taskSelectMenu =
			{
				displayObjects:new Array,
				labels:new Array,
				isoSprite:null
			};
		}
		
		public override function cleanup():void
		{
			super.cleanup();
			
			instance_ = null;
			
			tree.dispose();
			
			gameContext.gameObjects.removeEventListener(Event.REMOVED, onGameObjectRemoved);
		}
		
		public override function activate():void
		{
			super.activate();
			
			
			gameContext.gameObjects.addEventListener(Event.REMOVED, onGameObjectRemoved);
			
			tree = new BehaviorTree(
				NoOpTask.SUCCESS
			);
			
			taskBuilder = new TaskBuilder(gameContext, gameObject, 0, 0 /*the evil guy doesn't need energy, so he won't drain energy*/ );
			
		}
		
		private function setTaskTree(root:BehaviorTask):void
		{
			tree.dispose();
			tree = new BehaviorTree(root);
		}
		
		
		
		
		private function goBackFuntion(e:TimerEvent):void
		{
			trace("Evil guy: GO BACK!!");
			var piggy:GameObject = evilGuyStruct.scarePiggyObject;
			var statue:GameObject = evilGuyStruct.statueObject;
			var gb:PiggyTasksBehaviour = piggy.getBehaviourByType(PiggyTasksBehaviour) as PiggyTasksBehaviour;
			gb.buildAndAssignTask(statue.id, "guard");
			
			var timer:Timer = e.target as Timer;
			timer.stop();
			timer.removeEventListener(TimerEvent.TIMER_COMPLETE, goBackFuntion);
		} 
		
		private var evilGuyStruct:Object = 
		{
			doing:06,
			lastI:-1000.0,
			lastJ:-1000.0,
			lastCount:0,
			factory:null,
			target:null,
			waitingTime:0,
			timeToWait:0,
			piggy:null,
			runAway:4,
			soundPlaying:false,
			scarePiggyObject:null,
			statueObject:null
		}
		private function evilGuyAI(dt:Number):void
		{
			const BADGUY_RECIPE:String = "evilGuy-recipe";
			const EVILGUY_STOP:uint		 						= 00;
			const EVILGUY_MOVE:uint		 						= 01;
			const EVILGUY_GRAB:uint								= 02;
			const EVILGUY_GETAWAY:uint							= 03;
			const EVILGUY_GETAWAY_MOVE:uint						= 04;
			const EVILGUY_WAIT_TO_START:uint					= 05;
			const EVILGUY_ARRIVED_TO_FACTORY:uint				= 06;
			const EVILGUY_GETAWAY_WITH_GRAB:uint				= 07;
			const EVILGUY_GETAWAY_SCARED:uint					= 08;
			
			var evilGuy:GameObject;
			var evilGuyIS:IsoSprite;
			var piggies:Array = [];
			var statues:Array = [];
			var factories:Array = [];
			//var properties:PiggyPropertiesBehaviour = gameObject.getBehaviourByType(PiggyPropertiesBehaviour);
			
			function populate():void
			{
				gameContext.gameObjects.visit(function(item:GameObject):void
				{
					if(item.recipeName == BADGUY_RECIPE)
					{
						evilGuy = item;
					}
					else if(item.recipeName.toLowerCase().indexOf("pig") != -1)
					{
						var behaviour:PiggyTasksBehaviour = item.getBehaviourByType(PiggyTasksBehaviour);
						var taskToken = behaviour.getTaskToken()
						if(taskToken)
						{
							var type:String = taskToken.type as String;
							if(type)
								if(type.toLocaleLowerCase().indexOf("relax") == -1)
									piggies.push(item);
						}
						else
							piggies.push(item);
					}
					else if(item.recipeName.toLowerCase().indexOf("statue") != -1)
					{
						statues.push({statue:item, piggy:null});
					}
					else if(item.recipeName.toLowerCase().indexOf("evilfactory") != -1)
					{
						factories.push(item);
					}
				});
				
				// Find piggies that are in guard state
				var guardPiggies:Array = [];
				for each(var piggy:GameObject in piggies)
				{
					var ptb:PiggyTasksBehaviour = piggy.getBehaviourByType(PiggyTasksBehaviour);
					var tt:Object = ptb.getTaskToken();
					if(tt != null) if(tt.type == "guard") guardPiggies.push(piggy);
				}
				
				// Remove statues that are not with a piggy in the guard state
				for each(var statuePair:Object in statues)
				{
					var inStatue:Boolean = false;
					for each(var piggy:GameObject in guardPiggies)
					{
						var ptb:PiggyTasksBehaviour = piggy.getBehaviourByType(PiggyTasksBehaviour);
						var tt:Object = ptb.getTaskToken();
						if(tt != null && statuePair.statue.id == tt.targetId)
						{
							inStatue = true;
							statuePair.piggy = piggy;
							break;
						}
					}
					if(!inStatue)
						statues.splice( statues.indexOf( statuePair ), 1);
				}
				
				evilGuyIS = evilGuy.getIsoSprite();
			}
			
			function isStopped():Boolean
			{
				// If the evil guy is in the same position, he is stopped
				evilGuyIS = gameObject.getIsoSprite();
				if(evilGuyIS.offsetI == evilGuyStruct.lastI && evilGuyIS.offsetJ == evilGuyStruct.lastJ)
				{
					evilGuyStruct.lastCount++;
				}
				else evilGuyStruct.lastCount = 0;
				return evilGuyStruct.lastCount > 2 ? true : false;
			}
			
			function saveLastPos():void
			{
				evilGuyIS = gameObject.getIsoSprite();
				// Save this position as last position
				evilGuyStruct.lastI = evilGuyIS.offsetI;
				evilGuyStruct.lastJ = evilGuyIS.offsetJ;
			}
			
			switch(evilGuyStruct.doing)
			{
				case EVILGUY_ARRIVED_TO_FACTORY:
				{
					// The evil guy is not on the game
					gameContext.isEvilGuyOut = false;
					// set all the vars
					populate();
					
					//when the evil guy arrive to the factory, he will not be visible
					evilGuyIS.visible = false;
					//start a new count to show up
					evilGuyStruct.timeToWait = this.minTimeToShow + Math.random() * (this.maxTimeToShow - this.minTimeToShow);
					evilGuyStruct.waitingTime = 0;
					evilGuyStruct.doing = EVILGUY_WAIT_TO_START;
					
					// Stop the evil guy sound
					SoundManager.stop('SO_Evil_background');
					if(evilGuyStruct.soundPlaying)
					{	
						SoundManager.startBackgroundMusic();
						evilGuyStruct.soundPlaying = false;
					}
				}
				break;
				
				case EVILGUY_WAIT_TO_START:
				{
					evilGuyStruct.waitingTime += dt;
					
					// If the wait time is over
					if(evilGuyStruct.timeToWait < evilGuyStruct.waitingTime)
					{
						populate();
						
						// If we have more than X piggies
						if(minPiggiesToShowUp <= piggies.length && !gameContext.recoveringGameState)
						{
							evilGuyStruct.timeToWait = 0;
							evilGuyStruct.waitingTime = 0;
							
							// the evil guy is stopped
							evilGuyStruct.doing = EVILGUY_STOP;
						}
						else
						{
							//start a new count to show up
							evilGuyStruct.timeToWait = this.minTimeToShow + Math.random() * (this.maxTimeToShow - this.minTimeToShow);
							evilGuyStruct.waitingTime = 0;
						}
					}
				}
				break;
				
				case EVILGUY_STOP:
				{
					populate();
					
					// If no piggies are avaible to grab or no evil factories exists
					if(piggies.length == 0 || factories.length == 0)
					{
						// The evil guy doesnt show up
						evilGuyStruct.doing = EVILGUY_ARRIVED_TO_FACTORY;
						return;
					}
					
					// The evil guy is on the game
					gameContext.isEvilGuyOut = true;
					
					gameContext.incommingEvilGuy();
					
					// Choose a factory
					var factoryIdx:uint = Math.round(Math.random()*(factories.length-1));
					evilGuyStruct.factory = factories[factoryIdx];
					var factoryIsoSprite:IsoSprite = evilGuyStruct.factory.getIsoSprite();
					
					// Set the evil guy on the factory
					evilGuyIS.moveTo( factoryIsoSprite.offsetI+3, factoryIsoSprite.offsetJ+2);
					
					evilGuyIS.visible = true;
					
					// Select one piggy to be the target
					var pigIndex:uint = Math.round(Math.random()*(piggies.length-1));
					evilGuyStruct.target = piggies[pigIndex];
					buildAndAssignTask( evilGuyStruct.target.id, "grab", this.speedWalk);
					
					// Move the evil guy!
					evilGuyStruct.doing = EVILGUY_MOVE;
					
					// Change the background sound (play the evil guy sound)
					if(!evilGuyStruct.soundPlaying)
					{
						SoundManager.stopBackgroundMusic();
						SoundManager.playSound({id:'SO_Evil_background'/*, loopInterval:10*/}); // TODO:: the loop have some bug?!?
						evilGuyStruct.soundPlaying = true;
					}
					
					return;
				}
				break;
				
				
				case EVILGUY_MOVE:
				{
					populate();
					
					//If don't exists any piggy avaiable, go away
					if(piggies.length == 0)
					{
						evilGuyStruct.doing = EVILGUY_GETAWAY;
						// Save this position as last position
						saveLastPos();		
						return;
					}
					
					
					// If the evil guy is in the same position
					if(isStopped())
					{
						//
						if(--evilGuyStruct.runAway < 0)
						{
							//reset the counter
							evilGuyStruct.runAway = 4;
							
							//Ok, is better to go to the factory, we can't find a piggy to grab
							evilGuyStruct.doing = EVILGUY_GETAWAY;
							
							// Save this position as last position
							saveLastPos();		
							return;
						}
						else
						{
							// Select one piggy to be the target
							var pigIndex:uint = Math.round(Math.random()*(piggies.length-1));
							evilGuyStruct.target = piggies[pigIndex];
							buildAndAssignTask( evilGuyStruct.target.id, "grab", this.speedWalk);
						}
					}
					else
					{
						//reset the counter
						evilGuyStruct.runAway = 4;
					}
					
					
					
					
					// Find if the evil guy is near to ...
					var i:Number = 0.0;
					var j:Number = 0.0;
					var di:Number = 0.0;
					var dj:Number = 0.0;
					// ... statue
					for each( var statuePair:Object in statues)
					{
						var isoSprite:IsoSprite = statuePair.statue.getIsoSprite();
						di = isoSprite.offsetI - evilGuyIS.offsetI;
						dj = isoSprite.offsetJ - evilGuyIS.offsetJ;
						var piggy:GameObject = statuePair.piggy;
						if(!piggy) continue;
						
						var statueRange:Number = statuePair.statue.getBehaviourByType(GuardBehaviour).range;
						var piggyRange:Number = piggy.getBehaviourByType(PiggyPropertiesBehaviour).guarding;
						var range:Number = statueRange + 0.24 * piggyRange;
						
						// An imprecise test to see if he is close to the statue
						if(Math.abs(di) + Math.abs(dj) <= range)
						{
							// A more precise check
							if(di*di + dj*dj <= range * range)
							{
								// Play a sound
								SoundManager.playSound({id:'SO_Piggy_guard'});
								
								var speed:Number = piggy.getBehaviourByType(PiggyPropertiesBehaviour).speed * 0.5;
								var gb:PiggyTasksBehaviour = piggy.getBehaviourByType(PiggyTasksBehaviour) as PiggyTasksBehaviour;
								gb.buildAndAssignTask(gameObject.id, "scare");
								
								//The time to evil guy go away
								evilGuyStruct.timeToWait = 1.0;
								evilGuyStruct.waitingTime = 0;
								evilGuyStruct.doing = EVILGUY_GETAWAY_SCARED;
								evilGuyStruct.scarePiggyObject = piggy;
								evilGuyStruct.statueObject = statuePair.statue;
								
								gameContext.evilGuyScaredAway(piggy);
								
								// Save this position as last position
								saveLastPos();
								return;
							}
						}
					}
					// ... piggy
					for each( var piggy:GameObject in piggies)
					{
						di = Math.abs(piggy.getIsoSprite().offsetI - evilGuyIS.offsetI);
						dj = Math.abs(piggy.getIsoSprite().offsetJ - evilGuyIS.offsetJ);
						
						if(di + dj <= 0.5)
						{
							// Grab the piggy
							evilGuyStruct.piggy = piggy;
							// Move the evil guy!
							evilGuyStruct.doing = EVILGUY_GRAB;
							// Save this position as last position
							saveLastPos();
							
							gameContext.piggyCaptured(piggy);
							return;
						}
					}
					
					// Save this position as last position
					saveLastPos();					
				}
				break;
				
				
				case EVILGUY_GETAWAY_SCARED:
				{
					// Add the time
					evilGuyStruct.waitingTime += dt;
					
					// If the wait time is over
					if(evilGuyStruct.timeToWait < evilGuyStruct.waitingTime)
					{
						//Stop the background sound
						if(evilGuyStruct.soundPlaying)
						{
							SoundManager.stop('SO_Evil_background');
							SoundManager.startBackgroundMusic();
							evilGuyStruct.soundPlaying = false;
						}
						
						// The piggy goes back to the statue in 400ms
						var timer:Timer = new Timer(400, 1);
						timer.addEventListener(TimerEvent.TIMER_COMPLETE,goBackFuntion, false, 0, true);
						timer.start();
						
						// Clean the time counters
						evilGuyStruct.timeToWait = 0;
						evilGuyStruct.waitingTime = 0;
						
						// the evil guy run away
						evilGuyStruct.doing = EVILGUY_GETAWAY;
					}
					// Save this position as last position
					saveLastPos();
				}
				break;
				
				case EVILGUY_GRAB:
				{
					//destroyGameObject
					this.gameContext.deleteGameObject( evilGuyStruct.piggy );
					// The evil guy runs away!
					evilGuyStruct.doing = EVILGUY_GETAWAY_WITH_GRAB;
					
					// Play the grab
					SoundManager.playSound({id:'SO_Evil_catch'});
					// Change the background sound (stop evil guy sound)
					if(evilGuyStruct.soundPlaying)
					{
						SoundManager.stop('SO_Evil_background');
						SoundManager.startBackgroundMusic();
						evilGuyStruct.soundPlaying = false;
					}
					
					// Save this position as last position
					saveLastPos();
				}
				break;
				
				
				case EVILGUY_GETAWAY_WITH_GRAB:
				{
					gameObject.getBehaviourByType(PiggyInventoryBehaviour).holding = [{name:"foo", amount: 1}];
					
					// 
					buildAndAssignTask( evilGuyStruct.factory.id, "grab", this.speedRun);
					
					// Move the evil guy!
					evilGuyStruct.doing = EVILGUY_GETAWAY_MOVE;
				}
				break;
				
				case EVILGUY_GETAWAY:
				{
					// 
					buildAndAssignTask( evilGuyStruct.factory.id, "grab", this.speedRun);
					
					// Move the evil guy!
					evilGuyStruct.doing = EVILGUY_GETAWAY_MOVE;
				}
					break;
				
				case EVILGUY_GETAWAY_MOVE:
				{
					if(isStopped())
					{
						gameObject.getBehaviourByType(PiggyInventoryBehaviour).holding = null;
						
						evilGuyStruct.doing = EVILGUY_ARRIVED_TO_FACTORY;
						
						// Save this position as last position
						saveLastPos();
						return;
					}
					// Save this position as last position
					saveLastPos();
				}
				break;
				
				default:
				{
					trace("NOT IMPLEMENTED! do stop");
					// Move the evil guy!
					evilGuyStruct.doing = EVILGUY_STOP;
				}
			}
			// Save this position as last position
			//saveLastPos();
		}
		
		
		public override function update(dt:Number):void
		{
			evilGuyAI(dt);

			tree.update(dt);
		}
		
		
		
		
		
		
		
		
		
		
		
		private function onGameObjectRemoved(e:Event):void
		{
			//var id:int = e.data.id;
			//if (bb.getEntry("slotGameObjectId").value == id)
				//clearSlot();
		}
		
		public function buildAndAssignTask(
			targetId:uint,
			type:String,
			speed:Number
		):void
		{
			gameObject.onDeselect();
			//gameContext.taskTargetManager.showAll();
			
			var target:GameObject = gameContext.gameObjects.getById(targetId) as GameObject;
			if (!target)
			{
				taskToken = { };
				return;
			}
			
			taskToken =
			{
				targetId:targetId,
				type:type,
				speed:speed
			};
			
			
			switch (type)
			{
				case "grab":
					setTaskTree(
						taskBuilder.buildEvilGuyTask(target, taskToken, gameObject.getBehaviourByType(PiggyInventoryBehaviour))
					);
					break;
				
				default:
					logger.error("Unknown task type {0}", [type]);
					break;
			}
		}
		
		public function getTaskToken():Object { return taskToken; }
	}

}