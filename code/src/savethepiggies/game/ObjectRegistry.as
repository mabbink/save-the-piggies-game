package savethepiggies.game 
{
	import org.as3commons.logging.api.ILogger;
	import org.as3commons.logging.api.getLogger;
	
	import savethepiggies.HasId;
	
	import starling.events.Event;
	import starling.events.EventDispatcher;
	
	public final class ObjectRegistry extends EventDispatcher
	{
		private static const logger:ILogger = getLogger(ObjectRegistry);
		
		private var objects:Object;
		private var objectTagRegistry:Object;
		
		public function ObjectRegistry()
		{
			objects = { };
			objectTagRegistry = { };
		}
		
		public function register(object:HasId):Boolean
		{
			var exists:Boolean = getById(object.id) is HasId;
			if (!exists)
			{
				objects[object.id] = object;
				var o:GameObject = object as GameObject;
				if(o != null) logger.info("Registered object {0} with ID {1} and recipe name {2}", [object, object.id, o.recipeName]);
				else logger.info("Registered object {0} with ID {1}", [object, object.id]);
				
				if(o){
					var tags:Object = o.getTags();
					for(var tag:String in tags){
						if(!objectTagRegistry[tag]){
							objectTagRegistry[tag] = [o];
						}else{
							objectTagRegistry[tag].push(o);
						}
					}
				}
			}
			else
				logger.warn("Object already exists, not registering.");
				
			return !exists;
		}
		
		public function visit(f:Function):void
		{
			for (var i:* in objects)
			{
				f(objects[i]);
			}
		}
		
		public function deregister(object:HasId):Boolean
		{
			var exists:Boolean = getById(object.id) is HasId;
			if (exists)
			{
				var o:GameObject = objects[object.id] as GameObject;
				if(o){
					var tags = o.getTags();
					for(var tag:String in tags){
						
						if(objectTagRegistry[tag]){
							for(var i:int = objectTagRegistry[tag].length; i--;){
								if(objectTagRegistry[tag][i].id == o.id){
									objectTagRegistry[tag].splice(i, 1);
									break;
								}
							}
						}
					}
				}
				
				delete objects[object.id];
				logger.info("Deregistered object with ID {0}", [object.id]);
				
				dispatchEventWith(Event.REMOVED, false, { id: object.id } );
			}
			else
				logger.warn("Object doesn't exist, not deregistering.");
				
			return exists;
		}
		
		public function getById(id:uint):HasId
		{
			return objects[id];
		}
		
		public function getFirst():HasId
		{
			for (var s:String in objects)
				return objects[s];
				
			return null;
		}
		public function getSingleByTag(pTag:String):GameObject{
			if(objectTagRegistry[pTag]) return objectTagRegistry[pTag][0];
			return null;
		}
		public function getByTag(pTag:String):Array{
			if(objectTagRegistry[pTag]) return objectTagRegistry[pTag];
			return [];
		}
		
		public function getAmountOfChildren():int{
			var amount:int = 0;
			for (var s:String in objects){
				amount ++;
			}
			return amount;
		}
	}

}