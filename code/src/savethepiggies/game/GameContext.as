package savethepiggies.game 
{
	import savethepiggies.ReservedIdGenerator;
	import savethepiggies.map.Map;
	
	import starling.display.Sprite;
	
	public final class GameContext 
	{
		public var grid:GameGrid;
		public var gameObjects:ObjectRegistry;
		public var updatableGameObjects:ObjectRegistry;
		public var drawableGameObjects:ObjectRegistry;
		public var piggies:ObjectRegistry;
		public var taskTargetManager:TaskTargetManager;
		public var selectionGroupPiggies:SelectionGroup;
		public var selectionGroupTaskTargets:SelectionGroup;
		public var storage:Storage;
		public var map:Map;
		public var mapLayer:Sprite;
		public var isoSpriteDisplay:IsoSpriteDisplay;
		public var upgradeCallback:Function;
		public var removeCallback:Function;
		public var buyCallback:Function;
		public var moveCallback:Function;
		public var deleteGameObject:Function;
		public var idGenerator:ReservedIdGenerator;
		public var save:Function;
		
		public var cash:uint = 0;
		public var todaysTimestamp:Number;
		public var todaysPiggy1:String;
		public var todaysPiggy2:String;
		public var todaysPiggy3:String;
		public var tutorialStep:int;
		
		public var invitedPeople:String;
		
		public var recoveringGameState:Boolean = false;
		public var shownEvilGuyTip:Boolean = false;
		public var incommingEvilGuy:Function;
		public var piggyCaptured:Function;
		public var evilGuyScaredAway:Function;
		public var isEvilGuyOut:Boolean = false;
		
		public var enableFacebookPosting:Boolean = true;
		public var enableBackgroundMusic:Boolean = true;
		public var enableSoundEffects:Boolean = true;
	}

}