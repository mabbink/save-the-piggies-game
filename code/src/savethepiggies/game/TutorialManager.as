package savethepiggies.game
{
	import com.greensock.TweenNano;
	
	import org.gestouch.events.GestureEvent;
	import org.gestouch.gestures.TapGesture;
	
	import savethepiggies.GlobalContext;
	import savethepiggies.game.events.TutorialEvent;
	import savethepiggies.sound.SoundManager;
	
	import starling.core.Starling;
	import starling.display.DisplayObject;
	import starling.display.Image;
	import starling.display.Sprite;

	public final class TutorialManager {
		
		public const SHOW_MARKET_BUTTON_FROM 	= 14;
		public const POPUPS:Array				= [
			{nr:1, inGame:true, continueOnClick:true, pos:'piggy'},
			{nr:2, inGame:true, continueOnClick:true, pos:'piggy'},
			{nr:3, inGame:true, continueOnClick:true, pos:'piggy'},
			{nr:4, inGame:true, continueOnClick:true, pos:'piggy'},
			{nr:5, inGame:true, continueOnClick:false, pos:'piggy'},
			{nr:6, inGame:true, continueOnClick:false, pos:'fieldLupin', focus:'lupin'},
			{nr:7, inGame:true, continueOnClick:false, pos:'prepper', focus:'prepper'},
			{nr:8, inGame:true, continueOnClick:false, pos:'cooker', focus:'cooker'},
			{nr:9, inGame:true, continueOnClick:false, pos:'kitchen', focus:'kitchen'},
			{nr:10, inGame:true, continueOnClick:true, pos:'shop', iInc:1, focus:'shop'},
			
			{nr:11, inGame:false, continueOnClick:true, x:4, y:15},
			{nr:12, inGame:false, continueOnClick:true, x:0, y:15},
			{nr:13, inGame:false, continueOnClick:true, x:0, y:15},
			{nr:14, inGame:false, continueOnClick:true, x:4, y:0},
			{nr:15, inGame:false, continueOnClick:true, x:0, y:0},
			
			{nr:16, inGame:true, continueOnClick:true, pos:'statue', focus:'statue'}, //statue
			
			{nr:17, inGame:true, continueOnClick:true, pos:'Corn', focus:'corn'}, //hungry
			{nr:18, inGame:true, continueOnClick:true, pos:'house', focus:'house'}, //tired
			{nr:19, inGame:true, continueOnClick:true, pos:'mudpool', focus:'mudpool'} //relax
		];
		
		
		
		
		private var gameScene:GameScene;
		private var gameContext:GameContext;
		private var mapDisplay:MapDisplay;
		private var uiLayer:Sprite;
		private var globalContext:GlobalContext;
		private var cooker:Cooker;
		private var save:Function;
		private var destroyGameObject:Function;
		
		private var tutorialObj:GameObject;
		private var tutorialImage:Image;
		
		
		public function TutorialManager(gameScene:GameScene, gameContext:GameContext, mapDisplay:MapDisplay, uiLayer:Sprite, globalContext:GlobalContext, cooker:Cooker, save:Function, destroyGameObject:Function){
			this.gameScene = gameScene;
			this.gameContext = gameContext;
			this.mapDisplay = mapDisplay;
			this.uiLayer = uiLayer;
			this.globalContext = globalContext;
			this.cooker = cooker;
			this.save = save;
			this.destroyGameObject = destroyGameObject;
			
			Starling.current.stage.addEventListener(TutorialEvent.TUTORIAL_EVENT, onTutorialEvent);
			
			var piggy:GameObject = gameContext.piggies.getFirst() as GameObject;
			tutorialObj = cooker.cook('tutorial-recipe', piggy.createdAtI, piggy.createdAtJ);
			
			tutorialImage = new Image(globalContext.assetManager.getTexture('step11'));
			tutorialImage.visible = false;
			tutorialImage.useHandCursor = true;
			uiLayer.addChild(tutorialImage);
			
			var tapGesture:TapGesture = new TapGesture(tutorialImage);
			tapGesture.addEventListener(GestureEvent.GESTURE_RECOGNIZED, onTutorialClick);
			
		}
		
		
		private function onTutorialClick(pEvent:GestureEvent):void{
			moveTutorial({action:'goNextStep'});
		}
		private function onTutorialEvent(pEvent:TutorialEvent):void{
			moveTutorial(pEvent.eventData);
			pEvent.stopImmediatePropagation();
		}
		
		private function moveTutorial(pData:Object):void{
			function hasTag(pTag:String, pObj:Object):Boolean{
				for(var key in pObj){
					if(key == pTag) return true;
				}
				return false;
			}
			
			switch(gameContext.tutorialStep){
				case 5:
					if(pData.action == 'select'){
						if(hasTag('piggy', pData.tags)) showTutorialStep(6);
					}
					break;
				
				case 6:
					if(pData.action == 'deselect'){
						TweenNano.delayedCall(10, function(){ if(gameContext.tutorialStep == 6) showTutorialStep(5); });
						
					}else if(pData.action == 'taskSelect'){
						if(pData.recipe.indexOf('fieldLupin') > -1) showTutorialStep(0);
					}
					break;
				
				case 0:
					if(pData.action == 'storingItem'){
						if(pData.type == ApplicationVars.LUPIN) showTutorialStep(7);
					}
					break;
				
				case 7:
					if(pData.action == 'storingItem'){
						if(pData.type == ApplicationVars.PREPPED_LUPIN) showTutorialStep(8);
					}
					break;
				
				case 8:
					if(pData.action == 'storingItem'){
						if(pData.type == ApplicationVars.COOKED_LUPIN) showTutorialStep(9);
					}
					break;
				
				case 9:
					if(pData.action == 'taskSelect') if(pData.recipe.indexOf('kitchen') > -1){
						tutorialObj.getIsoSprite().getChildByName("step9").visible = false;
					}
					
					if(pData.action == 'storingItem'){
						var options:Array = [ApplicationVars.PICMAC, ApplicationVars.BURRITO, ApplicationVars.PIZZA, ApplicationVars.SMOOTHY, ApplicationVars.VIVERA_BURGER, ApplicationVars.SHASHLIK, ApplicationVars.FRUIT_SHASHLIK];
						if(options.indexOf(pData.type) > -1) showTutorialStep(10);
					}
					break;
				
				case 10:
					if(pData.action == 'taskSelect'){
						if(pData.recipe.indexOf('shop') > -1) showTutorialStep(11);
					}
					break;
				
				default:
					if(pData.action == 'goNextStep'){
						if(gameContext.tutorialStep > 0){
							if(POPUPS[gameContext.tutorialStep-1].continueOnClick) {
								showTutorialStep(gameContext.tutorialStep+1);
							}
						}
					}
					break;
			}
		}
		public function showTutorialStep(pNr:int):void{
			gameContext.tutorialStep = pNr;
			save();
			
			if(pNr > 0 && pNr < POPUPS.length+1) {
				var popup = POPUPS[pNr-1]; //{nr:16, inGame:true, continueOnClick:true}
			}else{
				var popup = {};
			}
			
			if(pNr > 1) SoundManager.playSound({id:'SO_Open_popup'});
			
			//show/hide
			if(!popup.nr){
				tutorialImage.visible = false;
				destroyGameObject(tutorialObj);
				
			}else if(popup.inGame == true){
				tutorialImage.visible = false;
				moveTutorialTo(popup.pos, popup.iInc ? popup.iInc : 0, popup.yInc ? popup.yInc : 0);
				
				var sprite:IsoSprite = tutorialObj.getIsoSprite();
				for (var i:int = 1; i < POPUPS.length+1; i++){
					var obj:DisplayObject = sprite.getChildByName("step" + i);
					if(obj) obj.visible = i == pNr;
				}
				if(popup.focus) gameScene.moveCameraTo( gameContext.gameObjects.getSingleByTag(popup.focus), 0.5 );
				
			}else if(popup.inGame == false){
				tutorialImage.visible = true;
				destroyGameObject(tutorialObj);
				
				reposition();
				tutorialImage.x = popup.x;
				tutorialImage.y = popup.y;
				tutorialImage.texture = globalContext.assetManager.getTexture('step'+pNr);
				tutorialImage.readjustSize();
			}
			
			
			function moveTutorialTo(pType:String, pIIncrease:int = 0, pJIncrease:int = 0):void{
				if(pType == 'piggy'){
					destroyGameObject(tutorialObj);
					var piggy:GameObject = gameContext.piggies.getFirst() as GameObject;
					tutorialObj = cooker.cook('tutorial-recipe', piggy.createdAtI+pIIncrease, piggy.createdAtJ+pJIncrease);
					
				}else{
					gameContext.gameObjects.visit(function(item:Object):void{
						if(item.recipeName.indexOf(pType) > -1){
							destroyGameObject(tutorialObj);
							tutorialObj = cooker.cook('tutorial-recipe', item.createdAtI+pIIncrease, item.createdAtJ+pJIncrease);
							return;
						}
					});
				}
			}	
		}
		
		
		private function reposition():void{
			POPUPS[11].x = Starling.current.stage.stageWidth - tutorialImage.width - 40; //shop
			
			POPUPS[12].x = Starling.current.stage.stageWidth - tutorialImage.width - 2; //ingredients
			
			POPUPS[13].y = Starling.current.stage.stageHeight - tutorialImage.height - 18; //market
			
			POPUPS[14].x = Starling.current.stage.stageWidth/2 - tutorialImage.width/2; //all done
			POPUPS[14].y = Starling.current.stage.stageHeight/2 - tutorialImage.height/2;
			
		}
	}
}