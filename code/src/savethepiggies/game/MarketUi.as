package savethepiggies.game 
{
	import com.greensock.TweenNano;
	
	import feathers.controls.Alert;
	import feathers.controls.Button;
	import feathers.controls.Label;
	import feathers.controls.LayoutGroup;
	import feathers.controls.List;
	import feathers.controls.Panel;
	import feathers.data.ListCollection;
	import feathers.layout.AnchorLayout;
	import feathers.layout.AnchorLayoutData;
	import feathers.layout.HorizontalLayout;
	import feathers.layout.HorizontalLayoutData;
	import feathers.utils.display.calculateScaleRatioToFill;
	import feathers.utils.display.calculateScaleRatioToFit;
	
	import org.as3commons.logging.api.ILogger;
	import org.as3commons.logging.api.getLogger;
	
	import savethepiggies.GlobalContext;
	import savethepiggies.game.events.MarketEvent;
	import savethepiggies.sound.SoundManager;
	
	import starling.display.Image;
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.utils.AssetManager;
	
	public final class MarketUi extends Sprite 
	{
		private static const logger:ILogger = getLogger(MarketUi);
		
		private var panelsContainer:LayoutGroup;
		
		private var globalContext:GlobalContext;
		private var gameContext:GameContext;
		private var gameScene:GameScene;
		private var assetManager:AssetManager;
		
		private var backButton:Button;
		private var goldLabel:Label;
		
		private var piggiesList:List;
		private var machinesList:List;
		private var ingredientsList:List;
		private var checkoutList:List;
		private var checkoutGoldLeft:Label;
		private var checkoutButton:Button;
		
		private var bg:Image;
		private var balloon:Image;
		
		public function MarketUi(
			globalContext:GlobalContext,
			gameContext:GameContext,
			assets:AssetManager,
			gameScene:GameScene
		) 
		{
			super();
			
			this.globalContext = globalContext;
			this.gameContext = gameContext;
			this.gameScene = gameScene;
			this.assetManager = assets;
			
			bg = new Image(assets.getTexture("marketbg"));
			addChild(bg);
			
			addEventListener(Event.ADDED_TO_STAGE, addedToStage);
			
			createPanelsContainer();
			addChild(panelsContainer);
			
			backButton = new Button;
			backButton.x = backButton.y = 1;
			backButton.label = "Go Back";
			addChild(backButton);
			backButton.addEventListener(Event.TRIGGERED, backButtonClick);
			
			goldLabel = new Label();
			goldLabel.y = 2;
			goldLabel.nameList.add('shopTotalGold');
			addChild(goldLabel);
			
			
			var piggiesPanel:Panel = createPanel("Piggies");
			var machinesPanel:Panel = createPanel("Machines");
			var ingredientsPanel:Panel = createPanel("Ingredients");
			var checkoutPanel:Panel = createPanel("Checkout", 90);
			
			panelsContainer.addChild(piggiesPanel);
			panelsContainer.addChild(machinesPanel);
			panelsContainer.addChild(ingredientsPanel);
			panelsContainer.addChild(checkoutPanel);
			
			piggiesList = createCatalogueList();
			machinesList = createCatalogueList();
			ingredientsList = createCatalogueList();
			
			piggiesPanel.addChild(piggiesList);
			machinesPanel.addChild(machinesList);
			ingredientsPanel.addChild(ingredientsList);
			
			checkoutPanel.addChild(createCheckout());
			
			
			balloon = new Image(globalContext.assetManager.getTexture('shopBalloon1'));
			balloon.y = 13;
			balloon.scaleX = balloon.scaleY = .5;
			addChild(balloon);
		}
		
		public function cleanup():void
		{
			removeEventListener(Event.ADDED_TO_STAGE, addedToStage);
			stage.removeEventListener(Event.RESIZE, resizeBg);
			
			backButton.removeEventListener(Event.TRIGGERED, backButtonClick);
			checkoutButton.removeEventListener(Event.TRIGGERED, checkoutButtonClick);
			
			piggiesList.removeEventListener(Event.CHANGE, catalogueListChange);
			machinesList.removeEventListener(Event.CHANGE, catalogueListChange);
			ingredientsList.removeEventListener(Event.CHANGE, catalogueListChange);
			
			checkoutList.removeEventListener(Event.CHANGE, checkoutListChange);
		}
		
		private function addedToStage():void
		{
			stage.addEventListener(Event.RESIZE, resizeBg);
			resizeBg();
		}
		
		private function resizeBg():void
		{
			var bgRatio:Number = calculateScaleRatioToFill(
				bg.texture.width, bg.texture.height, 
				stage.stageWidth,
				stage.stageHeight
			);
			bg.scaleX = bg.scaleY = bgRatio;
		}
		
		private function calculateTotal():uint
		{
			var total:int = 0;
			
			for each (var item:Object in checkoutList.dataProvider.data)
			{
				total += item.price;
			}
			return total;
		}
		
		private function checkBalance():void
		{
			var costs:int = calculateTotal();
			checkoutButton.isEnabled = costs <= gameContext.cash;
			
			checkoutGoldLeft.text = '$'+costs;
		}
		
		private function buildPurchaseList():Array
		{
			return checkoutList.dataProvider.data.concat();
		}
		
		private function backButtonClick():void
		{
			SoundManager.playSound({id:'SO_Close_popup'});
			closeShop();
		}
		private function closeShop():void
		{
			this.visible = false;
			SoundManager.stop('SO_Market');
			
			gameScene.marketClosed();
		}
		
		
		private function checkoutButtonClick():void
		{
			SoundManager.playSound({id:'SO_Sell'});
			
			gameContext.cash -= calculateTotal();
			
			var e:MarketEvent = new MarketEvent(
				MarketEvent.CHECKOUT
			);
			
			e.purchaseList = buildPurchaseList();
			
			var boughtPiggy:Boolean = false;
			for(var i:int = e.purchaseList.length; i--;){
				if(gameContext.todaysPiggy1 == e.purchaseList[i].name) {
					gameContext.todaysPiggy1 = '';
					boughtPiggy = true;
				}
				if(gameContext.todaysPiggy2 == e.purchaseList[i].name) {
					gameContext.todaysPiggy2 = '';
					boughtPiggy = true;
				}
				if(gameContext.todaysPiggy3 == e.purchaseList[i].name) {
					gameContext.todaysPiggy3 = '';
					boughtPiggy = true;
				}
			}
			if(boughtPiggy) gameContext.save();
			
			if (e.purchaseList.length > 0)
				dispatchEvent(e);
				
			closeShop();
		}
		
		private function checkoutListChange(e:Event):void
		{
			var list:List = e.target as List;
			if (!list)
				logger.error("Target is not a list!");
			else
			{
				if (!list.selectedItem)
					logger.info("No item selected!");
				else
				{
					SoundManager.playSound({id:'SO_Button'});
					
					list.dataProvider.removeItem(list.selectedItem);
					checkBalance();
				}
			}
		}
		
		private function catalogueListChange(e:Event):void
		{
			var list:List = e.target as List;
			if (!list)
				logger.error("Target is not a list!");
			else
			{
				if (!list.selectedItem)
					logger.info("No item selected!");
				else
				{
					
					var obj:Object = list.dataProvider.getItemAt(list.selectedIndex);
					
					if(obj.stats){
						tryBuyPiggy(list, obj);
						
					}else{
						if(obj.name == 'Refresh'){
							var alert:Alert = Alert.show(
								obj.description,
								"Refresh todays piggies",
								new ListCollection([{label:"Cancel"},{label:"Ok", triggered:refreshTodaysPiggies}]), 
								new Image(globalContext.assetManager.getTexture('carl'))
							);
							
						}else if(obj.locked){
							var alert:Alert = Alert.show(
								obj.requiresDescription,
								"Buy "+obj.name,
								new ListCollection([{label:"Ok"}]), 
								new Image(globalContext.assetManager.getTexture('carl'))
							);
						}else{
							tryBuyProduct(list, obj);
						}
					}
				}
			}
		}
		private function tryBuyProduct(list:List, product:Object):void{
			SoundManager.playSound({id:'SO_Button'});
			
			var alert:Alert = Alert.show(
				product.description,
				"Buy "+product.name,
				new ListCollection([
					{ label:"Buy", isEnabled:true, triggered:continueBuyProduct },
					{label:"Cancel", triggered:close}
				]), 
				new Image(globalContext.assetManager.getTexture('carl'))
			);
			
			
			function close():void{
				SoundManager.playSound({id:'SO_Close_popup'});
			}
			
			function continueBuyProduct(e:Event):void{
				if(gameContext.cash >= calculateTotal() + product.price){
					close();
					
					SoundManager.playSound({id:'SO_Buy'});
					
					checkoutList.dataProvider.addItem(cloneItem(list.selectedItem));
					checkBalance();
					list.selectedItem = null;
					
				}else{
					close();
					list.selectedItem = null;
					
					var alert:Alert = Alert.show(
						"HAHA, you don't have the coin to buy this",
						"Buy "+product.name,
						new ListCollection([
							{label:"Ok..", triggered:function(){
								SoundManager.playSound({id:'SO_Close_popup'});
							}}
						]),
						new Image(globalContext.assetManager.getTexture('butcherLaugh'))
					);
				}
			}
		}
		private function tryBuyPiggy(list:List, piggy:Object):void{
			SoundManager.playSound({id:'SO_Piggy_hungry'});
			
			var description:String = "So you want to buy my pig?";
			description += '\nRelaxation: '+(piggy.stats.relaxation*10)+'%';
			description += '\nSales: '+(piggy.stats.sales*10)+'%';
			description += '\nGuarding: '+(piggy.stats.guarding*10)+'%';
			description += '\nGathering: '+(piggy.stats.gathering*10)+'%';
			description += '\nPrepping: '+(piggy.stats.prepping*10)+'%';
			description += '\nSpeed: '+(piggy.stats.speed*10)+'%';
			description += '\n';
			description += '\nPrice: '+piggy.price+' gold';
			
			var alert:Alert = Alert.show(
				description,
				"Buy "+piggy.name,
				new ListCollection([
					{ label:"Free this piggy!", isEnabled:true, triggered:continueBuyPig },
					{label:"Cancel"}
				]), 
				new Image(globalContext.assetManager.getTexture('butcherAngry'))
			);
			alert.addEventListener(Event.CLOSE, close);
			
			
			function close():void{
				SoundManager.playSound({id:'SO_Close_popup'});
				alert.removeEventListener(Event.CLOSE, close);
			}
			
			function continueBuyPig(e:Event):void{
				if(gameContext.cash >= piggy.price){
					close();
					
					SoundManager.playSound({id:'SO_Piggy_assign_task2'});
					
					checkoutList.dataProvider.addItem(cloneItem(list.selectedItem));
					checkBalance();
					
					list.selectedItem = null;
					list.dataProvider.removeItemAt(list.dataProvider.getItemIndex(piggy));
					
					var alert:Alert = Alert.show(
						"Take it, it was useless anyway.",
						"Buy "+piggy.name,
						new ListCollection([
							{label:"Ok.."}
						]),
						new Image(globalContext.assetManager.getTexture('butcherLaugh2'))
					);
					
				}else{
					close();
					list.selectedItem = null;
					
					var alert:Alert = Alert.show(
						"HAHA, you don't have the coin to buy my pig!",
						"Buy "+piggy.name,
						new ListCollection([
							{label:"Ok..", triggered:function(){
								SoundManager.playSound({id:'SO_Close_popup'});
							}}
						]),
						new Image(globalContext.assetManager.getTexture('butcherLaugh'))
					);
				}
			}
		}
		
		private function cloneItem(item:Object):Object
		{
			var clone:Object = { };
			for (var s:String in item)
				clone[s] = item[s];
				
			return clone;
		}
		
		
		private function createPanelsContainer():void
		{
			panelsContainer = new LayoutGroup;
			
			var layout:HorizontalLayout = new HorizontalLayout;
			layout.distributeWidths = true;
			layout.verticalAlign = HorizontalLayout.VERTICAL_ALIGN_BOTTOM;
			layout.gap = 2;
			layout.lastGap = 5;
			layout.paddingTop = 55;
			layout.paddingLeft = 0;
			layout.paddingRight = 0;
			layout.paddingBottom = 5;
			
			panelsContainer.layout = layout;
		}
		
		private function createPanel(headerTitle:String, percentHeight:Number = 100):Panel
		{
			var panel:Panel = new Panel;
			panel.layout = new AnchorLayout;
			panel.headerProperties.title = headerTitle;
			var panelLayoutData:HorizontalLayoutData = new HorizontalLayoutData;
			panelLayoutData.percentHeight = percentHeight;
			panel.layoutData = panelLayoutData;
			
			return panel;
		}
		
		private function createCatalogueList():List
		{
			var list:List = new List
			list.dataProvider = new ListCollection([
			]);
			var listLayoutData:AnchorLayoutData = new AnchorLayoutData;
			listLayoutData.left = 0;
			listLayoutData.right = 0;
			listLayoutData.top = 0;
			listLayoutData.bottom = 0;
			list.layoutData = listLayoutData;
			list.itemRendererProperties.labelField = "text";
			list.itemRendererProperties.iconSourceField = "thumbnail";
			
			list.addEventListener(Event.CHANGE, catalogueListChange);
			
			return list;
		}
		
		private function createCheckout():LayoutGroup
		{
			var container:LayoutGroup = new LayoutGroup;
			var containerLayoutData:AnchorLayoutData = new AnchorLayoutData;
			containerLayoutData.left = 0;
			containerLayoutData.right = 0;
			containerLayoutData.top = 0;
			containerLayoutData.bottom = 0;
			container.layoutData = containerLayoutData;
			container.layout = new AnchorLayout;
			
			var list:List = new List;
			list.dataProvider = new ListCollection([
			]);
			var listLayoutData:AnchorLayoutData = new AnchorLayoutData;
			listLayoutData.left = 0;
			listLayoutData.right = 0;
			listLayoutData.top = 0;
			listLayoutData.bottom = 22;
			list.layoutData = listLayoutData;
			list.itemRendererProperties.labelField = "text";
			list.itemRendererProperties.iconSourceField = "thumbnail";
			
			list.addEventListener(Event.CHANGE, checkoutListChange);
			
			container.addChild(list);
			
			checkoutGoldLeft = new Label();
			checkoutGoldLeft.text = 'Cost:0';
			checkoutGoldLeft.x = 10;
			checkoutGoldLeft.y = 200;
			checkoutGoldLeft.nameList.add("balanceLabel");
			
			var checkoutGoldLeftData:AnchorLayoutData = new AnchorLayoutData;
			checkoutGoldLeftData.topAnchorDisplayObject = list;
			checkoutGoldLeftData.left = 0;
			checkoutGoldLeftData.right = 0;
			checkoutGoldLeftData.bottom = 15;
			checkoutGoldLeft.layoutData = checkoutGoldLeftData;
			
			container.addChild(checkoutGoldLeft);
			
			var button:Button = new Button;
			button.label = "Checkout";
			
			var buttonLayoutData:AnchorLayoutData = new AnchorLayoutData;
			buttonLayoutData.topAnchorDisplayObject = list;
			buttonLayoutData.left = 0;
			buttonLayoutData.right = 0;
			buttonLayoutData.bottom = 0;
			button.layoutData = buttonLayoutData;
			
			container.addChild(button);
			
			checkoutList = list;
			checkoutButton = button;
			
			checkoutButton.addEventListener(Event.TRIGGERED, checkoutButtonClick);
			
			return container;
		}
		
		private function populateStore():void
		{
			var assets:AssetManager = assetManager;
			var items = assets.getObject("store-items").items as Array
			
			machinesList.dataProvider.removeAll();
			piggiesList.dataProvider.removeAll();
			ingredientsList.dataProvider.removeAll();
				
			for each (var item:Object in items)
			{
				var list:List;
				
				switch (item.category)
				{
					case "machine":
						list = machinesList;
						break;
						
					case "piggy":
						list = piggiesList;
						break;
						
					case "ingredient":
						list = ingredientsList;
						break;
					
					case "recipes":
						continue;
					
					default: throw new Error("Unknown category " + item.category);
				}
				
				var text:String = item.name.toUpperCase();
				var stats:Object = null;
				
				if(item.category == "piggy"){
					var recipe:Object = globalContext.assetManager.getObject(item.recipe+'-recipe');
					
					var bestStat:Object = {};
					for(var i:int = 0; i < recipe.parts.length; i++){
						for(var j:int = 0; j < recipe.parts[i].components.length; j++){
							if(recipe.parts[i].components[j]['class'] == 'PiggyPropertiesBehaviour'){
								stats = recipe.parts[i].components[j];
								
								for(var key in stats){
									if(!(stats[key] is int)) continue;
									
									if(!bestStat.value || stats[key] > bestStat.value){
										bestStat.name = key;
										bestStat.value = stats[key];
									}
								}
								break;
							}
						}
					}
					
					text += "\n"+bestStat.name+" pig ("+bestStat.value+")";
				}
				text +=  "\nPRICE: " + item.price+' gold';
				
				var locked = false;
				if(item.requires){
					if(!gameContext.gameObjects.getSingleByTag(item.requires)) locked = true;
				}
				
				list.dataProvider.addItem({
					text:text,
					name: item.name,
					description: item.description,
					price:item.price,
					stats: stats,
					isPiggy: stats != null,
					thumbnail:assets.getTexture(item.thumbnail+(locked ? '-locked' : '')),
					locked:locked,
					requires:item.requires,
					requiresDescription:item.requiresDescription,
					w:item.w,
					h:item.h,
					texture:item.placeTexture,
					x:item.placeTextureX,
					y:item.placeTextureY,
					recipe:item.recipe,
					placeOnTopOf:item.placeOnTopOf
				});
			}
			
			piggiesList.dataProvider.addItem({
				text:'Refresh\ntodays piggies\nPRICE: 200',
				name: 'Refresh',
				description: 'Would you like to refresh todays piggies for 200 gold?',
				price:200,
				thumbnail:assets.getTexture('refresh-piggies')
			});
		}
		
		
		
		
		
		
		public function show():void{
			
			populateStore();
			
			defineTodaysPiggies();
			showTodaysPiggies();
			
			checkoutList.dataProvider.removeAll();
			
			visible = true;
			
			SoundManager.playSound({id:'SO_Market', loopInterval:30, volume:.2});
			
			var currentBalloon:int = 1;
			function tweenBalloon():void{
				if(!visible) return;
				
				balloon.texture = globalContext.assetManager.getTexture('shopBalloon'+currentBalloon);
				balloon.y = 14;
				balloon.alpha = 0;
				
				TweenNano.to(balloon, 1, {y:13, alpha:1});
				
				currentBalloon ++;
				if(currentBalloon == 4) currentBalloon = 1;
				TweenNano.delayedCall(5, tweenBalloon);
			}
			tweenBalloon();
			
			goldLabel.text = '$'+gameContext.cash;
			checkoutGoldLeft.text = '';
		}
		
		private function defineTodaysPiggies():void{
			var todaysPiggiesDetermend:Boolean = false;
			
			function determenTodaysPiggies():void{
				var options:Array = piggiesList.dataProvider.data as Array;
				var randomNr:int = Math.random()*options.length;
				
				if(randomNr-1 == -1){
					gameContext.todaysPiggy1 = options[options.length-1].name;
				}else{
					gameContext.todaysPiggy1 = options[randomNr-1].name;
				}
				
				gameContext.todaysPiggy2 = options[randomNr].name;
				
				if(randomNr+1 == options.length){
					gameContext.todaysPiggy3 = options[0].name;
				}else{
					gameContext.todaysPiggy3 = options[randomNr+1].name;
				}
				
				gameContext.todaysTimestamp = new Date().time;
				gameContext.save();
				
			}
			
			if(!gameContext.todaysTimestamp) {
				determenTodaysPiggies();
				
			}else{
				var determenDate:Date = new Date(gameContext.todaysTimestamp);
				var today:Date = new Date();
				
				if(determenDate.fullYear != today.fullYear || determenDate.month != today.month || determenDate.date != today.date) determenTodaysPiggies();
			}
		}
		private function showTodaysPiggies():void{
			
			var availablePiggies:Array = [gameContext.todaysPiggy1, gameContext.todaysPiggy2/*, gameContext.todaysPiggy3*/];
			
			var amountOfPiggies:int = gameContext.piggies.getAmountOfChildren();
			if(amountOfPiggies < 6) {
				availablePiggies[0] = 'Michelle pig'; //when we are done with the tutorial, the user can buy a fith pig. This piggy is cheap
				availablePiggies[1] = 'Bee pig'; //when we are done with the tutorial, the user can buy a fith pig. This piggy is cheap
			}
			
			for(var i:int = 0; i < piggiesList.dataProvider.data.length; i++){
				var name:String = piggiesList.dataProvider.data[i].name;
				if(name == 'Refresh') continue;
				
				var available:Boolean = false;
				for each(var option in availablePiggies){
					if(option == name) available = true;
				}
				
				if(!available){
					piggiesList.dataProvider.removeItemAt(i);
					i--;
				}
			}
		}
		private function refreshTodaysPiggies():void{
			gameContext.todaysTimestamp = null;
			gameContext.cash -= 200;
			show();
		}
		
		
		
		public function draw():void
		{
			if (visible)
			{
				
				var ratio:Number = calculateScaleRatioToFit(2048, 1536, stage.stageWidth, stage.stageHeight);
				
				var w:Number = 2048 * ratio;
				var h:Number = 1536 * ratio;
				
				panelsContainer.width = w;
				panelsContainer.height = h;
				panelsContainer.x = (stage.stageWidth - w) / 2;
				panelsContainer.y = (stage.stageHeight - h) / 2;
				
				balloon.x = stage.stageWidth * .45;
				goldLabel.x = stage.stageWidth - goldLabel.width - 2;
			}
		}
	}
}