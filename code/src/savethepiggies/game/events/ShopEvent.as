package savethepiggies.game.events
{
	import savethepiggies.game.GameObject;
	
	import starling.events.Event;

	public final class ShopEvent extends Event
	{
		public static const SELLER_DEFINED:String = "savethepiggies.game.events.ShopEvent.SELLER_DEFINED";
		public static const SELLER_STOPPED:String = "savethepiggies.game.events.ShopEvent.SELLER_STOPPED";
		
		public var piggy:GameObject;
		public var selling:String;
		
		public function ShopEvent(type:String, piggy:GameObject, selling:String) 
		{ 
			this.piggy = piggy;
			this.selling = selling;
			super(type, false, false);
		} 
	}
}