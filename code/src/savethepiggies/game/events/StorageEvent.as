package savethepiggies.game.events 
{
	import starling.events.Event;
	
	public final class StorageEvent extends Event 
	{
		public static const STORAGE_ADD:String = "savethepiggies.game.events.TutorialEvent.STORAGE_ADD";
		public static const STORAGE_REMOVE:String = "savethepiggies.game.events.TutorialEvent.STORAGE_REMOVE";
		
		public var ingredients:Array;
		
		public function StorageEvent(type:String, ingredients:Array) 
		{ 
			this.ingredients = ingredients;
			super(type, false, false);
		} 
	}
	
}