package savethepiggies.game.events 
{
	import starling.events.Event;
	import savethepiggies.game.TaskTarget;
	
	public final class TaskTargetSelectedEvent extends Event 
	{
		public static const TASK_TARGET_SELECTED:String = "savethepiggies.game::TaskSelectedEvent.TASK_TARGET_SELECTED";
		
		private var _taskTarget:TaskTarget;
		
		public function TaskTargetSelectedEvent(type:String, taskTarget:TaskTarget, bubbles:Boolean=false, cancelable:Boolean=false) 
		{
			super(type, bubbles, cancelable);
			
			_taskTarget = taskTarget;
		}
		
		public function get taskTarget():TaskTarget { return _taskTarget; }
	}

}