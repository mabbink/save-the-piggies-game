package savethepiggies.game.events 
{
	import starling.events.Event;
	
	public class PlacingEvent extends Event 
	{
		public static const PLACED:String = "savethepiggies.game.events.PlacingEvent.PLACED";
		public static const CANCELLED:String = "savethepiggies.game.events.PlacingEvent.CANCELLED";
		
		public var recipeName:String;
		public var i:int;
		public var j:int;
		public var callOnFinallyPlaced:Function;
		
		public function PlacingEvent(type:String, bubbles:Boolean=false, cancelable:Boolean=false) 
		{ 
			super(type, bubbles, cancelable);
		} 
	}
	
}