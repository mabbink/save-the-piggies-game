package savethepiggies.game.events 
{
	import savethepiggies.game.GameObject;
	
	import starling.events.Event;
	
	public final class PiggiesEvent extends Event 
	{
		public static const PIGGY_ADDED:String = "savethepiggies.game.events.TutorialEvent.PIGGY_ADDED";
		public static const PIGGY_REMOVED:String = "savethepiggies.game.events.TutorialEvent.PIGGY_REMOVED";
		public static const PIGGY_ASSIGNED_TASK:String = "savethepiggies.game.events.TutorialEvent.PIGGY_ASSIGNED_TASK";
		
		public var piggy:GameObject;
		public var piggyTarget:GameObject;
		
		public function PiggiesEvent(type:String, piggy:GameObject, piggyTarget:GameObject = null) 
		{ 
			this.piggy = piggy;
			this.piggyTarget = piggyTarget;
			super(type, false, false);
		} 
	}
	
}