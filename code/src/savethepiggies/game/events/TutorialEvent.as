package savethepiggies.game.events 
{
	import starling.events.Event;
	
	public final class TutorialEvent extends Event 
	{
		public static const TUTORIAL_EVENT:String = "savethepiggies.game.events.TutorialEvent.TUTORIAL_EVENT";
		public var eventData:Object;
		
		public function TutorialEvent(data:Object, type:String, bubbles:Boolean=false, cancelable:Boolean=false) 
		{ 
			eventData = data;
			super(type, bubbles, cancelable);
		} 
	}
	
}