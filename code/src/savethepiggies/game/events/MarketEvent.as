package savethepiggies.game.events 
{
	import starling.events.Event;
	
	public final class MarketEvent extends Event 
	{
		public static const CHECKOUT:String = "savethepiggies.game.events.MarketEvent.CHECKOUT";
		
		public var purchaseList:Array;
		
		public function MarketEvent(type:String, bubbles:Boolean=false, cancelable:Boolean=false) 
		{ 
			super(type, bubbles, cancelable);
		} 
	}
	
}