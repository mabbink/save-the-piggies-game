package savethepiggies.game 
{
	import feathers.controls.Alert;
	import feathers.controls.Label;
	import feathers.data.ListCollection;
	
	import godmode.TaskFactory;
	import godmode.core.BehaviorTask;
	import godmode.data.Blackboard;
	import godmode.data.Entry;
	import godmode.data.MutableEntry;
	
	import org.as3commons.logging.api.ILogger;
	import org.as3commons.logging.api.getLogger;
	
	import savethepiggies.game.behaviours.AnimationSwitcherBehaviour;
	import savethepiggies.game.behaviours.FieldBehaviour;
	import savethepiggies.game.behaviours.GuardBehaviour;
	import savethepiggies.game.behaviours.PiggyCounterBehaviour;
	import savethepiggies.game.behaviours.PiggyInventoryBehaviour;
	import savethepiggies.game.behaviours.PiggyPropertiesBehaviour;
	import savethepiggies.game.behaviours.PiggyTasksBehaviour;
	import savethepiggies.game.behaviours.ProducerBehaviour;
	import savethepiggies.game.behaviours.StorageBehaviour;
	import savethepiggies.game.events.PiggiesEvent;
	import savethepiggies.game.tasks.AnimationTask;
	import savethepiggies.game.tasks.ChangeEnergyTask;
	import savethepiggies.game.tasks.ChangeFoodTask;
	import savethepiggies.game.tasks.ObjectPropertyResource;
	import savethepiggies.game.tasks.PositionedAnimationResource;
	import savethepiggies.game.tasks.RunForTask;
	import savethepiggies.game.tasks.SlotSemaphoreDecorator;
	import savethepiggies.game.tasks.WalkToTask;
	import savethepiggies.sound.SoundManager;
	
	import starling.animation.Transitions;
	import starling.animation.Tween;
	import starling.core.Starling;
	
	public final class TaskBuilder 
	{
		private static const logger:ILogger = getLogger(TaskBuilder);
		
		private var gameContext:GameContext;
		private var piggy:GameObject;
		private var baseEnergyDrain:Number;
		private var baseFoodDrain:Number;
		private var ts:TaskSupport;
		private var tf:TaskFactory;
		
		public function TaskBuilder(
			gameContext:GameContext,
			piggy:GameObject,
			baseEnergyDrain:Number,
			baseFoodDrain:Number
		) 
		{
			this.gameContext = gameContext;
			this.piggy = piggy;
			this.baseEnergyDrain = baseEnergyDrain;
			this.baseFoodDrain = baseFoodDrain;
			
			tf = new TaskFactory(null);
			ts = new TaskSupport(gameContext, piggy);
		}
		
		private function chooseSlot(
			target:GameObject,
			targetId:MutableEntry,
			targetSlotIndex:MutableEntry,
			slotPath:MutableEntry
		):BehaviorTask
		{
			return tf.call(function():int {
				var candidates:Array = ts.getSlotCandidates(target);
				var iso:IsoSprite = target.getIsoSprite();
				
				var isPiggy:Boolean = false;
				var tags:Object = target.getTags();
				if(tags["piggy"] || tags["evilGuy"])
					isPiggy = true;
				
				for (var i:int = 0; i < candidates.length; ++i)
				{
					var slot:TaskTargetSlot = new TaskTargetSlot(Math.round(iso.offsetI) * 2, Math.round(iso.offsetJ) * 2);
					var points:Array = isPiggy ? ts.getPathToSlot(slot) : ts.getPathToSlot(candidates[i].slot);
					if (null != points)
					{
						targetId.store(target.id);
						targetSlotIndex.store(candidates[i].slotIndex);
						slotPath.store(points);
						
						return BehaviorTask.SUCCESS;
					}
				}
				
				return BehaviorTask.FAIL;
			});
		}
		
		private function findSlot(
			requiredTag:String,
			targetId:MutableEntry,
			targetSlotIndex:MutableEntry,
			slotPath:MutableEntry,
			filter:Function = null
		):BehaviorTask
		{
			return tf.call(function():int {
				var candidates:Array = ts.findSlotCandidates(requiredTag, filter);
				
				for (var i:int = 0; i < candidates.length; ++i)
				{
					var points:Array = ts.getPathToSlot(candidates[i].slot);
					if (null != points)
					{
						targetId.store(candidates[i].g.id);
						targetSlotIndex.store(candidates[i].slotIndex);
						slotPath.store(points);
						
						return BehaviorTask.SUCCESS;
					}
				}
				
				return BehaviorTask.FAIL;
			});
		}
		
		private function walkPath(
			path:Entry,
			forceThisInventory:* = -1,		//when the value is -1, this class will get the value from PiggyInventoryBehaviour
			forceThisSpeed:Number = -1.0	//when the value is -1, this class will get the value from PiggyPropertiesBehaviour
		):BehaviorTask
		{
			return new WalkToTask(piggy, path, forceThisInventory, forceThisSpeed);
		}
		
		private function withSlotSemaphore(
			targetId:Entry,
			slotIndex:Entry,
			task:BehaviorTask
		):BehaviorTask
		{
			var semaphore:MutableEntry = (new Blackboard).getEntry("s");
			
			return tf.sequence(
				tf.call(function():int {
					semaphore.store(ts.getSemaphore(targetId.value, slotIndex.value));
					if (!semaphore) return BehaviorTask.FAIL;
					return BehaviorTask.SUCCESS;
				}),
				new SlotSemaphoreDecorator(semaphore, task)
			);
		}
		
		private function withTokenPosition(
			token:Object,
			task:BehaviorTask
		):BehaviorTask
		{
			var bb:Blackboard = new Blackboard;
			var forceI:MutableEntry = bb.getEntry("forceI");
			var forceJ:MutableEntry = bb.getEntry("forceJ");
			
			return tf.sequence(
				tf.call(function():void {
					var s:IsoSprite = piggy.getIsoSprite();
					forceI.store(s.offsetI);
					forceJ.store(s.offsetJ);
				}),
				tf.using(new ObjectPropertyResource(token, "forceI", forceI),
					tf.using(new ObjectPropertyResource(token, "forceJ", forceJ),
						task
					)
				)
			);
		}
		
		private function runSlotAnimation(
			targetId:Entry,
			slotIndex:Entry,
			token:Object
		):BehaviorTask
		{
			var bb:Blackboard = new Blackboard;
			var name:MutableEntry = bb.getEntry("name");
			var posI:MutableEntry = bb.getEntry("i");
			var posJ:MutableEntry = bb.getEntry("j");
			var dir:MutableEntry = bb.getEntry("dir");
			
			return withTokenPosition(token,
				tf.sequence(
					tf.call(function():int {
						var target:GameObject = ts.getTarget(targetId.value);
						var slot:TaskTargetSlot = ts.getSlot(targetId.value, slotIndex.value);
						if (!slot || !target) {
							trace('Run slot animation fail, no target or no slot');
							return BehaviorTask.FAIL;
						}
						var sprite:IsoSprite = target.getIsoSprite();
						if (!sprite) return BehaviorTask.FAIL;
						
						var animSwitcher:AnimationSwitcherBehaviour =
							piggy.getBehaviourByType(AnimationSwitcherBehaviour);
						name.store(
							animSwitcher.getPiggyAnimName(
								slot.properties.animName,
								slot.properties.animDir)
						);
						posI.store(slot.properties.offsetI + sprite.offsetI);
						posJ.store(slot.properties.offsetJ + sprite.offsetJ);
						dir.store(slot.properties.animDir);
						
						if(token.type == "guard")
						{
							var ppb:PiggyPropertiesBehaviour = piggy.getBehaviourByType(PiggyPropertiesBehaviour);
							var gb:GuardBehaviour = target.getBehaviourByType(GuardBehaviour);
							gb.setPiggyRange(ppb.guarding);
						}
						
						return BehaviorTask.SUCCESS;
					}),
					tf.using(
						new PositionedAnimationResource(piggy, posI, posJ),
						new AnimationTask(piggy, name)
					)
				)
			);
		}
		
		
		
		private function buildEnsureInventoryStatusTask(requiredInventory:Array, taskToken:Object):BehaviorTask{
			if(!requiredInventory) requiredInventory = [{name:null, amount:0}];
			if(requiredInventory.length == 0) requiredInventory = [{name:null, amount:0}];
			
			var bb:Blackboard = new Blackboard;
			var storageId:MutableEntry = bb.getEntry("id");
			var storageSlotIndex:MutableEntry = bb.getEntry("slot index");
			var storageSlotPath:MutableEntry = bb.getEntry("slot path");
			
			function inventoryFilter(g:GameObject):Boolean
			{
				var storage:StorageBehaviour = g.getBehaviourByType(StorageBehaviour);
				if (null == requiredInventory[0].name) return storage.hasSpace;
				
				for each(var item:Object in requiredInventory)
				{
					var have:int = storage.countType(item.name);
					if(have < item.amount)
					{
						piggy.getBehaviourByType(PiggyTasksBehaviour).isConfused({reason:PiggyTasksBehaviour.CONFUSED_MISSING_ITEM, item:item.name});
						return false;
					}
				}
				return true;
			}
			
			if(taskToken.fromUser)
			{
				var pib:PiggyInventoryBehaviour = piggy.getBehaviourByType(PiggyInventoryBehaviour);
				if(pib.holding != null){
					//this piggy has stuff in his inventory
					
					var storages:Array = gameContext.gameObjects.getByTag('storage');
					var storageAvailable:Boolean = false;
					
					for each(var g:GameObject in storages){
						var storage:StorageBehaviour = g.getBehaviourByType(StorageBehaviour);
						if(storage.freeSpace > 0) {
							storageAvailable = true;
							break;
						}
					}
					if(!storageAvailable){
						function drop():void{
							pib.holding = null;
						}
						var alert:Alert = Alert.show(
							"Your storage is full and this piggy has something in his backpack which he can't drop anywhere. Would you like to discard this backpack?",
							"Discard this backpack?",
							new ListCollection([
								{label:"Drop it!", isEnabled:true, triggered:drop},
								{label:"Cancel"}
							])
						);
					}
				}
			}
			
			return tf.loopUntilSuccess(
				tf.selectWithPriority(
					tf.call(function():int {
						
						//This is a weird piece of code... because:
						//ts.getInventory = [{name:"Lupin", amount:1}]
						//requiredInventory = [{name:null, amount:0}]
						
						//this code does return true (2) or false (3) sometimes tho.
						//so this only return success when both are null
						
						var toFeed:Boolean = false;
						gameContext.gameObjects.visit(function(go:GameObject):void
						{
							if(go.id == taskToken.targetId)
							{
								var field:FieldBehaviour = go.getBehaviourByType(FieldBehaviour);
								if(field)
									toFeed = field.givesEnergy > 0.0;
							}
						});
						if(toFeed)
							return BehaviorTask.SUCCESS;
						
						var result:int = !ts.getInventory() && !requiredInventory[0].name ? BehaviorTask.SUCCESS : BehaviorTask.FAIL;
						
						return result;
						
					}),
					tf.sequence(
						findSlot("storage", storageId, storageSlotIndex, storageSlotPath, inventoryFilter),
						walkPath(storageSlotPath),
						withSlotSemaphore(storageId, storageSlotIndex,
							tf.sequence(
								new RunForTask(1, runSlotAnimation(storageId, storageSlotIndex, taskToken)),
								tf.call(function():int {
									
									var g:GameObject = ts.getTarget(storageId.value);
									if (!g) {
										piggy.getBehaviourByType(PiggyTasksBehaviour).isConfused({reason:PiggyTasksBehaviour.CONFUSED_NO_STORAGE_FOUND});
										return BehaviorTask.FAIL; //we cant find the storage
									}
									
									var storage:StorageBehaviour = g.getBehaviourByType(StorageBehaviour);
									
									if (null == requiredInventory[0].name)
									{
										if (ts.hasInventory())
										{
											if (!storage.storeItem(ts.getInventory())){
												piggy.getBehaviourByType(PiggyTasksBehaviour).isConfused({reason:PiggyTasksBehaviour.CONFUSED_STORAGE_IS_FULL});
												return BehaviorTask.FAIL; //there is no more space in this inventory
											}
											ts.setInventory(null);
										}
									}
									else
									{
										if (ts.hasInventory())
										{
											if(!storage.storeItem(ts.getInventory()))
											{
												piggy.getBehaviourByType(PiggyTasksBehaviour).isConfused({reason:PiggyTasksBehaviour.CONFUSED_STORAGE_IS_FULL});
												return BehaviorTask.FAIL; //we cant get the item from it, we need
											}
										}
											
										var result:Object = storage.retrieveItems(requiredInventory);
										if (!result.success)
										{
											piggy.getBehaviourByType(PiggyTasksBehaviour).isConfused({reason:PiggyTasksBehaviour.CONFUSED_MISSING_ITEM, item:result.missing[0]});
											return BehaviorTask.FAIL; //we cant get the item from it, we need
										}
										
										ts.setInventory(requiredInventory);
									}
									
									return BehaviorTask.SUCCESS;
								})
							)
						)
					)
				)
			);
		}
		
		public function buildUseFieldTask(
			field:GameObject,
			taskToken:Object
		):BehaviorTask
		{
			var bb:Blackboard = new Blackboard;
			var fieldId:MutableEntry = bb.getEntry("id");
			var fieldSlotIndex:MutableEntry = bb.getEntry("slot index");
			var fieldSlotPath:MutableEntry = bb.getEntry("slot path");
			
			var harvestLength:Number = 0.0;
			var obj:* = field.getBehaviourByType(FieldBehaviour);
			harvestLength = obj.harvestLength - (piggy.getBehaviourByType(PiggyPropertiesBehaviour).gathering / 2);
			
			function produceAvailable():Boolean
			{
				var field:GameObject = gameContext.gameObjects.getById(fieldId.value) as GameObject;
				if (!field) return false;
				return field.getBehaviourByType(FieldBehaviour).isProduceAvailable;
			}
			
			function needsPlanting():Boolean
			{
				var field:GameObject = gameContext.gameObjects.getById(fieldId.value) as GameObject;
				if (!field) return false;
				return field.getBehaviourByType(FieldBehaviour).needsPlanting;
			}
			
			function takeProduce():String
			{
				var field:GameObject = gameContext.gameObjects.getById(fieldId.value) as GameObject;
				if (!field) return null;
				return field.getBehaviourByType(FieldBehaviour).takeProduce(piggy);
			}
			
			function givesEnergy():Number
			{
				var field:GameObject = gameContext.gameObjects.getById(fieldId.value) as GameObject;
				if (!field) return null;
				return field.getBehaviourByType(FieldBehaviour).givesEnergy;
			}
			
			function plant():Boolean
			{
				var field:GameObject = gameContext.gameObjects.getById(fieldId.value) as GameObject;
				if (!field) return false;
				return field.getBehaviourByType(FieldBehaviour).plant();
			}
			
			return whileGameObjectExists(field.id,
				tf.sequence(
					buildEnsureInventoryStatusTask(null, taskToken),
					chooseSlot(field, fieldId, fieldSlotIndex, fieldSlotPath),
					walkPath(fieldSlotPath),
					withSlotSemaphore(fieldId, fieldSlotIndex,
						tf.selectWithPriority(
							// Harvest
							tf.runWhile(tf.pred(produceAvailable),
								tf.sequence(
									new RunForTask(harvestLength, runSlotAnimation(fieldId, fieldSlotIndex, taskToken)),
									tf.call(function():int {
										var produce:String = takeProduce(piggy);
										if (null == produce) {
											
											piggy.getBehaviourByType(PiggyTasksBehaviour).isConfused({reason:PiggyTasksBehaviour.CONFUSED_CANT_FIND_FIELD, item:field.recipeName});
											return BehaviorTask.FAIL;
										}
										
										if(givesEnergy()){
											
											var fieldBehaviour:FieldBehaviour = field.getBehaviourByType(FieldBehaviour);
											var counterBehaviour:PiggyCounterBehaviour = piggy.getBehaviourByType(PiggyCounterBehaviour);
											counterBehaviour.changeFullness(fieldBehaviour.givesEnergy);
											
										}else{
											ts.setInventory([{name:produce, amount:1}]);
										}
										return BehaviorTask.SUCCESS;
									})
								)
							),
							
							// Plant
							tf.runWhile(tf.pred(needsPlanting),
								tf.sequence(
									new RunForTask(1, runSlotAnimation(fieldId, fieldSlotIndex, taskToken)),
									tf.call(function():int {
										return plant() ? BehaviorTask.SUCCESS : BehaviorTask.FAIL;
									})
								)
							)
						)
					)
				)
			);
		}
		
		public function buildRelaxTask(taskToken:Object):BehaviorTask{
			var target:GameObject = getRandomGameObjectsByTag(['mudpool','haystack']);
			taskToken.targetId = target.id;
			if(target.hasTag('mudpool')){
				taskToken.type = 'mudRelax';
			}else{
				taskToken.type = 'hayRelax';
			}
			
			return buildUseMudTask(target, taskToken);
		}
		public function buildUseMudTask(
			field:GameObject,
			taskToken:Object
		):BehaviorTask
		{
			
			Starling.current.stage.dispatchEvent(new PiggiesEvent(PiggiesEvent.PIGGY_ASSIGNED_TASK, piggy, field));
			
			var bb:Blackboard = new Blackboard;
			var mudId:MutableEntry = bb.getEntry("id");
			var mudSlotIndex:MutableEntry = bb.getEntry("slot index");
			var mudSlotPath:MutableEntry = bb.getEntry("slot path");
			var relaxationSpeed = 1 + (piggy.getBehaviourByType(PiggyPropertiesBehaviour).relaxation*0.1);
			
			return whileGameObjectExists(field.id,
				tf.sequence(
					chooseSlot(field, mudId, mudSlotIndex, mudSlotPath),
					walkPath(mudSlotPath),
					withSlotSemaphore(mudId, mudSlotIndex,
						tf.parallel(
							runSlotAnimation(mudId, mudSlotIndex, taskToken),
							new ChangeEnergyTask(piggy, relaxationSpeed * baseEnergyDrain),
							new ChangeFoodTask(piggy, relaxationSpeed * baseFoodDrain)
						)
					)
				)
			);
		}
		
		public function buildUseProducerTask(
			machine:GameObject,
			taskToken:Object,
			input:Array
		):BehaviorTask
		{
			
			var bb:Blackboard = new Blackboard;
			var machineId:MutableEntry = bb.getEntry("id");
			var machineSlotIndex:MutableEntry = bb.getEntry("slot index");
			var machineSlotPath:MutableEntry = bb.getEntry("slot path");
			
			var b:ProducerBehaviour = machine.getBehaviourByType(ProducerBehaviour);
			
			function canFill():Boolean
			{
				return b.canBeFilled;
			}
			
			function fill():int
			{
				return b.fill(piggy.getBehaviourByType(PiggyPropertiesBehaviour).prepping / 2) ? BehaviorTask.SUCCESS : BehaviorTask.FAIL;
			}
			
			return whileGameObjectExists(machine.id,
				tf.selectWithPriority(
					// Fill?
					tf.runWhile(tf.pred(canFill),
						tf.sequence(
							buildEnsureInventoryStatusTask(input, taskToken),
							chooseSlot(machine, machineId, machineSlotIndex, machineSlotPath),
							walkPath(machineSlotPath),
							withSlotSemaphore(machineId, machineSlotIndex,
								tf.sequence(
									new RunForTask(1, runSlotAnimation(machineId, machineSlotIndex, taskToken)),
									tf.call(fill),
									tf.call(function():void {
										ts.setInventory(null);
									})
								)
							)
						)
					),
					tf.sequence(
						tf.call(function(){
							piggy.getBehaviourByType(PiggyTasksBehaviour).isWaiting();
						})
					)
				)
			);
		}
		
		public function buildEvilGuyTask(
			target:GameObject,
			taskToken:Object,
			inventary:* = -1
 		):BehaviorTask
		{
			var bb:Blackboard = new Blackboard;
			var targetId:MutableEntry = bb.getEntry("id");
			var targetSlotIndex:MutableEntry = bb.getEntry("slot index");
			var targetSlotPath:MutableEntry = bb.getEntry("slot path");
			
			return whileGameObjectExists(target.id, 
				tf.sequence(
					chooseSlot(target, targetId, targetSlotIndex, targetSlotPath),
					walkPath(targetSlotPath, inventary, taskToken.speed)
				)
			);
		}
		
		public function buildScareTask(
			target:GameObject,
			taskToken:Object
		):BehaviorTask
		{
			var bb:Blackboard = new Blackboard;
			var targetId:MutableEntry = bb.getEntry("id");
			var targetSlotIndex:MutableEntry = bb.getEntry("slot index");
			var targetSlotPath:MutableEntry = bb.getEntry("slot path");
			
			return whileGameObjectExists(target.id, 
				tf.sequence(
					chooseSlot(target, targetId, targetSlotIndex, targetSlotPath),
					walkPath(targetSlotPath)
				)
			);
		}
		
		
		public function buildGuardTask(
			field:GameObject,
			taskToken:Object
		):BehaviorTask
		{
			
			var bb:Blackboard = new Blackboard;
			var targetId:MutableEntry = bb.getEntry("id");
			var targetSlotIndex:MutableEntry = bb.getEntry("slot index");
			var targetSlotPath:MutableEntry = bb.getEntry("slot path");
			
			return whileGameObjectExists(field.id,
				tf.sequence(
					chooseSlot(field, targetId, targetSlotIndex, targetSlotPath),
					walkPath(targetSlotPath),
					withSlotSemaphore(targetId, targetSlotIndex,
						tf.parallel(
							runSlotAnimation(targetId, targetSlotIndex, taskToken)
						)
					)
				)
			);
		}
		
		
		
		public function buildProduceTask(
			machine:GameObject,
			taskToken:Object,
			input:Array,
			output:Array,
			onProduce:Function,
			storage:Storage
		):BehaviorTask
		{
			if (null == onProduce)
			{
				if(storage == null)
					onProduce = function():void{ };
				else
				{
					onProduce = function():void
					{
						var s:IsoSprite = machine.getIsoSprite();
						storage.store( output[0].name, s.offsetI, s.offsetJ);
					};
				}
			}
			
			
			
			var bb:Blackboard = new Blackboard;
			var machineId:MutableEntry = bb.getEntry("id");
			var machineSlotIndex:MutableEntry = bb.getEntry("slot index");
			var machineSlotPath:MutableEntry = bb.getEntry("slot path");
			
			return whileGameObjectExists(machine.id, 
				tf.sequence(
					buildEnsureInventoryStatusTask(input, taskToken),
					chooseSlot(machine, machineId, machineSlotIndex, machineSlotPath),
					walkPath(machineSlotPath),
					withSlotSemaphore(machineId, machineSlotIndex,
						tf.sequence(
							tf.call(function():void { ts.setInventory(null); } ),
							new RunForTask(5, runSlotAnimation(machineId, machineSlotIndex, taskToken)),
							tf.call(function():void {
								//ts.setInventory(output);
								onProduce()
							})
						)
					)
				)
			);
		}
		
		
		
		public function buildSellTask(target:GameObject, taskToken:Object, sellableItems:Array):BehaviorTask{
			var sellableItemsInStock:Array = this.gameContext.storage.getSellableOptions();
			
			var bestSell:Object = {name:'', price:0, amount:0};
			
			for(var i:int = 0; i < sellableItemsInStock.length; i++){
				var price:int = 0;
				for each(var item in sellableItems){
					if(item.outputItems[0].name == sellableItemsInStock[i].name){
						price = item.profit
							break;
					}
				}
				
				var amount = sellableItemsInStock[i].amount;
				amount = 1; //for now we just sell one thing at a time
				
				if(amount * price > bestSell.amount * bestSell.price){
					bestSell.name = sellableItemsInStock[i].name;
					bestSell.price = price;
					bestSell.amount = amount;
				}
			}
			
			if(bestSell.price == 0){
				
				return whileGameObjectExists(piggy.id,
					tf.selectWithPriority(
						tf.sequence(
							tf.call(function(){
								piggy.getBehaviourByType(PiggyTasksBehaviour).isConfused(
									{reason:PiggyTasksBehaviour.CONFUSED_MISSING_ITEM, item:ApplicationVars.VIVERA_BURGER}
								);
							})
						)
					)
				);
				
			}else{
				taskToken.selling = bestSell.name;
				
				
				function itemSold():void{
					var reward:int = bestSell.amount * bestSell.price;
					reward += Math.ceil(reward * ((piggy.getBehaviourByType(PiggyPropertiesBehaviour).sales-5) / 10));
					gameContext.cash += reward;
					
					SoundManager.playSound({id:'SO_Sell'});
					showLabel(target, 'shopGold', '$'+reward, -40);
				}
				
				return buildProduceTask(target, taskToken, [{name:bestSell.name, amount:bestSell.amount}], null, itemSold, null)
			}
		}
		
		
		private var labels:Vector.<Label> = new Vector.<Label>();
		private function showLabel(pTarget:GameObject, pType:String, pText:String, pStartY:int):void{
			var label:Label = labels.shift();
			if(!label) label = new Label();
			label.x = 0;
			label.nameList.add(pType);
			pTarget.getIsoSprite().addChild(label);
			
			label.text = pText;
			label.x = -5 + (Math.random()*10);
			label.y = pStartY;
			label.alpha = 1;
			
			var t:Tween = new Tween(label, 5, Transitions.EASE_OUT);
			t.animate('y', pStartY - 30);
			t.animate('alpha', 0);
			t.onComplete = animationDone;
			Starling.juggler.add(t);
			
			function animationDone():void{
				Starling.juggler.remove(t);
				
				pTarget.getIsoSprite().removeChild(label);
				labels.push(label);
			}
		}
		
		
		
		public function buildRestTask(
			taskToken:Object
		):BehaviorTask
		{
			
			var bb:Blackboard = new Blackboard;
			var houseId:MutableEntry = bb.getEntry("id");
			var houseSlotIndex:MutableEntry = bb.getEntry("slot index");
			var houseSlotPath:MutableEntry = bb.getEntry("slot path");
			var sleepSpeed = 2 + (piggy.getBehaviourByType(PiggyPropertiesBehaviour).relaxation*0.3);
			
			return tf.sequence(
				findSlot("house", houseId, houseSlotIndex, houseSlotPath),
				withSlotSemaphore(houseId, houseSlotIndex,
					tf.sequence(
						walkPath(houseSlotPath),
						tf.parallel(
							runSlotAnimation(houseId, houseSlotIndex, taskToken),
							new ChangeEnergyTask(piggy, sleepSpeed * baseEnergyDrain)
						)
					)
				)
			);
		}
		
		
		public function buildEatTask(
			taskToken:Object
		):BehaviorTask
		{
			var target:GameObject = getRandomGameObjectsByTag(['corn']);
			taskToken.targetId = target.id;
			taskToken.type = 'useField';
			
			Starling.current.stage.dispatchEvent(new PiggiesEvent(PiggiesEvent.PIGGY_ASSIGNED_TASK, piggy, target));
			
			return buildUseFieldTask(target, taskToken);
		}
		
		private function whileGameObjectExists(
			gameObjectId:uint,
			task:BehaviorTask
		):BehaviorTask
		{
			function exists():Boolean
			{
				return gameContext.gameObjects.getById(gameObjectId) as GameObject != null;
			}
			
			return tf.runWhile(
				tf.pred(exists),
				task
			);
		}
		
		
		
		
		
		private function getRandomGameObjectsByTag(pOptions:Array):GameObject{
			var result:Array = [];
			for each(var tag:String in pOptions){
				result = result.concat( gameContext.gameObjects.getByTag(tag) );
			}
			return result[Math.floor(Math.random() * result.length)]
		}
	}

}