package savethepiggies.game
{
	import feathers.controls.Button;
	import feathers.controls.Label;
	
	import savethepiggies.theme.Checkbox;
	
	import starling.display.Quad;
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.utils.AssetManager;

	public final class SettingsPanel extends Sprite {
		
		private var onSettingChange:Function;
		
		private var containers:Array;
		private var checks:Array;
		
		public function SettingsPanel(assetManager:AssetManager, onSettingChange){
			this.onSettingChange = onSettingChange;
			
			addChild(new Quad(100, 80, 0xffffff));
			
			containers = [];
			var titles:Array = ['Settings','Remove ingredients', 'Reboot'];
			for(var i:int = 0; i < 3; i++){
				var container:Sprite = new Sprite();
				container.visible = i == 0;
				addChild(container);
				containers.push(container);
				
				var title:Label = createText(titles[i], 'settingsTitle', 0, i == 0 ? 3 : 10, 100);
				container.addChild(title);
			}
			
			
			//container 0
			checks = [];
			var options:Array = ['Facebook posting', 'Enable background music', 'Enable sound effects', 'Empty ingredients', 'Reboot game'];
			var callbacks:Array = [toggleFacebookPosting, toggleBackgroundMusic, toggleSoundEffects, toggleEmpty, toggleReboot];
			for(var i = 0; i < options.length; i++){
				var label = createText(options[i], i < 3 ? "settingsLabel" : "settingsWarnLabel", 8, 20 + (i*10), 70);
				containers[0].addChild(label);
				
				var checkbox:Checkbox = new Checkbox(assetManager, callbacks[i]);
				checkbox.x = 86;
				checkbox.y = 21 + (i*10);
				containers[0].addChild(checkbox);
				checks.push(checkbox);
			}
			
			
			//container 1
			containers[1].addChild(
				createText('Are you sure you want to remove all of your ingredients permanently?','settingsLabelCenter', 10, 30, 80)
			);
			containers[1].addChild(
				createButton('Yes', 10, 60, 35, 10, true, emptyIngredients)
			);
			containers[1].addChild(
				createButton('No', 55, 60, 35, 10, false, goBack)
			);
			
			
			//container 2
			containers[2].addChild(
				createText('Are you sure you want to restart the entire game? All your progress will be lost.','settingsLabelCenter', 10, 30, 80)
			);
			containers[2].addChild(
				createButton('Yes', 10, 60, 35, 10, true, goReboot)
			);
			containers[2].addChild(
				createButton('No', 55, 60, 35, 10, false, goBack)
			);
		}
		public function init(pEnabledOptions:Array):void{
			for(var i = pEnabledOptions.length; i--;){
				checks[i].setEnabled(pEnabledOptions[i]);
			}
			goBack();
		}
		
		private function toggleFacebookPosting(pTrue:Boolean):void{
			onSettingChange('enableFacebookPosting', pTrue);
		}
		private function toggleBackgroundMusic(pTrue:Boolean):void{
			onSettingChange('enableBackgroundMusic', pTrue);
		}
		private function toggleSoundEffects(pTrue:Boolean):void{
			onSettingChange('enableSoundEffects', pTrue);
		}
		private function toggleEmpty(pTrue:Boolean):void{
			containers[0].visible = false;
			containers[1].visible = true;
		}
		private function emptyIngredients():void{
			onSettingChange('emptyStorage', true);
			goBack();
		}
		private function toggleReboot(pTrue:Boolean):void{
			containers[0].visible = false;
			containers[2].visible = true;
		}
		private function goReboot():void{
			onSettingChange('reboot', true);
		}
		
		
		
		private function goBack():void{
			for(var i = containers.length; i--;){
				containers[i].visible = i == 0;
			}
			checks[3].setEnabled(false);
			checks[4].setEnabled(false);
		}
		
		
		
		
		private function createText(pValue:String, pType:String, pX:int, pY:int, pWidth:int):Label{
			var title:Label = new Label();
			title.touchable = false;
			title.text = pValue;
			title.x = pX;
			title.y = pY;
			title.width = pWidth;
			title.nameList.add(pType);
			return title;
		}
		private function createButton(pValue:String, pX:int, pY:int, pWidth:int, pHeight:int, pIsMain:Boolean = true, pCallback:Function = null):Button{
			var button:Button = new Button();
			button.alpha = pIsMain ? 1 : 0.5;
			button.label = pValue;
			button.x = pX;
			button.y = pY;
			button.setSize(pWidth, pHeight);
			button.addEventListener(Event.TRIGGERED, pCallback);
			return button
		}
	}
}