package savethepiggies.game 
{
	import flash.geom.Rectangle;
	
	import org.as3commons.logging.api.ILogger;
	import org.as3commons.logging.api.getLogger;
	
	import starling.display.DisplayObject;
	import starling.display.Sprite;
	
	public final class IsoSpriteDisplay extends Sprite 
	{
		private static const logger:ILogger = getLogger(IsoSpriteDisplay);
		
		private var isoSprites:Array;
		
		private var layers:Array = [];
		
		public function IsoSpriteDisplay() 
		{
			isoSprites = [];
		}
		
		public function addLayer(layerName:String):void
		{
			if (layers.indexOf(layerName) != -1)
				logger.warn("Layer {0} already exists", [layerName]);
			else
				layers.push(layerName);
		}
		
		public function orderSprites(
			orderAll:Boolean
		):void
		{
			var visible:Array = getVisible(orderAll);
			buildStaticDag(visible);
			sortStaticDag(visible);
			visible.sort(function(a:IsoSprite, b:IsoSprite):int {
				if (a.isoDepth == b.isoDepth) return 0;
				else return a.isoDepth - b.isoDepth < 0 ? -1 : 1;
			});
			
			for each (var sprite:IsoSprite in visible)
			{
				setChildIndex(sprite, sprite.isoDepth);
			}
		}
		
		private function getVisible(includeAll:Boolean):Array
		{
			var visible:Array = [];
			
			var boundsRect:Rectangle = new Rectangle(0, 0, stage.stageWidth, stage.stageHeight);
			var out:Rectangle = new Rectangle;
			for (var i:int = 0; i < numChildren; ++i)
			{
				var child:DisplayObject = getChildAt(i);
				if (includeAll || child.getBounds(this.root, out).intersects(boundsRect))
					visible.push(child);
			}
			
			return visible;
		}
		
		private function sortStaticDag(visible:Array):void
		{
			var sortDepth:int = 0;
			for (var i:int = 0; i < visible.length; ++i)
			{
				visitNode(visible[i] as IsoSprite);
			}
			
			function visitNode(n:IsoSprite):void
			{
				if (!n.isoVisitedFlag)
				{
					n.isoVisitedFlag = true;
							 
					const spritesBehindLength:int = n.isoBehind.length;
					for (var i:int = 0; i < spritesBehindLength; ++i)
					{
						visitNode(n.isoBehind[i]);
					}
							 
					n.isoDepth = sortDepth++;
				}
			}
		}
		
		private function buildStaticDag(visible:Array):void
		{
			for (var i:int = 0; i < visible.length; ++i)
			{
				var a:IsoSprite = visible[i] as IsoSprite;
				a.isoVisitedFlag = false;
				a.isoBehind = [];

				for (var j:int = 0; j < visible.length; ++j)
				{
					if (i != j)
					{
						var b:IsoSprite = visible[j] as IsoSprite;
						if (isBehind(b, a)) a.isoBehind.push(b);
					}
				}
			}
		}
		
		// Sort normally. Doesn't really work properly due to mutually "unbehind"
		// objects not necessarily belonging next to each other in the display order.
		private function simpleSortFunc(a:IsoSprite, b:IsoSprite):int
		{
			var r:int = 0;
			
			if (isBehind(a, b)) r = -1;
			else if (isBehind(b, a)) r = 1;
			
			return r;
		}
		
		private function isBehind(a:IsoSprite, b:IsoSprite):Boolean
		{
			var overlapI:Boolean = !(a.maxI <= b.minI || b.maxI <= a.minI);
			var overlapJ:Boolean = !(a.maxJ <= b.minJ || b.maxJ <= a.minJ);
			
			var r:Boolean = false;
			
			if (overlapI && overlapJ) 
			{
				// Sort according to layers
				if (a.layerName != b.layerName)
				{
					r = layers.indexOf(a.layerName) < layers.indexOf(b.layerName);
				}
				else
				{
					// Special logic needed if these are to overlap...
				}
			}
			else
			{
				var behindI:Boolean = a.maxI <= b.minI;
				var behindJ:Boolean = a.maxJ <= b.minJ;
				
				     if (overlapI && behindJ) r = true;
				else if (overlapJ && behindI) r = true;
				else if (!overlapI && !overlapJ)
				{
					if (behindI && behindJ) r = true;
				}
			}
			
			return r;
		}
		
		public function addIsoSprite(
			sprite:IsoSprite
		):Boolean
		{
			var added:Boolean;
			
			if (isoSprites[sprite.id] != undefined)
				logger.warn("isometric sprite with ID {0} already added", [sprite.id]);
			else
			{
				addChild(sprite);
				
				isoSprites[sprite.id] = sprite;
				
				added = true;
				if (logger.infoEnabled)
					logger.info("Added isometric sprite with ID {0}", [sprite.id]);
			}
			
			return added;
		}
		
		public function removeIsoSprite(sprite:IsoSprite):void
		{
			if (undefined === isoSprites[sprite.id])
				logger.warn("can't find isometric sprite with id {0} in array", [sprite.id]);
			else
				delete isoSprites[sprite.id];
				
			if (getChildIndex(sprite) == -1)
				logger.warn("can't find isometric sprite with id {0} as child", [sprite.id]);
			else
				removeChild(sprite);
		}
		
		public function getIsoSprites():Array { return isoSprites; }
	}
}