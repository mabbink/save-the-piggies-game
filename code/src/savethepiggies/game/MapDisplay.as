package savethepiggies.game 
{
	import flash.geom.Point;
	
	import org.as3commons.logging.api.ILogger;
	import org.as3commons.logging.api.getLogger;
	
	import savethepiggies.map.Map;
	import savethepiggies.map.MapLayer;
	
	import starling.display.Image;
	import starling.display.QuadBatch;
	import starling.display.Sprite;
	import starling.textures.TextureAtlas;
	
	public final class MapDisplay extends Sprite
	{
		private static const logger:ILogger = getLogger(MapDisplay);
		
		private var atlas:TextureAtlas;
		
		public function MapDisplay(atlas:TextureAtlas)
		{
			this.atlas = atlas;
		}
		
		public function drawMap(map:Map):void
		{
			logger.debug("starting drawing map");
			// Revert to start state
			while (numChildren > 0) removeChildAt(0);
			
			// Draw map tiles for each layer
			var batch:QuadBatch = new QuadBatch();
			
			drawLayer(map.getLayerByName("Terrain"), map, batch);
			drawLayer(map.getLayerByName("Markings"), map, batch);
			drawLayer(map.getLayerByName("Buildings"), map, batch);
			logger.debug("...finished map drawing");
			
			addChild(batch);
		}
		
		private var drawPoint:Point = new Point;
		private function drawLayer(layer:MapLayer, map:Map, batch:QuadBatch):void
		{
			var w:int = map.width;
			var h:int = map.height;
			var htw:Number = map.tileWidth / 2.0;
			var th:Number = map.tileHeight;
			
			for (var j:int = 0; j < h; ++j)
			{
				for (var i:int = 0; i < w; ++i)
				{
					var tileId:uint = layer.data[i + j * w];
					if (0 !== tileId)
					{
						var textureName:String = map.getTilesetName(tileId) + "_" + map.getIndexInTileset(tileId);
						var offsetX:int = map.getTileOffsetX(tileId);
						var offsetY:int = map.getTileOffsetY(tileId);
						
						var image:Image = new Image(atlas.getTexture(textureName));
						map.isometricToCartesian(i, j, drawPoint);
						image.x = drawPoint.x + offsetX - htw;
						image.y = drawPoint.y + offsetY - image.height + th;
						batch.addImage(image);
					}
				}
			}
		}
	}

}