package savethepiggies.game 
{
	import savethepiggies.HasId;
	public final class GameObject implements HasId
	{
		private var _id:uint;
		private var isoSprites:ObjectRegistry;
		
		private var taskTargets:ObjectRegistry;
		private var behaviors:ObjectRegistry;
		private var specialNodes:ObjectRegistry;
		private var tags:Object;
		
		private var _recipeName:String;
		private var _createdAtI:int;
		private var _createdAtJ:int;
		
		public function GameObject(
			id:uint,
			recipeName:String,
			createdAtI:int,
			createdAtJ:int
		) 
		{
			_id = id;
			_recipeName = recipeName;
			_createdAtI = createdAtI;
			_createdAtJ = createdAtJ;
			
			isoSprites = new ObjectRegistry;
			behaviors = new ObjectRegistry;
			taskTargets = new ObjectRegistry;
			specialNodes = new ObjectRegistry;
			tags = { };
		}
		
		public function cleanup():void
		{
			behaviors.visit(function(b:Behaviour):void {
				b.cleanup();
			});
		}
		
		public function addTag(tag:String):void
		{
			tags[tag] = true;
		}
		
		public function hasTag(tag:String):Boolean
		{
			return tags.hasOwnProperty(tag);
		}
		public function getTags():Object{
			return tags;
		}
		
		public function addTaskTarget(
			taskTarget:TaskTarget
		):void
		{
			taskTargets.register(taskTarget);
		}
		
		public function getTaskTarget():TaskTarget
		{
			return taskTargets.getFirst() as TaskTarget;
		}
		
		public function visitTaskTargets(f:Function):void
		{
			taskTargets.visit(f);
		}
		
		public function addSpecialNode(
			specialNode:SpecialNode
		):void
		{
			specialNodes.register(specialNode);
		}
		
		public function visitSpecialNodes(f:Function):void
		{
			specialNodes.visit(f);
		}
		
		public function addBehaviour(
			behavior:Behaviour
		):void
		{
			behaviors.register(behavior);
		}
		
		public function getBehaviourByType(type:Class):*
		{
			var found:Behaviour = null;
			
			behaviors.visit(function(b:Behaviour):void {
				if (b is type) found = b;
			});
			
			return found;
		}
		
		public function getBehavioursByType(type:Class):*
		{
			var r:Array = [];
			
			behaviors.visit(function(b:Behaviour):void {
				if (b is type) r.push(b);
			});
			
			return r;
		}
		
		public function visitBehaviours(f:Function):void
		{
			behaviors.visit(f);
		}
		
		public function addIsoSprite(
			isoSprite:IsoSprite
		):void
		{
			isoSprites.register(isoSprite);
		}
		
		public function getIsoSpriteById(
			id:uint
		):IsoSprite
		{
			return isoSprites.getById(id) as IsoSprite;
		}
		
		public function getIsoSprite():IsoSprite
		{
			return isoSprites.getFirst() as IsoSprite;
		}
		public function getIsoSpriteWithLayerName(pName:String):IsoSprite{
			var sprite:IsoSprite;
			isoSprites.visit(function(currentSprite:IsoSprite):void{
				if(currentSprite.layerName == pName) sprite = currentSprite;
			});
			return sprite;
		}
		
		public function visitIsoSprites(f:Function):void
		{
			isoSprites.visit(f);
		}
		
		// "Events"
		public function onSelect():void
		{
			behaviors.visit(function(b:Behaviour):void {
				b.onSelect();
			});
		}
		
		public function onDeselect():void
		{
			behaviors.visit(function(b:Behaviour):void {
				b.onDeselect();
			});
		}
		
		public function activate():void
		{
			var that:GameObject = this;
			
			behaviors.visit(function(b:Behaviour):void {
				b.gameObject = that;
				b.activate();
			});
		}
		
		public var isPart:Boolean = false;
		public function get id():uint { return _id; }
		public function get recipeName():String { return _recipeName; }
		public function get createdAtI():int { return _createdAtI; }
		public function get createdAtJ():int { return _createdAtJ; }
	}

}