package savethepiggies.game 
{
	import flash.geom.Point;
	import flash.geom.Rectangle;
	
	import spark.primitives.Rect;
	
	import org.as3commons.logging.api.ILogger;
	import org.as3commons.logging.api.getLogger;
	import org.gestouch.events.GestureEvent;
	import org.gestouch.gestures.Gesture;
	import org.gestouch.gestures.PanGesture;
	import org.gestouch.gestures.TapGesture;
	
	import savethepiggies.game.events.PlacingEvent;
	import savethepiggies.map.Map;
	
	import starling.display.DisplayObject;
	import starling.display.Image;
	import starling.display.Sprite;
	import starling.filters.ColorMatrixFilter;
	import starling.textures.Texture;
	
	public final class PlaceUi extends Sprite 
	{
		private static const logger:ILogger = getLogger(PlaceUi);
		
		public var panFactor:Number = 1;
		
		private var toDispose:Array;
		private var checks:Array;
		
		private var gameContext:GameContext;
		private var map:Map;
		private var grid:GameGrid;
		private var gridTexture:Texture;
		private var addTexture:Texture;
		private var cancelTexture:Texture;
		
		public function PlaceUi(
			gameContext:GameContext,
			map:Map,
			grid:GameGrid,
			gridTexture:Texture,
			addTexture:Texture,
			cancelTexture:Texture
		)
		{
			super();
			
			this.gameContext = gameContext;
			this.map = map;
			this.grid = grid;
			this.gridTexture = gridTexture;
			this.addTexture = addTexture;
			this.cancelTexture = cancelTexture;
			
			toDispose = [];
			checks = [];
		}
		
		public function cleanup():void
		{
			for each (var g:Gesture in toDispose)
				g.dispose();
				
			removeEventListeners();
		}
		
		public function place(
			recipe:Object,
			placeOnTopOf:Array,
			d:DisplayObject,
			w:uint,
			h:uint,
			x:Number,
			y:Number,
			startPosX:Number,
			startPosY:Number,
			showCancel:Boolean = false,
			callOnFinallyPlaced:Function = null
		):void
		{
			var s:Sprite = new Sprite;
			var gridGraphic:DisplayObject = createGridGraphic(w, h);
			d.alpha = 0.5;
			d.x = x;
			d.y = y;
			d.touchable = false;
			s.addChild(gridGraphic);
			s.addChild(d);
			addChild(s);
			
			var ui:Sprite = createUi(showCancel);
			s.addChild(ui);
			
			var panGesture:PanGesture = new PanGesture(gridGraphic);
			panGesture.addEventListener(GestureEvent.GESTURE_CHANGED, onPanChange);
			toDispose.push(panGesture);
			
			var addGesture:TapGesture = new TapGesture(ui.getChildByName("add"));
			addGesture.addEventListener(GestureEvent.GESTURE_RECOGNIZED, onAddTap);
			toDispose.push(addGesture);
			
			var cancelGesture:TapGesture = new TapGesture(ui.getChildByName("cancel"));
			cancelGesture.addEventListener(GestureEvent.GESTURE_RECOGNIZED, onCancelTap);
			toDispose.push(cancelGesture);
			
			s.x = startPosX;
			s.y = startPosY;
			
			var realPosX:Number = s.x;
			var realPosY:Number = s.y;
			var rect:Rectangle = new Rectangle(s.x, s.y, gridGraphic.width, gridGraphic.height);
			
			checks.push(drawCheck);
			
			var p:Point = new Point;
			
			function check():Boolean
			{
				map.cartesianToIsometric(realPosX, realPosY, p);
				
				var iPos:int = 2 * Math.round(p.x);
				var jPos:int = 2 * Math.round(p.y);
				
				for (var j:int = jPos; j < jPos + 2 * h; ++j)
				{
					for (var i:int = iPos; i < iPos + 2 * w; ++i)
					{
						var nodes:Array = grid.getSpecialNodes(i, j);
						if (placeOnTopOf.length > 0 && nodes.length == 0){
							return false;
						}else{
							for each (var node:SpecialNode in nodes)
							{
								if (placeOnTopOf.length > 0)
								{
									for each (var onTopType:String in placeOnTopOf)
									{
										if (node.occupiedType && node.occupiedType != onTopType)
										{
											if (logger.debugEnabled)
											logger.debug("{0} != {1}", [node.occupiedType, onTopType]);
											return false;
										}
									}
								}
								else
								{
									if (node.occupiedType){
										return false;
									}
								}
							}
						}
					}
				}
				
				var valid:Boolean = true;
				rect.x = s.x - (rect.width/2);
				rect.y = s.y;
				gameContext.piggies.visit(function(piggy:GameObject):void{
					if(rect.contains(piggy.getIsoSprite().x, piggy.getIsoSprite().y)){
						valid = false;
					}
				});
				
				return valid;
			}
			
			var gridGoodFilter:ColorMatrixFilter;
			var gridBadFilter:ColorMatrixFilter;
			var addGoodFilter:ColorMatrixFilter;
			var addBadFilter:ColorMatrixFilter;
			
			gridGoodFilter = new ColorMatrixFilter;
			gridBadFilter = new ColorMatrixFilter;
			addGoodFilter = new ColorMatrixFilter;
			addBadFilter = new ColorMatrixFilter;
			
			gridBadFilter.adjustHue( -0.53);
			addBadFilter.adjustSaturation( -1);
			
			toDispose.push(gridGoodFilter);
			toDispose.push(gridBadFilter);
			toDispose.push(addGoodFilter);
			toDispose.push(addBadFilter);
			
			function drawCheck():void
			{
				var valid:Boolean = check();
				ui.getChildByName("add").filter = valid ? addGoodFilter : addBadFilter;
				gridGraphic.filter = valid ? gridGoodFilter : gridBadFilter;
				
				if (ui.getChildByName("add").useHandCursor && !valid)
					ui.getChildByName("add").useHandCursor = false;
					
				if (!ui.getChildByName("add").useHandCursor && valid)
					ui.getChildByName("add").useHandCursor = true;
			}
			
			drawCheck();
			
			function onPanChange(e:GestureEvent):void
			{
				var g:PanGesture = e.target as PanGesture;
				
				var offsetX:Number = g.offsetX * panFactor;
				var offsetY:Number = g.offsetY * panFactor;
				
				realPosX += offsetX;
				realPosY += offsetY;
				
				map.cartesianToIsometric(realPosX, realPosY, p);
				p.x = Math.round(p.x);
				p.y = Math.round(p.y);
				map.isometricToCartesian(p.x, p.y, p);
				s.x = p.x;
				s.y = p.y;
			}
			
			function onAddTap():void
			{
				if (check())
				{
					removeThis();
					
					map.cartesianToIsometric(realPosX, realPosY, p);
					
					if(recipe is String){
						var e:PlacingEvent = new PlacingEvent(PlacingEvent.PLACED);
						e.i = Math.round(p.x);
						e.j = Math.round(p.y);
						e.recipeName = recipe as String;
						e.callOnFinallyPlaced = callOnFinallyPlaced;
						dispatchEvent(e);
					}else{
						for each(var obj:Object in recipe){
							var e:PlacingEvent = new PlacingEvent(PlacingEvent.PLACED);
							e.i = Math.round(p.x);
							e.j = Math.round(p.y);
							e.recipeName = obj.recipe;
							dispatchEvent(e);
						}
					}
				}
			}
			
			function onCancelTap():void
			{
				removeThis();
				
				var e:PlacingEvent = new PlacingEvent(PlacingEvent.CANCELLED);
				dispatchEvent(e);
			}
			
			function removeThis():void
			{
				checks.splice(checks.indexOf(check), 1);
				clearDisposable(panGesture);
				clearDisposable(addGesture);
				clearDisposable(cancelGesture);
				clearDisposable(gridGoodFilter);
				clearDisposable(gridBadFilter);
				clearDisposable(addGoodFilter);
				clearDisposable(addBadFilter);
				
				removeChild(s);
			}
			
			function clearDisposable(disposable:*):void
			{
				toDispose.splice(toDispose.indexOf(disposable), 1);
				disposable.dispose();
			}
		}
		
		private function createUi(showCancel:Boolean = true):Sprite
		{	
			var ui:Sprite = new Sprite;
			
			var add:Image = new Image(addTexture);
			ui.addChild(add);
			add.x = 7;
			add.y = -20;
			add.name = "add";
			add.useHandCursor = true;	
			
			if(showCancel)
			{
				var cancel:Image = new Image(cancelTexture);
				ui.addChild(cancel);
				cancel.x = -7 - add.width;
				cancel.y = add.y;
				cancel.name = "cancel";
				cancel.useHandCursor = true;
			}
			
			return ui;
		}
		
		private function createGridGraphic(w:uint, h:uint):DisplayObject
		{
			var s:Sprite = new Sprite;
			for (var j:uint = 0; j < h; ++j)
			{
				for (var i:uint = 0; i < w; ++i)
				{
					var img:Image = new Image(gridTexture);
					img.x = map.isometricToCartesian(i, j).x - 0.5 * img.width;
					img.y = map.isometricToCartesian(i, j).y;
					s.addChild(img);
				}
			}
			
			s.alpha = 0.5;
			
			return s;
		}
		
		public function draw():void
		{
			for each (var check:Function in checks)
				check();
		}
	}
}