package savethepiggies.game 
{
	import flash.display.StageDisplayState;
	import flash.events.MouseEvent;
	import flash.external.ExternalInterface;
	
	import feathers.controls.Alert;
	import feathers.controls.Label;
	import feathers.data.ListCollection;
	
	import savethepiggies.GlobalContext;
	import savethepiggies.game.behaviours.PiggyPropertiesBehaviour;
	import savethepiggies.game.behaviours.TappableBehaviour;
	import savethepiggies.game.events.MarketEvent;
	import savethepiggies.game.events.PiggiesEvent;
	import savethepiggies.game.events.ShopEvent;
	import savethepiggies.game.events.StorageEvent;
	import savethepiggies.sound.SoundManager;
	import savethepiggies.theme.HeaderButton;
	import savethepiggies.theme.HeaderListItem;
	import savethepiggies.theme.HeaderPiggyItem;
	import savethepiggies.theme.HeaderSellerItem;
	
	import starling.animation.Tween;
	import starling.core.Starling;
	import starling.display.Image;
	import starling.display.Quad;
	import starling.display.Sprite;
	import starling.events.Touch;
	import starling.events.TouchEvent;
	import starling.events.TouchPhase;
	import starling.textures.Texture;
	
	public final class Hud extends Sprite 
	{		
		private var gameScene:GameScene;
		private var gameContext:GameContext;
		private var globalContext:GlobalContext;
		
		private var bg:Quad;
		private var moneyLabel:Label;
		private var inviteButton:Sprite;
		
		
		private var storageButton:HeaderButton;
		private var storageListContainer:Sprite;
		private var storageListItems:Vector.<HeaderListItem>;
		
		private var shopButton:HeaderButton;
		private var shopListContainer:Sprite;
		private var shopListItems:Vector.<HeaderListItem>;
		
		private var piggiesButton:HeaderButton;
		private var piggiesListContainer:Sprite;
		private var piggiesListItems:Vector.<HeaderPiggyItem>;
		
		private var sellers:Array;
		private var sellersItems:Vector.<HeaderSellerItem>;
		
		private var _showSettings:Sprite;
		private var _goFullScreen:Sprite;
		private var _goPromo:Sprite;
		
		private var redGlow:Image;
		
		
		
		private var ingredients:Object = [
			{id:ApplicationVars.LUPIN, 			amount:0, profit:0, label:'Lupine'},
			{id:ApplicationVars.PREPPED_LUPIN, 	amount:0, profit:0, label:'Prepared lupine'},
			{id:ApplicationVars.COOKED_LUPIN,	amount:0, profit:0, label:'Cooked lupine'},
			{id:ApplicationVars.TOMATO,			amount:0, profit:0, label:'Tomatos'},
			{id:ApplicationVars.CUCUMBER,		amount:0, profit:0, label:'Cucumbers'},
			{id:ApplicationVars.PEPPER,			amount:0, profit:0, label:'Peppers'},
			{id:ApplicationVars.APPLE,			amount:0, profit:0, label:'Apples'},
			{id:ApplicationVars.BANANA,			amount:0, profit:0, label:'Bananas'},
			{id:ApplicationVars.GRAPE,			amount:0, profit:0, label:'Grapes'},
			{id:ApplicationVars.FLOWER,			amount:0, profit:0, label:'Flowers'},
			{id:ApplicationVars.FLOUR,			amount:0, profit:0, label:'Flour'}
		];
		private var shopItems:Object = [
			{id:ApplicationVars.PICMAC,			amount:0, profit:0, label:'Pigmac'},
			{id:ApplicationVars.BURRITO,		amount:0, profit:0, label:'Burrito'},
			{id:ApplicationVars.PIZZA,			amount:0, profit:0, label:'Pizza'},
			{id:ApplicationVars.SMOOTHY,		amount:0, profit:0, label:'Smoothy'},
			{id:ApplicationVars.VIVERA_BURGER,	amount:0, profit:0, label:'Vivera burger'},
			{id:ApplicationVars.SHASHLIK,		amount:0, profit:0, label:'Shashlik'},
			{id:ApplicationVars.FRUIT_SHASHLIK,	amount:0, profit:0, label:'Fruit Shashlik'}
		];
		
		
		public function Hud(
			gameScene:GameScene,
			gameContext:GameContext,
			globalContext:GlobalContext
		) 
		{
			super();
			
			this.gameScene = gameScene;
			this.gameContext = gameContext;
			this.globalContext = globalContext;
			
			
			var items:Array = globalContext.assetManager.getObject("shop-items").items[0].recipes;
			for each(var item:Object in items){
				for each(var shopItem in shopItems){
					if(shopItem.id == item.outputItems[0].name) shopItem.profit = item.profit;
				}
			}
			
			bg = new Quad(2, 11, 0xffffffff);
			addChild(bg);
			
			moneyLabel = new Label;
			moneyLabel.text = "Gold: 0";
			moneyLabel.x = 3;
			moneyLabel.y = 2;
			moneyLabel.nameList.add("moneyLabel");
			addChild(moneyLabel);
			
			inviteButton = new Sprite();
			inviteButton.y = 1;
			inviteButton.x = 36;
			inviteButton.addChild(new Image(globalContext.assetManager.getTexture('invite')));
			addChild(inviteButton);
			inviteButton.addEventListener(TouchEvent.TOUCH, goInvite);
			
			if(ExternalInterface.available) ExternalInterface.addCallback("invitedPeople", invitedPeople);
			
			
			_showSettings = new Sprite();
			_showSettings.useHandCursor = true;
			_showSettings.addChild(new Image(globalContext.assetManager.getTexture('settings')));
			_showSettings.addEventListener(TouchEvent.TOUCH, showSettings);
			_showSettings.x = inviteButton.x + inviteButton.width;
			_showSettings.y = 1;
			addChild(_showSettings);
			
			
			_goFullScreen = new Sprite();
			_goFullScreen.useHandCursor = true;
			_goFullScreen.addChild(new Image(globalContext.assetManager.getTexture('fullscreen')));
			_goFullScreen.addEventListener(TouchEvent.TOUCH, goFullscreen);
			_goFullScreen.x = _showSettings.x + _showSettings.width;
			_goFullScreen.y = 1;
			addChild(_goFullScreen);
			
			
			_goPromo = new Sprite();
			_goPromo.useHandCursor = true;
			_goPromo.addChild(new Image(globalContext.assetManager.getTexture('promo')));
			_goPromo.addEventListener(TouchEvent.TOUCH, showPromo);
			_goPromo.x = _goFullScreen.x + _goFullScreen.width;
			_goPromo.y = 1;
			addChild(_goPromo);
			
			
			
			
			sellers = []
			sellersItems = new Vector.<HeaderSellerItem>();
			
			
			
			piggiesListContainer = new Sprite();
			piggiesListContainer.visible = false;
			addChild(piggiesListContainer);
			piggiesListItems = new Vector.<HeaderPiggyItem>();
			
			piggiesButton = new HeaderButton(globalContext, 'Piggies');
			addChild(piggiesButton);
			piggiesButton.addEventListener(TouchEvent.TOUCH, togglePiggies);
			
			
			
			storageListContainer = new Sprite();
			storageListContainer.visible = false;
			addChild(storageListContainer);
			storageListItems = new Vector.<HeaderListItem>();
			
			var empty:HeaderListItem = new HeaderListItem('Your storage is empty..', -1, 0, true, null, false);
			empty.name = 'empty';
			storageListContainer.addChild(empty);
			storageListItems.push(empty);
			
			storageButton = new HeaderButton(globalContext, 'Storage');
			addChild(storageButton);
			storageButton.addEventListener(TouchEvent.TOUCH, toggleStorage);
			
			
			
			shopListContainer = new Sprite();
			shopListContainer.visible = false;
			addChild(shopListContainer);
			shopListItems = new Vector.<HeaderListItem>();
			
			empty = new HeaderListItem('Your shop is empty..', -1, 0, true, null, false);
			empty.name = 'empty';
			shopListContainer.addChild(empty);
			shopListItems.push(empty);
			
			shopButton = new HeaderButton(globalContext, 'Shop');
			addChild(shopButton);
			shopButton.addEventListener(TouchEvent.TOUCH, toggleShop);
			
			
			
			
			redGlow = new Image(globalContext.assetManager.getTexture('red_glow'));
			redGlow.touchable = false;
			redGlow.visible = false;
			addChild(redGlow);
			
			
			
			
			Starling.current.stage.addEventListener(StorageEvent.STORAGE_ADD, stuffAdded);
			Starling.current.stage.addEventListener(StorageEvent.STORAGE_REMOVE, stuffRemoved);
			
			Starling.current.stage.addEventListener(ShopEvent.SELLER_DEFINED, sellerDefined);
			Starling.current.stage.addEventListener(ShopEvent.SELLER_STOPPED, sellerStopped);
			
			Starling.current.stage.addEventListener(PiggiesEvent.PIGGY_ADDED, piggyAdded);
			Starling.current.stage.addEventListener(PiggiesEvent.PIGGY_REMOVED, piggyRemoved);
			Starling.current.stage.addEventListener(PiggiesEvent.PIGGY_ASSIGNED_TASK, piggyAssignedTask);
			
			Starling.current.nativeStage.addEventListener(MouseEvent.MOUSE_UP, goFullscreen2);
		}
		
		
		private function goInvite(e:TouchEvent):void{
			var touches:Vector.<Touch> = e.getTouches(this,TouchPhase.ENDED);
			if(touches.length == 0) return;
			
			var invite = function(){
				if (ExternalInterface.available) ExternalInterface.call('fbInvitePeople');
			}
				
			Alert.show(
				"Invite your friends to help us to! You will get a gold bonus as a reward.\nInvite 1 friend: 10 gold\nInvite 2 friends: 40 gold\nInvite 3 friends:90 gold\nInvite 4 friends:160 gold",
				"Invite friends",
				new ListCollection([
					{label:"Invite", triggered:invite},
					{label:"Cancel"}
				]),
				new Image(globalContext.assetManager.getTexture('ralf'))
			);
		}
		private function invitedPeople(pPeople:Array):void{
			if(pPeople.length > 0){
				var invitedPeople:Array = gameContext.invitedPeople.split(',');
				var reward:int = 0;
				
				for(var i:int = 0; i < pPeople.length; i++){
					var found:Boolean = false;
					for each(var invited:String in invitedPeople){
						if(invited == pPeople[i]) {
							found = true;
							break;
						}
					}
					if(!found){
						reward += 20;
						gameContext.invitedPeople += ','+pPeople[i];
					}else{
						reward += 4;
					}
				}
				reward = reward * (pPeople.length*.5);
				
				gameContext.cash += reward;
				
				gameScene.save();
				
			}
		}
		
		
		private var canGoFullscreen:Number = -1;
		
		private function goFullscreen(e:TouchEvent):void{
			if(e.getTouches(_goFullScreen,TouchPhase.HOVER).length > 0 || e.getTouches(_goFullScreen,TouchPhase.BEGAN).length > 0){
				canGoFullscreen = new Date().getTime();
			}else{
				canGoFullscreen = 0;
			}
		}
		private function goFullscreen2(pEvent:MouseEvent):void{
			if(canGoFullscreen < new Date().getTime() - 2000 && canGoFullscreen != -1) return;
			canGoFullscreen = 0;
			
			if(Starling.current.nativeStage.displayState == StageDisplayState.NORMAL){
				Starling.current.nativeStage.displayState = StageDisplayState.FULL_SCREEN_INTERACTIVE;
				Image(_goFullScreen.getChildAt(0)).texture = globalContext.assetManager.getTexture('fullscreen-small');
			}else{
				Starling.current.nativeStage.displayState = StageDisplayState.NORMAL;
				Image(_goFullScreen.getChildAt(0)).texture = globalContext.assetManager.getTexture('fullscreen');
			}
		}
		
		
		private var _settingsBg:Sprite;
		private var _settingsPanel:SettingsPanel;
		
		public function showSettings(e:TouchEvent = null):void{
			if(e){
				var touches:Vector.<Touch> = e.getTouches(this,TouchPhase.BEGAN);
				if(touches.length == 0) return;
			}
			gameScene.pause(null);
			
			if(!_settingsBg){
				_settingsBg = new Sprite();
				_settingsBg.addChild(new Quad(2,2,0xffffff));
				_settingsBg.addEventListener(TouchEvent.TOUCH, closeSettings);
				_settingsBg.alpha = 0.6;
				
				_settingsPanel = new SettingsPanel(globalContext.assetManager, onSettingChange);
			}
			addChild(_settingsBg);
			_settingsBg.width = Starling.current.stage.stageWidth;
			_settingsBg.height = Starling.current.stage.stageHeight;
			
			addChild(_settingsPanel);
			_settingsPanel.x = (Starling.current.stage.stageWidth/2) - (_settingsPanel.width/2);
			_settingsPanel.y = (Starling.current.stage.stageHeight/2) - (_settingsPanel.height/2);
			_settingsPanel.init([
				gameContext.enableFacebookPosting,
				gameContext.enableBackgroundMusic,
				gameContext.enableSoundEffects
			]);
		}
		private function onSettingChange(pSetting:String, pValue:Boolean):void{
			switch(pSetting){
				case 'enableBackgroundMusic':
					gameContext[pSetting] = pValue;
					SoundManager.enableMusic(pValue, gameContext.enableSoundEffects);
					break;
				
				case 'enableSoundEffects':
					gameContext[pSetting] = pValue;
					SoundManager.enableMusic(gameContext.enableBackgroundMusic, pValue);
					break;
				
				case 'enableFacebookPosting':
					gameContext[pSetting] = pValue;
					break;
				
				case 'emptyStorage':
					gameContext.storage.removeAll(true, false);
					break;
				
				case 'reboot':
					gameScene.reboot();
					break;
			}
		}
		private function closeSettings(e:TouchEvent):void{
			var touches:Vector.<Touch> = e.getTouches(this,TouchPhase.BEGAN);
			if(touches.length == 0) return;
			
			removeChild(_settingsBg);
			removeChild(_settingsPanel);
			
			gameScene.resume(null);
		}
		
		
		
		private var _promoPanelBG:Sprite;
		private var _promoPanel:PromoPanel;
		
		private function showPromo(e:TouchEvent):void{
			var touches:Vector.<Touch> = e.getTouches(this,TouchPhase.BEGAN);
			if(touches.length == 0) return;
			
			gameScene.pause(null);
			if(!_promoPanel){
				_promoPanelBG = new Sprite();
				_promoPanelBG.addChild(new Quad(2,2,0xffffff));
				_promoPanelBG.addEventListener(TouchEvent.TOUCH, closePromo);
				_promoPanelBG.alpha = 0.6;
				
				_promoPanel = new PromoPanel(giveReward);
			}
			addChild(_promoPanelBG);
			_promoPanelBG.width = Starling.current.stage.stageWidth;
			_promoPanelBG.height = Starling.current.stage.stageHeight;
			
			addChild(_promoPanel);
			_promoPanel.x = (Starling.current.stage.stageWidth/2) - (_promoPanel.width/2);
			_promoPanel.y = (Starling.current.stage.stageHeight/2) - (_promoPanel.height/2);
			_promoPanel.init();
		}
		private function closePromo(e:TouchEvent = null):void{
			if(e){
				var touches:Vector.<Touch> = e.getTouches(this,TouchPhase.BEGAN);
				if(touches.length == 0) return;
			}
			
			removeChild(_promoPanelBG);
			removeChild(_promoPanel);
			
			gameScene.resume(null);
		}
		private function giveReward():void{
			closePromo();
			
			var nr:int = Math.random()*100;
			
			if(nr < 10){
				giveItem('piggy');
				var message = 'You just earned a piggy!';
				
			}else if(nr < 30){
				giveItem('ingredient');
				var message = 'You just earned a ingredient!';
				
			}else if(nr < 70){
				giveItem('machine');
				var message = 'You just earned a machine!';
				
			}else{
				var options = [200,400,600,800];
				var amount:int = options[Math.floor(Math.random()*options.length)];
				gameContext.cash += amount;
				var message = 'You just earned '+amount+' gold!';
			}
			var alert:Alert = Alert.show(
				message,
				"Congratulations",
				new ListCollection([{label:"Ok"}]), 
				new Image(globalContext.assetManager.getTexture('ralf'))
			);
		}
		
		private function giveItem(pItemCategory:String):void{
			var piggies:Array = [];
			var storeItems:Array = globalContext.assetManager.getObject("store-items").items as Array;
			for each (var item:Object in storeItems){
				switch (item.category){
					case pItemCategory:
						piggies.push({
							text:"",
							name: item.name,
							price:0,
							stats: "",
							isPiggy: true,
							thumbnail:globalContext.assetManager.getTexture(item.thumbnail),
							w:item.w,
							h:item.h,
							texture:item.placeTexture,
							x:item.placeTextureX,
							y:item.placeTextureY,
							recipe:item.recipe,
							placeOnTopOf:item.placeOnTopOf
						});
						break;
				}
			}
			
			var piggy:Object = piggies[Math.floor(Math.random()*piggies.length)];
			
			var e:MarketEvent = new MarketEvent(MarketEvent.CHECKOUT);
			e.purchaseList = [piggy];
			gameScene.onCheckout(e);
		}
		
		
		
		
		
		private function sellerDefined(pEvent:ShopEvent):void{
			pEvent.stopImmediatePropagation();
			
			var exists:Boolean = false;
			for each(var sellerObj:Object in sellers){
				if(sellerObj.piggy.id == pEvent.piggy.id){
					sellerObj.selling = pEvent.selling;
					exists = true;
					break;
				}
			}
			if(!exists) sellers.push({piggy:pEvent.piggy, selling:pEvent.selling});
			
			updateShopList();
		}
		private function sellerStopped(pEvent:ShopEvent):void{
			pEvent.stopImmediatePropagation();
			for(var i:int = 0; i < sellers.length; i++){
				if(sellers[i].piggy.id == pEvent.piggy.id){
					sellers.splice(i, 1);
					i--;
				}
			}
			updateShopList();
		}
		
		private function stuffAdded(pEvent:StorageEvent):void{
			pEvent.stopImmediatePropagation();
			
			for(var i:int = pEvent.ingredients.length; i--;){
				var ingredient:Object = pEvent.ingredients[i];
				for each(var obj:Object in ingredients){
					if(obj.id == ingredient.name) obj.amount += ingredient.amount;
				}
				for each(var obj:Object in shopItems){
					if(obj.id == ingredient.name) obj.amount += ingredient.amount;
				}
			}
			
			updateCounters();
		}
		private function stuffRemoved(pEvent:StorageEvent):void{
			pEvent.stopImmediatePropagation();
			
			for(var i:int = pEvent.ingredients.length; i--;){
				var ingredient:Object = pEvent.ingredients[i];
				for each(var obj:Object in ingredients){
					if(obj.id == ingredient.name) obj.amount -= ingredient.amount;
				}
				for each(var obj:Object in shopItems){
					if(obj.id == ingredient.name) obj.amount -= ingredient.amount;
				}
			}
			
			updateCounters();
		}
		private function updateCounters():void{
			var counter:int = 0;
			for(var i:int = ingredients.length; i--;){
				counter += ingredients[i].amount;
			}
			storageButton.updateCounter(counter);
			
			counter = 0;
			for(i = shopItems.length; i--;){
				counter += shopItems[i].amount;
			}
			shopButton.updateCounter(counter);
			
			updateShopList();
			updateStorageList();
		}
		
		
		
		
		private function piggyAdded(pEvent:PiggiesEvent):void{
			pEvent.stopImmediatePropagation();
			
			var piggyIcon:Texture = globalContext.assetManager.getTexture( pEvent.piggy.getBehaviourByType(PiggyPropertiesBehaviour).icon );
			var item:HeaderPiggyItem = new HeaderPiggyItem(pEvent.piggy, piggyIcon);
			item.y = piggiesListItems.length * item.height;
			piggiesListContainer.addChild(item);
			piggiesListItems.push(item);
			
			sortPiggyList();
			
			piggiesButton.updateCounter(piggiesListItems.length);
			
			item.addEventListener(TouchEvent.TOUCH, onPiggyItemTouch);
		}
		private function piggyRemoved(pEvent:PiggiesEvent):void{
			pEvent.stopImmediatePropagation();
			
			for(var i = piggiesListItems.length; i--;){
				if(piggiesListItems[i].piggy.id == pEvent.piggy.id){
					piggiesListContainer.removeChild(piggiesListItems[i]);
					piggiesListItems[i].removeEventListener(TouchEvent.TOUCH, onPiggyItemTouch);
					piggiesListItems.splice(i, 1);
					break;
				}
			}
			sortPiggyList();
			
			piggiesButton.updateCounter(piggiesListItems.length);
		}
		private function piggyAssignedTask(pEvent:PiggiesEvent):void{
			pEvent.stopImmediatePropagation();
			
			for(var i = piggiesListItems.length; i--;){
				if(piggiesListItems[i].piggy.id == pEvent.piggy.id){
					piggiesListItems[i].updateTask(pEvent.piggyTarget);
					break;
				}
			}
			sortPiggyList();
		}
		private function sortPiggyList():void{
			piggiesListItems.sort(function(item1:HeaderPiggyItem, item2:HeaderPiggyItem):int{
				if(item1.getTask() > item2.getTask()) return 1;
				return -1;
			});
			var currentY:int = 0;
			var prefTask:int = -1;
			for(var i = 0; i < piggiesListItems.length; i++){
				
				if(prefTask != -1 && prefTask != piggiesListItems[i].getGroup()) currentY += 1;
				prefTask = piggiesListItems[i].getGroup();
				
				piggiesListItems[i].y = currentY;
				currentY += piggiesListItems[i].height;
			}
		}
		private function onPiggyItemTouch(e:TouchEvent):void{
			var item:HeaderPiggyItem = e.currentTarget as HeaderPiggyItem;
			
			if(e.getTouch(item, TouchPhase.HOVER)){
				item.onMouseOver();
			}else{
				item.onMouseOut();
			}
			if(e.getTouch(item, TouchPhase.ENDED)) {
				TappableBehaviour(item.piggy.getBehaviourByType(TappableBehaviour)).onTapped();
				gameScene.moveCameraTo(item.piggy, 0.5);
			}
		}
		
		
		
		private function toggleStorage(e:TouchEvent):void{
			var touches:Vector.<Touch> = e.getTouches(this,TouchPhase.ENDED);
			if(touches.length == 0) return;
			
			showTab(2);
			if(storageListContainer.visible) {
				
				updateListView(storageListItems, storageListContainer, ingredients, false);
				tweenListIn(storageListContainer);
				
			}else{
				
				SoundManager.playSound({id:'SO_Close_popup'});
			}
		}
		private function toggleShop(e:TouchEvent):void{
			var touches:Vector.<Touch> = e.getTouches(this,TouchPhase.ENDED);
			if(touches.length == 0) return;
			
			showTab(1);
			if(shopListContainer.visible) {
				
				updateListView(shopListItems, shopListContainer, shopItems, true);
				tweenListIn(shopListContainer);
				
			}else{
				SoundManager.playSound({id:'SO_Close_popup'});
			}
		}
		private function togglePiggies(e:TouchEvent):void{
			var touches:Vector.<Touch> = e.getTouches(this,TouchPhase.ENDED);
			if(touches.length == 0) return;
			
			showTab(0);
			if(piggiesListContainer.visible) {
				
				tweenListIn(piggiesListContainer);
				
			}else{
				SoundManager.playSound({id:'SO_Close_popup'});
			}
		}
		private function showTab(pNr:int):void{
			var items = [piggiesListContainer, shopListContainer, storageListContainer];
			for(var i = items.length; i--;){
				if(i == pNr){
					items[i].visible = !items[i].visible;
				}else{
					items[i].visible = false;
				}
			}
		}
		
		
		private function tweenListIn(pTarget:Sprite):void{
			pTarget.y = storageButton.height-3.3;
			pTarget.alpha = 0;
			var tween:Tween = new Tween(pTarget, .3);
			tween.animate('y', storageButton.height-1.3);
			tween.animate('alpha', 1);
			Starling.juggler.add(tween);
			
			SoundManager.playSound({id:'SO_Open_popup'});
		}
		
		
		
		
		
		
		private function updateShopList():void{
			if(shopListContainer.visible) updateListView(shopListItems, shopListContainer, shopItems, true);
		}
		private function updateStorageList():void{
			if(storageListContainer.visible) updateListView(storageListItems, storageListContainer, ingredients, false);
		}
		private function updateListView(items:Vector.<HeaderListItem>, container:Sprite, pIngredients:Object, pIsShop:Boolean):void{
			var haveIngredients:Boolean = false;
			
			if(pIsShop){
				for(var i:int = 0; i < sellers.length; i++){
					var sellerItem:HeaderSellerItem = null;
					var sellingIcon:Texture = globalContext.assetManager.getTexture( sellers[i].selling );
					
					for(var j:int = sellersItems.length; j--;){
						if(sellersItems[j].piggy.id == sellers[i].piggy.id) sellerItem = sellersItems[j];
					}
					if(!sellerItem){
						var piggyIcon:Texture = globalContext.assetManager.getTexture( sellers[i].piggy.getBehaviourByType(PiggyPropertiesBehaviour).icon );
						sellerItem = new HeaderSellerItem(sellers[i].piggy, piggyIcon, sellingIcon);
						container.addChild(sellerItem);
						sellersItems.push(sellerItem);
					}else{
						sellerItem.updateSelling(sellingIcon);
					}
					sellerItem.visible = true;
					sellerItem.y = i * sellerItem.height;
				}
				for(var i:int = sellersItems.length; i--;){
					var stillSelling:Boolean = false;
					for(var j:int = sellers.length; j--;){
						if(sellers[j].piggy.id == sellersItems[i].piggy.id) stillSelling = true;
					}
					if(!stillSelling) {
						sellersItems[i].visible = false;
					}
				}
			}
			
			for(var j:int = 0; j < pIngredients.length; j++){
				var key = pIngredients[j].id;
				var item:HeaderListItem = null;
				
				var exists:Boolean = false;
				for(var i:int = items.length; i--;){
					if(items[i].name == key){
						item = items[i];
						item.updateAmount(pIngredients[j].amount);
						exists = true;
						break;
					}
				}
				if(!exists && pIngredients[j].amount > 0){
					item = new HeaderListItem(
						pIngredients[j].label, 
						pIngredients[j].amount,
						pIngredients[j].profit,
						items.length % 2 != 0, 
						globalContext.assetManager.getTexture(pIngredients[j].id),
						pIsShop
					);
					item.name = key;
					items.push(item);
					container.addChild(item);
				}
				if(item){
					item.visible = pIngredients[j].amount != 0;
					if(item.visible) haveIngredients = true;
				}
			}
			
			//reposition
			var counter:int = 0;
			for(var i:int = 0; i < items.length; i++){
				if(items[i].name == 'empty' && haveIngredients) items[i].visible = false;
				
				if(items[i].visible){
					items[i].y = counter * items[i].height;
					if(pIsShop && sellers.length > 0) items[i].y += sellers.length * sellersItems[0].height;
					items[i].reColor(counter % 2 != 0);
					counter ++;
				}
			}
		}
		
		
		
		public function incommingEvilGuy(pTrue:Boolean):void{
			redGlow.visible = pTrue;
		}
		public function evilGuyIsCommingin():Boolean{
			return redGlow.visible;
		}
		
		
		public function cleanup():void{
			
		}
		
		public function draw():void
		{
			var width:int = Starling.current.stage.stageWidth;
			var height:int = Starling.current.stage.stageHeight;
			
			bg.width = width;
			
			moneyLabel.text = "Gold: " + gameContext.cash;
			
			storageButton.x = width - storageButton.width;
			shopButton.x = width - storageButton.width - shopButton.width;
			piggiesButton.x = width - storageButton.width - shopButton.width - piggiesButton.width;
			
			storageListContainer.x = width - 60;
			shopListContainer.x = width - 60 - storageButton.width;
			piggiesListContainer.x = piggiesButton.x;
			
			redGlow.width = width;
			redGlow.height = height;
		}
		
	}

}