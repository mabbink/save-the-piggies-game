package savethepiggies.game
{
	import com.greensock.TweenNano;
	
	import flash.events.Event;
	import flash.net.URLLoader;
	import flash.net.URLLoaderDataFormat;
	import flash.net.URLRequest;
	import flash.net.URLRequestMethod;
	import flash.net.URLVariables;
	
	import feathers.controls.Button;
	import feathers.controls.Label;
	import feathers.controls.TextInput;
	import feathers.events.FeathersEventType;
	
	import starling.display.Quad;
	import starling.display.Sprite;
	import starling.events.Event;

	public final class PromoPanel extends Sprite {
		
		private var _doneCallback:Function;
		private var _title:Label;
		private var _input:TextInput;
		private var _enterButton:Button;
		
		private var _loader:URLLoader;
		
		public function PromoPanel(doneCallback){
			_doneCallback = doneCallback;
			
			addChild(new Quad(100, 80, 0xffffff));
			
			_title = createText('Enter your promo code', 'settingsTitle', 0, 10, 100);
			this.addChild(_title);
			
			var bgQuad:Quad = new Quad(60, 16, 0xaaaaaa);
			bgQuad.x = 20;
			bgQuad.y = 30;
			addChild(bgQuad);
			
			_input = new TextInput();
			_input.prompt = "xxxxxxxx";
			_input.maxChars = 16;
			_input.width = 52;
			_input.height = 12;
			_input.x = 24;
			_input.y = 34;
			_input.textEditorProperties.textAlign = 'center';
			_input.textEditorProperties.fontSize = 8;
			_input.textEditorProperties.fontFamily = "Helvetica";
			_input.textEditorProperties.color = 0xffffff;
			_input.addEventListener( starling.events.Event.CHANGE, input_changeHandler );
			_input.addEventListener( FeathersEventType.ENTER, enterCode );
			this.addChild(_input);
			
			_enterButton = createButton('Enter', 25, 60, 50, 10, true, enterCode);
			this.addChild(_enterButton);
			
			
			_loader = new URLLoader();
			_loader.addEventListener(flash.events.Event.COMPLETE, codeValidated);
			_loader.dataFormat = URLLoaderDataFormat.TEXT;
		}
		public function init():void{
			_title.text = 'Enter your promo code';
			_input.text = '';
			_input.setFocus();
			_enterButton.alpha = .5;
		}
		
		
		private function input_changeHandler(e:starling.events.Event):void{
			if(_input.text.length > 6){
				_enterButton.alpha = 1;
			}else{
				_enterButton.alpha = .5;
			}
		}
		private function enterCode(e:Object = null):void{
			if(_enterButton.alpha == 1){
				_title.text = 'Validating...';
				
				var req:URLRequest = new URLRequest('validatePromoCode/');
				req.method = URLRequestMethod.POST;
				
				var requestVars:URLVariables = new URLVariables();
				requestVars.code = _input.text;
				req.data = requestVars;
				
				try{
					_loader.load(req);
				}
				catch (e:Error)
				{
					_title.text = 'Oops.. something went wrong..';
				}
			}
		}
		private function codeValidated(pEvent:flash.events.Event):void{
			switch(int(_loader.data)){
				case 0: 
				case 500:
					_title.text = 'Something went wrong..'; break;
				case 501:
				case 503:
					_title.text = 'Invalid code...'; break;
				case 502:
					_title.text = 'This code has been used before'; break;
				case 200:
					_title.text = 'Congratulations!';
					TweenNano.delayedCall(2, _doneCallback);
					break;
			}
		}
		
		
		
		
		private function createText(pValue:String, pType:String, pX:int, pY:int, pWidth:int):Label{
			var title:Label = new Label();
			title.touchable = false;
			title.text = pValue;
			title.x = pX;
			title.y = pY;
			title.width = pWidth;
			title.nameList.add(pType);
			return title;
		}
		private function createButton(pValue:String, pX:int, pY:int, pWidth:int, pHeight:int, pIsMain:Boolean = true, pCallback:Function = null):Button{
			var button:Button = new Button();
			button.alpha = pIsMain ? 1 : 0.5;
			button.label = pValue;
			button.x = pX;
			button.y = pY;
			button.setSize(pWidth, pHeight);
			button.addEventListener(starling.events.Event.TRIGGERED, pCallback);
			return button
		}
	}
}