package savethepiggies.game 
{
	import flash.geom.Rectangle;
	import flash.utils.getDefinitionByName;
	
	import savethepiggies.GlobalContext;
	import savethepiggies.game.behaviours.AnimationSwitcherBehaviour;
	import savethepiggies.game.behaviours.BuyableBehavior;
	import savethepiggies.game.behaviours.CookerProducerBehaviour;
	import savethepiggies.game.behaviours.DrawableBehaviour;
	import savethepiggies.game.behaviours.EvilGuyTasksBehaviour;
	import savethepiggies.game.behaviours.FieldBehaviour;
	import savethepiggies.game.behaviours.FieldPersistBehaviour;
	import savethepiggies.game.behaviours.GuardBehaviour;
	import savethepiggies.game.behaviours.HouseHoverHideBehaviour;
	import savethepiggies.game.behaviours.HoverHideBehaviour;
	import savethepiggies.game.behaviours.MovableBehaviour;
	import savethepiggies.game.behaviours.PiggyCounterBehaviour;
	import savethepiggies.game.behaviours.PiggyInventoryBehaviour;
	import savethepiggies.game.behaviours.PiggyPersistBehaviour;
	import savethepiggies.game.behaviours.PiggyPropertiesBehaviour;
	import savethepiggies.game.behaviours.PiggyTasksBehaviour;
	import savethepiggies.game.behaviours.PiggyUiBehaviour;
	import savethepiggies.game.behaviours.PrepperProducerBehaviour;
	import savethepiggies.game.behaviours.ProducerPersistBehaviour;
	import savethepiggies.game.behaviours.ProgressBehaviour;
	import savethepiggies.game.behaviours.RemovableBehaviour;
	import savethepiggies.game.behaviours.SelectionRingBehaviour;
	import savethepiggies.game.behaviours.ShopFillingBehaviour;
	import savethepiggies.game.behaviours.StorageBehaviour;
	import savethepiggies.game.behaviours.StoragePersistBehaviour;
	import savethepiggies.game.behaviours.StorageUiBehaviour;
	import savethepiggies.game.behaviours.TappableBehaviour;
	import savethepiggies.game.behaviours.TutorialBehaviour;
	import savethepiggies.game.behaviours.UpdatableBehaviour;
	import savethepiggies.game.behaviours.UpgradableBehaviour;
	import savethepiggies.game.events.PiggiesEvent;
	
	import starling.core.Starling;
	import starling.display.Image;
	import starling.display.MovieClip;
	
	public final class Cooker 
	{
		SelectionRingBehaviour;
		PiggyInventoryBehaviour;
		AnimationSwitcherBehaviour;
		PiggyCounterBehaviour;
		PiggyUiBehaviour;
		PrepperProducerBehaviour;
		StorageBehaviour;
		StorageUiBehaviour;
		PiggyPersistBehaviour;
		FieldPersistBehaviour;
		StoragePersistBehaviour;
		ProducerPersistBehaviour;
		HouseHoverHideBehaviour;
		ShopFillingBehaviour;
		
		private var globalContext:GlobalContext;
		private var gameContext:GameContext;
		
		public function Cooker(
			globalContext:GlobalContext,
			gameContext:GameContext
		) 
		{
			this.globalContext = globalContext;
			this.gameContext = gameContext;
		}
		
		public function cook(recipeName:String, offsetI:Number, offsetJ:Number, forceGameObjectId:uint = 0):*
		{
			var recipe:Object = globalContext.assetManager.getObject(recipeName);
			
			var res:GameObject;
			
			for (var i:int = 0; recipe.parts && i < recipe.parts.length; ++i)
			{
				var id:uint = gameContext.idGenerator.generate();
				
				var part:Object = recipe.parts[i];
				switch (part.type)
				{
					case "isoSprite":
						cookIsoSprite(id, part, offsetI, offsetJ);
						break;
						
					case "specialNode":
						cookSpecialNode(id, part, offsetI, offsetJ);
						break;
						
					case "specialNodeBlock":
						cookBlock(part.w, part.h, function(ioff:int, joff:int):void {
							cookSpecialNode(id, part, offsetI, offsetJ, ioff, joff);
						});
						break;
						
					case "gameObject":
						if (0 != forceGameObjectId)	// Force the ID of the first gameobject
						{	
							id = forceGameObjectId;
							forceGameObjectId = 0;
						}
						res = cookGameObject(recipeName, id, part, offsetI, offsetJ);
						res.isPart = i > 0;
						
						break;
					
					default:
						throw new Error("Don't know how to build part " + part.type);
						break;
				}
			}
			
			return res;
		}
		
		private function cookGameObject(recipeName:String, id:uint, recipe:Object, offsetI:Number, offsetJ:Number):GameObject
		{
			var g:GameObject = new GameObject(id, recipeName, offsetI, offsetJ);
			if (recipe.tags){
				for each (var tag:String in recipe.tags)
					g.addTag(tag);
			}
			gameContext.gameObjects.register(g);
			
			
			
			for (var i:int = 0; recipe.components && i < recipe.components.length; ++i)
			{
				var componentRecipe:Object = recipe.components[i];
				
				switch (componentRecipe.type)
				{
					case "isoSprite":
						g.addIsoSprite(
							cookIsoSprite(gameContext.idGenerator.generate(), componentRecipe, offsetI, offsetJ)
						);
						break;
						
					case "behaviour":
						g.addBehaviour(
							cookBehaviour(gameContext.idGenerator.generate(), componentRecipe, offsetI, offsetJ)
						);
						break;
						
					case "taskTarget":
						g.addTaskTarget(
							cookTaskTarget(gameContext.idGenerator.generate(), g.id, componentRecipe, offsetI, offsetJ)
						);
						break;
						
					case "specialNode":
						g.addSpecialNode(
							cookSpecialNode(gameContext.idGenerator.generate(), componentRecipe, offsetI, offsetJ)
						);
						break;	
						
					case "specialNodeBlock":
						cookBlock(componentRecipe.w, componentRecipe.h, function(ioff:int, joff:int):void {
							g.addSpecialNode(
								cookSpecialNode(gameContext.idGenerator.generate(), componentRecipe, offsetI, offsetJ, ioff, joff)
							);
						});
						break;
					
					default:
						throw new Error("Unknown type " + componentRecipe.type);
						break;
				}
			}
			
			if (g.getBehaviourByType(UpdatableBehaviour))
				gameContext.updatableGameObjects.register(g);
			
			if (g.getBehaviourByType(DrawableBehaviour))
				gameContext.drawableGameObjects.register(g);
				
			if (g.hasTag("piggy")){
				gameContext.piggies.register(g);
				Starling.current.stage.dispatchEvent(new PiggiesEvent(PiggiesEvent.PIGGY_ADDED, g));
			}
			g.activate();
			
			return g;
		}
		
		private function cookBlock(w:int, h:int, f:Function):void
		{
			for (var j:int = 0; j < h; ++j)
			{
				for (var i:int = 0; i < w; ++i)
				{
					f(i, j);
				}
			}
		}
		
		private function cookTaskTarget(
			id:uint, targetId:uint, recipe:Object, offsetI:Number, offsetJ:Number
		):TaskTarget
		{
			var bounds:Rectangle = cookRect(recipe.modelSpaceBounds);
			bounds.offset(offsetI, offsetJ);
			var taskTarget:TaskTarget = new TaskTarget(id, targetId, bounds, recipe.taskType);
			
			var maxUsers:int = 1;
			if (recipe.hasOwnProperty("maxUsers"))
				maxUsers = recipe.maxUsers;
				
			if ( -1 == maxUsers) maxUsers = int.MAX_VALUE;
			
			if (recipe.slots)
			{
				for (var i:int = 0; i < recipe.slots.length; ++i)
				{
					var semaphoreName:String = null;
					if (recipe.slots[i].hasOwnProperty("semaphoreName"))
						semaphoreName = recipe.slots[i].semaphoreName;
					
					var slot:TaskTargetSlot = taskTarget.addSlot(
						recipe.slots[i].i + 2 * offsetI,
						recipe.slots[i].j + 2 * offsetJ,
						maxUsers,
						semaphoreName
					);
					if (recipe.slots[i].properties)
					{
						for (var s:String in recipe.slots[i].properties)
							slot.properties[s] = recipe.slots[i].properties[s];
					}
				}
			}
			
			gameContext.taskTargetManager.addTarget(taskTarget);
			return taskTarget;
		}
		
		private function cookBehaviour(id:uint, recipe:Object, offsetI:Number, offsetJ:Number):Behaviour
		{
			var behaviour:Behaviour = null;
			
			var className:String = recipe["class"];
			switch (className)
			{
				case "TappableBehaviour":
					behaviour = cookTappableBehaviour(id, recipe);
					break;
					
				case "HoverHideBehaviour":
					behaviour = cookHoverHideBehaviour(id, recipe, offsetI, offsetJ);
					break;	
				
				case "HouseHoverHideBehaviour":
					behaviour = cookHouseHoverHideBehaviour(id, recipe, offsetI, offsetJ);
					break;
				
				case "FieldBehaviour":
					behaviour = cookFieldBehaviour(id, recipe);
					break;	
				
				case "PiggyTasksBehaviour":
					behaviour = cookPiggyTasksBehaviour(id, recipe);
					break;
				
				case "UpgradableBehaviour":
					behaviour = cookUpgradableBehaviour(id, recipe, offsetI, offsetJ);
					break;
				
				case "RemovableBehaviour":
					behaviour = cookRemovableBehaviour(id, recipe, offsetI, offsetJ);
					break;
				
				case "StorageBehaviour":
					behaviour = cookStorageBehaviour(id, recipe, offsetI, offsetJ);
					break;
					
				case "StorageUiBehaviour":
					behaviour = cookStorageUiBehaviour(id, recipe, offsetI, offsetJ);
					break;
				
				case "ShopFillingBehaviour":
					behaviour = cookShopFillingBehaviour(id);
					break;
					
				case "PrepperProducerBehaviour":
					behaviour = cookPrepperProducerBehaviour(id, recipe);
					break;
					
				case "CookerProducerBehaviour":
					behaviour = cookCookerProducerBehaviour(id, recipe);
					break;
				
				case "MovableBehaviour":
					behaviour = cookMovableBehaviour(id, recipe, offsetI, offsetJ);
					break;
				
				case "PiggyPropertiesBehaviour":
					behaviour = cookPiggyPropertiesBehaviour(id, recipe);
					break;
				
				case "BuyableBehavior":
					behaviour = cookBuyableBehavior(id, recipe, offsetI, offsetJ);
					break;
				
				case "TutorialBehaviour":
					behaviour = cookTutorialBehaviour(id, recipe);
					break;
				
				case "EvilGuyTasksBehaviour":
					behaviour = cookEvilGuyTasksBehaviour(id, recipe);
					break;
				
				case "GuardBehaviour":
					behaviour = cookGuardBehaviour(id, recipe);
					break;
					
				case "ProgressBehaviour":
					behaviour = cookProgressBehaviour(id, recipe);
					break;
				
				default:
					behaviour = cookStandardBehaviour(id, className);
					break;
			}
			
			return behaviour;
		}
		
		private function cookStandardBehaviour(id:uint, className:String):Behaviour
		{
			var c:Class = Class(getDefinitionByName("savethepiggies.game.behaviours::" + className));
			return new c(id);
		}
		private function cookShopFillingBehaviour(id:uint):Behaviour{
			var b:ShopFillingBehaviour = new ShopFillingBehaviour(id);
			b.storage = gameContext.storage;
			return b;
		}
		private function cookPiggyPropertiesBehaviour(id:uint, recipe:Object):Behaviour{
			var b:PiggyPropertiesBehaviour = new PiggyPropertiesBehaviour(id);
			b.load(recipe);
			return b;
		}
		private function cookProgressBehaviour(id:uint, recipe:Object):Behaviour{
			var b:ProgressBehaviour = new ProgressBehaviour(id);
			trace('Creating');
			b.assetManager = globalContext.assetManager;
			b.useColors = recipe.useColors;
			return b;
		}
		
		private function cookBuyableBehavior(id:uint, recipe:Object, offsetI:Number, offsetJ:Number):Behaviour
		{
			var b:BuyableBehavior = new BuyableBehavior(id);
			b.recipe = recipe;
			b.offsetI = offsetI;
			b.offsetJ = offsetJ;
			b.buyCallback = gameContext.buyCallback;
			b.selectionGroup = gameContext.selectionGroupTaskTargets
			return b;
		}
		
		private function cookTappableBehaviour(id:uint, recipe:Object):Behaviour
		{
			var b:TappableBehaviour = new TappableBehaviour(id, recipe.sound);
			b.selectionGroup = gameContext.selectionGroupPiggies;
			return b;
		}
		
		private function cookFieldBehaviour(id:uint, recipe:Object):Behaviour
		{
			var b:FieldBehaviour = new FieldBehaviour(id, recipe.produce, recipe.harvestTime);
			b.levels = recipe.levels;
			b.mustPlant = recipe.mustPlant;
			b.produceCount = recipe.produceCount;
			b.growInterval = 2;
			b.givesEnergy = recipe.givesEnergy;
			b.assetManager = globalContext.assetManager;
			return b;
		}
		
		private function cookGuardBehaviour(id:uint, recipe:Object):Behaviour
		{
			var b:GuardBehaviour = new GuardBehaviour(id, recipe.range, gameContext.selectionGroupTaskTargets);
			return b;
		}
		
		private function cookPiggyTasksBehaviour(id:uint, recipe:Object):Behaviour
		{
			var b:PiggyTasksBehaviour = new PiggyTasksBehaviour(id);
			b.gameContext = gameContext;
			b.globalContext = globalContext;
			return b;
		}
		
		private function cookEvilGuyTasksBehaviour(id:uint, recipe:Object):Behaviour
		{
			var b:EvilGuyTasksBehaviour = new EvilGuyTasksBehaviour(id);
			b.minTimeToShow = recipe.minTimeToShow;
			b.maxTimeToShow = recipe.maxTimeToShow;
			b.speedRun = recipe.speedRun;
			b.speedWalk = recipe.speedWalk;
			b.minPiggiesToShowUp = recipe.minPiggiesToShowUp;
			b.gameContext = gameContext;
			b.globalContext = globalContext;
			return b;
		}
		
		private function cookHoverHideBehaviour(id:uint, recipe:Object, offsetI:Number, offsetJ:Number):Behaviour
		{
			var b:HoverHideBehaviour = new HoverHideBehaviour(id);
			b.mapLayer = gameContext.mapLayer;
			b.map = gameContext.map;
			var bounds:Rectangle = cookRect(recipe.modelSpaceBounds);
			bounds.x += offsetI;
			bounds.y += offsetJ;
			b.bounds = bounds;
			return b;
		}
		private function cookHouseHoverHideBehaviour(id:uint, recipe:Object, offsetI:Number, offsetJ:Number):Behaviour
		{
			var b:HouseHoverHideBehaviour = new HouseHoverHideBehaviour(id);
			b.mapLayer = gameContext.mapLayer;
			b.map = gameContext.map;
			var bounds:Rectangle = cookRect(recipe.modelSpaceBounds);
			bounds.x += offsetI;
			bounds.y += offsetJ;
			b.bounds = bounds;
			return b;
		}
		
		private function cookStorageBehaviour(id:uint, recipe:Object, offsetI:int, offsetJ:int):Behaviour
		{
			var b:StorageBehaviour = new StorageBehaviour(id, recipe.capacity);
			b.assetManager = globalContext.assetManager;
			return b;
		}
		
		private function cookStorageUiBehaviour(id:uint, recipe:Object, offsetI:int, offsetJ:int):Behaviour
		{
			var b:StorageUiBehaviour = new StorageUiBehaviour(id);
			b.selectionGroup = gameContext.selectionGroupTaskTargets;
			return b;
		}
		
		private function cookUpgradableBehaviour(id:uint, recipe:Object, offsetI:int, offsetJ:int):Behaviour
		{
			var b:UpgradableBehaviour = new UpgradableBehaviour(id);
			b.selectionGroup = gameContext.selectionGroupTaskTargets;
			b.upgradeCallback = gameContext.upgradeCallback;
			b.offsetI = offsetI;
			b.offsetJ = offsetJ;
			b.recipeName = recipe.recipe;
			b.cost = uint(recipe.cost);
			b.description = String(recipe.description);
			return b;
		}
		private function cookRemovableBehaviour(id:uint, recipe:Object, offsetI:int, offsetJ:int):Behaviour
		{
			var b:RemovableBehaviour = new RemovableBehaviour(id);
			b.selectionGroup = gameContext.selectionGroupTaskTargets;
			b.removeCallback = gameContext.removeCallback;
			b.offsetI = offsetI;
			b.offsetJ = offsetJ;
			b.cost = uint(recipe.cost);
			b.description = String(recipe.description);
			return b;
		}
		private function cookTutorialBehaviour(id:uint, recipe:Object):Behaviour{
			var b:TutorialBehaviour = new TutorialBehaviour(id);
			return b;
		}
		
		private function cookMovableBehaviour(id:uint, recipe:Object, offsetI:int, offsetJ:int):Behaviour
		{
			var b:MovableBehaviour = new MovableBehaviour(id);
			b.selectionGroup = gameContext.selectionGroupTaskTargets;
			b.moveCallback = gameContext.moveCallback;
			b.offsetI = offsetI;
			b.offsetJ = offsetJ;
			b.placeOnTopOf = recipe.placeOnTopOf;
			b.w = recipe.w;
			b.h = recipe.h;
			return b;
		}
		
		private function cookCookerProducerBehaviour(id:uint, recipe:Object):Behaviour
		{
			var b:CookerProducerBehaviour= new CookerProducerBehaviour(id, recipe.space, recipe.produceTime);
			b.storage = gameContext.storage;
			return b;
		}
		
		private function cookPrepperProducerBehaviour(id:uint, recipe:Object):Behaviour
		{
			var b:PrepperProducerBehaviour = new PrepperProducerBehaviour(id, recipe.space, recipe.produceTime);
			b.storage = gameContext.storage;
			return b;
		}
		
		private function cookSpecialNode(id:uint, recipe:Object, offsetI:int, offsetJ:int, halfOffsetI:int = 0, halfOffsetJ:int = 0):SpecialNode
		{
			var occupiedType:String = null;
			if (recipe.hasOwnProperty("occupiedType"))
				occupiedType = recipe.occupiedType;
			
			return gameContext.grid.addSpecialNode(
				id, 
				recipe.i + 2 * offsetI + halfOffsetI, 
				recipe.j + 2 * offsetJ + halfOffsetJ, 
				recipe.blockLeft, 
				recipe.blockUp, 
				recipe.blockRight, 
				recipe.blockDown, 
				occupiedType
			);
		}
		
		private function cookIsoSprite(id:uint, recipe:Object, offsetI:Number, offsetJ:Number):IsoSprite
		{
			var posI:Number = recipe.i + offsetI;
			var posJ:Number = recipe.j + offsetJ;
			
			var layerName:String = recipe.layer ? recipe.layer : "default";
			
			var isoSprite:IsoSprite = new IsoSprite(id, gameContext.map, posI, posJ, cookRect(recipe.modelSpaceBounds), layerName);
			gameContext.isoSpriteDisplay.addIsoSprite(isoSprite);
			
			if (recipe.hasOwnProperty("properties"))
				for (var n:String in recipe.properties)
					isoSprite[n] = recipe.properties[n];
			
			for (var i:int = 0; i < recipe.children.length; ++i)
			{
				var childRecipe:Object = recipe.children[i];
				switch (childRecipe.type)
				{
					case "image":
						isoSprite.addChild(cookImage(childRecipe));
						break;
						
					case "movieClip":
						isoSprite.addChild(cookMovieClip(childRecipe));
						break;
					
					default:
						throw new Error("Unknown type " + childRecipe.type);
						break;
				}
			}
			
			return isoSprite;
		}
		
		private function cookImage(recipe:Object):Image
		{
			var image:Image = new Image(globalContext.assetManager.getTexture(recipe.texture));
			image.x = recipe.x;
			image.y = recipe.y;
			
			if (recipe.hasOwnProperty("properties"))
				for (var n:String in recipe.properties)
					image[n] = recipe.properties[n];
					
			return image;
		}
		
		private function cookMovieClip(recipe:Object):MovieClip
		{
			var fps:Number = recipe.hasOwnProperty("fps") ? recipe.fps : 12;
			var clip:MovieClip = new MovieClip(globalContext.assetManager.getTextures(recipe.texturePrefix), fps);
			clip.x = recipe.x;
			clip.y = recipe.y;
			
			if (recipe.hasOwnProperty("properties"))
				for (var n:String in recipe.properties)
					clip[n] = recipe.properties[n];
			
			return clip;
		}
		
		private function cookRect(recipe:Object):Rectangle
		{
			return new Rectangle(recipe.i, recipe.j, recipe.w, recipe.h);
		}
	}

}