package savethepiggies.game 
{
	import flash.geom.Point;
	import flash.geom.Rectangle;
	import savethepiggies.game.Component;
	import savethepiggies.map.Map;
	import starling.display.Sprite;
	
	/**
	 * A sprite appearing on an isometric map.
	 */
	public final class IsoSprite extends Sprite implements Component
	{	
		public var isoDepth:int;
		public var isoBehind:Array;
		public var isoVisitedFlag:Boolean;
		
		private var map:Map;
		
		private var _id:uint;
		private var _offsetI:Number;
		private var _offsetJ:Number;
		private var modelSpaceBounds:Rectangle;
		private var _layerName:String;
		
		public function IsoSprite(
			id:uint,
			map:Map,
			offsetI:Number,
			offsetJ:Number,
			modelSpaceBounds:Rectangle,
			layerName:String
		) 
		{
			_id = id;
			_layerName = layerName;
			
			this._offsetI = offsetI;
			this._offsetJ = offsetJ;
			this.map = map;
			this.modelSpaceBounds = modelSpaceBounds.clone();
			
			if (modelSpaceBounds.width <= 0 || modelSpaceBounds.height <= 0)
				throw new Error("Width or height cannot be negative or 0");
			
			moveTo(offsetI, offsetJ);
		}
		
		private var movePoint:Point = new Point;
		public function moveTo(offsetI:Number, offsetJ:Number):void
		{
			this._offsetI = offsetI;
			this._offsetJ = offsetJ;
			
			map.isometricToCartesian(offsetI, offsetJ, movePoint);
			this.x = movePoint.x;
			this.y = movePoint.y;
		}
		
		public function get id():uint { return _id; }
		public function get layerName():String { return _layerName; }
		public function get offsetI():Number { return _offsetI; }
		public function get offsetJ():Number { return _offsetJ; }
		public function get minI():Number { return modelSpaceBounds.left + _offsetI; }
		public function get maxI():Number { return modelSpaceBounds.right + _offsetI; }
		public function get minJ():Number { return modelSpaceBounds.top + _offsetJ; }
		public function get maxJ():Number { return modelSpaceBounds.bottom + _offsetJ; }
	}
}