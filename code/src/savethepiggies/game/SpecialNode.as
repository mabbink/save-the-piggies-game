package savethepiggies.game 
{
	public final class SpecialNode implements Component
	{
		private var _id:uint;
		public var blocked:Vector.<Boolean>;
		public var occupiedType:String;
		public var i:int, j:int;
		
		public function SpecialNode(id:uint, blocked0:Boolean, blocked1:Boolean, blocked2:Boolean, blocked3:Boolean, occupiedType:String = null)
		{
			_id = id;
			blocked = new <Boolean>[blocked0, blocked1, blocked2, blocked3];
			this.occupiedType = occupiedType;
		}
		
		public function get id():uint { return _id; }
	}

}