package savethepiggies.game 
{
	import savethepiggies.game.behaviours.StorageBehaviour;
	
	public final class Storage 
	{		
		private var gameContext:GameContext;
		
		public function Storage(gameContext:GameContext) 
		{
			this.gameContext = gameContext;
		}
		
		
		public function giveStartingIngredients():void{
			var storages:Array = gameContext.gameObjects.getByTag('storage');
			for each(var g:GameObject in storages){
				
				var behaviour:StorageBehaviour = g.getBehaviourByType(StorageBehaviour);
				if(CONFIG::instantFillStorage){
					behaviour.storeItem([
						/*
						{name:ApplicationVars.LUPIN, 			amount:100},
						{name:ApplicationVars.COOKED_LUPIN, 	amount:100},
						{name:ApplicationVars.PREPPED_LUPIN, 	amount:100},
						{name:ApplicationVars.APPLE, 			amount:100},
						{name:ApplicationVars.BANANA, 			amount:100},
						{name:ApplicationVars.CUCUMBER, 		amount:100},
						{name:ApplicationVars.FLOWER, 			amount:100},
						{name:ApplicationVars.GRAPE,	 		amount:100},
						{name:ApplicationVars.PEPPER, 			amount:100},
						{name:ApplicationVars.TOMATO, 			amount:100},
						*/
						{name:ApplicationVars.BURRITO,			amount: 10},
						{name:ApplicationVars.FRUIT_SHASHLIK,	amount: 10},
						{name:ApplicationVars.PICMAC,			amount: 10},
						{name:ApplicationVars.PIZZA,			amount: 10},
						{name:ApplicationVars.SHASHLIK,			amount: 5},
						{name:ApplicationVars.SMOOTHY,			amount: 5}
					], false);
				}else{
					behaviour.storeItem([
						{name:ApplicationVars.COOKED_LUPIN, 	amount:1}
					], false);
				}
			}
		}
		
		public function removeAll(pIngredients:Boolean = true, pProducts:Boolean = true):void{
			var storages:Array = gameContext.gameObjects.getByTag('storage');
			for each(var g:GameObject in storages){
				StorageBehaviour(g.getBehaviourByType(StorageBehaviour)).getAllItems(true, pIngredients, pProducts);
			}
		}
		
		public function store(
			type:String,
			nearI:Number,
			nearJ:Number
		):Boolean
		{
			var candidates:Array = [];
			var storages:Array = gameContext.gameObjects.getByTag('storage');
			
			for each(var g:GameObject in storages){
				if (g.getBehaviourByType(StorageBehaviour).hasSpace)
					candidates.push(g);	
			};
			
			candidates.sort(function(a:GameObject, b:GameObject):int {
				var spriteA:IsoSprite = a.getIsoSprite();
				var spriteB:IsoSprite = b.getIsoSprite();
				var diA:Number = spriteA.offsetI - nearI;
				var diB:Number = spriteB.offsetI - nearI;
				var djA:Number = spriteA.offsetJ - nearJ;
				var djB:Number = spriteB.offsetJ - nearJ;
				var dstASqr:Number = diA * diA + djA * djA;
				var dstBSqr:Number = diB * diB + djB * djB;
				if (dstASqr == dstBSqr) return 0;
				if (dstASqr > dstBSqr) return 1;
				return -1;
			});
			
			if (candidates.length == 0) return false;
			
			var storage:StorageBehaviour = candidates[0].getBehaviourByType(StorageBehaviour);
			return storage.storeItem([{name:type, amount:1}]);
		}
		
		
		public function getSellableOptions():Array{
			var items:Array = [];
			var storages:Array = gameContext.gameObjects.getByTag('storage');
			for each(var g:GameObject in storages){
				var b:StorageBehaviour = g.getBehaviourByType(StorageBehaviour);
				items = items.concat(b.getState());
			};
			
			var sellables:Array = ApplicationVars.SELLABLES.concat();
			for(var i:int = 0; i < sellables.length; i++){
				sellables[i] = {name:sellables[i], amount:0};
			}
			
			for(i = 0; i < sellables.length; i++){
				for each(var item in items){
					if(item == sellables[i].name) sellables[i].amount ++;
				}
				if(sellables[i].amount == 0){
					sellables.splice(i, 1);
					i--;
				}
			}
			return sellables;
		}
	}

}