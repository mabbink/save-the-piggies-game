package savethepiggies.game 
{
	import flash.geom.Rectangle;
	import godmode.core.Semaphore;
	
	public final class TaskTarget implements Component 
	{
		private var _id:uint;
		private var _targetId:uint;
		private var _bounds:Rectangle;
		private var _type:String;
		private var _slots:Vector.<TaskTargetSlot>;
		private var _semaphores:Array;
		
		public function TaskTarget(
			id:uint,
			targetId:uint,
			bounds:Rectangle,
			type:String
		) 
		{
			_id = id;
			_targetId = targetId;
			_type = type;
			
			if (bounds.width <= 0) bounds.width = 0.01;
			if (bounds.height <= 0) bounds.height = 0.01;
			if (bounds.width <= 0 || bounds.height <= 0)
			{
				trace("Width or height is negative or 0 in the object with the ID:"+_id);
				//throw new Error("Width or height cannot be negative or 0");
			}
				
				
			_bounds = bounds.clone();
			_slots = new Vector.<TaskTargetSlot>;
			_semaphores = [];
		}
		
		public function addSlot(i:int, j:int, maxUsers:int, semaphoreName:String = ""):TaskTargetSlot
		{
			if (!semaphoreName) semaphoreName = "___dn" + _slots.length;
			
			var slot:TaskTargetSlot = new TaskTargetSlot(i, j);
			_slots.push(slot);
			
			var existingSemaphore:Semaphore = null;
			for each (var semaphore:Semaphore in _semaphores)
				if (semaphore.name == semaphoreName) existingSemaphore = semaphore;
			
			if (existingSemaphore)
				semaphore = existingSemaphore;
			else
				semaphore = new Semaphore(semaphoreName, maxUsers);
				
			_semaphores.push(semaphore);
			
			return slot;
		}
		
		public function getSlotSemaphore(slotIndex:int):Semaphore
		{
			return _semaphores[slotIndex];
		}
		
		public function get id():uint { return _id; }
		public function get targetId():uint { return _targetId; }
		public function get type():String { return _type; }
		public function get bounds():Rectangle { return _bounds; }
		public function get slots():Vector.<TaskTargetSlot> { return _slots; }
	}

}