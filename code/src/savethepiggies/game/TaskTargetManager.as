package savethepiggies.game 
{
	import flash.geom.Point;
	import flash.geom.Rectangle;
	import org.as3commons.logging.api.*;
	import org.gestouch.events.GestureEvent;
	import org.gestouch.gestures.TapGesture;
	import savethepiggies.map.Map;
	import starling.display.*;
	import savethepiggies.game.events.TaskTargetSelectedEvent;
	
	public final class TaskTargetManager extends Sprite 
	{
		private static const logger:ILogger = getLogger(TaskTargetManager);
	
		private var registry:ObjectRegistry;
		
		private var map:Map;
		
		private var tap:TapGesture;
		private var tapTransform:Function;
		
		public function TaskTargetManager(
			map:Map,
			tapTransform:Function
		) 
		{
			this.map = map;
			this.tapTransform = tapTransform;
			
			registry = new ObjectRegistry();
			
			tap = new TapGesture(this);
			tap.addEventListener(GestureEvent.GESTURE_RECOGNIZED, onTap);
			
			hideAll();
		}
		
		public function cleanup():void
		{
			tap.dispose();
		}
		
		public function getByType(type:String):Array
		{
			var r:Array = [];
			
			for (var i:int = 0; i < numChildren; ++i)
			{
				var child:DisplayObject = getChildAt(i);
				var id:uint = uint(child.name);
				var taskTarget:TaskTarget = registry.getById(id) as TaskTarget;
				
				if (taskTarget.type == type) r.push(taskTarget);
			}
			
			return r;
		}
		
		public function hideAll():void
		{
			visible = false;
		}
		
		public function showAll():void
		{
			showByFilter();
		}
		
		public function showByFilter(filter:Function = null):void
		{
			for (var i:int = 0; i < numChildren; ++i)
			{
				var child:DisplayObject = getChildAt(i);
				var id:uint = uint(child.name);
				var taskTarget:TaskTarget = registry.getById(id) as TaskTarget;
				if (!taskTarget)
				{
					logger.error("Couldn't find task target with id {0}", [id]);
					child.visible = false;
				}
				else
				{
					if(filter == null){
						child.visible = true;
						child.alpha = .1;
					}else{
						child.visible = filter(taskTarget);
						if(child.visible) child.alpha = 1;
					}
				}
			}
			
			visible = true;
		}
		
		private var tapPoint:Point = new Point;
		private function onTap(e:GestureEvent):void
		{
			var gesture:TapGesture = e.target as TapGesture;
			tapPoint.x = gesture.location.x;
			tapPoint.y = gesture.location.y;
			tapTransform(tapPoint);
			
			var hitObject:PolyHitShape = hitTest(tapPoint, true) as PolyHitShape;
			
			if (hitObject)
			{
				var target:TaskTarget = registry.getById(uint(hitObject.name)) as TaskTarget;
				if (target)
				{
					dispatchEvent(
						new TaskTargetSelectedEvent(
							TaskTargetSelectedEvent.TASK_TARGET_SELECTED,
							target
						)
					);
				}
			}
		}
		
		public function addTarget(target:TaskTarget):void
		{
			if (!registry.register(target))
				logger.info("Warning, tried to register target twice");
			else
			{
				createShape(target.bounds, target.id);
			}
		}
		
		public function removeTarget(target:TaskTarget):void
		{
			registry.deregister(target);
			if (getChildByName("" + target.id))
				removeChild(getChildByName("" + target.id));
			else
				logger.info("Warning, couldn't find target image");
		}
		
		private var p1:Point = new Point, p2:Point = new Point, p3:Point = new Point, p4:Point = new Point;
		private function createShape(bounds:Rectangle, id:uint):void
		{
			map.isometricToCartesian(bounds.left, bounds.top, p1);
			map.isometricToCartesian(bounds.right, bounds.top, p2);
			map.isometricToCartesian(bounds.right, bounds.bottom, p3);
			map.isometricToCartesian(bounds.left, bounds.bottom, p4);
			
			var shape:PolyHitShape = new PolyHitShape(p1, p2, p3, p4);
			shape.name = "" + id;
			shape.touchable = true;
			shape.useHandCursor = true;
			shape.visible = false;
			
			var visshape:Shape = new Shape;
			visshape.graphics.beginFill(0xFFFFFF, 0.5);
			visshape.graphics.moveTo(p1.x, p1.y);
			visshape.graphics.lineTo(p2.x, p2.y);
			visshape.graphics.lineTo(p3.x, p3.y);
			visshape.graphics.lineTo(p4.x, p4.y);
			visshape.graphics.lineTo(p1.x, p1.y);
			visshape.graphics.endFill();
			visshape.touchable = false;
			shape.addChild(visshape);
			
			addChild(shape);
		}
	}
}