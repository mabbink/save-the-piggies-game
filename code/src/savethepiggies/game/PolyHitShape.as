package savethepiggies.game 
{
	import flash.geom.Point;
	import starling.display.DisplayObject;
	import starling.display.Sprite;
	
	public final class PolyHitShape extends Sprite 
	{
		private var vertx:Vector.<Number>;
		private var verty:Vector.<Number>;
		
		public function PolyHitShape(p1:Point, p2:Point, p3:Point, p4:Point) 
		{
			super();
			
			vertx = new <Number>[p1.x, p2.x, p3.x, p4.x];
			verty = new <Number>[p1.y, p2.y, p3.y, p4.y];
		}
		
		private function pnpoly(testx:Number, testy:Number):Boolean
		{
			var nvert:int = 4;
			var i:int, j:int;
			var c:Boolean = false;
			for (i = 0, j = nvert-1; i < nvert; j = i++) {
				if ( ((verty[i]>testy) != (verty[j]>testy)) &&
					(testx < (vertx[j]-vertx[i]) * (testy-verty[i]) / (verty[j]-verty[i]) + vertx[i]) )
						c = !c;
		  }
		  
		  return c;
		}
		
		public override function hitTest(localPoint:Point, forTouch:Boolean = false):DisplayObject
		{
			if (forTouch)
				if (!visible || !touchable) return null;
			
			return pnpoly(localPoint.x, localPoint.y) ? this : null;
		}
	}

}