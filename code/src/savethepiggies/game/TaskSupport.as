package savethepiggies.game 
{
	import flash.geom.Point;
	
	import godmode.core.Semaphore;
	
	import org.as3commons.logging.api.ILogger;
	import org.as3commons.logging.api.getLogger;
	
	import savethepiggies.game.behaviours.PiggyInventoryBehaviour;
	
	import supportlib.grid.GridPathfinder;
	
	public final class TaskSupport 
	{
		private static const logger:ILogger = getLogger(TaskSupport);
		
		private var gameContext:GameContext;
		private var piggy:GameObject;
		
		public function TaskSupport(
			gameContext:GameContext,
			piggy:GameObject
		) 
		{
			this.gameContext = gameContext;
			this.piggy = piggy;
		}
		
		public function getSlotCandidates(gameObject:GameObject):Array
		{
			var candidates:Array = [];
			
			var taskTarget:TaskTarget = gameObject.getTaskTarget();
			
			for (var i:int = 0; i < taskTarget.slots.length; ++i)
				candidates.push( {
					g:gameObject,
					slotIndex:i,
					slot:taskTarget.slots[i],
					semaphore:taskTarget.getSlotSemaphore(i)
				});
				
			sortSlotCandidates(candidates);
			
			return candidates;
		}
		
		public function findSlotCandidates(requiredTag:String, filter:Function = null):Array
		{
			if (null === filter) filter = function(g:GameObject):Boolean { return true; }
			
			var candidates:Array = [];
			
			var objects:Array = gameContext.gameObjects.getByTag(requiredTag);
			for each(var g:GameObject in objects){
				if (filter(g)){
					var taskTarget:TaskTarget = g.getTaskTarget();
					for (var i:int = 0; i < taskTarget.slots.length; ++i)
						candidates.push( {
							g:g,
							slotIndex:i,
							slot:taskTarget.slots[i],
							semaphore:taskTarget.getSlotSemaphore(i)
						});
				}
			};
			
			sortSlotCandidates(candidates);
			
			return candidates;
		}
		
		public function sortSlotCandidates(candidates:Array):void
		{
			var s:IsoSprite = piggy.getIsoSprite();
			var si:Number = s.offsetI * 2;
			var sj:Number = s.offsetJ * 2;
			candidates.sort(function(a:Object, b:Object):int {
				var slotA:TaskTargetSlot = a.slot;
				var slotB:TaskTargetSlot = b.slot;
				var semaphoreA:Semaphore = a.semaphore;
				var semaphoreB:Semaphore = b.semaphore;
				if (semaphoreA.isMaxed && !semaphoreB.isMaxed) return 1;
				if (semaphoreB.isMaxed && !semaphoreA.isMaxed) return -1;
				if (semaphoreA.numUsers < semaphoreB.numUsers) return -1;
				if (semaphoreA.numUsers > semaphoreB.numUsers) return 1;
				
				var diA:Number = slotA.i - si;
				var djA:Number = slotA.j - sj;
				var diB:Number = slotB.i - si;
				var djB:Number = slotB.j - sj;
				var dSqrA:Number = diA * diA + djA * djA;
				var dSqrB:Number = diB * diB + djB * djB;
				if (dSqrA == dSqrB) return 0;
				if (dSqrA < dSqrB) return -1;
				return 1;
			});
		}
		
		public function getPathToSlot(slot:TaskTargetSlot):Array
		{
			var points:Array = null;
			var target:Point = null;
			
			var isoSprite:IsoSprite = piggy.getIsoSprite();
			
			var startNearestGridX:int = Math.round(2 * isoSprite.offsetI - .25);
			var startNearestGridY:int = Math.round(2 * isoSprite.offsetJ - .25);
			var startIndex:int = startNearestGridX + startNearestGridY * gameContext.grid.getWidth();
			
			var goalIndex:int = slot.i + slot.j * gameContext.grid.getWidth();
			
			if (startIndex == goalIndex) return [];
			
			var p:GridPathfinder = new GridPathfinder(gameContext.grid);
			
			if (p.find(goalIndex, startIndex))
			{
				target = new Point(slot.i / 2.0, slot.j / 2.0);
				
				var pathIndices:Vector.<int> = p.buildPath(goalIndex, startIndex);
				pathIndices.push(goalIndex);
					
				if (pathIndices.length > 0)
				{
					function pathIndexToPoint(index:int):Point
					{
						var posX0:int = pathIndices[index] % gameContext.grid.getWidth();
						var posY0:int = (pathIndices[index] - posX0) / gameContext.grid.getWidth();
						
						return new Point(posX0 / 2.0 + .25, posY0 / 2.0 + .25);
					}
					
					points = [];
					
					for (var k:int = 0; k < pathIndices.length; ++k)
						points.push(pathIndexToPoint(k));
				}
			}
			else
			{
				logger.debug("Couldn't find path");
			}
			return points;
		}
		
		public function getTarget(id:int):GameObject
		{
			return gameContext.gameObjects.getById(id) as GameObject;
		}
		
		public function getTaskTarget(id:int):TaskTarget
		{
			var target:GameObject = getTarget(id);
			if (!target) return null;
			return target.getTaskTarget();
		}
		
		public function getSlot(id:int, index:int):TaskTargetSlot
		{
			var taskTarget:TaskTarget = getTaskTarget(id);
			if (!taskTarget) return null;
			if (index >= taskTarget.slots.length) return null;
			return taskTarget.slots[index];
		}
		
		public function getSemaphore(id:int, index:int):Semaphore
		{
			var taskTarget:TaskTarget = getTaskTarget(id);
			if (!taskTarget) return null;
			if (index >= taskTarget.slots.length) return null;
			return taskTarget.getSlotSemaphore(index);
		}
		
		public function hasInventory():Boolean
		{
			return PiggyInventoryBehaviour(piggy.getBehaviourByType(PiggyInventoryBehaviour)).holding != null;
		}
		
		public function setInventory(type:Array):void
		{
			PiggyInventoryBehaviour(piggy.getBehaviourByType(PiggyInventoryBehaviour)).holding = type;
		}
		
		public function getInventory():Array
		{
			return PiggyInventoryBehaviour(piggy.getBehaviourByType(PiggyInventoryBehaviour)).holding;
		}
	}
}