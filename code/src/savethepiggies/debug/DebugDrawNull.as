package savethepiggies.debug 
{
	import savethepiggies.game.GameGrid;
	import savethepiggies.map.Map;
	
	public class DebugDrawNull implements DebugDraw 
	{
		
		public function DebugDrawNull() 
		{
			
		}
		
		public function draw(map:Map, grid:GameGrid, isoSprites:Array, hidden:Boolean = false):void 
		{
			
		}
		
	}

}