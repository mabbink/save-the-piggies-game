package savethepiggies.debug 
{
	import savethepiggies.game.GameGrid;
	import savethepiggies.map.Map;
	
	public interface DebugDraw  
	{
		function draw(map:Map, grid:GameGrid, isoSprites:Array, hidden:Boolean = false):void;
	}
}