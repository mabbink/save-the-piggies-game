package savethepiggies.debug 
{
	import flash.geom.Point;
	import savethepiggies.game.GameGrid;
	import savethepiggies.game.IsoSprite;
	import savethepiggies.game.SpecialNode;
	import savethepiggies.map.Map;
	import starling.display.BlendMode;
	import starling.display.DisplayObjectContainer;
	import starling.display.Image;
	import starling.display.Sprite;
	import starling.utils.AssetManager;
	
	public class DebugDrawActive extends Sprite implements DebugDraw 
	{
		private var gridLayer:Sprite;
		private var spriteLayer:Sprite;
		private var assetManager:AssetManager;
		
		public function DebugDrawActive(
			container:DisplayObjectContainer,
			assetManager:AssetManager
		) 
		{
			touchable = false;
			
			this.assetManager = assetManager;
			
			gridLayer = new Sprite;
			spriteLayer = new Sprite;
			gridLayer.blendMode = BlendMode.ADD;
			spriteLayer.blendMode = BlendMode.ADD;
			container.addChild(gridLayer);
			container.addChild(spriteLayer);
		}
		
		public function draw(
			map:Map,
			grid:GameGrid,
			isoSprites:Array,
			hidden:Boolean = false
		):void 
		{
			while (gridLayer.numChildren > 0) gridLayer.removeChildAt(0);
			while (spriteLayer.numChildren > 0) spriteLayer.removeChildAt(0);
			
			if (!hidden && grid) drawGrid(grid, map);
			if (!hidden && isoSprites) drawIsoSprites(isoSprites, map);
		}
		
		private function drawIsoSprites(isoSprites:Array, map:Map):void
		{
			for each (var isoSprite:IsoSprite in isoSprites)
			{
				drawCorner("debug-location-marker", isoSprite.offsetI, isoSprite.offsetJ, 0.5, 1);
				drawCorner("debug-bounds-right", isoSprite.maxI, isoSprite.minJ, 1, 0.5);
				drawCorner("debug-bounds-top", isoSprite.minI, isoSprite.minJ, 0.5, 0);
				drawCorner("debug-bounds-bottom", isoSprite.maxI, isoSprite.maxJ, 0.5, 1);
				drawCorner("debug-bounds-left", isoSprite.minI, isoSprite.maxJ, 0, 0.5);
				
				function drawCorner(texture:String, i:Number, j:Number, hAlign:Number, vAlign:Number):void
				{
					var drawPoint:Point = map.isometricToCartesian(i, j);
					var image:Image = new Image(assetManager.getTexture(texture));
					image.touchable = false;
					image.x = drawPoint.x - image.width * hAlign;
					image.y = drawPoint.y - image.height * vAlign;
					spriteLayer.addChild(image);
				}
			}
		}
		
		private function drawGrid(grid:GameGrid, map:Map):void
		{
			for (var j:int = 0; j < grid.getHeight(); ++j)
			{
				for (var i:int = 0; i < grid.getWidth(); ++i)
				{
					if (grid.countSpecialNodes(i, j) > 0)
					{
						var special:Array = grid.getSpecialNodes(i, j);
						var toDraw:Array = [];
						
						var blocked:Array = [false, false, false, false];
						var occupied:Boolean = false;
						for each (var node:SpecialNode in special)
						{
							if (node.blocked[0]) blocked[0] = true;
							if (node.blocked[1]) blocked[1] = true;
							if (node.blocked[2]) blocked[2] = true;
							if (node.blocked[3]) blocked[3] = true;
							if (node.occupiedType) occupied = true;
						}
						
						if (false&&occupied)
							toDraw.push("debug-occupied");
						
						if (blocked[0]) toDraw.push("debug_blocked_0");
						if (blocked[1]) toDraw.push("debug_blocked_1");
						if (blocked[2]) toDraw.push("debug_blocked_2");
						if (blocked[3]) toDraw.push("debug_blocked_3");
						
						for each (var textureName:String in toDraw)
						{
							var drawPoint:Point = map.isometricToCartesian(i / 2.0, j / 2.0);
							var image:Image = new Image(assetManager.getTexture(textureName));
							image.touchable = false;
							image.x = drawPoint.x - map.tileWidth / 4;
							image.y = drawPoint.y;
							gridLayer.addChild(image);
						}
					}
				}
			}
		}
	}

}