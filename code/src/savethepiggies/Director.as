package savethepiggies
{
	import flash.display.DisplayObjectContainer;
	import flash.display.Stage;
	import flash.utils.ByteArray;
	
	import feathers.system.DeviceCapabilities;
	
	import org.as3commons.logging.api.ILogger;
	import org.as3commons.logging.api.getLogger;
	import org.gestouch.core.Gestouch;
	import org.gestouch.core.IInputAdapter;
	import org.gestouch.extensions.starling.StarlingDisplayListAdapter;
	import org.gestouch.extensions.starling.StarlingTouchHitTester;
	import org.gestouch.input.NativeInputAdapter;
	
	import savethepiggies.game.GameScene;
	import savethepiggies.initialization.LoadingScene;
	
	import starling.core.Starling;
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.events.ResizeEvent;
	
	import supportlib.scene.SceneManager;
	import supportlib.scene.StarlingSceneManager;

	/**
	 * Controls game flow.
	 * e.g. can initiate the title screen scene, or game scene, etc.
	 * 
	 */
	public final class Director
	{
		private static const logger:ILogger = getLogger(Director);
		
		// Data flow
		private var globalContext:GlobalContext;
		
		// Game scene management
		private var sceneManager:SceneManager;
		
		private var starlingInstance:Starling;
		
		public function Director(
			parent:DisplayObjectContainer
		)
		{
			initContext();
			parent.stage.showDefaultContextMenu = false;
			
			Starling.handleLostContext = true;
			var starling:Starling = new Starling(Sprite, parent.stage);
			starlingInstance = starling;
			//starling.addEventListener(Event.CONTEXT3D_CREATE, function():void { sizeStage(); } );
			sizeStage();
			
			starling.stage.addEventListener(ResizeEvent.RESIZE, sizeStage);
			
			Gestouch.inputAdapter ||= IInputAdapter(new NativeInputAdapter(parent.stage));
			Gestouch.addDisplayListAdapter(starling.display.DisplayObject, new StarlingDisplayListAdapter());
			Gestouch.addTouchHitTester(new StarlingTouchHitTester(starling), -1);
			
			starling.addEventListener(Event.ROOT_CREATED, begin);
			starling.start();
			
			function begin(e:*):void
			{
				starling.removeEventListener(Event.ROOT_CREATED, begin);
				starling.showStats = Main.IsDebugEnvironment();
				initSceneManager(starling);
				start();
			}
		}
		
		public function get starling():Starling
		{
			return starlingInstance;
		}
		
		private function sizeStage():void
		{
			var stage:Stage = Starling.current.nativeStage;
			
			const sw:Number = Math.max(800, stage.stageWidth);
			const sh:Number = Math.max(600, stage.stageHeight);
			
			Starling.current.viewPort.width = sw;
			Starling.current.viewPort.height = sh;
			
			// Change depending on what resolution graphics are being used
			// when creating mobile version.
			var scale:Number = 4;
			Starling.current.stage.stageWidth = sw / scale;
			Starling.current.stage.stageHeight = sh / scale;
			
			DeviceCapabilities.dpi = 72 * 2;	// Desktop only
			
			Starling.current.showStatsAt("right", "bottom" , 1 / scale);
		}
		
		private function initContext():void
		{
			globalContext = new GlobalContext;
			globalContext.director = this;
		}
		
		private function initSceneManager(starling:Starling):void
		{
			sceneManager = new StarlingSceneManager(starling);
		}
		
		private function start():void
		{
			showLoadingScene();
		}
		
		private function showLoadingScene():void
		{
			sceneManager.setActiveScene(
				new LoadingScene(globalContext)
			);
		}
		
		public function runGame(saveData:Array = null):void
		{
			sceneManager.setActiveScene(
				new GameScene(globalContext, saveData)
			);
		}
	}
}