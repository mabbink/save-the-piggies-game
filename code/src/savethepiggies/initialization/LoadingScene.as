package savethepiggies.initialization
{
	import com.greensock.TweenNano;
	import com.greensock.easing.Strong;
	import com.spikything.utils.SWFAsset;
	
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.display.Stage;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.external.ExternalInterface;
	import flash.net.URLLoader;
	import flash.net.URLLoaderDataFormat;
	import flash.net.URLRequest;
	import flash.text.TextField;
	import flash.utils.ByteArray;
	
	import facebook.GraphAPIExternalInterface;
	
	import feathers.core.PopUpManager;
	
	import org.as3commons.logging.api.ILogger;
	import org.as3commons.logging.api.getLogger;
	
	import savethepiggies.GlobalContext;
	import savethepiggies.persistence.FacebookBackendPersistence;
	import savethepiggies.persistence.SharedObjectPersistence;
	import savethepiggies.theme.PiggiesTheme;
	
	import starling.core.Starling;
	import starling.display.Quad;
	import starling.utils.AssetManager;
	
	import supportlib.scene.Scene;
	import supportlib.scene.SceneManager;
	
	public final class LoadingScene implements supportlib.scene.Scene
	{
		private static const logger:ILogger = getLogger(LoadingScene);
		
		private var globalContext:GlobalContext;
		
		private var container:Sprite;
		private var bg:MovieClip;
		private var title:MovieClip;
		private var loginButton:MovieClip;
		private var percentageBalloon:MovieClip;
		private var viveraLogo:MovieClip;
		private var messages:Array;
		private var currentMessage:int;
		private var loadingDone:Boolean;
		
		private var assetsLoading:AssetManager;
		
		public function LoadingScene(globalContext:GlobalContext)
		{
			logger.debug('Starting loading screen');
			
			this.globalContext = globalContext;
			container = new Sprite;
			container.scaleX = .25;
			container.scaleY = .25;
			
			toggleEvents(true);
			
			loadingDone = false;
			assetsLoading = loadAssetsFromJson("loader/loaderAssets.json", loaderInit );
		}
		
		public function cleanup(manager:SceneManager):void
		{
			toggleEvents(false);
			container.parent.removeChild(container);
		}
		
		private function toggleEvents(on:Boolean):void
		{
			var f:String = on ? "addEventListener" : "removeEventListener";
			
			container[f](MouseEvent.CLICK, onClick);
		}
		
		private function onClick(e:MouseEvent):void
		{
			logger.debug("{0} clicked", [e.target.name]);
			
			switch (e.target.name)
			{
				case "loginButton": 
					loginButton.loginButton.mouseEnabled = false;
					loginButton.loginButton.alpha = 0.5;
					globalContext.graphApi.login(function(r:Object):void
						{
							loginButton.loginButton.mouseEnabled = true;
							loginButton.loginButton.alpha = 1;
							if (r.status === "connected")
							{
								loginButton.visible = false;
								finish();
							}
						});
					break;
			}
		}
		
		private function initGraphApi():void
		{
			
			var header:TextField = TextField(percentageBalloon.getChildByName('_header'));
			header.text = '';
			
			var field:TextField = TextField(percentageBalloon.getChildByName('_percentage'));
			field.text = 'Done!';
			
			TweenNano.to(field, 1, {y:15, onComplete:initGraphApi2});
		}
		private function initGraphApi2():void{
			
			if (ExternalInterface.available)
			{
				TweenNano.to(percentageBalloon, 1, {y:'-30', alpha:0});
				
				var appId:String = assetsLoading.getObject("loaderConfig").appId;
				logger.info(JSON.stringify(assetsLoading.getObject("loaderConfig")));
				logger.info("Initializing Facebook with app id: {0}", [appId]);
				globalContext.graphApi = new GraphAPIExternalInterface();
				globalContext.graphApi.init(appId, onFacebookInit);
				
				globalContext.persistence = new FacebookBackendPersistence(globalContext.graphApi);
			}
			else
			{
				logger.info("ExternalInterface not available, can't initialize Facebook");
				
				globalContext.persistence = new SharedObjectPersistence("localSave");
				
				loadingDone = true;
				
				finish();
			}
		}
		
		private function finish():void
		{
			new PiggiesTheme(Starling.current.stage, globalContext.assetManager.getTextureAtlas("ui-desktop"));

			PopUpManager.overlayFactory = function():*
			{
				var quad:Quad = new Quad(1, 1, 0x000000);
				quad.alpha = 0.65;
				return quad;
			};
			
			globalContext.persistence.load(function(r:Object):void {
				globalContext.director.runGame(r.data);
			});
		}
		
		private function onFacebookInit():void
		{
			logger.info("Facebook initialized");
			logger.info("Checking login status");
			globalContext.graphApi.getLoginStatus(function(response:Object):void
				{
					logger.info("Login status: {0}", [response.status]);
					if (response.status !== "connected")
						loadingDone = true;
					else
						finish();
				});
		}
		
		private function initDisplay(stage:Stage):void
		{
		}
		
		private function loadAssetsFromJson(path:String, onComplete:Function):AssetManager
		{
			
			var loader:URLLoader = new URLLoader();
			loader.dataFormat = URLLoaderDataFormat.TEXT;
			loader.addEventListener(
				Event.COMPLETE,
				jsonLoadComplete
			);
			loader.load(new URLRequest(path));
			
			var assetManager:AssetManager = new AssetManager(Starling.current.contentScaleFactor);
			
			function jsonLoadComplete(e:*):void
			{
				loader.removeEventListener(
					Event.COMPLETE,
					jsonLoadComplete
				);
				
				assetManager.enqueue(JSON.parse(loader.data).assets);
				assetManager.loadQueue(function(ratio:Number):void {
					if (percentageBalloon)
						TextField(percentageBalloon.getChildByName('_percentage')).text = Math.floor(ratio*100)+'%';
					
					//logger.info("Load of assets from {0} {1}% complete", [path, ratio * 100]);
					if (ratio === 1)
					{
						logger.info("Load of assets from {0} complete", [path]);
						onComplete();
					}
				});
			}
			
			return assetManager;
		}
		
		private function loaderInit():void
		{
			buildLoaderUi();
			
			globalContext.assetManager = loadAssetsFromJson("loader/assets.json", function():void {
				initGraphApi();
			});
		}
		
		private function buildLoaderUi():void
		{
			logger.info('Loading bytes...');
			var bytes:ByteArray = assetsLoading.getByteArray("initialization");
			logger.info('bytes:'+bytes.length);
			
			var loaderMovieClipHack:SWFAsset = new SWFAsset(bytes, 
				function():void {
					
					logger.info('SWF loaded!');
					
					bg 					= loaderMovieClipHack.getMovieClipAsset("BG");
					title 				= loaderMovieClipHack.getMovieClipAsset("Title");
					loginButton 		= loaderMovieClipHack.getMovieClipAsset("LoginButton");
					percentageBalloon 	= loaderMovieClipHack.getMovieClipAsset("PercentageBalloon");
					viveraLogo			= loaderMovieClipHack.getMovieClipAsset("ViveraLogo");
					messages			= [
						loaderMovieClipHack.getMovieClipAsset("PigMessage1"),
						loaderMovieClipHack.getMovieClipAsset("PigMessage2"),
						loaderMovieClipHack.getMovieClipAsset("PigMessage3"),
						loaderMovieClipHack.getMovieClipAsset("PigMessage4"),
						loaderMovieClipHack.getMovieClipAsset("PigMessage5"),
						loaderMovieClipHack.getMovieClipAsset("PigMessage6"),
						loaderMovieClipHack.getMovieClipAsset("PigMessage7")
					];
					
					container.addChild(bg);
					container.addChild(title);
					container.addChild(loginButton);
					container.addChild(viveraLogo);
					container.addChild(percentageBalloon);
					
					
					loginButton.visible = false;
					loginButton.alpha = 0;
					percentageBalloon.alpha = 0;
					
					container.stage.addEventListener(Event.RESIZE, resizeLoaderUi);
					container.addEventListener(Event.REMOVED_FROM_STAGE, removedFromStage);
					
					currentMessage = Math.floor(Math.random()*(messages.length-1));
					function showRandomPiggyMessage():void{
						if(!container.stage) return;
						
						var dest:int = (container.stage.stageHeight / 2) - 120;
						if(loadingDone){
							
							if(currentMessage == 0){
								TweenNano.to(messages[messages.length-1], .4, {y:dest-30, alpha:0, ease:Strong.easeIn});
							}else{
								TweenNano.to(messages[currentMessage-1], .4, {y:dest-30, alpha:0, ease:Strong.easeIn});
							}
							loginButton.visible = true;
							TweenNano.to(loginButton, .4, {y:dest, alpha:1, ease:Strong.easeOut});
							
						}else{
							var item:MovieClip = messages[currentMessage];
							item.x = (container.stage.stageWidth / 2) - (item.width / 2);
							item.y = dest + 30;
							item.alpha = 0;
							container.addChildAt(item, 1);
							
							TweenNano.to(item, .4, {y:dest, alpha:1, ease:Strong.easeOut});
							TweenNano.delayedCall(4, function(){
								TweenNano.to(item, .4, {y:dest-30, alpha:0, ease:Strong.easeIn, onComplete: showRandomPiggyMessage});
							});
							
							currentMessage ++;
							if(currentMessage == messages.length) currentMessage = 0;
						}
					}
					showRandomPiggyMessage();
					
					function removedFromStage(e:*):void{
						container.stage.removeEventListener(Event.RESIZE, resizeLoaderUi);
						container.removeEventListener(Event.REMOVED_FROM_STAGE, removedFromStage);
					}
					function resizeLoaderUi(e:* = null):void{
						
						bg.width = container.stage.stageWidth;
						bg.height = container.stage.stageHeight;
						
						title.y = 40;
						title.getChildByName('_title').width = container.stage.stageWidth;
						
						percentageBalloon.x = (container.stage.stageWidth / 2) - (percentageBalloon.width / 2);
						percentageBalloon.y = container.stage.stageHeight - 270;
						TweenNano.to(percentageBalloon, 1, {y:container.stage.stageHeight-300, alpha:1});
						
						loginButton.x = (container.stage.stageWidth / 2) - (loginButton.width / 2);
						loginButton.y = (container.stage.stageHeight / 2) - 120;
						
						viveraLogo.x = container.stage.stageWidth - viveraLogo.width - 20;
						viveraLogo.y = container.stage.stageHeight - viveraLogo.height - 20;
					}
					
					resizeLoaderUi();
				}
			);
		}
		
		public function init(manager:SceneManager):void
		{
			initDisplay(Starling.current.nativeStage);
			Starling.current.nativeOverlay.addChild(container);
		}
		
		public function update(msElapsed:int):void
		{
		}
		
		public function draw():void
		{
		}
		
		public function resume(manager:SceneManager):void
		{
		}
	}
}