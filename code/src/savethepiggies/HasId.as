package savethepiggies 
{
	public interface HasId 
	{
		function get id():uint;
	}
}