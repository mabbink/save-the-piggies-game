package savethepiggies
{
	import facebook.GraphAPI;
	import starling.utils.AssetManager;
	
	import savethepiggies.persistence.Persistence;

	public final class GlobalContext
	{
		public var director:Director;
		public var assetManager:AssetManager;
		
		public var graphApi:GraphAPI;
		public var persistence:Persistence;
	}
}