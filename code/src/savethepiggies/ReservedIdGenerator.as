package savethepiggies 
{
	public final class ReservedIdGenerator implements IdGenerator 
	{
		private var nextId:uint;
		private var reserved:Array;
		
		public function ReservedIdGenerator(
			nextId:uint = 1
		) 
		{
			this.nextId = nextId;
			reserved = [];
		}
		
		public function reserve(id:uint):void
		{
			reserved.push(id);
		}
		
		public function generate():uint 
		{
			if (nextId == uint.MAX_VALUE)
				throw new Error("Out of IDS!");
			
			++nextId;
			if (reserved.indexOf(nextId) != -1)
				return generate();
				
			return nextId;
		}
		
	}

}