package com.spikything.utils 
{
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.DisplayObject;
	import flash.display.InteractiveObject;
	import flash.display.Loader;
	import flash.display.MovieClip;
	import flash.display.PixelSnapping;
	import flash.display.SimpleButton;
	import flash.events.Event;
	import flash.media.Sound;
	import flash.system.ApplicationDomain;
	import flash.system.LoaderContext;
	import flash.utils.ByteArray;
	
	public class SWFAsset
	{
		private var _Domain :ApplicationDomain;
		private var _onComplete :Function;
		private var _init :Boolean;
		
		public function SWFAsset(SWFData:ByteArray, onComplete:Function):void
		{
			setup(SWFData, onComplete);
		}
		
		private function setup(SWFData:ByteArray, onComplete:Function):void
		{
			if (_init) 
				return;
			
			if (SWFData is ByteArray) {
				_onComplete = onComplete;
				var loader:Loader = new Loader;
				loader.contentLoaderInfo.addEventListener(Event.COMPLETE, init);
				
				var lc = new LoaderContext(false, ApplicationDomain.currentDomain);
				
				try{
					lc.allowLoadBytesCodeExecution = true;
				}catch(pError:Error){
				
				}
				loader.loadBytes(SWFData as ByteArray, lc);
			} else {
				throw new ArgumentError("The SWFData passed to the method 'setup' must be a ByteArray.");
			}
		}
		
		public function getAssetClass(ID:String):Class
		{
			if (!_init || _Domain == null) 
				throw new Error("SWFAsset has not been initialised or is not ready yet. Please call SWFAsset.setup(onCompleteFunction) before calling getAsset.");
			
			var classDef:Class;
			
			if (_Domain.hasDefinition(ID)) {
				classDef = _Domain.getDefinition(ID) as Class;
			} else {
				throw new Error("Symbol '" + ID + "' not found.");
			}
			
			return classDef;
		}
		
		public function getAsset(ID:String):DisplayObject
		{
			return new (getAssetClass(ID) as Class) as DisplayObject;
		}
		
		public function getButtonAsset(ID:String):SimpleButton
		{
			return getAsset(ID) as SimpleButton;
		}
		
		public function getInteractiveAsset(ID:String):InteractiveObject
		{
			return getAsset(ID) as InteractiveObject;
		}
		
		public function getMovieClipAsset(ID:String):MovieClip
		{
			return getAsset(ID) as MovieClip;
		}
		
		public function getBitmapAsset(
			ID:String,
			pixelSnapping:String = PixelSnapping.AUTO,
			smoothing:Boolean = false
		):Bitmap
		{
			var data:BitmapData = (new (getAssetClass(ID))) as BitmapData;
			return new Bitmap(data, pixelSnapping, smoothing); 
		}
		
		public function getBitmapDataAsset(
			ID:String
		):BitmapData
		{
			return (new (getAssetClass(ID))) as BitmapData;
		}
		
		public function getSoundAsset(ID:String):Sound
		{
			return new (getAssetClass(ID) as Class) as Sound;
		}
		
		private function init(e:Event):void 
		{
			if (_init) 
				return;
			
			_Domain = e.target.applicationDomain as ApplicationDomain;
			_init = true;
			_onComplete();
		}
		
	}
	
}