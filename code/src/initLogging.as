package
{
	import com.demonsters.debugger.MonsterDebugger;
	import com.timkeir.logging.MONSTER_DEBUGGER_V3_TARGET;
	
	import flash.display.Stage;
	import flash.external.ExternalInterface;
	
	import org.as3commons.logging.api.LOGGER_FACTORY;
	import org.as3commons.logging.setup.ILogTarget;
	import org.as3commons.logging.setup.LevelTargetSetup;
	import org.as3commons.logging.setup.LogSetupLevel;
	import org.as3commons.logging.setup.SimpleTargetSetup;
	import org.as3commons.logging.setup.target.TraceTarget;

	public function initLogging(stage:Stage, onlyErrors:Boolean = false):void{
		
		if (ExternalInterface.available && CONFIG::debug){
			MonsterDebugger.initialize(this);
			LOGGER_FACTORY.setup = new SimpleTargetSetup(MONSTER_DEBUGGER_V3_TARGET);
			
		}else{
			var target:ILogTarget;
			 
			target = new TraceTarget;
			
			if (onlyErrors)
				LOGGER_FACTORY.setup = new LevelTargetSetup(target, LogSetupLevel.ERROR);
			else
				LOGGER_FACTORY.setup = new SimpleTargetSetup(target);
		}
	}
}