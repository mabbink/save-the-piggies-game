package facebook
{
	import flash.external.ExternalInterface;
	
	import org.as3commons.logging.api.ILogger;
	import org.as3commons.logging.api.getLogger;

	/**
	 * ExternalInterface/JavaScript implementation of Facebook API.
	 * 
	 * Suitable for Flash embedded in a desktop web browser.
	 * 
	 */
	public class GraphAPIExternalInterface implements GraphAPI
	{
		private static const logger:ILogger = getLogger(GraphAPIExternalInterface);
		
		private var initialized:Boolean;
		
		private var initCallback:Function;
		
		private var getLoginStatusCallbacks:CallbackRegistry;
		private var loginCallbacks:CallbackRegistry;
		private var apiCallbacks:CallbackRegistry;
		
		public function GraphAPIExternalInterface()
		{
			initExternalInterface();
			
			getLoginStatusCallbacks = new CallbackRegistry();
			loginCallbacks = new CallbackRegistry();
			apiCallbacks = new CallbackRegistry();
		}
		
		public function init(appId:String, callback:Function):void
		{
			if (initialized)
				throw new Error("Init already called!");
			
			initCallback = callback;
			ExternalInterface.call("fbInit", appId);
			
			initialized = true;
		}
		
		public function getLoginStatus(callback:Function, force:Boolean = false):void
		{
			ExternalInterface.call(
				"fbGetLoginStatus",
				getLoginStatusCallbacks.registerCallback(callback),
				force
			);
		}
		
		public function login(callback:Function):void
		{
			ExternalInterface.call(
				"fbLogin",
				loginCallbacks.registerCallback(callback)
			);
		}
		
		public function api(
			path:String, 
			method:String, 
			params:Object, 
			callback:Function
		):void
		{
			ExternalInterface.call(
				"fbApi",
				apiCallbacks.registerCallback(callback == null ? function():void{} : callback),
				path,
				method,
				JSON.stringify(params)
			);
		}
		
		private function initExternalInterface():void
		{
			ExternalInterface.addCallback("flInit", flInit);
			ExternalInterface.addCallback("flGetLoginStatus", flGetLoginStatus);
			ExternalInterface.addCallback("flLogin", flLogin);
			ExternalInterface.addCallback("flApi", flApi);
		}
		
		private function flApi(id:int, params:String = null):void
		{
			logger.debug("flApi: {0}", [params]);
			
			if (params)
				apiCallbacks.callCallback(id, JSON.parse(params));
			else
				apiCallbacks.callCallback(id);
		}
		
		private function flLogin(id:int, params:String):void
		{
			logger.debug("flLogin: {0}", [params]);
			loginCallbacks.callCallback(id, JSON.parse(params));
		}
		
		private function flGetLoginStatus(id:int, params:String):void
		{
			logger.debug("flGetLoginStatus: {0}", [params]);
			getLoginStatusCallbacks.callCallback(id, JSON.parse(params));
		}
		
		private function flInit():void
		{
			logger.debug("flInit");
			
			if (null != initCallback)
				initCallback();
			else
				logger.error("No initCallback registered! This shouldn't happen with correct usage.");
		}
	}
}

import org.as3commons.logging.api.ILogger;
import org.as3commons.logging.api.getLogger;

class CallbackRegistry
{
	private static const logger:ILogger = hideLogger(CallbackRegistry);
	
	private var id:int = 0;
	private var callbacks:Object = {};
	
	public function registerCallback(callback:Function):int
	{
		callbacks[++id] = callback;
		if (logger.debugEnabled)
			logger.debug("Registered callback with ID {0} ({1} total)", [id, countCallbacks()]);
		return id;
	}
	
	public function callCallback(id:int, params:* = null):Boolean
	{
		var found:Boolean = callbacks[id];
		
		if (found)
		{
			found = true;

			if (params)
				callbacks[id](params);
			else
				callbacks[id]();
			
			delete callbacks[id];
			
			if (logger.debugEnabled)
				logger.debug("Called and deleted callback {0} ({1} remaining)", [id, countCallbacks()]);
		}
		else
			logger.warn("No such callback with ID {0}", [id]);
		
		return found;
	}
	
	private function countCallbacks():int
	{
		var count:int = 0;
		
		for (var s:* in callbacks)
			++count;
		
		return count;
	}
}