package facebook
{
	public interface GraphAPI
	{
		function init(appId:String, callback:Function):void;
		function getLoginStatus(callback:Function, force:Boolean = false):void;
		function login(callback:Function):void;
		function api(path:String, method:String, params:Object, callback:Function):void;
	}
}