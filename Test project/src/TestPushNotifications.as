package
{
	import flash.events.RemoteNotificationEvent;
	import flash.events.StatusEvent;
	import flash.notifications.NotificationStyle;
	import flash.notifications.RemoteNotifier;
	import flash.notifications.RemoteNotifierSubscribeOptions;

	public final class TestPushNotifications {
		
		private var remoteNot:RemoteNotifier;
		
		
		public function TestPushNotifications():void{
			if(RemoteNotifier.supportedNotificationStyles.toString() == " ") {	
				trace("push notifications not supported");
			}
			else{
				trace('Supported');
				
				remoteNot = new RemoteNotifier();
				remoteNot.addEventListener(RemoteNotificationEvent.TOKEN,tokenHandler); 
				remoteNot.addEventListener(RemoteNotificationEvent.NOTIFICATION, notificationHandler);
				remoteNot.addEventListener(StatusEvent.STATUS, status);
				
				var preferredStyles:Vector.<String> = new Vector.<String>();
				preferredStyles.push(NotificationStyle.ALERT ,NotificationStyle.BADGE,NotificationStyle.SOUND );
				
				var subscribeOptions:RemoteNotifierSubscribeOptions = new RemoteNotifierSubscribeOptions();
				subscribeOptions.notificationStyles = preferredStyles;
				
				remoteNot.subscribe(subscribeOptions);
			}
		}
		private function tokenHandler(pEvent:RemoteNotificationEvent):void{
			trace('Event:'+pEvent.tokenId);
		}
		private function notificationHandler(pEvent:RemoteNotificationEvent):void{
			trace('Notification!');
		}
		private function status(pEvent:StatusEvent):void{
			trace('Status:'+pEvent.level);
		}
	}
}