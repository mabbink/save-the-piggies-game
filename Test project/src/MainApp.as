package {
	
	import flash.display.Sprite;
	import flash.display.StageAlign;
	import flash.display.StageQuality;
	import flash.display.StageScaleMode;
	import flash.events.Event;
	
	[SWF(backgroundColor='#333333',frameRate='48', width="1024", height="768")]
	public final class MainApp extends Sprite {
		
		public function MainApp() {
			trace('Start');
			// support autoOrients
			stage.align = StageAlign.TOP_LEFT;
			stage.scaleMode = StageScaleMode.NO_SCALE;
			stage.quality = StageQuality.LOW;
			
			stage.addEventListener(Event.ACTIVATE,activateHandler);
		}
		private function activateHandler(pEvent:Event):void{
			trace('Active');
			var test:TestPushNotifications = new TestPushNotifications();
		}
	}
}